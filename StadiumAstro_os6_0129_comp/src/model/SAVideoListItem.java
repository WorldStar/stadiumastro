package model;

import java.util.Calendar;
public class SAVideoListItem {	
	public String m_copyright = "";
	public Calendar m_creationDate = null;
	public String m_defaultImageUrl = "";
	public String m_description = "";
	public String m_duration = "";
	public String m_id = "";
	public String m_iPadVideoUrl = "";
	public String m_iPhoneVideoUrl = "";
	public String m_title = "";
		
	public SAVideoListItem(){		
//		this.m_defaultImageUrl = ResourceConstants.SA_IMG_NEWS_TABLE_CELL_THUMB;		
	}
}
