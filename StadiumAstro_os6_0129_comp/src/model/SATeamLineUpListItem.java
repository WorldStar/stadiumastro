package model;

public class SATeamLineUpListItem {
	public String m_playerName;
	public String m_playerNo;
	public String m_position;
	public String m_positionDesc;
	
	public SATeamLineUpListItem(){
		m_playerName = "";
		m_playerNo = "";
		m_position = "";
		m_positionDesc = "";
	}
}
