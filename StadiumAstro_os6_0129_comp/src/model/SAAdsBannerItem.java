package model;

public class SAAdsBannerItem {
	public String m_full_file = "adsBannerFullFile";
	public String m_full_url = "adsBannerFullURL";
	public String m_mobile_file = "adsBannerMobileFile";
	public String m_mobile_url = "adsBannerMobileURL";
	public String m_table_file = "adsBannerTableFile";
	public String m_table_vr_file = "adsBannerTableVrFile";
	public String m_tablet_url = "adsBannerTabletURL";
	public String m_tablet_vr_url = "adsBannerTabletVrURL";

	public SAAdsBannerItem(){
		
		m_full_file = "adsBannerFullFile";
		m_full_url = "adsBannerFullURL";
		m_mobile_file = "adsBannerMobileFile";
		m_mobile_url = "adsBannerMobileURL";
		m_table_file = "adsBannerTableFile";
		m_table_vr_file = "adsBannerTableVrFile";
		m_tablet_url = "adsBannerTabletURL";
		m_tablet_vr_url = "adsBannerTabletVrURL";
	}
}
