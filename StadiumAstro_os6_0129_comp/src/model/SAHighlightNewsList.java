package model;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.AppSession;
import application.ResourceConstants;

public class SAHighlightNewsList extends SAModel{
	
	public Vector m_array;
	
	public SAHighlightNewsList()
	{
		this.initArrayWithParsedData();
	}
	
	public void initArrayWithParsedData()
	{			
		execute();
	}
	

	public int totalCount(){
		if(m_array == null){
			return 0;
		}
		return m_array.size();
	}
	public SAHighlightNewsListItem getDataAtIndex(int index){
		if(m_array.size() > 0 && index < m_array.size() && index >= 0)
			return (SAHighlightNewsListItem)m_array.elementAt(index); 
		return null;
	}

	public void execute() {
		// TODO Auto-generated method stub
		// get server response
		String strResponse;
		if(AppSession.useLocalDataForTest)
			strResponse = ResourceConstants.SA_JSON_DATA_HIGHLIGHT_NEWS_LIST;
		else
		{
			// request json data to server
			String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + 
					ResourceConstants.SA_HIGHLIGHT_NEWS_API_PATH + AppSession.sharedSession().m_curSportEventContentKey;
			strResponse = DataManager.getResponseDataFromServer(strRequest);			
		}
		
		// parse json data
		try{
			JSONObject objHighlightNewList = new JSONObject(strResponse);
			
			JSONArray arrHighlightNewsList = objHighlightNewList.getJSONArray(ResourceConstants.SA_KEY_HIGHLIGHT_NEWS);
			
			int count = arrHighlightNewsList.length();
			m_array = new Vector(count);
			
			for(int i = 0; i < count; i++)
			{
				JSONObject objHighlightNewsItem = (JSONObject)arrHighlightNewsList.get(i);
				
				SAHighlightNewsListItem highlightNewsListItem = new SAHighlightNewsListItem();
				highlightNewsListItem.m_fullBody = objHighlightNewsItem.getString(ResourceConstants.SA_KEY_HIGHLIGHT_NEWS_FULL_BODY);				
				highlightNewsListItem.m_publishDate = objHighlightNewsItem.getString(ResourceConstants.SA_KEY_HIGHLIGHT_NEWS_PUBLISH_DATE);
				highlightNewsListItem.m_title = objHighlightNewsItem.getString(ResourceConstants.SA_KEY_HIGHLIGHT_NEWS_TITLE);
				highlightNewsListItem.m_displayOrder = objHighlightNewsItem.getInt(ResourceConstants.SA_KEY_HIGHLIGHT_NEWS_DISPLAY_ORDER);
				
				// determine image file path
				String strImageFilePath;
				
				if(AppSession.useLocalDataForTest)
					strImageFilePath = ResourceConstants.SA_IMG_HSCROLL;
				else
				{
					strImageFilePath = objHighlightNewsItem.getString(ResourceConstants.SA_KEY_HIGHLIGHT_NEWS_IMAGE_URL); 
				}
				
				highlightNewsListItem.m_imageUrl = strImageFilePath;
				
				m_array.addElement(highlightNewsListItem);
			}
			
			objHighlightNewList = null;
		}
		catch(JSONException e)
		{
			System.out.println("Hightlight news list data error!");
			System.out.println(e.toString());
		}
	}
}
