package model;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.AppSession;
import application.ResourceConstants;

public class SATeamLineUpList extends SAModel{
	public Vector m_arrayAwayTeam = null;
	public Vector m_arrayHomeTeam = null;

	//Temporary variable for executing command.
	String m_strRefKey;
	public SATeamLineUpList(String strRefKey){
		initArrayWithParsedData(strRefKey);
	}
	
	public void initArrayWithParsedData(String strRefKey)
	{
		m_strRefKey = strRefKey;
		execute();
	}
	
	public int totalAwayTeamCount(){
		return m_arrayAwayTeam.size();
	}
	
	public int totalHomeTeamCount(){
		return m_arrayHomeTeam.size();
	}
	
	public SATeamLineUpListItem getAwayTeamDataAtIndex(int index){
		if(m_arrayAwayTeam.size() > 0 && index < m_arrayAwayTeam.size() && index >= 0)
			return (SATeamLineUpListItem)m_arrayAwayTeam.elementAt(index); 
		
		return null;
	}
	
	public SATeamLineUpListItem getHomeTeamDataAtIndex(int index){
		if(m_arrayHomeTeam.size() > 0 && index < m_arrayHomeTeam.size() && index >= 0)
			return (SATeamLineUpListItem)m_arrayHomeTeam.elementAt(index);
		
		return null;
	}

	public void execute() {
		// TODO Auto-generated method stub
		try{
			// get json data
			String strResponse;
			
			if(AppSession.useLocalDataForTest)
				strResponse = ResourceConstants.SA_JSON_DATA_TEAMS_LINEUP_FOR_PARTICULAR_MATCH;
			else
			{
				String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + 
						ResourceConstants.SA_SOCCER_MATCH_LINEUP_FOR_PATICULAR_MATCH_API_PATH + m_strRefKey;
				strResponse = DataManager.getResponseDataFromServer(strRequest);
			}
			
			// parse data
			JSONObject objTeamLineUpList = new JSONObject(strResponse);
			
			m_arrayAwayTeam = new Vector();
			m_arrayHomeTeam = new Vector();
			
			JSONArray arrAwayTeamList = objTeamLineUpList.getJSONArray(ResourceConstants.SA_KEY_ANALYSIS_TEAM_LINEUP_AWAY_TEAM);
			int nCount = arrAwayTeamList.length();
			for(int i = 0; i < nCount; i++)
			{
				JSONObject objTeamLineUpItem = (JSONObject)arrAwayTeamList.get(i);
				
				SATeamLineUpListItem teamLineUpItem = new SATeamLineUpListItem();
				teamLineUpItem.m_playerName = objTeamLineUpItem.getString(ResourceConstants.SA_KEY_ANALYSIS_TEAM_LINEUP_PLAYER_NAME);
				teamLineUpItem.m_playerNo = objTeamLineUpItem.getString(ResourceConstants.SA_KEY_ANALYSIS_TEAM_LINEUP_PLAYER_NO);
				teamLineUpItem.m_position = objTeamLineUpItem.getString(ResourceConstants.SA_KEY_ANALYSIS_TEAM_LINEUP_POSITION);
				teamLineUpItem.m_positionDesc = objTeamLineUpItem.getString(ResourceConstants.SA_KEY_ANALYSIS_TEAM_LINEUP_POSITION_DESC);
				
				m_arrayAwayTeam.addElement(teamLineUpItem);
			}
			
			JSONArray arrHomeTeamList = objTeamLineUpList.getJSONArray(ResourceConstants.SA_KEY_ANALYSIS_TEAM_LINEUP_HOME_TEAM);
			nCount = arrHomeTeamList.length();
			for(int i = 0; i < nCount; i++)
			{
				JSONObject objTeamLineUpItem = (JSONObject)arrHomeTeamList.get(i);
				
				SATeamLineUpListItem teamLineUpItem = new SATeamLineUpListItem();
				teamLineUpItem.m_playerName = objTeamLineUpItem.getString(ResourceConstants.SA_KEY_ANALYSIS_TEAM_LINEUP_PLAYER_NAME);
				teamLineUpItem.m_playerNo = objTeamLineUpItem.getString(ResourceConstants.SA_KEY_ANALYSIS_TEAM_LINEUP_PLAYER_NO);
				teamLineUpItem.m_position = objTeamLineUpItem.getString(ResourceConstants.SA_KEY_ANALYSIS_TEAM_LINEUP_POSITION);
				teamLineUpItem.m_positionDesc = objTeamLineUpItem.getString(ResourceConstants.SA_KEY_ANALYSIS_TEAM_LINEUP_POSITION_DESC);
				
				m_arrayHomeTeam.addElement(teamLineUpItem);
			}
			
			objTeamLineUpList = null;
		}
		catch(JSONException e){
			System.out.println("Team Line UP data error!");
			System.out.println(e.toString());
		}
	}
	
}
