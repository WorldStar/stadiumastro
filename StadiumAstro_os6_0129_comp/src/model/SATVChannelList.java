package model;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.AppSession;
import application.ResourceConstants;


public class SATVChannelList extends SAModel{
	
	public Vector m_array;
	
	public SATVChannelList()
	{
		this.initArrayWithParsedData();
	}
	
	public void initArrayWithParsedData()
	{
		// get response data from server			
		execute();
	}
	
	public int totalCount(){
		return m_array.size();
	}
	public SATVChannelListItem getDataAtIndex(int index){
		if(m_array.size() > 0 && index < m_array.size() && index >= 0)
			return (SATVChannelListItem)m_array.elementAt(index); 
		return null;
	}

	public void execute() {
		// TODO Auto-generated method stub
		String strResponse;		
		if(AppSession.useLocalDataForTest)
			strResponse = ResourceConstants.SA_JSON_DATA_TV_CHANNEL_LIST;
		else
		{
			String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + ResourceConstants.SA_TV_CHANNEL_LIST_API_PATH + AppSession.sharedSession().m_curSportEventContentKey;
			strResponse = DataManager.getResponseDataFromServer(strRequest);
		}
		
		try
		{
			// parsing data
			JSONObject objTvChannelList = new JSONObject(strResponse);
			
			JSONArray arrTvChannelList = objTvChannelList.getJSONArray(ResourceConstants.SA_KEY_TV_CHANNEL_LIST);
			
			int nCount = arrTvChannelList.length();
			m_array = new Vector(nCount);
			
			for(int i = 0; i < nCount; i++)
			{
				JSONObject objTvChannelItem = (JSONObject)arrTvChannelList.get(i);
				
				// create data item from json data
				SATVChannelListItem tvChannelItem = new SATVChannelListItem();
				
				tvChannelItem.m_id = objTvChannelItem.getString(ResourceConstants.SA_KEY_TV_CHANNEL_LIST_ID);
				tvChannelItem.m_name = objTvChannelItem.getString(ResourceConstants.SA_KEY_TV_CHANNEL_LIST_NAME);
		
				if(i == 0)    
				{
					// top Cell
					tvChannelItem.m_bitmapURL = ResourceConstants.SA_IMG_CELL_BACK_TOP;
				}
				else if(i == nCount - 1) 
				{
					// bottom cell
					tvChannelItem.m_bitmapURL = ResourceConstants.SA_IMG_CELL_BACK_BOTTOM;
				}
				// save to array
				m_array.addElement(tvChannelItem);				
			}
			
			objTvChannelList = null;
		}
		catch(JSONException e){
			System.out.println("TV Channel List data error!");
			System.out.println(e.toString());
		}
	}
}
