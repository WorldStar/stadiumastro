package model;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.AppSession;
import application.ResourceConstants;

public class SAFeatureList extends SAModel {
//	public Vector m_arrayVideo = null;
	public Vector m_arrayNews = null;

	public SAFeatureList(){
		initArrayWithParsedData();
	}
	
	public void initArrayWithParsedData()
	{
		execute();
	}
	
//	public int totalVideoCount(){
//		return m_arrayVideo.size();
//	}
	
	public int totalNewsCount(){
		return m_arrayNews.size();
	}
	
//	public SAFeatureListItem getVideoDataAtIndex(int index){
//		if(m_arrayVideo.size() > 0 && index < m_arrayVideo.size() && index >= 0)
//			return (SAFeatureListItem)m_arrayVideo.elementAt(index); 
//		
//		return null;
//	}
	
	public SAFeatureListItem getNewsDataAtIndex(int index){
		if(m_arrayNews.size() > 0 && index < m_arrayNews.size() && index >= 0)
			return (SAFeatureListItem)m_arrayNews.elementAt(index);
		
		return null;
	}

	public void execute() {
		// TODO Auto-generated method stub
		try{
			// get json data
			String strResponse;
			
			if(AppSession.useLocalDataForTest)
				strResponse = ResourceConstants.SA_JSON_DATA_FEATURE_LIST;
			else
			{
				String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + 
						ResourceConstants.SA_FEATURE_NEWS_VIDEO_LIST_API_PATH + AppSession.sharedSession().m_curSportEventContentKey;
				strResponse = DataManager.getResponseDataFromServer(strRequest);
			}
			
			// parse data
			JSONObject objFeatureList = new JSONObject(strResponse);
			
			JSONArray arrFeatureList = objFeatureList.getJSONArray(ResourceConstants.SA_KEY_FEATURE);

//			m_arrayVideo = new Vector();
			m_arrayNews = new Vector();

			int nCount = arrFeatureList.length();
			for(int i = 0; i < nCount; i++)
			{
				JSONObject objFeatureItem = (JSONObject)arrFeatureList.get(i);
				
				SAFeatureListItem featureItem = new SAFeatureListItem();
				featureItem.m_dateInt = objFeatureItem.getLong(ResourceConstants.SA_KEY_FEATURE_LIST_DATE_INT);
				featureItem.m_description = objFeatureItem.getString(ResourceConstants.SA_KEY_FEATURE_LIST_DESCRIPTION);
				featureItem.m_fullBody = objFeatureItem.getString(ResourceConstants.SA_KEY_FEATURE_LIST_FULL_BODY);
				featureItem.m_mediaID = objFeatureItem.getString(ResourceConstants.SA_KEY_FEATURE_LIST_MEDIA_ID);
				featureItem.m_iphoneUrl = objFeatureItem.getString(ResourceConstants.SA_KEY_FEATURE_LIST_IPHONE_VIDEO_URL);
				featureItem.m_ipadUrl = objFeatureItem.getString(ResourceConstants.SA_KEY_FEATURE_LIST_IPAD_VIDEO_URL);
				featureItem.m_date = objFeatureItem.getString(ResourceConstants.SA_KEY_FEATURE_LIST_DATE);
				featureItem.m_title = objFeatureItem.getString(ResourceConstants.SA_KEY_FEATURE_LIST_TITLE);
				featureItem.m_type = objFeatureItem.getString(ResourceConstants.SA_KEY_FEATURE_LIST_TYPE);
				
				String strImagePath = "";
				
				if(AppSession.useLocalDataForTest)
				{
					strImagePath = ResourceConstants.SA_IMG_VIDEO_TABLE_CELL_THUMB;
				}
				else
				{
					strImagePath = objFeatureItem.getString(ResourceConstants.SA_KEY_FEATURE_LIST_IMAGE_URL);					
				}					

				featureItem.m_imageUrl = strImagePath;

//				if(featureItem.m_type.equals("VIDEO"))
//					m_arrayVideo.addElement(featureItem);
//				else if(featureItem.m_type.equals("NEWS"))
				if(featureItem.m_type.equals("NEWS") || featureItem.m_type.equals("VIDEO"))
					m_arrayNews.addElement(featureItem);
			}	
			
			objFeatureList = null;
		}
		catch(JSONException e){
			System.out.println("Feature news & video list data error!");
			System.out.println(e.toString());
		}		
	}
}
