package model;

public class MoreMenuItem {
	// menu icon
	public String m_iconUrl = null;
	
	// menu title
	public String m_title = null;
	
	public MoreMenuItem(String iconUrl, String title)
	{
		m_iconUrl = iconUrl;
		m_title = title;		
	}
}
