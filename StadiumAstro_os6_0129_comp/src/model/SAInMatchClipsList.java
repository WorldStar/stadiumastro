package model;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.AppSession;
import application.ResourceConstants;
import application.Utils;

public class SAInMatchClipsList extends SAModel {
	public Vector m_array;
	public String m_strRefKey;
	public SAInMatchClipsList(String strRefKey){
		this.initArrayWithParsedData(strRefKey);
	}
	
	public void initArrayWithParsedData(String strRefKey)
	{
		m_strRefKey = strRefKey;
		
		execute();

	}

	public int totalCount(){
		return m_array.size();
	}
	public SAVideoListItem getDataAtIndex(int index){
		if(m_array.size() > 0 && index < m_array.size() && index >= 0)
			return (SAVideoListItem)m_array.elementAt(index); 
		return null;
	}

	public void execute() {
		// TODO Auto-generated method stub
		//get response
		String strResponse;
		if(AppSession.useLocalDataForTest){
			strResponse = ResourceConstants.SA_JSON_DATA_IN_MATCH_VIDEO_CLIPS;
		}else{
			String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + 
					ResourceConstants.SA_IN_MATCH_VIDEO_LIST_FOR_PARTICULAR_MATCH_API_PATH + m_strRefKey;
			strResponse = DataManager.getResponseDataFromServer(strRequest);
		}
		
		//parse json data
		try{
			//get root jsonObject
			JSONObject objMatchClips = new JSONObject(strResponse);
			//get content 'soccerCalendars' jsonArray
			JSONArray arrMatchClips = objMatchClips.getJSONArray(ResourceConstants.SA_KEY_ANALYSIS_IN_MATCH_CLIPS);
			
			//fill data into Item array.
			int nCount = arrMatchClips.length();
			m_array = new Vector(nCount);
			
			for(int i = 0; i < nCount; i++){
				//get content Json object
				JSONObject objMatchClipsItem = arrMatchClips.getJSONObject(i);
				
				SAVideoListItem  matchClipItem = new SAVideoListItem();
				
				matchClipItem.m_id = objMatchClipsItem.getString(ResourceConstants.SA_KEY_ANALYSIS_IN_MATCH_CLIPS_ID);
				matchClipItem.m_creationDate = Utils.getDateFromString(objMatchClipsItem.getString(ResourceConstants.SA_KEY_ANALYSIS_IN_MATCH_CLIPS_CREATION_DATE));
				matchClipItem.m_description = objMatchClipsItem.getString(ResourceConstants.SA_KEY_ANALYSIS_IN_MATCH_CLIPS_DESCRIPTION);
				matchClipItem.m_duration = objMatchClipsItem.getString(ResourceConstants.SA_KEY_ANALYSIS_IN_MATCH_CLIPS_DURATION);
				matchClipItem.m_iPadVideoUrl = objMatchClipsItem.getString(ResourceConstants.SA_KEY_ANALYSIS_IN_MATCH_CLIPS_IPAD_URL);
				matchClipItem.m_iPhoneVideoUrl = objMatchClipsItem.getString(ResourceConstants.SA_KEY_ANALYSIS_IN_MATCH_CLIPS_IPHONE_URL);
				matchClipItem.m_title = objMatchClipsItem.getString(ResourceConstants.SA_KEY_ANALYSIS_IN_MATCH_CLIPS_TITLE);
				
				String strImageFilePath = "";	
				
				if(AppSession.useLocalDataForTest)
					strImageFilePath = ResourceConstants.SA_IMG_VIDEO_TABLE_CELL_THUMB;
				else
				{					
					strImageFilePath = objMatchClipsItem.getString(ResourceConstants.SA_KEY_VIDEO_FEED_IMAGE_URL);
				}
				matchClipItem.m_defaultImageUrl = strImageFilePath;
				m_array.addElement(matchClipItem);
				
			}
			objMatchClips = null;
			
		}catch(JSONException e){
			System.out.println("In-Match Clips List data error!");
			System.out.println(e.toString());
		}
	}
}
