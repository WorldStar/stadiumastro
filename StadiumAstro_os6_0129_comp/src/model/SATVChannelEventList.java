package model;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.AppSession;
import application.ResourceConstants;

public class SATVChannelEventList extends SAModel{
	public Vector m_array;
	
	String m_strID;
	public SATVChannelEventList(String strID) {
		this.initArrayWithParsedData(strID);
	}
	
	
	public void initArrayWithParsedData(String strID)
	{
		m_strID = strID;
		execute();
	}
	
	public int totalCount(){
		return m_array.size();
	}
	public SATVChannelEventListItem getDataAtIndex(int index){
		if(m_array.size() > 0 && index < m_array.size() && index >= 0)
			return (SATVChannelEventListItem)m_array.elementAt(index); 
		return null;
	}


	public void execute() {
		// TODO Auto-generated method stub
		
		try{
			// get json data
			String strResponse;
			
			if(AppSession.useLocalDataForTest)
				strResponse = ResourceConstants.SA_JSON_DATA_TV_CHANNEL_EVENT_LIST;
			else
			{
				String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + 
						ResourceConstants.SA_TV_CHANNEL_EVENT_LIST_API_PATH + m_strID;
				strResponse = DataManager.getResponseDataFromServer(strRequest);
			}
			
			// parse data
			JSONObject objTvEventList = new JSONObject(strResponse);
			
			JSONArray arrTvEventList = objTvEventList.getJSONArray(ResourceConstants.SA_KEY_TV_CHANNEL_EVENT_LIST);
			
			int nCount = arrTvEventList.length();
			m_array = new Vector(nCount);
			
			for(int i = 0; i < nCount; i++)
			{
				JSONObject objTvEventListItem = (JSONObject)arrTvEventList.get(i);
				
				SATVChannelEventListItem tvEventItem = new SATVChannelEventListItem();
				
				tvEventItem.m_dateTime = objTvEventListItem.getString(ResourceConstants.SA_KEY_TV_CHANNEL_EVENT_DATE_TIME);
				tvEventItem.m_endTime = objTvEventListItem.getString(ResourceConstants.SA_KEY_TV_CHANNEL_EVENT_END_TIME);
				tvEventItem.m_eventName = objTvEventListItem.getString(ResourceConstants.SA_KEY_TV_CHANNEL_EVENT_NAME);
				tvEventItem.m_startTime = objTvEventListItem.getString(ResourceConstants.SA_KEY_TV_CHANNEL_EVENT_START_TIME);
				
				m_array.addElement(tvEventItem);
			}	
			
			objTvEventList = null;
		}
		catch(JSONException e){
			System.out.println("Tv Channel Event List data error!");
			System.out.println(e.toString());
		}
	}
	
}
