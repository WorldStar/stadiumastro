package model;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.AppSession;
import application.ResourceConstants;
import application.Utils;

public class SANewsList extends SAModel{	

	public Vector m_array;
	
	//Temporary variable to execute command.
	String m_strRefKey;
	public SANewsList(String strRefKey)
	{
		this.initArrayWithParsedData(strRefKey);
	}
	
	public void initArrayWithParsedData(String strRefKey)
	{
		m_strRefKey = strRefKey;
		execute();
	}
	
	public int totalCount(){
		return m_array.size();
	}
	public SANewsListItem getDataAtIndex(int index){
		if(m_array.size() > 0 && index < m_array.size() && index >= 0)
			return (SANewsListItem)m_array.elementAt(index); 
		return null;
	}

	public void execute() {
		// TODO Auto-generated method stub

		// get response data from server			
		String strResponse;		
		if(AppSession.useLocalDataForTest)
			strResponse = ResourceConstants.SA_JSON_DATA_NEWS_FEED_LIST;
		else
		{
			String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + ResourceConstants.SA_NEWS_FEED_LIST_API_PATH + m_strRefKey;
			strResponse = DataManager.getResponseDataFromServer(strRequest);
		}
		
		try
		{
			// parsing data
			JSONObject objNewsFeedList = new JSONObject(strResponse);
			
			JSONArray arrNewsFeedList = objNewsFeedList.getJSONArray(ResourceConstants.SA_KEY_NEWS_FEED_LIST);
			
			int nCount = arrNewsFeedList.length();
			m_array = new Vector(nCount);
			
			for(int i = 0; i < nCount; i++)
			{
				JSONObject objNewsFeedItem = (JSONObject)arrNewsFeedList.get(i);
				
				// create data item from json data
				SANewsListItem feedItem = new SANewsListItem();
				feedItem.m_date = objNewsFeedItem.getString(ResourceConstants.SA_KEY_NEWS_FEED_PUBLISH_DATE);
				feedItem.m_publishDate = Utils.getDateFromString(feedItem.m_date);
				feedItem.m_title = objNewsFeedItem.getString(ResourceConstants.SA_KEY_NEWS_FEED_TITLE);
				feedItem.m_description = objNewsFeedItem.getString(ResourceConstants.SA_KEY_NEWS_FEED_DESCRIPTION);
				feedItem.m_fullBody = objNewsFeedItem.getString(ResourceConstants.SA_KEY_NEWS_FEED_FULLBODY);
				
				// determine image file path
				String strImageFilePath = "";	
				
				if(AppSession.useLocalDataForTest)
					strImageFilePath = ResourceConstants.SA_IMG_NEWS_TABLE_CELL_THUMB;
				else
				{
					strImageFilePath = objNewsFeedItem.getString(ResourceConstants.SA_KEY_NEWS_FEED_IMAGE_URL);
				}
				feedItem.m_imageLink = strImageFilePath;
				
				// save to array
				m_array.addElement(feedItem);				
			}
			
			objNewsFeedList = null;
		}
		catch(JSONException e){
			System.out.println("Video Feed List data error!");
			System.out.println(e.toString());
		}
	}

}
