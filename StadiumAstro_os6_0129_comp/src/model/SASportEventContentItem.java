package model;


public class SASportEventContentItem {
	public String m_bySportsGroupName = null;
	public boolean m_commentaryFlag = true;
	public String m_defaultNewsImagePhoneUrl = null;
	public String m_defaultNewsImageTabletURL = null;
	public int m_displayOrder = 0;
	public String m_generalNewsLabel = null;
	public String m_generalVideoLabel = null;
	public String m_groupMobileMenuIcon = null;
	public String m_groupTabletMenuIcon = null;
	public String m_inAppUrlLink = null;
	public boolean m_inMatchClipsFlag = true;
	public boolean m_includeAdsBanner = true;
	public boolean m_includeFixture = true;
	public boolean m_includePlayerStatistic = true;
	public boolean m_includeStanding = true;
	public boolean m_includeTVChannelList = true;
	public boolean m_includeTeamList = true;
	public boolean m_launchInAppBrowserFlag = true;
	public String m_refKey = null;
	public String m_sportsEventIconUrl = null;
	public String m_sportsEventName = null;
	public int m_videoClipFilter = 0;
			
	public SASportEventContentItem()
	{
		
	}
}
