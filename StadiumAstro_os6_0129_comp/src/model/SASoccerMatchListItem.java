/*
 * SASoccerMatch.java
 */

package model;

import java.util.Calendar;

public class SASoccerMatchListItem {
	
	public String m_channelInfo;
	public String m_gameWeek;
	public String m_group;
	
	public String m_homeTeamLogo;
	public String m_homeTeamAlias;
	public String m_homeTeamId;
	public String m_homeTeamName;
	public String m_homeTeamScore;
	
	public String m_date;
	public String m_year;
	public int m_month;
	public int m_day;
	public String m_hours;
	public String m_minutes;
	public String m_seconds;
	public String m_time;
	public Calendar m_dateCalendar;
	public String m_timeZoneOffset;
	public String m_hasDetailFlag;
	public String m_matchID;
	public String m_matchPeriod;
	public String m_matchStatus;
	public String m_matchTimeZoneDate;
	public String m_roundType;
	public String m_sportsEventsRefKey;
	public String m_stadiumCity;
	public String m_stadiumName;
	public String m_stage;
	public String m_strID;

	public String m_visitingTeamLogo; 
	public String m_visitingTeamAlias;
	public String m_visitingTeamId;
	public String m_visitingTeamName;
	public String m_visitingTeamScore;		
	
	public SASoccerMatchListItem() {
		m_homeTeamLogo = "";
		m_homeTeamAlias = "";
		m_homeTeamId = "";
		m_homeTeamName = "";
		m_homeTeamScore = "";
		
		m_visitingTeamLogo = "";
		m_visitingTeamAlias = "";
		m_visitingTeamId = "";
		m_visitingTeamName = "";
		m_visitingTeamScore = "";
		
		m_channelInfo = "";
	}	
}
