package model;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.ResourceConstants;

public class SAInfoContent extends SAModel{
	
	public String m_content;
	public String m_curRefKey;

	public SAInfoContent()
	{
		m_content = "";
	}
	
	public void initArrayWithParsedData(String strRefKey)
	{	
		m_curRefKey = strRefKey;
		execute();
		
	}
	public String getContent()
	{
		
		return m_content;
	}

	public void execute() {
		// TODO Auto-generated method stub
		if(m_curRefKey.equals(ResourceConstants.SA_INFO_KEY_ABOUT))
		{
			m_content = "<html>"
					+ "<body bgcolor='BLACK' style='font-size:22px;'>"
					+ "<font color='white' >"
					+ "<p>"
					+ "Stadium Astro brings you all the action from your favourite sports both local and international. Missed the weekend's big game from the Barclays Premier"
					+ "League or want to know who scored the winning goal in a Malaysian Super League match? Get it all on the app purpose-made for Malaysian sports fans!"
					+ "</p>"
					+ "<ul style='line-height:45px; list-style:url(round.png);'>"
					+ "<li>Video match highlights, pre and post-game interviews from the Barclays Premier League</li>"
					+ "<li>TV schedule and channel listings in Malaysian time</li>"
					+ "<li>Videos, news, statistics and standings for the top European football leagues tournaments and more</li>"
					+ "<li>Local sports content (news, video and features)</li>"
					+ "</ul>"
					+ "</font>"
					+ "</body>"
					+ "</html>";
		}
		else
		{
			String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + ResourceConstants.SA_INFO_CONTENT_API_PATH + m_curRefKey;
			try {
				String strResponse = DataManager.getResponseDataFromServer(strRequest);
			
				JSONObject objInfoContent= new JSONObject(strResponse);
				JSONObject objDesc = objInfoContent.getJSONObject("description");		
				m_content = (String) objDesc.getString("description");
				m_content = m_content.replace('�', '\"');
				m_content = m_content.replace('�', '\"');
				int index = 0;
				if((index = m_content.indexOf("size="))!=-1){
					String b = m_content.substring(0, index);
					String a = m_content.substring(index+8, m_content.length());
					m_content = b + a;
				}
				m_content = "<html>"
						+ "<body bgcolor='BLACK' style='font-size:24px;'>"
						+ "<font color = 'white'>"
						+ m_content
						+ "</font>"
						+ "</body>"
						+ "</html>";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				m_content = "Server connection failed.";
				m_content = "<html>"
						+ "<body bgcolor='BLACK' style='font-size:24px;'>"
						+ "<font color = 'white'>"
						+ m_content
						+ "</font>"
						+ "</body>"
						+ "</html>";
				
				//e.printStackTrace();
			}
		}
	}
	
}