package model;

public class SATVChannelEventListItem {
	public String m_endTime;
	public String m_dateTime;
	public String m_eventName;
	public String m_startTime;
	
	public SATVChannelEventListItem(){
		m_endTime = "";
		m_dateTime = "";
		m_eventName = "";
		m_startTime = "";
	}
}
