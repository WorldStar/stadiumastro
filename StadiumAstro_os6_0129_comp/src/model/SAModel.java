package model;

import net.rim.device.api.ui.UiApplication;

public abstract class SAModel implements Runnable{
	
	SAModelListener m_listener;
	boolean bRunning = false;
	
	public abstract void execute();
	public void setModelListener(SAModelListener listener)
	{
		m_listener = listener;
		  
	}
	public void notifyReady()
	{
		if(m_listener != null)
			m_listener.onModelNotify(this, true);
	}
	public void notifyFail()
	{
		if(m_listener != null)
			m_listener.onModelNotify(this, false);
	}
	public void run()
	{
		if(bRunning)
		{
			notifyFail();
			return;
		}
		
		bRunning = true;
		try{
			execute();
		}
		catch(Exception ex){
			
		}
		UiApplication.getUiApplication().invokeLater(new Runnable(){
			public void run()
			{
				notifyReady();
			}
		});
		
		bRunning = false;
	}
	
	public interface SAModelListener
	{
		public void onModelNotify(SAModel model, boolean bSuccess);
		
	}
}
