package model;

public class SAHighlightNewsListItem {
	public String m_categoryName;
	public String m_description;
	public int m_displayOrder;
	public String m_fullBody;
	public String m_imageUrl;
	public boolean m_isEnable;
	public boolean m_selected;
	public String m_sportsEventID;
	public String m_publishDate;
	public String m_title;	
	
	public SAHighlightNewsListItem(){
		m_categoryName = "";
		m_description = "";
		m_displayOrder = 0;
		m_fullBody = "";
		m_imageUrl = "";
		m_isEnable = true;
		m_selected = false;
		m_sportsEventID = "";
		m_publishDate = "";
		m_title = "";
	}
}
