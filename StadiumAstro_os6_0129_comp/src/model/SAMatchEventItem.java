package model;

public class SAMatchEventItem {

	public String m_eventDesc;
	public String m_eventType;
	public int m_intMinutes;
	public String m_minute;
	public String m_playerName;
	public String m_teamType;
	
	public SAMatchEventItem(){
		m_eventDesc = "";
		m_eventType = "";
		m_intMinutes = 69;
		m_minute = "";
		m_playerName = "";
		m_teamType = "";
	}
}
