package model;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.AppSession;
import application.ResourceConstants;

public class SAStandingList extends SAModel{

	public Vector m_array;
	
	public SAStandingList() {
		this.initArrayWithParsedData();
	}
	
	
	public void initArrayWithParsedData()
	{
		execute();
	}
	
	public int totalCount(){
		return m_array.size();
	}
	public SAStandingListItem getDataAtIndex(int index){
		if(m_array.size() > 0 && index < m_array.size() && index >= 0)
			return (SAStandingListItem)m_array.elementAt(index); 
		return null;
	}


	public void execute() {
		// TODO Auto-generated method stub

		try{
			// get json data
			String strResponse;
			
			if(AppSession.useLocalDataForTest)
				strResponse = ResourceConstants.SA_JSON_DATA_TABLE_STANDING;
			else
			{
				String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + 
						ResourceConstants.SA_TABLE_STANDING_API_PATH + AppSession.sharedSession().m_curSportEventContentKey;
				strResponse = DataManager.getResponseDataFromServer(strRequest);
			}
			
			// parse data
			JSONObject objStandingList = new JSONObject(strResponse);
			
			JSONArray arrStandingList = objStandingList.getJSONArray(ResourceConstants.SA_KEY_STANDING);
			
			int nCount = arrStandingList.length();
			m_array = new Vector(nCount);
			
			for(int i = 0; i < nCount; i++)
			{
				JSONObject objStandingItem = (JSONObject)arrStandingList.get(i);
				
				SAStandingListItem standingItem = new SAStandingListItem();
				
				standingItem.m_groupName = objStandingItem.getString(ResourceConstants.SA_KEY_STANDING_BY_GROUP_NAME);
				standingItem.m_goalAgainst = objStandingItem.getString(ResourceConstants.SA_KEY_STANDING_GOAL_AGAINST);
				standingItem.m_goalScored = objStandingItem.getString(ResourceConstants.SA_KEY_STANDING_GOAL_SCORE);
				int goalScored = Integer.parseInt(standingItem.m_goalScored,10);
				int goalAgainst = Integer.parseInt(standingItem.m_goalAgainst,10);
				int goalDiff = goalScored - goalAgainst;
				standingItem.m_goalDiff = String.valueOf(goalDiff);
				
				standingItem.m_draw = objStandingItem.getString(ResourceConstants.SA_KEY_STANDING_DRAW);
				standingItem.m_won = objStandingItem.getString(ResourceConstants.SA_KEY_STANDING_WON);
				standingItem.m_lost = objStandingItem.getString(ResourceConstants.SA_KEY_STANDING_LOST);
				standingItem.m_pos = objStandingItem.getString(ResourceConstants.SA_KEY_STANDING_POS);
				standingItem.m_played = objStandingItem.getString(ResourceConstants.SA_KEY_STANDING_PLAYED);
				standingItem.m_points = objStandingItem.getString(ResourceConstants.SA_KEY_STANDING_POINTS);
				standingItem.m_hasDetailFlag = objStandingItem.getString(ResourceConstants.SA_KEY_STANDING_HAS_DETAIL_FLAG);
				standingItem.m_teamId = objStandingItem.getString(ResourceConstants.SA_KEY_STANDING_TEAM_ID);
				standingItem.m_teamAlias = objStandingItem.getString(ResourceConstants.SA_KEY_STANDING_TEAM_ALIAS);
				standingItem.m_teamName = objStandingItem.getString(ResourceConstants.SA_KEY_STANDING_TEAM_NAME);
				
				if(AppSession.useLocalDataForTest)
				{
					standingItem.m_teamLogo = ResourceConstants.SA_IMG_TEAMLOGO;
				}else
				{
					standingItem.m_teamLogo = AppSession.sharedSession().getTeamLogoImage(standingItem.m_teamId, ResourceConstants.SA_KEY_TEAM_LOGO_22x22);
				}
				m_array.addElement(standingItem);
			}	
			
			objStandingList = null;
		}
		catch(JSONException e){
			System.out.println("Table Standing List data error!");
			System.out.println(e.toString());
		}
	}

}
