package model;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.AppSession;
import application.ResourceConstants;

public class SANewsCategoryList extends SAModel {
	
	public Vector m_array;
	
	public SANewsCategoryList()
	{
		this.initArrayWithParsedData();
	}

	public void initArrayWithParsedData()
	{
		execute();
	}
	
	public int totalCount(){
		return m_array.size();
	}
	public SACategoryItem getDataAtIndex(int index){
		if(m_array.size() > 0 && index < m_array.size() && index >= 0)
			return (SACategoryItem)m_array.elementAt(index); 
		return null;
	}

	public void execute() {
		// TODO Auto-generated method stub
		// get response data from server			
		String strResponse;		
		if(AppSession.useLocalDataForTest)
			strResponse = ResourceConstants.SA_JSON_DATA_NEWS_CATEGORY_LIST;
		else
		{
			String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + ResourceConstants.SA_NEWS_CATEGORY_LIST_API_PATH + AppSession.sharedSession().m_curSportEventContentKey;
			strResponse = DataManager.getResponseDataFromServer(strRequest);
		}
		
		try{
			// parsing data
			JSONObject objNewsCategoryList = new JSONObject(strResponse);
			
			JSONArray arrNewsCategoryList = objNewsCategoryList.getJSONArray(ResourceConstants.SA_KEY_NEWS_CATEGORY_LIST);
			
			int nCount = arrNewsCategoryList.length();
			m_array = new Vector(nCount);
			
			for(int i = 0; i < nCount; i++)
			{
				JSONObject objVideoCategoryItem = (JSONObject)arrNewsCategoryList.get(i);
				
				SACategoryItem item = new SACategoryItem();
				item.m_name = objVideoCategoryItem.getString(ResourceConstants.SA_KEY_CATEGORY_NAME);
				item.m_refKey = objVideoCategoryItem.getString(ResourceConstants.SA_KEY_CATEGORY_REFKEY);
				
				if(i == 0)
				{
					// top Cell
					item.m_bitmapURL = ResourceConstants.SA_IMG_CELL_BACK_TOP;
				}
				else if(i == nCount - 1) 
				{
					// bottom cell
					item.m_bitmapURL = ResourceConstants.SA_IMG_CELL_BACK_BOTTOM;
				}
				
				m_array.addElement(item);
			}
			
			objNewsCategoryList = null;
		}
		catch(JSONException e)
		{
			System.out.println("News Category List data parsing error!");
			System.out.println(e.toString());
		}
	}
	
}
