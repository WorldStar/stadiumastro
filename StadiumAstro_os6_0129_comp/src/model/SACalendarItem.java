package model;

import java.util.Calendar;
import java.util.Date;

import application.Utils;

public class SACalendarItem {
	public String m_year = null;
	public String m_month = null;
	public String m_day = null;
	public String m_isActive = null;
	public String m_strId = null;
	public boolean m_hasMatch = false;

	public SACalendarItem(){
		this.m_year = "";
		this.m_month = "";
		this.m_day = "";
		this.m_isActive = "";
		this.m_strId = "";
	}
	
	public SACalendarItem getNextDayItem()
	{
		SACalendarItem result = new SACalendarItem();
		
		// get calendar object of current date
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, Integer.parseInt(m_year));
		cal.set(Calendar.MONTH, Utils.getMonthFromString(m_month));
		cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(m_day));
		
		Date curDate = cal.getTime();
		
		long nOneDayAdditive = 24 * 3600 * 1000;
		curDate.setTime(curDate.getTime() + nOneDayAdditive);
		
		// set next date to calendar object
		cal.setTime(curDate);
		
		// set data to return object
		result.m_year = Integer.toString(cal.get(Calendar.YEAR));
		result.m_month = Utils.getMonthString(cal.get(Calendar.MONTH));
		result.m_day = Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
		result.m_isActive = "N";
		result.m_strId = "";
		result.m_hasMatch = false;
		
		return result;
	}
	
	/**
	 * 
	 * @param 
	 * 		otherItem : Calendar item to be compared
	 * @return
	 * 		compare current item's data and param's date, return value as 1 / 0 / -1.
	 */
	public int compare(SACalendarItem otherItem)
	{
		int year = Integer.parseInt(m_year);
		int month = Utils.getMonthFromString(m_month);
		int day = Integer.parseInt(m_day);
		
		int otherYear = Integer.parseInt(otherItem.m_year);
		int otherMonth = Utils.getMonthFromString(otherItem.m_month);
		int otherDay = Integer.parseInt(otherItem.m_day);
		
		if(year > otherYear) return 1;
		else if(year < otherYear) return -1;
		
		if(month > otherMonth) return 1;
		else if(month < otherMonth) return -1;
		
		if(day > otherDay) return 1;
		else if(day < otherDay) return -1;
		
		return 0;
	}
}
