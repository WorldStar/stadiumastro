package model;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.AppSession;
import application.ResourceConstants;

public class SAMatchEventList extends SAModel{
	public Vector m_array;
	String m_strRefKey;
	public SAMatchEventList(String strRefKey){
		this.initArrayWithParsedData(strRefKey);
	}
	
	public void initArrayWithParsedData(String strRefKey)
	{
		m_strRefKey = strRefKey;
		execute();
	}
	
	public int totalCount(){
		return m_array.size();
	}
	public SAMatchEventItem getDataAtIndex(int index){
		if(m_array.size() > 0 && index < m_array.size() && index >= 0)
			return (SAMatchEventItem)m_array.elementAt(index); 
		return null;
	}

	public void execute() {
		// TODO Auto-generated method stub
		//get response
				String strResponse;
				if(AppSession.useLocalDataForTest){
					strResponse = ResourceConstants.SA_JSON_DATA_SOCCER_MATCH_EVENTS;
				}else{
					String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + 
							ResourceConstants.SA_SOCCER_MATCH_SUMMARY_FOR_PARTICULAR_MATCH_API_PATH + m_strRefKey;
					strResponse = DataManager.getResponseDataFromServer(strRequest);
				}
				
				//parse json data
				try{
					//get root jsonObject
					JSONObject objMatchEvent = new JSONObject(strResponse);
					//get content 'soccerCalendars' jsonArray
					JSONArray arrMatchEvents = objMatchEvent.getJSONArray(ResourceConstants.SA_KEY_ANALYSIS_MATCH_EVENTS);
					
					//fill data into Item array.
					int nCount = arrMatchEvents.length();
					m_array = new Vector(nCount);
					
					for(int i = 0; i < nCount; i++){
						//get content Json object
						JSONObject objMatchEventItem = arrMatchEvents.getJSONObject(i);
						
						SAMatchEventItem matchEventItem = new SAMatchEventItem();
						
						matchEventItem.m_eventDesc = objMatchEventItem.getString(ResourceConstants.SA_KEY_ANALYSIS_MATCH_EVENTS_EVENT_DESC);
						matchEventItem.m_eventType = objMatchEventItem.getString(ResourceConstants.SA_KEY_ANALYSIS_MATCH_EVENTS_EVENT_TYPE);
						matchEventItem.m_intMinutes = objMatchEventItem.getInt(ResourceConstants.SA_KEY_ANALYSIS_MATCH_EVENTS_EVENT_INT_MINUTES);
						matchEventItem.m_minute = objMatchEventItem.getString(ResourceConstants.SA_KEY_ANALYSIS_MATCH_EVENTS_EVENT_MINUTE);
						matchEventItem.m_playerName = objMatchEventItem.getString(ResourceConstants.SA_KEY_ANALYSIS_MATCH_EVENTS_EVENT_PLAYER_NAME);
						matchEventItem.m_teamType = objMatchEventItem.getString(ResourceConstants.SA_KEY_ANALYSIS_MATCH_EVENTS_EVENT_TEAM_TYPE);
								
						m_array.addElement(matchEventItem);
					}
					objMatchEvent = null;
					
				}catch(JSONException e){
					System.out.println("Match Events List data error!");
					System.out.println(e.toString());
				}
	}
	
}
