package model;

import application.ResourceConstants;

public class SATVChannelListItem {
	public String m_id;
	public String m_name;
	public String m_bitmapURL;
	public SATVChannelListItem(){
		m_id = "";
		m_name = "";
		m_bitmapURL = ResourceConstants.SA_IMG_CELL_BACK_MIDDLE;
	}
}
