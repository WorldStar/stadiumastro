package model;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.AppSession;
import application.ResourceConstants;

public class SACalendarList extends SAModel {

	public Vector m_array = null;

	public SACalendarList(){
		this.initArrayWithParsedData();		
	}
	
	public void initArrayWithParsedData()
	{
		//get response
		execute();
	}
	
	public int totalCount(){
		return m_array.size();
	}
	
	public SACalendarItem getDataAtIndex(int index){
		if(m_array.size() > 0 && index < m_array.size() && index >= 0)
			return (SACalendarItem)m_array.elementAt(index);
		return null;
	}
	
	/**
	 * insert items of date that has no matches
	 */
	public void rearrangeListData(Vector srcVec)
	{	
		int nIndex = 0;
		SACalendarItem prevItem = (SACalendarItem)srcVec.elementAt(0);
		SACalendarItem nextItem;
		
		while(nIndex < srcVec.size() - 1){
			nextItem = (SACalendarItem)srcVec.elementAt(nIndex + 1);
			SACalendarItem newItem = prevItem;
			do{
				newItem = newItem.getNextDayItem();
				newItem.m_hasMatch = false;
				if(newItem.compare(nextItem) != -1)
					break;
				else
				{
					nIndex++;
					srcVec.insertElementAt(newItem, nIndex);
				}
			}while(true);
			
			prevItem = nextItem;
			nIndex++;
		}		
	}

	public void execute() {
		// TODO Auto-generated method stub
		String strResponse;
		if(AppSession.useLocalDataForTest){
			strResponse = ResourceConstants.SA_JSON_DATA_CALENDAR_LIST;
		}else{
			String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + 
					ResourceConstants.SA_FIXTURES_CALENDAR_LIST_API_PATH + AppSession.sharedSession().m_curSportEventContentKey;
			strResponse = DataManager.getResponseDataFromServer(strRequest);
		}
		
		//parse json data
		try{
			//get root jsonObject
			JSONObject objCalendar = new JSONObject(strResponse);
			//get content 'soccerCalendars' jsonArray
			JSONArray arrCalendars = objCalendar.getJSONArray(ResourceConstants.SA_KEY_CALENDAR);
			
			//fill data into Item array.
			int nCount = arrCalendars.length();	
			
			Vector arrData = new Vector();
			for(int i = 0; i < nCount; i++){
				//get content Json object
				JSONObject objCalendarItem = arrCalendars.getJSONObject(i);
				
				SACalendarItem calendarItem = new SACalendarItem();
				
				calendarItem.m_year = objCalendarItem.get(ResourceConstants.SA_KEY_CALENDAR_LIST_YEAR).toString();
				calendarItem.m_month = objCalendarItem.get(ResourceConstants.SA_KEY_CALENDAR_LIST_MONTH).toString();
				calendarItem.m_day = objCalendarItem.get(ResourceConstants.SA_KEY_CALENDAR_LIST_DAY).toString();
				calendarItem.m_isActive = objCalendarItem.get(ResourceConstants.SA_KEY_CALENDAR_LIST_ACTIVE).toString();
				calendarItem.m_strId = objCalendarItem.get(ResourceConstants.SA_KEY_CALENDAR_LIST_STRID).toString();
				calendarItem.m_hasMatch = true;
				
				arrData.addElement(calendarItem);
			}
			objCalendar = null;
			
			//rearrangeListData(arrData);			
			m_array = arrData;
			
		}catch(JSONException e){
			System.out.println("Calendar List data error!");
			System.out.println(e.toString());
		}
	}
}

