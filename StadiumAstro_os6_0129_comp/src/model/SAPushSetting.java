package model;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.rim.samples.device.push.PushConfig;
import application.AppSession;
import application.ResourceConstants;
import application.Utils;

public class SAPushSetting{
	
	public static int CMD_LOAD_SETTING = 1;
	public static int CMD_ENABLE_PUSH = 2;
	public static int CMD_ENABLE_EVENT = 3;
	Hashtable m_settingTable;
	String m_strID;
	
	//Temporary variable for enable push command
	public boolean m_bCmdStatus;
	
	boolean m_bEnable;
	int m_curCmd;
	String m_eventKey;
	
	public SAPushSetting(String strID)
	{
		m_settingTable = new Hashtable();
		init(strID);
	}
	public void init(String strID)
	{
		String strResponse;
		m_strID = strID;
		m_curCmd = CMD_LOAD_SETTING;
		if(AppSession.useLocalDataForTest)
			strResponse = ResourceConstants.SA_JSON_DATA_PUSH_CONFIG_SETTING;
		else
		{
			String requestUrl = ResourceConstants.SA_PUSH_SERVER_URL + ResourceConstants.SA_LOAD_PUSH_SETTING;
			String pinCode= Utils.getPINCode();
	
			m_settingTable.clear();
	
	
			requestUrl += "&p1=" + String.valueOf(pinCode);
			requestUrl += "&p2=" + PushConfig.getAppId();
			requestUrl += "&p3=" + m_strID;
	
			strResponse = DataManager.getResponseDataFromServer(requestUrl);
		}
		try {

			JSONObject objAppData = new JSONObject(strResponse);
			String value = objAppData.getString("pushEnableFlag");
			m_settingTable.put("pushEnableFlag", value);

			value = objAppData.getString("matchDisciplineAlert");
			m_settingTable.put("MDISC", value);

			value = objAppData.getString("matchStartAlert");
			m_settingTable.put("MSTART", value);

			value = objAppData.getString("matchFullTimeAlert"); 
			m_settingTable.put("MFULL", value);

			value = objAppData.getString("matchHalfTimeAlert");
			m_settingTable.put("MHALF", value);

			value = objAppData.getString("matchGoalAlert");
			m_settingTable.put("MGOAL", value);

			Vector teamsetting = new Vector();
			JSONArray teamArray = objAppData.getJSONArray("teams");
			JSONObject teamObjArray[] = new JSONObject[teamArray.length()];
			for(int i = 0; i < teamArray.length(); i++){
				teamObjArray[i] = teamArray.getJSONObject(i);
			}

			Hashtable teamList = AppSession.sharedSession().m_teamLogoList;

			Enumeration keys = teamList.keys();
			while(keys.hasMoreElements())
			{

				String teamId = (String)keys.nextElement();
				Hashtable teamData = (Hashtable)teamList.get(teamId);

				//Push status for team.
				int i;
				for(i = 0; i < teamObjArray.length; i++){
					if(teamId.compareTo(teamObjArray[i].getString("teamID")) == 0)
						break;
				}

				if(i == teamObjArray.length)
					value = null;
				else
					value = teamObjArray[i].getString("enableFlag");

				//Create team push status record.
				Hashtable teamPushState = new Hashtable();
				teamPushState.put("teamId", teamId);
				teamPushState.put("teamName", teamData.get("teamName"));

				if(value == null)
					teamPushState.put("state", "N");
				else
					teamPushState.put("state", value);

				boolean bInserted = false;
				for(i = 0; i < teamsetting.size(); i++){
					Hashtable aData = (Hashtable)teamsetting.elementAt(i);
					String oldTeamName = aData.get("teamName").toString();
					String newTeamName = teamPushState.get("teamName").toString();
					if(newTeamName.compareTo(oldTeamName) < 0){
						teamsetting.insertElementAt(teamPushState, i);
						bInserted = true;
						break;
					}
				}

				if(bInserted == false)
					teamsetting.addElement(teamPushState);
			}

			m_settingTable.put("teamsetting", teamsetting);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean enablePush(boolean bEnable)
	{
		m_curCmd = CMD_ENABLE_PUSH;	
		m_bEnable = bEnable;
		String requestUrl,enableStr;
		try{
			requestUrl = ResourceConstants.SA_PUSH_SERVER_URL + ResourceConstants.SA_ENABLE_PUSH_SETTING;
					
			if(m_bEnable)
				enableStr = "Y";
			else
				enableStr = "N";
			
			requestUrl += "&p1=" + enableStr;
			requestUrl += "&p2=" + Utils.getPINCode();
			requestUrl += "&p3=" + PushConfig.getAppId();
			requestUrl += "&p4=" + m_strID;
			String response = DataManager.getResponseDataFromServer(requestUrl);
			JSONObject jsonObj = new JSONObject(response);
			String statusStr = jsonObj.getString("status");
			if(statusStr.compareTo("OK") == 0)
			{
				return true;
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		return false;
		
	}
	
	public boolean enablePushForEvent(String key, boolean bEnable)
	{
		m_curCmd = CMD_ENABLE_EVENT;
		m_bEnable = bEnable;
		m_eventKey = key;
		String [] data = {"MSTART", "Kick Off", "MHALF", "Half Time", "MFULL", "Full Time", "MGOAL", "Goal", "MDISC","Yellow/Red Card"};
		int i;
		for(i = 0; i < data.length; i++){
			if(m_eventKey == data[i]) break;
		}
		String requestUrl;
		if(i < data.length)
			requestUrl = ResourceConstants.SA_PUSH_SERVER_URL + ResourceConstants.SA_PUSH_MATCH_EVENT_SETTING;
		else
			requestUrl = ResourceConstants.SA_PUSH_SERVER_URL + ResourceConstants.SA_PUSH_TEAM_EVENT_SETTING;
		String enableStr;
		try{
			if(m_bEnable)
				enableStr = "Y";
			else
				enableStr = "N";
			
			requestUrl += "&p1=" + enableStr;
			requestUrl += "&p2=" + m_eventKey;
			requestUrl += "&p3=" + Utils.getPINCode();
			requestUrl += "&p4=" + PushConfig.getAppId();
			requestUrl += "&p5=" + m_strID;
			
			String response = DataManager.getResponseDataFromServer(requestUrl);
			
				JSONObject jsondata = new JSONObject(response);
				String statusStr = jsondata.getString("status");
//				if(statusStr.compareTo("OK") == 0)
				if(statusStr.compareTo("OK") == 0 || statusStr.compareTo("ON") == 0 || statusStr.compareTo("OFF") == 0)
				{
					return true;
				}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		return false;
	}
	
	public Object getSettingForKey(String key)
	{
		return m_settingTable.get(key);
	}
}
