package model;

public class SAOtherAppListItem {
	public String m_appDesc;
	public String m_appIOSUrl;
	public String m_appName;
	public String m_appLogoURL;
	public String m_appURL;
	
	public SAOtherAppListItem()
	{
		m_appDesc = "";
		m_appIOSUrl = "";
		m_appName = "";
		m_appLogoURL = "";
		m_appURL = "";
		
	}
}
