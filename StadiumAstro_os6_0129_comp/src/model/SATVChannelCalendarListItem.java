package model;

public class SATVChannelCalendarListItem {
	
	public String m_id;
	public String m_channelNo;
	public String m_displayDay;
	public String m_displayMonth;
	public String m_displayYear;
	
	public SATVChannelCalendarListItem(){
		m_id = "";
		m_channelNo = "";
		m_displayDay = "";
		m_displayMonth = "";
		m_displayYear = "";
		
	}
}
