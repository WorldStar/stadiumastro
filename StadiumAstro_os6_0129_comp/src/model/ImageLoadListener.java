package model;

import net.rim.device.api.system.Bitmap;

public interface ImageLoadListener {

	public void imageLoadSuccess(String url, Bitmap bmp);
	public void imageLoadFailed(String url, String err);
	public void imageLoadCanceled(String url);
}
