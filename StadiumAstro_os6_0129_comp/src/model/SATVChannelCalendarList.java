package model;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.AppSession;
import application.ResourceConstants;

public class SATVChannelCalendarList extends SAModel{
	public Vector m_array;
	
	public SATVChannelCalendarList() {
		this.initArrayWithParsedData();
	}
	
	
	public void initArrayWithParsedData()
	{
		execute();
	}
	
	public int totalCount(){
		return m_array.size();
	}
	public SATVChannelCalendarListItem getDataAtIndex(int index){
		if(m_array.size() > 0 && index < m_array.size() && index >= 0)
			return (SATVChannelCalendarListItem)m_array.elementAt(index); 
		return null;
	}


	public void execute() {
		// TODO Auto-generated method stub

		try{
			// get json data
			String strResponse;
			
			if(AppSession.useLocalDataForTest)
				strResponse = ResourceConstants.SA_JSON_DATA_TV_CHANNEL_CALENDAR_LIST;
			else
			{
				String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + 
						ResourceConstants.SA_TV_CHANNEL_CALENDAR_LIST_API_PATH + AppSession.sharedSession().m_curTVChannelKey;
				strResponse = DataManager.getResponseDataFromServer(strRequest);
			}
			
			// parse data
			JSONObject objTvCalendarList = new JSONObject(strResponse);
			
			JSONArray arrTvCalendarList = objTvCalendarList.getJSONArray(ResourceConstants.SA_KEY_TV_CHANNEL_CALENDAR_LIST);
			
			int nCount = arrTvCalendarList.length();
			m_array = new Vector(nCount);
			
			for(int i = 0; i < nCount; i++)
			{
				JSONObject objTvCalendarListItem = (JSONObject)arrTvCalendarList.get(i);
				
				SATVChannelCalendarListItem tvCalendarItem = new SATVChannelCalendarListItem();
				
				tvCalendarItem.m_id = objTvCalendarListItem.getString(ResourceConstants.SA_KEY_TV_CHANNEL_CALENDAR_ID);
				tvCalendarItem.m_channelNo = objTvCalendarListItem.getString(ResourceConstants.SA_KEY_TV_CHANNEL_CALENDAR_NO);
				tvCalendarItem.m_displayDay = objTvCalendarListItem.getString(ResourceConstants.SA_KEY_TV_CHANNEL_CALENDAR_DAY);
				tvCalendarItem.m_displayMonth = objTvCalendarListItem.getString(ResourceConstants.SA_KEY_TV_CHANNEL_CALENDAR_MONTH);
				tvCalendarItem.m_displayYear = objTvCalendarListItem.getString(ResourceConstants.SA_KEY_TV_CHANNEL_CALENDAR_YEAR);
				
				m_array.addElement(tvCalendarItem);
			}	
			
			objTvCalendarList = null;
		}
		catch(JSONException e){
			System.out.println("Tv Channel Calendar List data error!");
			System.out.println(e.toString());
		}
	}

}
