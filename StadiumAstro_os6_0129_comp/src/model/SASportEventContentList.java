package model;

import java.util.Hashtable;
import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.AppSession;
import application.ResourceConstants;
import application.Utils;

public class SASportEventContentList extends SAModel {
	public Hashtable m_menuItemTable = null;
	
	public SASportEventContentList()
	{
		m_menuItemTable = new Hashtable();
		initData();
	}
	
	public void initData()
	{
		execute();
		
	}
	public void appendMoreMenuData()
	{
		Hashtable moreMenuTable = new Hashtable(3);
		
		// add 'information' sub menu items
		Vector informationVec = new Vector();
		informationVec.addElement(new MoreMenuItem(ResourceConstants.SA_ICON_MENU_ABOUT, ResourceConstants.SA_TITLE_MENUITEM_ABOUT));
		informationVec.addElement(new MoreMenuItem(ResourceConstants.SA_ICON_MENU_FAQ, ResourceConstants.SA_TITLE_MENUITEM_FAQ));
		informationVec.addElement(new MoreMenuItem(ResourceConstants.SA_ICON_MENU_TERM_OF_SERVICES, ResourceConstants.SA_TITLE_MENUITEM_TOS));
		informationVec.addElement(new MoreMenuItem(ResourceConstants.SA_ICON_MENU_TERMS_AND_CONDITIONS, ResourceConstants.SA_TITLE_MENUITEM_TAC));
		informationVec.addElement(new MoreMenuItem(ResourceConstants.SA_ICON_MENU_PRIVACY, ResourceConstants.SA_TITLE_MENUITEM_PRIVACY));
		informationVec.addElement(new MoreMenuItem(ResourceConstants.SA_ICON_MENU_APP_VERSION, ResourceConstants.SA_TITLE_MENUITEM_VERSION));
		
		moreMenuTable.put(ResourceConstants.SA_TITLE_SUBMENU_INFORMATION, informationVec);
		
		// add 'contacts' sub menu items
		Vector contactsVec = new Vector();
		contactsVec.addElement(new MoreMenuItem(ResourceConstants.SA_ICON_MENU_FEEDBACK, ResourceConstants.SA_TITLE_MENUITEM_FEEDBACK));
		contactsVec.addElement(new MoreMenuItem(ResourceConstants.SA_ICON_MENU_REPORT, ResourceConstants.SA_TITLE_MENUITEM_REPORT));
		
		moreMenuTable.put(ResourceConstants.SA_TITLE_SUBMENU_CONTACTS, contactsVec);
		
		// add 'others' sub menu items
		Vector othersVec = new Vector();
		othersVec.addElement(new MoreMenuItem(ResourceConstants.SA_ICON_MENU_TELL, ResourceConstants.SA_TITLE_MENUITEM_TELL));
		othersVec.addElement(new MoreMenuItem(ResourceConstants.SA_ICON_MENU_OTHER_APP, ResourceConstants.SA_TITLE_MENUITEM_OTHER_APP));
		
		moreMenuTable.put(ResourceConstants.SA_TITLE_SUBMENU_OTHERS, othersVec);
		
		m_menuItemTable.put(ResourceConstants.SA_TITLE_MAIN_SUBMENU_MORE, moreMenuTable);
	}

	public void execute() {
		// TODO Auto-generated method stub

		// get server response
		String strResponse;
		if(AppSession.useLocalDataForTest)
			strResponse = ResourceConstants.SA_JSON_DATA_SPORTS_EVENT_LIST;
		else
		{
			// request json data to server
			String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + ResourceConstants.SA_SPORTS_EVENTS_LIST_API_PATH;
			strResponse = DataManager.getResponseDataFromServer(strRequest);			
		}
		
		// parse json data
		try{
			JSONObject objSportEventList = new JSONObject(strResponse);
			
			JSONArray arrSportEventContent = objSportEventList.getJSONArray(ResourceConstants.SA_KEY_SPORTS_EVENT);	
			
			int nCount = arrSportEventContent.length();
			
			for(int i = 0; i < nCount; i++)
			{
				JSONObject objSportEventContent = (JSONObject)arrSportEventContent.get(i);
				
				// get data from json object and put into model object
				SASportEventContentItem newItem = new SASportEventContentItem();
				newItem.m_bySportsGroupName = objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_GROUP_NAME);
				
				newItem.m_commentaryFlag = Utils.getBooleanFromString(objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_COMMENTARY_FLAG));
				newItem.m_defaultNewsImagePhoneUrl = objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_DEFAULT_NEWS_IMAGE_PHONE_URL);
				newItem.m_defaultNewsImageTabletURL = objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_DEFAULT_NEWS_IMAGE_TABLET_URL);
				newItem.m_displayOrder = objSportEventContent.getInt(ResourceConstants.SA_KEY_SPORTS_EVENT_DISPLAY_ORDER);
				newItem.m_generalNewsLabel = objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_GENERAL_NEWS_LABEL);
				newItem.m_generalVideoLabel = objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_GENERAL_VIDEO_LABEL);
				newItem.m_groupMobileMenuIcon = objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_GROUP_MOBILE_MENU_ICON);
				newItem.m_groupTabletMenuIcon = objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_GROUP_TABLET_MENU_ICON);
				newItem.m_inAppUrlLink = objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_IN_APP_URL_LINK);
				newItem.m_inMatchClipsFlag= Utils.getBooleanFromString(objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_IN_MATCH_CLIPS_FLAG));
				newItem.m_includeAdsBanner = Utils.getBooleanFromString(objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_INCLUDE_ADS_BANNER));
				newItem.m_includeFixture = Utils.getBooleanFromString(objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_INCLUDE_FIXTURE));
				newItem.m_includePlayerStatistic = Utils.getBooleanFromString(objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_INCLUDE_PLAYER_STATISTIC));
				newItem.m_includeStanding = Utils.getBooleanFromString(objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_INCLUDE_STANDING));
				newItem.m_includeTVChannelList = Utils.getBooleanFromString(objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_INCLUDE_TV_CHANNEL_LIST));
				newItem.m_includeTeamList = Utils.getBooleanFromString(objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_INCLUDE_TEAM_LIST));
				newItem.m_launchInAppBrowserFlag = Utils.getBooleanFromString(objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_LAUNCH_IN_APP_BROWSER_FLAG));
				newItem.m_refKey = objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_REF_KEY);
				newItem.m_sportsEventIconUrl = objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_ICON_URL);
				newItem.m_sportsEventName = objSportEventContent.getString(ResourceConstants.SA_KEY_SPORTS_EVENT_NAME);
				newItem.m_videoClipFilter = objSportEventContent.getInt(ResourceConstants.SA_KEY_SPORTS_EVENT_VIDEO_CLIP_FILTER);
				
				// icon image processing
				if(AppSession.useLocalDataForTest)
				{
					newItem.m_sportsEventIconUrl = ResourceConstants.SA_ICON_MENU_SPORTS;
					
				}
				else
				{
					newItem.m_sportsEventIconUrl = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + newItem.m_sportsEventIconUrl;
				}
				
				// detect group's existence
				Vector vecGroup;
				if(m_menuItemTable.containsKey(newItem.m_bySportsGroupName))
				{
					vecGroup = (Vector)m_menuItemTable.get(newItem.m_bySportsGroupName);
				}
				else
				{
					vecGroup = new Vector();
					m_menuItemTable.put(newItem.m_bySportsGroupName, vecGroup);
				}
				
				
				vecGroup.addElement(newItem);
				
				// set default selected data
				if(i == 0)
				{
					AppSession.sharedSession().setSportEventContent(newItem);
				}
			}
			
			objSportEventList = null;
		}
		catch(JSONException e)
		{
			System.out.println("Sports Events list data error!");
			System.out.println(e.toString());
		}
		appendMoreMenuData();
	}
}
