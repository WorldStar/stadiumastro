package model;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.ResourceConstants;

public class SAOtherAppList extends SAModel{
	
	public Vector m_array;
	public SAOtherAppList()
	{
		initArrayWithParsedData();
	}
	
	public void initArrayWithParsedData()
	{
		execute();
	}
	public int totalCount()
	{
		return m_array.size();
	}
	public SAOtherAppListItem getDataAtIndex(int index)
	{
		return (SAOtherAppListItem)m_array.elementAt( index );
	}

	public void execute() {
		// TODO Auto-generated method stub
		m_array = new Vector();

		String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + ResourceConstants.SA_OTHER_APP_BY_ASTRO_LIST;
		String strResponse = DataManager.getResponseDataFromServer(strRequest);

		try {
			JSONObject objAppData = new JSONObject(strResponse);
			JSONArray objAppArray = objAppData.getJSONArray("appContents");
			
			int appCnt = objAppArray.length();
			for(int i = 0; i < appCnt; i++)
			{
				JSONObject objAppItem = objAppArray.getJSONObject(i);
				
				SAOtherAppListItem itemObj = new SAOtherAppListItem();
				itemObj.m_appDesc = objAppItem.getString("appDesc");
				itemObj.m_appIOSUrl = objAppItem.getString("appBBURL");
				itemObj.m_appLogoURL = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + objAppItem.getString("appLogoURL");
				itemObj.m_appName = objAppItem.getString("appName");
				itemObj.m_appURL = objAppItem.getString("appURL");
				
				m_array.addElement(itemObj);
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
