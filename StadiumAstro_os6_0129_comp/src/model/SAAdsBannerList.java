package model;


import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;
import application.AppSession;
import application.ResourceConstants;
import model.SAModel;

public class SAAdsBannerList{

	public SAAdsBannerItem m_adsBannerItem = null;

	public SAAdsBannerList(){
		this.initArrayWithParsedData();
	}
	
	public void initArrayWithParsedData()
	{
		execute();
	}

	public void execute() {
		// TODO Auto-generated method stub
		String strResponse;
		
		// get response data from server
		if(AppSession.useLocalDataForTest)
			strResponse = ResourceConstants.SA_JSON_DATA_ADS_BANNER_LIST;
		else
		{
			String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + ResourceConstants.SA_ADS_BANNER_LIST_API_PATH + AppSession.sharedSession().m_curSportEventContentKey;
			strResponse = DataManager.getResponseDataFromServer(strRequest);
		}
		
		try{
			// parse json data
			JSONObject objAdsBannerList = new JSONObject(strResponse);
			
			JSONArray arrAdsBannerContent = objAdsBannerList.getJSONArray(ResourceConstants.SA_KEY_ADS_BANNER);
			
			// use only first ads banner content
			JSONObject objAdsBanner = (JSONObject)arrAdsBannerContent.get(0);
			
			String strImageFilePath = "";
			
			// determine image file path
			if(AppSession.useLocalDataForTest)
				strImageFilePath = null;//ResourceConstants.SA_IMG_ADS_BAR;
			else
			{
				// search pre-loaded ads image at image buffer
				strImageFilePath =  ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + objAdsBanner.getString(ResourceConstants.SA_KEY_ADS_BANNER_MOBILE_FILE);
			}
			// set data to array
			m_adsBannerItem = new SAAdsBannerItem();
			
			m_adsBannerItem.m_full_file = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + objAdsBanner.get(ResourceConstants.SA_KEY_ADS_BANNER_FULL_FILE).toString();
			m_adsBannerItem.m_full_url = objAdsBanner.get(ResourceConstants.SA_KEY_ADS_BANNER_FULL_URL).toString();
			m_adsBannerItem.m_mobile_file = strImageFilePath; 
			m_adsBannerItem.m_mobile_url = objAdsBanner.get(ResourceConstants.SA_KEY_ADS_BANNER_MOBILE_URL).toString();
			m_adsBannerItem.m_table_file = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + objAdsBanner.get(ResourceConstants.SA_KEY_ADS_BANNER_TABLE_FILE).toString();
			m_adsBannerItem.m_table_vr_file = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + objAdsBanner.get(ResourceConstants.SA_KEY_ADS_BANNER_TABLE_VR_FILE).toString();
			m_adsBannerItem.m_tablet_url = objAdsBanner.get(ResourceConstants.SA_KEY_ADS_BANNER_TABLET_URL).toString();
			m_adsBannerItem.m_tablet_vr_url = objAdsBanner.get(ResourceConstants.SA_KEY_ADS_BANNER_TABLET_VR_URL).toString();
								
			objAdsBannerList = null;
		}
		catch(JSONException e)
		{
			System.out.println(e.toString());
		}
		
	}
}
