package model;

import application.ResourceConstants;

public class SAStandingListItem {
	public String m_groupName;
	public String m_goalScored;
	public String m_goalAgainst;
	public String m_hasDetailFlag;
	public String m_pos;
	public String m_teamId;
	public String m_teamAlias;
	public String m_teamLogo;
	public String m_teamName;
	
	public String m_played; 
	public String m_won;
	public String m_lost;	
	
	public String m_draw;
	public String m_goalDiff;
	public String m_points;
	
	public SAStandingListItem() {

		m_pos = "1";
		m_teamLogo = ResourceConstants.SA_IMG_TEAMLOGO;
		m_teamId = "634";
		m_teamAlias = "CHS";
		m_teamName = "Chel Sea";
		m_played = "39";
		m_won = "36";
		m_draw = "1";
		m_lost = "2";
		m_goalDiff = "99";
		m_points = "78";
		m_hasDetailFlag = "N";
		
	}
}
