package model;

import application.ResourceConstants;

public class SACategoryItem {
	public String m_name;
	public String m_refKey;
	public String m_bitmapURL;
	
	public SACategoryItem(){
		this.m_name = "";
		this.m_bitmapURL = ResourceConstants.SA_IMG_CELL_BACK_MIDDLE;
		m_refKey = "";
	}
}
