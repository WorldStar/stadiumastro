package model;

import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.AppSession;
import application.ResourceConstants;
import application.Utils;

public class SASoccerMatchList{
	public Vector m_array;
	
	//Temporary variable for executing thread.
	String m_strID;
	public SASoccerMatchList()
	{
	}

	public void initArrayWithParsedData(String strID)
	{
		m_strID = strID;
		execute();
	}
	
	public int totalCount(){
		if(m_array == null){
			return 0;
		}
		return m_array.size();
	}
	public SASoccerMatchListItem getDataAtIndex(int index){
		if(m_array.size() > 0 && index < m_array.size() && index >= 0)
			return (SASoccerMatchListItem)m_array.elementAt(index); 
		return null;
	}

	public void execute() {
		// TODO Auto-generated method stub

		try{
			String strResponse;
			if(m_strID == null){	//in case of HomeScreen's SoccerMatchList
				if(AppSession.useLocalDataForTest)
				{
					// use local model data(same as server response)
					strResponse = ResourceConstants.SA_JSON_DATA_SOCCER_MATCH_LIST;
				}
				else
				{
					// request data to server
					String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + ResourceConstants.SA_SOCCER_MATCH_LIST_API_PATH + AppSession.sharedSession().m_curSportEventContentKey;
					strResponse = DataManager.getResponseDataFromServer(strRequest);
				}	
			}else{		//in case of MatchesScreen's MatchListForParticularDate
				if(AppSession.useLocalDataForTest)
				{
					// use local model data(same as server response)
					strResponse = ResourceConstants.SA_JSON_DATA_MATCH_LIST_FOR_PARTICULAR_DATE;
				}
				else
				{
					// request data to server
					String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + ResourceConstants.SA_FIXTURES_CALENDAR_MATCH_LIST_FOR_PARTICULAR_CALENDAR_DATE_API_PATH + m_strID;
					strResponse = DataManager.getResponseDataFromServer(strRequest);
				}
			}
			// parse json data
			JSONObject objMatchList = new JSONObject(strResponse);
			
			JSONArray arrSoccerMatch = objMatchList.getJSONArray(ResourceConstants.SA_KEY_SOCCER_MATCH);
			
			// get array size
			int nCount = arrSoccerMatch.length();
			m_array = new Vector(nCount);
			
			for(int i = 0; i < nCount; i++){
				JSONObject matchObj = (JSONObject)arrSoccerMatch.get(i);
				
				// create object
				SASoccerMatchListItem matchInfo = new SASoccerMatchListItem();
				
				// set object data
				JSONObject matchDate = matchObj.getJSONObject(ResourceConstants.SA_KEY_SOCCER_MATCH_MATCHDATE);			
				Date date = new Date(matchDate.getLong(ResourceConstants.SA_KEY_SOCCER_MATCH_TIME));
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(date);
				
				matchInfo.m_month = calendar.get(Calendar.MONTH);
				matchInfo.m_date = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
				matchInfo.m_year = String.valueOf(calendar.get(Calendar.YEAR));
				matchInfo.m_day = calendar.get(Calendar.DAY_OF_WEEK);
				 
				String strMinute = String.valueOf(calendar.get(Calendar.MINUTE));
				if(strMinute.length() == 1)
					strMinute = "0" + strMinute;
				
				matchInfo.m_hours = String.valueOf(calendar.get(Calendar.HOUR));
				
				matchInfo.m_time = String.valueOf(calendar.get(Calendar.HOUR)) 
				+ ":" 
				+ strMinute
				+ (calendar.get(Calendar.AM_PM) == Calendar.AM ? " AM" : " PM");
					
				matchInfo.m_dateCalendar = calendar;
				
				matchInfo.m_homeTeamAlias = matchObj.getString(ResourceConstants.SA_KEY_SOCCER_MATCH_HOMETEAM_ALIAS);
				matchInfo.m_homeTeamScore = matchObj.getString(ResourceConstants.SA_KEY_SOCCER_MATCH_HOMETEAM_SCORE);
				matchInfo.m_homeTeamId = matchObj.getString(ResourceConstants.SA_KEY_SOCCER_MATCH_HOMETEAM_ID);
				
				matchInfo.m_visitingTeamAlias = matchObj.getString(ResourceConstants.SA_KEY_SOCCER_MATCH_VISITINGTEAM_ALIAS);
				matchInfo.m_visitingTeamScore = matchObj.getString(ResourceConstants.SA_KEY_SOCCER_MATCH_VISITINGTEAM_SCORE);
				
				matchInfo.m_channelInfo = matchObj.getString(ResourceConstants.SA_KEY_SOCCER_MATCH_CHANNEL_INFO);
//				matchInfo.m_channelInfo = strChannelInfo.equals("")? "" : "Ch " + strChannelInfo;
				
				
				matchInfo.m_matchID = matchObj.getString(ResourceConstants.SA_KEY_SOCCER_MATCH_ID);
				
				if(AppSession.useLocalDataForTest)
				{
					matchInfo.m_homeTeamLogo = "img/temp/team_logo.png";
					matchInfo.m_visitingTeamLogo = "img/temp/team_logo.png";
				}
				else{
					AppSession _sharedSession = AppSession.sharedSession();
					
					matchInfo.m_homeTeamLogo = _sharedSession.getTeamLogoImage(matchObj.getString(ResourceConstants.SA_KEY_SOCCER_MATCH_HOMETEAM_ID), ResourceConstants.SA_KEY_TEAM_LOGO_44x44);
					matchInfo.m_visitingTeamLogo = _sharedSession.getTeamLogoImage(matchObj.getString(ResourceConstants.SA_KEY_SOCCER_MATCH_VISITINGTEAM_ID), ResourceConstants.SA_KEY_TEAM_LOGO_44x44);
					
					matchInfo.m_homeTeamId = matchObj.getString(ResourceConstants.SA_KEY_SOCCER_MATCH_HOMETEAM_ID);
					matchInfo.m_visitingTeamId = matchObj.getString(ResourceConstants.SA_KEY_SOCCER_MATCH_VISITINGTEAM_ID);
				}
				matchInfo.m_strID = matchObj.getString("strId");
				matchInfo.m_stadiumCity = matchObj.getString(ResourceConstants.SA_KEY_SOCCER_MATCH_STADIUM_CITY);
				matchInfo.m_stadiumName = matchObj.getString(ResourceConstants.SA_KEY_SOCCER_MATCH_STADIUM_NAME);
				
				matchInfo.m_matchStatus = matchObj.getString(ResourceConstants.SA_KEY_SOCCER_MATCH_STATUS);
				
				m_array.addElement(matchInfo);
			}
			
			objMatchList = null;			
		}
		catch(JSONException e)
		{
			System.out.println("Soccer match list data error!");
			System.out.println(e.toString());
		}
	}
	
}
