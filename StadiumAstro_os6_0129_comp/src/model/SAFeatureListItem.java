package model;

public class SAFeatureListItem {
	public long m_dateInt;
	public String m_description;
	public String m_fullBody;
	public String m_imageUrl;
	public String m_ipadUrl;
	public String m_iphoneUrl;
	public String m_date;
	public String m_title;
	public String m_type;	
	public String m_mediaID;

	public SAFeatureListItem(){
		m_dateInt = 0;
		m_description = "";
		m_fullBody = "";
		m_ipadUrl = "";
		m_iphoneUrl = "";
		m_imageUrl = "";
		m_title = "";
		m_date = "";
		m_type = "";
		m_mediaID = "";
	}
}
