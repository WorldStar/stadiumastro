package model;


import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.AppSession;
import application.ResourceConstants;
import application.Utils;

public class SAMatchPreview extends SAModel{
	public String m_body;
	public String m_publishedDate;
	public String m_refKey;
	public String m_title;
	
	//Temporary variable for execute command
	String m_strRefKey;
	public SAMatchPreview(String strRefKey){
		
		this.initArrayWithParsedData(strRefKey);
	}
	public void initArrayWithParsedData(String strRefKey)
	{
		m_strRefKey = strRefKey;
		
		execute();
	}
	public void execute() {
		// TODO Auto-generated method stub

		String strResponse;
		String strRequest;
		// get response data from server
		if(AppSession.useLocalDataForTest){
			strResponse = ResourceConstants.SA_JSON_DATA_SOCCER_MATCH_PREVIEW;
			strResponse = Utils.replaceString(strResponse,"&lt;","<");
			strResponse = Utils.replaceString(strResponse,"&gt;",">");
			strResponse = Utils.replaceString(strResponse,"&#39;","'");
			strResponse = Utils.replaceString(strResponse,"&#163;","f");
			strResponse = Utils.replaceString(strResponse,"&quot;","\\\"");
			strResponse = Utils.replaceString(strResponse,"\r","");
			strResponse = Utils.replaceString(strResponse,"\n","");
			
		}
		else
		{
			strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + ResourceConstants.SA_SOCCER_MATCH_PREVIEW_FOR_PARTICULAR_MATCH_API_PATH + m_strRefKey;
			strResponse = DataManager.getResponseDataFromServer(strRequest);
		
		}
		
		try{
			// parse json data
			JSONObject objMatchPreview = new JSONObject(strResponse);
			
			// set data
			m_body = objMatchPreview.getString(ResourceConstants.SA_KEY_ANALYSIS_DETAIL_BODY);
			
			m_publishedDate = objMatchPreview.getString(ResourceConstants.SA_KEY_ANALYSIS_DETAIL_PUB_DATE);
			m_refKey = objMatchPreview.getString(ResourceConstants.SA_KEY_ANALYSIS_DETAIL_REF_KEY);
			m_title = objMatchPreview.getString(ResourceConstants.SA_KEY_ANALYSIS_DETAIL_TITLE);
			
			objMatchPreview = null;
		}
		catch(JSONException e)
		{
			System.out.println("Match Preview data parsing error");
			System.out.println(e.toString());
		}
	}
	
}
