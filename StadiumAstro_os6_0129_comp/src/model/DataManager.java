package model;

import java.io.InputStream;
import java.util.Enumeration;

import java.util.Vector;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import application.AppSession;
import application.Utils;

import net.rim.device.api.io.IOUtilities;
import net.rim.device.api.servicebook.ServiceBook;
import net.rim.device.api.servicebook.ServiceRecord;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.CoverageInfo;
import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.system.EncodedImage;
import net.rim.device.api.system.WLANInfo;
import net.rim.device.api.ui.UiApplication;

//import net.rim.device.api.io.messaging.*;
//import net.rim.device.api.io.URI;



/**
 * 
 * @author Administrator
 *	This class is a data model manager which does Network connecting and JSON data parsing...
 *	Here network request_info managing class did not make and the part of send request/ receive response did not implemented.  
 */

class Dispatcher implements Runnable{
	Bitmap m_bmp;
	byte[] m_data;
	ImageLoadListener m_listener;
	String m_request;
	public Dispatcher(Bitmap bmp, ImageLoadListener listener, String request)
	{
		m_bmp = bmp;
		m_listener = listener;
		m_request = request;
	}
	
	public void run()
	{
		if(m_bmp != null)
			m_listener.imageLoadSuccess(m_request, m_bmp);
		else
			m_listener.imageLoadFailed(m_request, null);
	}
}

public class DataManager {

	public Vector dispatchTable;
	static DataManager sharedManager;
	Thread loader;
	public DataManager()
	{
		dispatchTable = new Vector();
		//Image Loader.
		loader = new Thread()
		{
			public void run()
			{
				String request;
				ImageLoadListener listener;
				
				while(true)
				{
//					If dispatch table is empty, then stop loop.
					
					synchronized(dispatchTable)
					{	
						try{
							if(dispatchTable.isEmpty())	
								dispatchTable.wait();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					
						
						Vector dispatchSet = (Vector) dispatchTable.elementAt(0);
						
						request = (String) dispatchSet.elementAt(0);
						listener = (ImageLoadListener) dispatchSet.elementAt(1);
						dispatchTable.removeElementAt(0);

					}
										
					byte[] data = getImageFileFromServer(request);
					try{
						if(data == null)
							throw new Exception();
						
						EncodedImage eImg = EncodedImage.createEncodedImage(data, 0, data.length);
						eImg = Utils.scaleEncodedImage(eImg, 300);
						
						Bitmap image = eImg.getBitmap();
						Dispatcher m_dispatch = new Dispatcher(image, listener, request);
						UiApplication uiApp = UiApplication.getUiApplication();
						
						uiApp.invokeAndWait(m_dispatch);
						System.out.println("Saved image file");
					}
					catch(Exception ex)
					{
						System.out.println("exception occurred: " + ex.getMessage());
						ex.printStackTrace();
						
						Dispatcher m_dispatch = new Dispatcher(null, listener, request);
						UiApplication uiApp = UiApplication.getUiApplication();
						uiApp.invokeAndWait(m_dispatch);
					}
				}
			}
		};
		loader.start();
	}
	
	
	
	
	public synchronized static DataManager getSharedDataManager()
	{
		if(sharedManager == null)
			sharedManager = new DataManager();
		return sharedManager;
	}
	
	/**
	 * @author Jet
	 * @param: None
	 * @return: None
	 * @desc : Clear dispatch queue. Synchronizing is necessary.
	 */
	
	public void clearDispatchQueue()
	{
		synchronized (dispatchTable) {
			Enumeration enumerator = dispatchTable.elements();
			while(enumerator.hasMoreElements())
			{
				Vector dispatchRecord = (Vector) enumerator.nextElement();
				
				String request = (String) dispatchRecord.firstElement();
				ImageLoadListener listener = (ImageLoadListener) dispatchRecord.lastElement();
				if(request != null && listener != null)
				{
					listener.imageLoadCanceled(request);
				}
			}
			
			dispatchTable.removeAllElements();
		}
	}
	
	/**
	 * 
	 * @param strRequest
	 * 		request string to be sent to server
	 * @param savePath
	 * 		local path to save loaded image.
	 * @param bmpTarget
	 * 		bitmap target to set image to.
	 * @return
	 * 		if success return 1
	 * 		else return 0
	 */
	
	public synchronized int getImageFileFromServer(String strRequest, ImageLoadListener listener)
	{	
		
		if(listener == null)
			return 0;
		if(strRequest == null)
			return 0;
		
		boolean loadStart = false;
		synchronized(dispatchTable){
			
			if(dispatchTable.isEmpty())
				loadStart = true;
	
			Vector dispatchSet = new Vector();
			dispatchSet.addElement(strRequest);
			dispatchSet.addElement(listener);
			dispatchTable.addElement(dispatchSet);
			
			if(loadStart)
				dispatchTable.notify();
		}
		
		return 1;
	}
	
	/**
	 * @see
	 * 		This is blocking method. 
	 * @param 
	 * 		strRequest: request string to be send to server
	 * @return
	 * 
	 * 		local path string of image to be saved
	 */
	public static synchronized byte[] getImageFileFromServer(String strRequest)
	{
		// TODO: send request to server, receive image file, save it to local storage and return its save path
		try{

			HttpConnection httpConn = null;
			while(strRequest != null && strRequest.length() != 0)
			{
				if(httpConn != null)
					httpConn.close();
				
				if(AppSession.useSimulator)
					httpConn = (HttpConnection) Connector.open(strRequest + getConnectionString());
				else
					httpConn = (HttpConnection) Connector.open(strRequest + getConnectionString());
		        
		        httpConn.setRequestMethod(HttpConnection.GET);
		        
//		        Redirect url
		        strRequest = httpConn.getHeaderField("Location");
		        System.out.println("redirected = " + strRequest);
			}
			
	        InputStream is = httpConn.openInputStream();
	        byte[] data = IOUtilities.streamToBytes(is);
	        
	        is.close();
	        httpConn.close();
	        
	        return data;
		}
		catch(Exception ex)
		{
			System.out.println("Can't load image from server " + ex.getMessage());
			return null;
		}
	}
	
	/**
	 * 
	 * @param 
	 * 		strRequest: request string to be send to server
	 * @return
	 * 		json response data from server
	 */
	
	public static String getResponseDataFromServer(String strRequest)
	{
		// TODO: send request string to server and get response data
		try{
			
			//In case of simulator
			HttpConnection httpConn;
			if(AppSession.useSimulator)
				httpConn = (HttpConnection) Connector.open(strRequest + getConnectionString());
			else
				httpConn = (HttpConnection) Connector.open(strRequest + getConnectionString());
	        
	        httpConn.setRequestMethod(HttpConnection.GET);
	
	        httpConn.getResponseCode();
	
	        InputStream is = httpConn.openInputStream(); 
	
	        byte[] data = IOUtilities.streamToBytes(is);
	        
	        is.close();
	        httpConn.close();
	        
	        String json = new String(data, "UTF-8");
	        return json;
	        
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
			return "";
		}
	}
	
	 public static String getConnectionString() {

	     String connectionString = null;

	     // Simulator behaviour is controlled by the USE_MDS_IN_SIMULATOR
	     // variable.
	     if (DeviceInfo.isSimulator()) {

	         connectionString = ";deviceside=true";
	     }

	     // Wifi is the preferred transmission method
	     else if (WLANInfo.getWLANState() == WLANInfo.WLAN_STATE_CONNECTED) {

	         connectionString = ";interface=wifi";
	     }

	     // Is the carrier network the only way to connect?
	     else if ((CoverageInfo.getCoverageStatus() & CoverageInfo.COVERAGE_DIRECT) == CoverageInfo.COVERAGE_DIRECT) {

	         String carrierUid = getCarrierBIBSUid();

	         if (carrierUid == null) {
	             // Has carrier coverage, but not BIBS. So use the carrier's TCP
	             // network

	             connectionString = ";deviceside=false";
	         } else {
	             // otherwise, use the Uid to construct a valid carrier BIBS
	             // request

	             connectionString = ";deviceside=false;connectionUID="+carrierUid + ";ConnectionType=mds-public";
	         }
	     }

	     // Check for an MDS connection instead (BlackBerry Enterprise Server)
	     else if ((CoverageInfo.getCoverageStatus() & CoverageInfo.COVERAGE_MDS) == CoverageInfo.COVERAGE_MDS) {

	         connectionString = ";deviceside=false";
	     }

	     // If there is no connection available abort to avoid hassling the user
	     // unnecssarily.
	     else if (CoverageInfo.getCoverageStatus() == CoverageInfo.COVERAGE_NONE) {
	         connectionString = "none";

	     }

	     // In theory, all bases are covered by now so this shouldn't be reachable.But hey, just in case ...
	     else {
	         connectionString = ";deviceside=true";
	     }

	     return connectionString;
	 }

	 /**
	  * Looks through the phone's service book for a carrier provided BIBS
	  * network
	  * 
	  * @return The uid used to connect to that network.
	  */
	 private synchronized static String getCarrierBIBSUid() {
	     ServiceRecord[] records = ServiceBook.getSB().getRecords();
	     int currentRecord;

	     for (currentRecord = 0; currentRecord < records.length; currentRecord++) {
	         if (records[currentRecord].getCid().toLowerCase().equals("ippp")) {
	             if (records[currentRecord].getName().toLowerCase()
	                     .indexOf("bibs") >= 0) {
	                 return records[currentRecord].getUid();
	             }
	         }
	     }

	     return null;
	 }
}
