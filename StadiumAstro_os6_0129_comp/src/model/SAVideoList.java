package model;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.AppSession;
import application.ResourceConstants;
import application.Utils;

public class SAVideoList extends SAModel{
	
	public Vector m_array;
	
	//Temporary variable for executing.
	String m_strRefKey;
	public SAVideoList(String strRefKey)
	{
		this.initArrayWithParsedData(strRefKey);
	}
	
	public void initArrayWithParsedData(String strRefKey)
	{
		// get response data from server
		m_strRefKey = strRefKey;	
		execute();
	}

	public int totalCount(){
		return m_array.size();
	}
	public SAVideoListItem getDataAtIndex(int index){
		if(m_array.size() > 0 && index < m_array.size() && index >= 0)
			return (SAVideoListItem)m_array.elementAt(index); 
		return null;
	}

	public void execute() {
		// TODO Auto-generated method stub
		String strResponse;		
		if(AppSession.useLocalDataForTest)
			strResponse = ResourceConstants.SA_JSON_DATA_VIDEO_FEED_LIST;
		else
		{
			String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + ResourceConstants.SA_VIDEOS_FEED_LIST_API_PATH + m_strRefKey;
			strResponse = DataManager.getResponseDataFromServer(strRequest);
		}
		
		try
		{
			// parsing data
			JSONObject objVideoFeedList = new JSONObject(strResponse);
			
			JSONArray arrVideoFeedList = objVideoFeedList.getJSONArray(ResourceConstants.SA_KEY_VIDEO_FEED_LIST);
			
			int nCount = arrVideoFeedList.length();
			m_array = new Vector(nCount);
			
			for(int i = 0; i < nCount; i++)
			{
				JSONObject objVideoFeedItem = (JSONObject)arrVideoFeedList.get(i);
				
				// create data item from json data
				SAVideoListItem feedItem = new SAVideoListItem();
				feedItem.m_creationDate = Utils.getDateFromString(objVideoFeedItem.getString(ResourceConstants.SA_KEY_VIDEO_FEED_CREATIONDATE));
				feedItem.m_title = objVideoFeedItem.getString(ResourceConstants.SA_KEY_VIDEO_FEED_TITLE);
				feedItem.m_copyright = objVideoFeedItem.getString(ResourceConstants.SA_KEY_VIDEO_FEED_COPYRIGHT);
				feedItem.m_description = objVideoFeedItem.getString(ResourceConstants.SA_KEY_VIDEO_FEED_DESCRIPTION);
				feedItem.m_duration = objVideoFeedItem.getString(ResourceConstants.SA_KEY_VIDEO_FEED_DURATION);
				feedItem.m_id = objVideoFeedItem.getString(ResourceConstants.SA_KEY_VIDEO_FEED_ID);
				feedItem.m_iPadVideoUrl = objVideoFeedItem.getString(ResourceConstants.SA_KEY_VIDEO_FEED_IPAD_VIDEO_URL);
				feedItem.m_iPhoneVideoUrl = objVideoFeedItem.getString(ResourceConstants.SA_KEY_VIDEO_FEED_IPHONE_VIDEO_URL);
				
				
				// determine image file path
				String strImageFilePath = "";	
				
				if(AppSession.useLocalDataForTest)
					strImageFilePath = ResourceConstants.SA_IMG_VIDEO_TABLE_CELL_THUMB;
				else
				{
					strImageFilePath = objVideoFeedItem.getString(ResourceConstants.SA_KEY_VIDEO_FEED_IMAGE_URL);
				}
				feedItem.m_defaultImageUrl = strImageFilePath;
				
				// save to array
				m_array.addElement(feedItem);				
			}
			
			objVideoFeedList = null;
		}
		catch(JSONException e){
			System.out.println("Video Feed List data error!");
			System.out.println(e.toString());
		}
	}
}
