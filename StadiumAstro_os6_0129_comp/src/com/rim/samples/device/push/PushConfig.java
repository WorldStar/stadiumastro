/**
 * AUTO_COPYRIGHT_SUB_TAG
 */
package com.rim.samples.device.push;

import net.rim.device.api.util.Persistable;

/**
 * Encapsulates push configuration for the application
 */
public class PushConfig implements Persistable {

    // Default properties used for the demo.
    // You should register with BlackBerry Push API server and obtain
    // similar values for your application.
    
    // TCP port to listen for push messages
    private int port = 33556;
    // Application ID generated during Push API registration
    private String appId = "3855-s784457B9m331044n0cr03mii8c725l1615";
    // URL to the Push BPS server
    private String bpsUrl = "http://pushapi.eval.blackberry.com";
 
    // Property specifying whether push communication is going through enterprise server (BES)
    private boolean isEnterprise = false;

    private static PushConfig instance;
    
    static {
        instance = new PushConfig();
    }

    /**
     * Defines whether application supports application level acknowledgment
     */
    private boolean applicationAcknoledgment = true;

    public static int getPort() {
        return instance.port;
    }

    public static String getAppId() {
        return instance.appId;
    }

    public static String getBpsUrl() {
        return instance.bpsUrl;
    }

    public static boolean isApplicationAcknoledgementSupported() {
        return instance.applicationAcknoledgment;
    }
    public static void setPort( int port ) {
        instance.port = port;
    }

    public static void setAppId( String appId ) {
        instance.appId = appId;
    }

    public static void setBpsUrl( String bpsUrl ) {
        instance.bpsUrl = bpsUrl;
    }

    public static void setApplicationAcknoledgment( boolean applicationAcknoledgment ) {
        instance.applicationAcknoledgment = applicationAcknoledgment;
    }

    public static boolean isEnterprise() {
        return instance.isEnterprise;
    }

    public static void setEnterprise( boolean isEnterprise ) {
        instance.isEnterprise = isEnterprise;
    }
    
}
