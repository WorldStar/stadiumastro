/**
 * AUTO_COPYRIGHT_SUB_TAG
 */
package com.rim.samples.device.push;

import net.rim.device.api.system.*;
import net.rim.device.api.util.*;

/**
 * Stores persistent data in the push sample application.
 * 
 * <p>
 * Includes the following data - current registration status, message received count, last message received timestamp, push
 * configuration, log messages and SIM card information.
 */
public class PersistentStorage implements Persistable {

    private static long ID = 0x26bd52087559fde8L; // PersistentStorage
    private static PersistentStorage ps;
    
   
    // Specifies whether the application has been successfully registered
    // with Content Provider and BPS server
    private boolean registered;

    static {
        PersistentObject po = PersistentStore.getPersistentObject( ID );
        ps = (PersistentStorage) po.getContents();
        if( ps == null ) {
            ps = new PersistentStorage();
            po.setContents( ps );
            po.commit();
        }
    }

    private PersistentStorage() {
    }

    public static boolean isRegistered() {
        return ps.registered;
    }

    public static void setRegistered( boolean registered ) {
        ps.registered = registered;
        PersistentObject.commit( ps );
    }

    /**
     * Erases persistent store, used for debug purposes only
     */
    public static void erase() {
        PersistentStore.destroyPersistentObject( ID );
    }

}
