/**
 * AUTO_COPYRIGHT_SUB_TAG
 */
package com.rim.samples.device.push.lib;

import java.io.IOException;

/**
 * Network protocol for BPS server. Its implementation depends on handheld firmware and will use the latest API available.
 */
public interface BpasProtocol {

    /**
     * Registers with BPS server
     */
    public void register(boolean isEnterprise ) throws Exception;

    /**
     * Unregisters from BPS server
     */
    public void unregister( boolean isEnterprise ) throws Exception;

}
