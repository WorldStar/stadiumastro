/**
 * AUTO_COPYRIGHT_SUB_TAG
 */
package com.rim.samples.device.push;

import java.io.*;

import javax.microedition.io.*;

import net.rim.device.api.io.http.*;
import net.rim.device.api.system.*;
import net.rim.device.api.util.*;

/**
 * Small utilities for common usage in the push sample application
 */
public class PushUtils {

    /**
     * Declines push message with given reason
     */
    public static void decline( PushInputStream pis, int reason ) {
        try {
            pis.decline( reason );
        } catch( IOException e ) {
        }
    }

    /**
     * Safely closes connection and streams
     */
    public static void close( Connection conn, InputStream is, OutputStream os ) {
        if( os != null ) {
            try {
                os.close();
            } catch( IOException e ) {
            }
        }
        if( is != null ) {
            try {
                is.close();
            } catch( IOException e ) {
            }
        }
        if( conn != null ) {
            try {
                conn.close();
            } catch( IOException e ) {
            }
        }
    }

    public static void runOnEventThread( Runnable r ) {
        if( Application.isEventDispatchThread() ) {
            r.run();
        } else {
            Application.getApplication().invokeLater( r );
        }
    }

    
    /**
     * Forms listen URL based on port number
     */
    public static String formPushListenUrl() {
        String url = "http://:" + PushConfig.getPort();
        url += getConnectionSuffix();
        return url;
    }

    private static String getConnectionSuffix() {
        return ";deviceside=false;ConnectionType=mds-public";
    }

}
