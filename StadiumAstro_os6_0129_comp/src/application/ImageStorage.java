package application;
import java.util.Enumeration;
import java.util.Vector;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.PersistentStore;


public class ImageStorage {
	public Vector m_imageFileBuffer;
	static ImageStorage sharedBuffer = null;
	
	static long imageStoreKey = 0x10c4f86623ad2a6bL;
	PersistentObject m_storageInfo;
	int maxStorageObj;
	//True: Save into storage, False save into memory.
	

	public static ImageStorage getSharedImageBuffer()
	{
		if(sharedBuffer == null)
			sharedBuffer = new ImageStorage();
		
		return sharedBuffer;
			
	}
	public ImageStorage()
	{
		m_storageInfo = PersistentStore.getPersistentObject(imageStoreKey);
		
		Object obj = m_storageInfo.getContents();
		if(obj instanceof Vector)
		{
			m_imageFileBuffer = (Vector) m_storageInfo.getContents();
		}
		if(m_imageFileBuffer == null)
			m_imageFileBuffer = new Vector();
		
		maxStorageObj = 50;
	}
		
	public void saveImage(String imageName, Bitmap bmp)
	{
		if(bmp == null)
			return;
		if(m_imageFileBuffer.size() > maxStorageObj)
			freeStorage();
		
		int hashcode = imageName.hashCode();
		Integer l_hashcode = new Integer(hashcode);
		
		PersistentObject obj = PersistentStore.getPersistentObject(hashcode);
		if(obj.getContents() == null)
		{
			PersistableBitmap storedBmp = new PersistableBitmap(bmp);
		
			obj.setContents(storedBmp);
			obj.commit();
		
			m_imageFileBuffer.addElement(l_hashcode);
			
			m_storageInfo.setContents(m_imageFileBuffer);
			m_storageInfo.commit();
		}
	}
	
	public Bitmap getImage(String imageName)
	{
		int hashcode = imageName.hashCode();
		
		PersistentObject storage = PersistentStore.getPersistentObject(hashcode);
		if(storage.getContents() != null)
		{
			PersistableBitmap storageBmp = (PersistableBitmap)storage.getContents();
			
			Bitmap bmp = storageBmp.getBitmapImage();
			return bmp;
		}
		else
			return null;
		
	}
	
	public void freeStorage()
	{
		//Free 3/4 of current storage. Please use when storage is full.
		Integer element;
		int freeAmount = m_imageFileBuffer.size() * 3 / 4;
		for(int i = 0; i < freeAmount; i++)
		{
			element = (Integer)m_imageFileBuffer.firstElement();
			PersistentStore.destroyPersistentObject(element.longValue());
			m_imageFileBuffer.removeElement(element);
		}
		m_storageInfo.setContents(m_imageFileBuffer);
		m_storageInfo.commit();
	}
	
	public void clearStorage()
	{
	
		Integer element;
		Enumeration elements = m_imageFileBuffer.elements();
		//Destroy all persistent object 
		while(elements.hasMoreElements())
		{
			element = (Integer)elements.nextElement();
			PersistentStore.destroyPersistentObject(element.longValue());
		}
		m_storageInfo.setContents(null);
		m_storageInfo.commit();

		m_imageFileBuffer.removeAllElements();
	}
	
	protected void finalize()
	{
		clearStorage();
	}
}
