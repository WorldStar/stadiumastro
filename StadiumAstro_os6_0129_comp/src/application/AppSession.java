package application;

import java.util.Hashtable;
import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import view.component.AdsBar;

import model.DataManager;
import model.SASportEventContentItem;


/**
 * 
 * @author Administrator
 *	AppSession Class manages instance and values that used like global data.
 *	All of the global data stores in this class and everyone can call this anywhere.
 */

public class AppSession {

	public String m_curSportEventContentKey;
	public static AppSession session = null;
	
	public Hashtable m_teamLogoList;
	public Hashtable m_imageFileBuffer;
	
	public static boolean useLocalDataForTest = false; // flag of test or real product
	public static boolean useSimulator = true;
	
	public String m_curSportsEventName;
	public SASportEventContentItem m_sportEventContent = null;
	public Vector m_mainSubMenus = null; 
	
	public AdsBar m_AdsBar = null;
	public boolean m_showAdsBar = true;
	
	public String m_curTVChannelKey;
	
	public AppSession(){
		m_teamLogoList = null;
		m_curSportEventContentKey = "";
		m_imageFileBuffer = new Hashtable();
		
		m_mainSubMenus = new Vector();
	}
	
	/*
	 * Singleton method for this class
	 */
	public static AppSession sharedSession(){
		if(session == null){
			session = new AppSession();
		}
		return session;
	}
	
	
	// added by star	
	/*
	 * Set team logo list data with current sport event content refkey.
	 */
	public void setTeamLogoListData()
	{
		if(m_teamLogoList == null)
			m_teamLogoList  = new Hashtable();
		
		try{
			String strTeamsLogoResponse;
						
			if(useLocalDataForTest)
				strTeamsLogoResponse = ResourceConstants.SA_JSON_DATA_TEAMS_LOGO_LIST;
			else
			{
				// get team logo data from server
				String strTeamsLogoRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + ResourceConstants.SA_TEAMS_LOGO_API_PATH + m_curSportEventContentKey;
				strTeamsLogoResponse = DataManager.getResponseDataFromServer(strTeamsLogoRequest);
			}
			
			// parsing teams logo list data
			JSONObject objTeamsLogoList = new JSONObject(strTeamsLogoResponse);
			
			JSONArray arrTeamsLogo = objTeamsLogoList.getJSONArray(ResourceConstants.SA_KEY_TEAMS_LOGO_LIST_CONTENT);
			
			int nCount = arrTeamsLogo.length();
			for(int i = 0; i < nCount; i++)
			{
				JSONObject teamObj = (JSONObject)arrTeamsLogo.get(i);
				
				// create logo map data for each team
				String strTeamID = teamObj.getString(ResourceConstants.SA_KEY_TEAM_ID);
				
				Hashtable teamLogoData = new Hashtable();				
				String strTeamLogo100x100 = teamObj.getString(ResourceConstants.SA_KEY_TEAM_LOGO_100x100);
				teamLogoData.put(ResourceConstants.SA_KEY_TEAM_LOGO_100x100, strTeamLogo100x100);
				String strTeamLogo22x22 = teamObj.getString(ResourceConstants.SA_KEY_TEAM_LOGO_22x22);
				teamLogoData.put(ResourceConstants.SA_KEY_TEAM_LOGO_22x22, strTeamLogo22x22);
				String strTeamLogo44x44 = teamObj.getString(ResourceConstants.SA_KEY_TEAM_LOGO_44x44);
				teamLogoData.put(ResourceConstants.SA_KEY_TEAM_LOGO_44x44, strTeamLogo44x44);
				String strTeamName = teamObj.getString(ResourceConstants.SA_KEY_TEAM_NAME);
				teamLogoData.put(ResourceConstants.SA_KEY_TEAM_NAME, strTeamName);
				
				// add to logo list
				m_teamLogoList.put(strTeamID, teamLogoData);
			}
		}
		catch(JSONException e)
		{
			System.out.println(e.toString());
		}
	}
	
	/**
	 * 
	 * @param strTeamId: specific team's id string
	 * @param strImageSize: image size string to need(its value relative with ResourceConstants.SA_KEY_TEAM_LOGO_***x***)
	 * @return	logo image's path string
	 *  	
	 */
	public String getTeamLogoImage(String strTeamId, String strImageSize)
	{	
		// produce key string for image buffer
		String strImageBufferKey = strTeamId + "_" + strImageSize;
		
		if(m_imageFileBuffer != null)
		{
			// search in image buffer
			Object objImagePath = m_imageFileBuffer.get(strImageBufferKey);
			if(objImagePath != null)
				return objImagePath.toString();
		}
		
		if(m_teamLogoList == null)
			return "";
		
		Hashtable teamLogoData = (Hashtable)m_teamLogoList.get(strTeamId);
		if(teamLogoData == null)
			return "";
		
		// get image's url string
		String strImageUrl = teamLogoData.get(strImageSize).toString();
		
		String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + strImageUrl;
		
		// request image file to server
//		String strImageFislePath = DataManager.getImageFileFromServer(strRequest);
		
		// append image file path string to image file buffer map
		m_imageFileBuffer.put(strImageBufferKey, strRequest);
		
		// return image file's path string
		return strRequest;
	}
	
	/**
	 * Determine current home screen's display content
	 * 		
	 * @param 
	 * 		item : SASportEventContentItem object that is current selected 
	 */
	public void setSportEventContent(SASportEventContentItem item)
	{
		if(m_curSportEventContentKey != item.m_refKey)
		{
			m_curSportEventContentKey = item.m_refKey;
			setTeamLogoListData();
		}
		System.out.println("Item = ");
		System.out.println(item);
		
		m_curSportsEventName = item.m_sportsEventName;
		m_sportEventContent = item;
	}
	
}
