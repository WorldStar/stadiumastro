package application;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.util.Persistable;

public class PersistableBitmap implements Persistable {
    int width;
    int height;
    int[] argbData;
    boolean isValid;
    public PersistableBitmap(Bitmap image) {
    	if(image != null)
    	{
    		width = image.getWidth();
    		height = image.getHeight();
    		argbData = new int[width * height];
    		image.getARGB(argbData, 0, width, 0, 0, width, height);
    		isValid = true;
    	}
    	else
    		isValid = false;
    }

    public static Bitmap cropBitmap(Bitmap original, int width, int height) {
    	if(original !=  null)
    	{
    		Bitmap bmp = new Bitmap(width, height);

    		int x = (original.getWidth() / 2) - (width / 2); // Center the new size by width
    		int y = (original.getHeight() / 2) - (height / 2); // Center the new size by height
    		int[] argb = new int[width * height]; 
    		original.getARGB(argb, 0, width, x, y, width, height);
    		bmp.setARGB(argb, 0, width, 0, 0, width, height);

    		return bmp;
    	}
    	return null;
    }
    
    public Bitmap getBitmapImage() {
        Bitmap image = new Bitmap(width, height);
        image.setARGB(argbData, 0, width, 0, 0, width, height);
        return image;
    }
    public boolean isValidObject(){
    	return isValid;
    }
}
