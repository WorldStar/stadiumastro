package application;

import java.util.Stack;

import javax.microedition.io.StreamConnection;

import com.rim.samples.device.push.PushConfig;

import com.rim.samples.device.push.PersistentStorage;
import com.rim.samples.device.push.PushMessage;
import com.rim.samples.device.push.PushMessageReader;
import com.rim.samples.device.push.PushUtils;
import com.rim.samples.device.push.lib.BpasProtocol;
import com.rim.samples.device.push.lib.PushMessageListener;

import view.component.MainMenu;
import view.screen.*;

import model.DataManager;
import model.SASoccerMatchListItem;
import net.rim.blackberry.api.push.PushApplication;
import net.rim.blackberry.api.push.PushApplicationDescriptor;
import net.rim.blackberry.api.push.PushApplicationRegistry;
import net.rim.blackberry.api.push.PushApplicationStatus;
import net.rim.device.api.io.http.PushInputStream;
import net.rim.device.api.system.ApplicationDescriptor;
import net.rim.device.api.system.CoverageInfo;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;

import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.MainScreen;

/**
 * This class extends the UiApplication class, providing a
 * graphical user interface.
 */
public class StadiumAstroApp extends UiApplication implements PushApplication,BpasProtocol, PushMessageListener
{
	public SplashScreen mSplashScreen;
	public PartnerLogoScreen mPartnerLogoScreen;
	public HomeScreen mHomeScreen;
	public HelpScreen mHelpScreen = null;
	
	//for Navigation
	
	public int _pushedScreenCountFromHome;
	MessageReadingThread thread;

    /**
     * Entry point for application
     * @param args Command line arguments (not used)
     */ 
    public static void main(String[] args)
    {
        // Create a new instance of the application and make the currently
        // running thread the application's event dispatch thread.
    	
    	Utils.init();
    	
        StadiumAstroApp theApp = new StadiumAstroApp();       
        theApp.enterEventDispatcher();
    }
    /**
     * Creates a new StadiumAstro object
     */
    public StadiumAstroApp()
    {
    	int coverage = CoverageInfo.getCoverageStatus();
    	if((coverage | CoverageInfo.COVERAGE_DIRECT) == 0)
    		return;
    	mSplashScreen = new SplashScreen();
    	mHelpScreen = new HelpScreen();
        pushScreen(mSplashScreen);
        
        try{
        	this.register(false);
        }
        catch(Exception ex)
        {
        }
    }
    /**
	 * @function goto Secondary Logo Page from First Splash Screen. 
	 */
    public void gotoPartnerScreen()
    {
    	mPartnerLogoScreen = new PartnerLogoScreen();
    	
     	//fading animation for screen transition
    	
    	UiEngineInstance engine = Ui.getUiEngineInstance();
    	
    	TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_FADE);
    	transition.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
    	transition.setIntAttribute(TransitionContext.ATTR_KIND, TransitionContext.KIND_IN);
    	
    	engine.setTransition(mSplashScreen, mPartnerLogoScreen, UiEngineInstance.TRIGGER_PUSH, transition);
        
    	popScreen(mSplashScreen);
    	pushScreen(mPartnerLogoScreen);
    	
    }
    
    /**
     * first time home screen loaded...
     */
    public void gotoHomeScreenFromPartnerLogoScreen()
    {
    	if(mHomeScreen != null) mHomeScreen.stopHighLightNewTimer();
    	mHomeScreen = new HomeScreen();
    	
    	UiEngineInstance engine = Ui.getUiEngineInstance();
    	TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
    	transition.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
    	transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
    	transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
    	
    	engine.setTransition(mPartnerLogoScreen, mHomeScreen, UiEngineInstance.TRIGGER_PUSH, transition);
    	
    	popScreen(mPartnerLogoScreen);
    	pushScreen(mHomeScreen);

    }
    /**
	 * @function goto Home Page from anywhere.  Here init beforePage and set current and beforePage == home! 
	 */
	public void gotoHomeScreen() {
		DataManager.getSharedDataManager().clearDispatchQueue();
    	
    	while(getScreenCount() > 1)
    	{
    		popScreen(getActiveScreen());
    	}
	}
	/**
	 * @function HomeScreen->New Home Screen 
	 * @description Replace Home screen with new home screen object.
	 */
	public void replaceHomeScreen(MainScreen screen)
	{
		DataManager.getSharedDataManager().clearDispatchQueue();
		gotoHomeScreen();
		popScreen(getActiveScreen());
		pushScreen(screen);

    	if(mHomeScreen != null) mHomeScreen.stopHighLightNewTimer();
		mHomeScreen = (HomeScreen)screen;
	}
	
	/**
	 * @function HomeScreen -> VideoMainScreen.
	 */
	public void gotoVideoMainScreen(){
		VideoMainScreen screen = new VideoMainScreen();
    	gotoAnyScreen(screen);
    	
	}
	/**
	 * @function VideoMainScreen -> VideoListScreen.
	 */
	public void gotoVideoListScreen(String strCategoryKey){
		VideoListScreen screen = new VideoListScreen(strCategoryKey);
		gotoAnyScreen(screen);
    	
	}
	/**
	 * @function Any of Screen -> VideoPlayerScreen.
	 */
	public void gotoVideoPlayerScreen(MainScreen fromScreen, String title, String url){
		VideoPlayerScreen screen = new VideoPlayerScreen(url,title);
    	gotoAnyScreen(screen);
	}
	
	public void gotoVideoDetailScreen(MainScreen fromScreen, Object dataItem){
		VideoDetailScreen screen = new VideoDetailScreen(dataItem);
    	gotoAnyScreen(screen);
	}
	/**
	 * @function HomeScreen -> NewsMainScreen.
	 */
	public void gotoNewsMainScreen(){
		NewsMainScreen screen = new NewsMainScreen();
    	gotoAnyScreen( screen );
	}
	/**
	 * @function NewsMainScreen -> NewsListScreen.
	 */
	public void gotoNewsListScreen(String strCategoryKey , String categoryName){
		NewsListScreen screen = new NewsListScreen(strCategoryKey , categoryName);
    	gotoAnyScreen( screen );
    	
	}
	/**
	 * @function Any of Screen -> NewsDetailScreen.
	 */
	public void gotoNewsDetailScreen(MainScreen fromScreen, Object dataItem ){
		NewsDetailScreen screen = new NewsDetailScreen(dataItem);
		gotoAnyScreen( screen );
	}
	
	/**
	 * @function HomeScreen -> FixtureMatchScreen.
	 */
	public void gotoFixtureMatchScreen(){
		FixtureMatchScreen screen = new FixtureMatchScreen();
		gotoAnyScreen( screen );
		
	}
	
	/**
	 * @function FixtureMatchScreen -> FixtureAnalysisScreen.
	 */
	public void gotoFixtureAnalysisScreen(MainScreen fromScreen , SASoccerMatchListItem matchItem){
			
		if(AppSession.sharedSession().m_sportEventContent.m_inMatchClipsFlag)
		{
			FixtureAnalysisScreen screen = new FixtureAnalysisScreen(matchItem);
			gotoAnyScreen(screen);
		}
	}
	
	/**
	 * @function HomeScreen -> StandingScreen.
	 */
	public void gotoStandingScreen(){
		StandingScreen screen = new StandingScreen();
		gotoAnyScreen(screen);
		
	}
	
	/**
	 * @function HomeScreen -> MoreScreen.
	 */
	public void gotoMoreScreen(){
		MoreScreen screen = new MoreScreen();
		gotoAnyScreen(screen);
		
	}
	
	/**
	 * @function ->PushConfigScreen
	 */
	public void gotoPushConfigScreen()
	{
		PushConfigScreen screen = new PushConfigScreen();
		gotoAnyScreen(screen);
	}
	
	/**
	 * @function MoreScreen -> TVChannelScreen.
	 */
	public void gotoTVChannelScreen(){
		TVChannelListScreen screen = new TVChannelListScreen();
		gotoAnyScreen(screen);
	}
	
	/**
	 * @function TVChaneelScreen -> TVChannelDetailScreen.
	 */
	public void gotoTVChannelDetailScreen(String title){
		TVChannelDetailScreen screen = new TVChannelDetailScreen(title);
		gotoAnyScreen(screen);
	}
	
	/**
	 * @function gotoOtherAppScreen from Menu!
	 * @param : None
	 */
	
	public void gotoOtherAppScreen()
	{
		OtherAppScreen screen = new OtherAppScreen();
		gotoAnyScreen(screen);		
	}
	
	/**
	 * @function gotoInfoScreen
	 * @param: String title - title of information screen.
	 */
	
	public void gotoInfoScreen(String title)
	{
		AppSession.sharedSession().m_showAdsBar = false;
		InfoScreen infoScreen = new InfoScreen(title);
		gotoAnyScreen(infoScreen);
	}
	
	/**
	 * @function goto Any Page with fromScreen And toScreen. Here set beforePages[currentPageNumber-1] & currentPageNumber++; 
	 */
	public void gotoAnyScreen(MainScreen toScreen){
		
		DataManager.getSharedDataManager().clearDispatchQueue();
		ImageMemory.getSharedImageBuffer().clearMemory();
		
//		ImageStorage.getSharedImageBuffer().clearStorage();
		
		UiEngineInstance engine = Ui.getUiEngineInstance();
    	TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
    	transition.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
    	transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
    	transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
    	
    	engine.setTransition(null, toScreen, UiEngineInstance.TRIGGER_PUSH, transition);
    	pushScreen(toScreen);
  	}
	
	
	/**
	 * @function goto Before Page with beforePages[beforePageNumber] & currentPage == BeforePage ,  currentPageNumber--;
	 */
	public void gotoBackScreen(){
		
		DataManager.getSharedDataManager().clearDispatchQueue();
//		ImageMemory.getSharedImageBuffer().clearMemory();
		
		UiEngineInstance engine = Ui.getUiEngineInstance();
    	TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
    	transition.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
    	transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
    	transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
    	
    	MainScreen curScreen = (MainScreen) getActiveScreen();
    	engine.setTransition(curScreen, null, UiEngineInstance.TRIGGER_POP, transition);
		popScreen(curScreen);

	}

	// -------------------------Push Notification Setting----------------------------------
	// -------------------------- BPas Protocol -----------------------------------------
	 
	/**
	 * @function Register to push service
	 */
	public void register(boolean isEnterprise) throws Exception {
		int port = PushConfig.getPort();
        String appId = PushConfig.getAppId();
        String bpsUrl = PushConfig.getBpsUrl();
        
        ApplicationDescriptor ad = ApplicationDescriptor.currentApplicationDescriptor();
        
        // server type depends whether we get pushes through BES or BIS
        byte serverType = isEnterprise ? PushApplicationDescriptor.SERVER_TYPE_NONE : PushApplicationDescriptor.SERVER_TYPE_BPAS;
        // if enterprise network then there is no server URL
        bpsUrl = isEnterprise ? null : bpsUrl;

        PushApplicationDescriptor pad = new PushApplicationDescriptor( appId, port, bpsUrl, serverType, ad );

        // check whether already registered or registration pending
        PushApplicationStatus pushApplicationStatus = PushApplicationRegistry.getStatus( pad );
        byte pasStatus = pushApplicationStatus.getStatus();
        if( pasStatus == PushApplicationStatus.STATUS_ACTIVE ) {
            // we already registered, update the statuses
        	System.out.println( "Already registered with Push API" );
        	startListening();
            return;
            
        } else if( pasStatus == PushApplicationStatus.STATUS_PENDING ) {
            // we already scheduled registration, wait for its result
        	System.out.println( "Registration with Push API already scheduled");
            
        } else {
            // not registered yet, register
        	System.out.println( "Scheduled registration with Push API");
            PushApplicationRegistry.registerApplication( pad );
        }
	}
	
	/**
	 * @function Unregister to push service
	 */
	
	public void unregister(boolean isEnterprise)
			throws Exception {
		// TODO Auto-generated method stub
		PushApplicationRegistry.unregisterApplication();
		System.out.println( "Un-registred from Push API" );
	}

	/**
	 * @function Get push notification
	 */
	public void onMessage(PushInputStream inputStream, StreamConnection conn) {
		// TODO Auto-generated method stub
		MessageReadingThread reader = thread;
		if( reader != null ) {
			reader.enqueue( inputStream, conn );
		} else {
			// Thread not listening, decline and close the connection
			if( PushConfig.isApplicationAcknoledgementSupported() ) {
				PushUtils.decline( inputStream,
						PushInputStream.DECLINE_REASON_USERPND );
			}
			PushUtils.close( conn, inputStream, null );
		}
	}

	/**
	 * @function Callback on push service registeration. 
	 */
	public void onStatusChange(PushApplicationStatus status) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		byte bpsStatus = status.getStatus();
//		byte regReason = status.getReason();
		String error = status.getError();

		boolean simChanged = false;

		String logStatus = "Unknown " + bpsStatus;

		switch( bpsStatus ) {
		case PushApplicationStatus.STATUS_ACTIVE:
			logStatus = "Active";
			PersistentStorage.setRegistered(true);
			startListening();
			break;
		case PushApplicationStatus.STATUS_FAILED:
			logStatus = "Failed";
			PersistentStorage.setRegistered(false);
			break;
		case PushApplicationStatus.STATUS_NOT_REGISTERED:
			logStatus = "Not Registered";
			switch( status.getReason() ) {
			case PushApplicationStatus.REASON_SIM_CHANGE:
				simChanged = true;
				break;
			case PushApplicationStatus.REASON_API_CALL:
				// should not happen, even if called then we already unregistered
				break;
			case PushApplicationStatus.REASON_REJECTED_BY_SERVER:
			case PushApplicationStatus.REASON_NETWORK_ERROR:
			case PushApplicationStatus.REASON_INVALID_PARAMETERS:
				// registration failed
				Dialog.alert("Push register failed.");
				break;
			}
			break;
		case PushApplicationStatus.STATUS_PENDING:
			logStatus = "Pending";
			break;
		}

		if( error != null ) {
			System.out.println( "Push API status callback: " + logStatus + ", error " + error );
		} else {
			System.out.println( "Push API status callback: " + logStatus );
		}

		if( simChanged ) {
			
		}
	}


	public void startListening() {
		// TODO Auto-generated method stub
		// start listening thread
        if( thread == null ) {
            thread = new MessageReadingThread();
            thread.start();
        }
	}


	public void stopListening() {
		// TODO Auto-generated method stub
		if( thread != null ) {
            thread.stopRunning();
            thread = null;
        }
	}


	public boolean applicationCanQuit() {
		// TODO Auto-generated method stub
		// 5.0+ API will start our application when something interesting happens
        // this means that the listener doesn't have to be constantly running in the background
        return true;
	}
	public void displayMessage(PushMessage message)
	{
		Dialog.alert(new String(message.getData()));
	}
}

class MessageReadingThread extends Thread {

    private Stack queue;
    private boolean running;

    public MessageReadingThread() {
        this.queue = new Stack();
        this.running = true;
    }

    public void enqueue( PushInputStream inputStream, StreamConnection conn ) {
        synchronized( queue ) {
            queue.insertElementAt( inputStream, 0 );
            queue.insertElementAt( conn, 0 );
            queue.notifyAll();
        }
    }

    public void run() {
    	System.out.println( "Listening for push messages through PushAPI" );
        try {
            PushInputStream inputStream;
            StreamConnection conn;
            while( running ) {
                inputStream = null;
                conn = null;
                synchronized( queue ) {
                    if( queue.isEmpty() ) {
                        queue.wait();
                    } else {
                        inputStream = (PushInputStream) queue.pop();
                        conn = (StreamConnection) queue.pop();
                    }
                }
                if( inputStream != null ) {
                    PushMessageReader.process( inputStream, conn );
                }
            }
        } catch( InterruptedException e ) {
        }
        System.out.println( "Stopped listening for push messages" );
    }

    public void stopRunning() {
        running = false;
        synchronized( queue ) {
            queue.notifyAll();
        }
    }

}