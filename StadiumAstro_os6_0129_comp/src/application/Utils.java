package application;

import java.util.Calendar;
import java.util.TimeZone;

import net.rim.device.api.browser.field.RenderingOptions;
import net.rim.device.api.browser.field.RenderingSession;
import net.rim.device.api.browser.field2.BrowserField;
import net.rim.device.api.math.Fixed32;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.EncodedImage;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.Graphics;


/**
 * support constants and methods for this application.
 */



public class Utils {

	/*
	 * Constants
	 */

	/** 
	 * The width of display for original resources, default value is 640px.
	 */
	private static float ORIGINAL_DISPLAY_WIDTH = 640;

	/** 
	 * The height of display for original resources, default value is 480px.
	 */ 
	private static float ORIGINAL_DISPLAY_HEIGHT = 480;

	/** 
	 * The ratio between current-device display width and original display width for resources
	 */
	public static float H_RATIO;

	/** 
	 * The ratio between current-device display height and original display width for resources
	 */
	public static float V_RATIO;	


	/**
	 * The duration of viewing splash logo-image, default value is 2 seconds.
	 */
	public final static long LOGO_SCREEN_DURATION = 2000;

	/**
	 * The time that appear logo image after, default value is 3 seconds.
	 */
	public final static long SKIPADS_APPEAR_AFTER_TIME = 3000;	

	public static final String [] arrMonthName = {"Jan" , "Feb" , "Mar" , "Apr" , "May" , "Jun" , "Jul" , "Aug", "Sep", "Oct", "Nov", "Dec"};


	/*
	 * Methods
	 */
	/**
	 * Initialize utility for this application 
	 */	
	public static void init()
	{
		H_RATIO = Utils.getDisplayWidth() / ORIGINAL_DISPLAY_WIDTH;
		V_RATIO = Utils.getDisplayHeight() / ORIGINAL_DISPLAY_HEIGHT;

	}


	/**
	 * Creates a bitmap from provided name resource with H_RATIO and V_RATIO
	 * 
	 * @param filename - Name of the bitmap resource.  
	 * @return New Bitmap object scaled by H_RATIO and V_RATIO , or null if this method couldn't find your named resource. 
	 * @throws NullPointerException - If the name parameter is null.
	 */
	public static Bitmap getBitmapResource(String filename) {

		if (filename == null) {
			throw new NullPointerException();
		}

		System.out.println("pppppppppppppppppppp:" + filename);
		Bitmap src = Bitmap.getBitmapResource(filename);
		int width = (int)(H_RATIO * src.getWidth());
    	int height = (int)(V_RATIO * src.getHeight());
    	return getScaledBitmap(src, width, height);
	}
	public static Bitmap getBitmapResource1(String filename) {

		if (filename == null) {
			throw new NullPointerException();
		}

		System.out.println("pppppppppppppppppppp:" + filename);
		Bitmap src = Bitmap.getBitmapResource(filename);
		int width = (int)(H_RATIO * 640);
    	int height = (int)(V_RATIO * 480);
    	return getScaledBitmap(src, width, height);
	}
	
	public static Bitmap getBitmapURL(String url)
	{
		Bitmap bmp;
		System.out.println("image url:" + url);
		try{
			if(!Utils.isNetworkUrl(url))
			{
				if((bmp = ImageMemory.getSharedImageBuffer().getImage(url)) != null)
					return bmp;
				
				bmp = Utils.getBitmapResource(url);
				ImageMemory.getSharedImageBuffer().cacheImage(url, bmp);
				
				return bmp;
			}
			else
			{	
				bmp = ImageStorage.getSharedImageBuffer().getImage(url);
				System.out.println(bmp == null);
				return bmp;
			}
		}
		catch(Exception ex)
		{
			return null;
		}
	}

	/**
	 * Return a bitmap from provided bitmap with Ratio.
	 * 
	 * @param Bitmap - source Bitmap  
	 * @return New Bitmap object scaled by RATIO. 
	 * @throws NullPointerException - If the name parameter is null.
	 */
	public static Bitmap getScaledBitmap(Bitmap src, double ratio) {

		if (src == null || ratio <= 0) {
			throw new NullPointerException();
		}
		int width = (int)(ratio * src.getWidth());
		int height = (int)(ratio * src.getHeight());
		int length = width * height;

		int[] argbData = new int[length];
		for(int i = 0; i < length; i++)
			argbData[i] = 0; // set as transparent    

		Bitmap dst = new Bitmap(width, height);  
		dst.setARGB(argbData, 0, width, 0, 0, width, height);
		argbData = null;
        
		src.scaleInto(dst, Bitmap.FILTER_BILINEAR, Bitmap.SCALE_TO_FILL);    	
		src = null;
		return dst;
	}
	
	public static Bitmap getScaledBitmap(Bitmap src, int width, int height) {
		if (src == null || width <= 0 || height <= 0) {
			return null;
		}
		int length = width * height;

		int[] argbData = new int[length];
		for(int i = 0; i < length; i++)
			argbData[i] = 0; // set as transparent    

		Bitmap dst = new Bitmap(width, height);  
		dst.setARGB(argbData, 0, width, 0, 0, width, height);
		argbData = null;

		src.scaleInto(dst, Bitmap.FILTER_BILINEAR, Bitmap.SCALE_STRETCH);    	
		src = null;
		return dst;
	}
	
	public static EncodedImage scaleEncodedImage(EncodedImage original, int limitDim)
	{
		int imgWidth = original.getWidth();
		int imgHeight = original.getHeight();
		float ratio = 1;
		
		int maxDim = Math.max(original.getWidth(), original.getHeight());
		
		if(maxDim > limitDim)
		{
			ratio = (float)maxDim / limitDim;
		}
		else
			return original;
		
		int fixedRatio = (int)(ratio * 10000);
		
		fixedRatio = Fixed32.tenThouToFP(fixedRatio);
		
		EncodedImage shrinkedImg= original.scaleImage32(fixedRatio, fixedRatio);
		return shrinkedImg;
	}
	
	/**
	 * 
	 * @return Current Device display width & height.
	 */
	
	public static int getDisplayWidth(){
		return Display.getWidth();
	}
	public static int getDisplayHeight(){
		return Display.getHeight();
	}

	/**
	 * 
	 * @return Current Device Display Size : Default Size (640 X 480)
	 */
	public static float getHRatio(){
		return Utils.H_RATIO;
	}
	public static float getVRatio(){
		return Utils.V_RATIO;
	}

	/**
	 * 
	 * @return fontFamily which program used.
	 * 		You can change font here....
	 */
	public static FontFamily getTypeFace(){
		FontFamily typeface;
		try {
			typeface = FontFamily.forName(FontFamily.FAMILY_SYSTEM);
			return typeface;

		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	/**
	 * return MarginX/Y with Ratio H/V 
	 */
	public static int toDPIx(int x){
		return (int)(x * Utils.getHRatio());
	}
	public static int toDPIy(int y){
		return (int)(y * Utils.getVRatio());
	}

	public static String getWeekDayString(int weekday)
	{
		String[] weekdayStr = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
		//return weekdayStr[weekday - 1];

		return weekdayStr[(weekday + 6) % 7];
	}
	/**
	 * @param 
	 * 		month: index of month(start from 0)
	 * @return 
	 * 		short-Month string  
	 */
	public static String getMonthString(int month){
		String result = "";
		try{
			result = arrMonthName[month];
		}catch(Exception e){
			result = "";
		}

		return result;
	}	

	/**
	 * 
	 * @param 
	 * 		strMonth :  short month string
	 * @return
	 * 		suitable month value
	 */
	public static int getMonthFromString(String strMonth)
	{
		for(int i = 0; i < 12; i++)
		{
			if(strMonth.equals(arrMonthName[i]))
				return i;
		}
		System.out.println("Invalid Month String.");
		return -1;
	}

	/**
	 * 
	 * @param strDate : date string format as 'yyyy-mm-dd hh:mm:ss +xxxx'
	 * @return
	 * 		calendar object that is suitable for date string
	 */
	public static Calendar getDateFromString(String strDate)
	{
		if(strDate == null || strDate.equals(""))
			return null;

		try{
			int nYear = Integer.parseInt(strDate.substring(0, 4));
			int nMonth = Integer.parseInt(strDate.substring(5, 7));
			int nDay = Integer.parseInt(strDate.substring(8, 10));
			int nHour = Integer.parseInt(strDate.substring(11, 13));
			int nMinute = Integer.parseInt(strDate.substring(14, 16));
			int nSecond = Integer.parseInt(strDate.substring(17, 19));

			String nTimeZoneSignal = strDate.substring(20, 21);

			int nTimeZoneHour = Integer.parseInt(strDate.substring(21, 23));
			int nTimeZoneMinute = Integer.parseInt(strDate.substring(23, 25));

			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, nYear);
			cal.set(Calendar.MONTH, nMonth - 1);
			cal.set(Calendar.DAY_OF_MONTH, nDay);
			cal.set(Calendar.HOUR_OF_DAY, nHour);
			cal.set(Calendar.MINUTE, nMinute);
			cal.set(Calendar.SECOND, nSecond);

			cal.setTimeZone(TimeZone.getTimeZone("GMT" + nTimeZoneSignal + nTimeZoneHour + ":" + nTimeZoneMinute));

			return cal;
		}
		catch(Exception e)
		{
			System.out.println("Invalid Date String.");
			return null;
		}
	}
	
	public static String getStringFromDate(Calendar date)
	{
		if(date == null)
			return null;

		try{
			String sYear = String.valueOf(date.get(Calendar.YEAR));
			String sMonth = Utils.getMonthString(date.get(Calendar.MONTH));
			String sDay = String.valueOf(date.get(Calendar.DATE));
			if(sDay.length() == 1){
				sDay = "0" + sDay;
			}
			String sDayOfWeek = Utils.getWeekDayString(date.get(Calendar.DAY_OF_WEEK));
			
			String sHour = String.valueOf(date.get(Calendar.HOUR));
			
			String sMinute = String.valueOf(date.get(Calendar.MINUTE));
			if (sMinute.length() == 1){
				sMinute = "0" + sMinute;
			}
			
//			String sSecond = String.valueOf(date.get(Calendar.SECOND));
			String sAM_PM;
			if(date.get(Calendar.AM_PM)==Calendar.AM)
				sAM_PM = "AM";
			else
				sAM_PM = "PM";
			String dateStr = sDayOfWeek + ", " + sDay + " " + sMonth + " " + sYear + " " + sHour + ":" + sMinute + " " + sAM_PM;
			return dateStr;
		}
		catch(Exception e)
		{
			System.out.println("Invalid Date String.");
			return null;
		}
	}
	public static String getStringFromDate1(Calendar date)
	{
		if(date == null)
			return null;

		try{
			String sYear = String.valueOf(date.get(Calendar.YEAR));
			String sMonth = Utils.getMonthString(date.get(Calendar.MONTH));
			String sDay = String.valueOf(date.get(Calendar.DATE));
			if(sDay.length() == 1){
				sDay = "0" + sDay;
			}
			String sDayOfWeek = Utils.getWeekDayString(date.get(Calendar.DAY_OF_WEEK));
			
			String sHour = String.valueOf(date.get(Calendar.HOUR));
			
			String sMinute = String.valueOf(date.get(Calendar.MINUTE));
			if (sMinute.length() == 1){
				sMinute = "0" + sMinute;
			}
			
//			String sSecond = String.valueOf(date.get(Calendar.SECOND));
			String sAM_PM;
			if(date.get(Calendar.AM_PM)==Calendar.AM)
				sAM_PM = "AM";
			else
				sAM_PM = "PM";
			String dateStr = sHour + ":" + sMinute + " " + sAM_PM;
			return dateStr;
		}
		catch(Exception e)
		{
			System.out.println("Invalid Date String.");
			return "Full Time";
		}
	}
	
	/**
	 * @param src: source string
	 * @param width: display area's width
	 * @param font: display font
	 * @return
	 * 		if need to be omitted, then return omitted string which format as 'xxx...' 
	 */
	public static String getOmittedString(String src, int width, Font font)
	{
		String strResult;

		int nLen = src.length();		
		if(font.getBounds(src) < width)
		{
			strResult = src;			
		}
		else{
			// assume process
			do
			{
				nLen--;
				strResult = src.substring(0, nLen);
			}while(font.getBounds(strResult) >= width);

			nLen -= 2;
			strResult = src.substring(0, nLen).concat("...");
		}

		return strResult;
	}

	/**	  
	 * @param 
	 * 		boolStr : "Y" or "N" string as JSON response
	 * @return
	 * 		proper boolean value
	 */
	public static boolean getBooleanFromString(String boolStr)
	{
		if(boolStr.equals("Y"))
			return true;
		else if(boolStr.equals("N"))
			return false;

		System.out.println("undefined boolean string!");
		return false;
	}


	/**
	 * 
	 * @param srcStr - source string
	 * @param strTarget - substring which should be replaced
	 * @param strReplace - replace string
	 * @return
	 */
	public static String replaceString(String srcStr , String strTarget , String strReplace){
		String result = new String("");

		//search target string's index in srcString
		int nextKeyIndex = srcStr.indexOf(strTarget);  
		int beforeKeyIndex = 0;

		while(nextKeyIndex != -1){
			String addString = srcStr.substring(beforeKeyIndex, nextKeyIndex) + strReplace;
			result += addString;			
			beforeKeyIndex = nextKeyIndex + strTarget.length();

			nextKeyIndex = srcStr.indexOf(strTarget, beforeKeyIndex);
		}
		//last sub string.
		int len = srcStr.length();
		result += srcStr.substring(beforeKeyIndex , len);
		if(result.equals(""))
			result = srcStr;
		return result;
	}

	public static boolean isNetworkUrl(String url)
	{
		try{
			if(url.startsWith("http:") || url.startsWith("https:"))
				return true;
			else
				return false;
		}
	    catch(Exception e){
			return false;
		}
	}
	
	public static String getPINCode()
	{
		return Integer.toHexString(DeviceInfo.getDeviceId());
	}
	
	public static String getOmittedString(String src, int width, int height, Font font)
	{		
		// define token split character
		char chSpace = ' ';
		
		// define line width
		int lineWidth = width;
		
		int curLineTextWidth = 0;
		int beforeIndex = 0;
		int nextIndex = src.indexOf(chSpace);
		int line_number = 0;
		int limit_line_count = (int)(height / font.getHeight());
		
		while(nextIndex != -1){
			// get next token string
			String strNextToken = src.substring(beforeIndex, nextIndex + 1);
			int tokenLen = font.getBounds(strNextToken);
			if(curLineTextWidth +  tokenLen > lineWidth)
			{
				// if next word ranges out of current line's width								
				line_number++;
				if(line_number == limit_line_count)
				{
					String result = src.substring(0, beforeIndex) + "...";
					return result;
}
				curLineTextWidth = tokenLen;
				beforeIndex = nextIndex + 1;
			}
			else
			{
				// continue search word
				beforeIndex = nextIndex + 1;
				curLineTextWidth += tokenLen;
			}
			
			// search next token
			nextIndex = src.indexOf(chSpace, beforeIndex);
		}
		
		// if reached at string's end
		if(beforeIndex < src.length())
		{
			// get last token string
			String strNextToken = src.substring(beforeIndex);
			int tokenLen = font.getBounds(strNextToken);
			if(curLineTextWidth +  tokenLen > lineWidth)
			{	
				line_number++;
				if(line_number == limit_line_count)
				{
					String result = src.substring(0, beforeIndex) + "...";
					return result;
				}
			}		
		}
		
		return src;
	}
	
	//Deprecated : Browser Field has his minimum font size, so don't use this function
	//Passion: Please check NewsDetailScreen.java
	
	public static String setHTMLContentSize(String htmlContentString, int fontSize, String bgColor, String foreColor){
		if(htmlContentString == null)
			return null;
		
		//Passion
		int nFontSize = (int)(Utils.getVRatio() * fontSize);
		
		htmlContentString =  "<html>"
				+ "<body bgcolor='" + bgColor 
				+ "'>"
				+ "<font color='" + foreColor
				+ "' >"
//				+ "<font size=" 
//				+ nFontSize + "> " 
				+ htmlContentString
				+ "</font>"
				+ "</body>"
				+ "</html>";
		return htmlContentString;
	}

	//Passion for processing font size of Browser field
	public static void setFontSizeForBrowserField(BrowserField browser, int nFontSize)
	{
		//Passion for Browser field's minimum font size process
       	browser.getRenderingOptions().setProperty(RenderingOptions.CORE_OPTIONS_GUID, RenderingOptions.MINIMUM_FONT_SIZE, nFontSize);
       	browser.getRenderingOptions().setProperty(RenderingOptions.CORE_OPTIONS_GUID, RenderingOptions.MINIMUM_FONT_SIZE_DEFAULT, nFontSize);
       	RenderingSession renderingSession = RenderingSession.getNewInstance();
       	renderingSession.getRenderingOptions().setProperty(RenderingOptions.CORE_OPTIONS_GUID, RenderingOptions.MINIMUM_FONT_SIZE, nFontSize);
       	renderingSession.getRenderingOptions().setProperty(RenderingOptions.CORE_OPTIONS_GUID, RenderingOptions.MINIMUM_FONT_SIZE_DEFAULT, nFontSize);
       	
       	return;
	}
}

