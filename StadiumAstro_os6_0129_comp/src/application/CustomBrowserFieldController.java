package application;

import javax.microedition.io.InputConnection;

import net.rim.blackberry.api.browser.Browser;
import net.rim.blackberry.api.browser.BrowserSession;
import net.rim.device.api.browser.field2.BrowserField;
import net.rim.device.api.browser.field2.BrowserFieldController;
import net.rim.device.api.browser.field2.BrowserFieldRequest;
import net.rim.device.api.browser.field2.ProtocolController;

public class CustomBrowserFieldController extends ProtocolController implements BrowserFieldController{
	BrowserField browserField;
	public CustomBrowserFieldController(BrowserField browserField) {

		super(browserField);

		this.browserField = browserField;

	}
	public void handleNavigationRequest(BrowserFieldRequest request)
	{
		try {
//			BrowserSession b = Browser.getDefaultSession();
			String requestUrl  = request.getURL();
			if(AppSession.useSimulator)
				requestUrl = requestUrl + ";deviceside=true";
			else
				requestUrl = requestUrl + ";interface=wifi";
			request.setURL(requestUrl);

			super.handleNavigationRequest(request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public InputConnection handleResourceRequest(BrowserFieldRequest request)
	{
		String requestUrl  = request.getURL();
		if(AppSession.useSimulator)
			requestUrl = requestUrl + ";deviceside=true";
		else
			requestUrl = requestUrl + ";interface=wifi";
		request.setURL(requestUrl);
		try {
			return super.handleResourceRequest(request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
