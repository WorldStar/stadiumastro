package application;
import java.util.Hashtable;
import net.rim.device.api.system.Bitmap;



public class ImageMemory {
	public Hashtable m_imageFileBuffer;
	static ImageMemory sharedBuffer = null;
	int imageCount;

	//True: Save into storage, False save into memory.
	public static ImageMemory getSharedImageBuffer()
	{
		if(sharedBuffer == null)
			sharedBuffer = new ImageMemory();
		
		return sharedBuffer;
			
	}
	public ImageMemory()
	{
		m_imageFileBuffer = new Hashtable();
		imageCount = 0;
	}
	
	public void cacheImage(String imageName, Bitmap bmp)
	{
		if(bmp == null)
			return;
		if(imageCount > 15)
		{
			imageCount = 0;
			m_imageFileBuffer.clear();
		}
		
		m_imageFileBuffer.put(imageName, bmp);
		imageCount ++;
	}
	
	public Bitmap getImage(String imageName)
	{
		if(m_imageFileBuffer.containsKey(imageName))
			return (Bitmap)m_imageFileBuffer.get(imageName);
		return null;
	}
	
	public void clearMemory()
	{
		m_imageFileBuffer.clear();
		imageCount = 0;
	}
	
}
