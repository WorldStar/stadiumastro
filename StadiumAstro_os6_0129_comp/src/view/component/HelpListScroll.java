package view.component;


import application.ResourceConstants;
import application.Utils;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;


/**
 * 
 * @author Administrator
 * This is Match Highlight news list Scroll that shows on HomeScreen.
 * Here all of match preview Image is same. You can change this by changing static Constants SA_IMG_HSCROLL and then calc Count of this rather than using SA_VAL_TESTING_PREVIEW_IMG_COUNT.
 */

public class HelpListScroll extends HorizontalFieldManager {

	public int _curPageNumber;		//current white dot index
	public int m_count;				//Dots count which is same with image count.
	
	private int oldTouchPosX;
	public boolean swipeLeft = true;
	
	public HelpListScroll(long style){
		super(style);
		this.setScrollingInertial(false);
		
		this.m_count = ResourceConstants.SA_COUNT_HELP_SCREEN;
		
		//add medium page at middle.
		for (int i = 0; i < this.m_count; i++) {
			VerticalFieldManager eachPageViewManager = new VerticalFieldManager(Field.FIELD_HCENTER);
			
			String strFilePath = ResourceConstants.SA_IMG_HELP_SCREEN_PREFIX;
			strFilePath = strFilePath + String.valueOf(i + 1) + ".png";
			
			Bitmap bmpHelpScreen = Utils.getBitmapResource(strFilePath);
			
			eachPageViewManager.add(new BitmapField(bmpHelpScreen));
			
			this.add(eachPageViewManager);
		}
		
		_curPageNumber = 0;
	}

	public void nextPage()
	{
		if(_curPageNumber == m_count + 1){
			_curPageNumber = 1;
			setHorizontalScroll(_curPageNumber * getWidth(),false);
		}
		_curPageNumber++;
		setHorizontalScroll(_curPageNumber * getWidth(),true);
	}
	
	public void beforePage()
	{
		if(_curPageNumber == 0){
			_curPageNumber = m_count;
			setHorizontalScroll(_curPageNumber * getWidth(),false);
		}
		_curPageNumber--;
		setHorizontalScroll(_curPageNumber * getWidth(),true);
	}
	
	protected void sublayout(int maxWidth, int maxHeight) {
		// TODO Auto-generated method stub
		maxWidth = Utils.getDisplayWidth();
		maxHeight = Utils.getDisplayHeight();
		this.setExtent(maxWidth , maxHeight);
		super.sublayout(maxWidth, maxHeight);
		
		this.setHorizontalScroll(_curPageNumber * this.getWidth(),false);
	}

	/**
     * Event handler
     */
	
    protected boolean navigationClick(int status, int time) {
    	if (status != 0){
    		fieldChangeNotify(0);
    		click();
    	}
        return true;
    }

    public boolean keyChar(char key, int status, int time) {
        if (key == Characters.ENTER) {
            fieldChangeNotify(0);
            click();
            return true;
        }
        return false;
    }
    
    protected boolean trackwheelClick(int status, int time)
    {  
    	
        if (status != 0) click(); 
        return true;
    }
	
    protected boolean invokeAction(int action) 
    {
        switch( action ) {
            case ACTION_INVOKE: {
                click(); 
                return true;
            }
        }
        return super.invokeAction(action);
    }
	
	
	protected boolean touchEvent(TouchEvent message)
	 {
		int x = message.getX(1);
		int y = message.getY(1);
		if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
            return false;
        }
		if(message.getEvent() == TouchEvent.DOWN){
			oldTouchPosX = x;

		}else if(message.getEvent() == TouchEvent.UP){
			clickUp(x,y);
		}
		if(message.getEvent() == TouchEvent.CLICK)
			click();
		return super.touchEvent(message); 
	 }

	public void click(){
	}
	
	public void clickUp(int x,int y){
		fieldChangeNotify(0);
		if(Math.abs(Math.abs(x) - Math.abs(oldTouchPosX)) >= ResourceConstants.SA_VAL_HIGHLIGHT_SCROLL_MAGNITUDE_FORCE){
			if(x <= oldTouchPosX){
				if (_curPageNumber < m_count) {
					_curPageNumber++;
					swipeLeft = true;	
				}
				
			}else{
				if (_curPageNumber > 0) {
					_curPageNumber--;
					swipeLeft = false;	
				}
			}
			scrollToCurPage();
		}else
		{
			scrollToCurPage();
		}
	}
	
	public void scrollToCurPage(){
		this.setHorizontalScroll(_curPageNumber * this.getWidth(), true);
	}

	protected int nextFocus(int dir,int axis){
		int nIndex = _curPageNumber;
		if(axis == Field.AXIS_HORIZONTAL){
			if(nIndex == 0 && dir == -1)
				return m_count-1;
			if(nIndex == m_count - 1  && dir == 1)
				return 0;
			return nIndex + dir;
		}
		else
			return -1;
	}
}

