package view.component;

import application.ImageMemory;
import application.ImageStorage;
import application.ResourceConstants;
import application.StadiumAstroApp;
import application.Utils;
import model.SASoccerMatchListItem;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.*;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

/**
 * 
 * @author Administrator
 *	This class need only for Match Tab Bar in homeScreen.
 *	All contents of this MatchTabBar come from SAMatchTabBar which contains downloaded info from web server.
 */
public class MatchTab extends HorizontalFieldManager {

	private Bitmap mBgBitmap = null; 

	CustomLabelField monthLabel, dateLabel, timeLabel;
	
	CustomBitmapField homeTeamLogoImage, visitingTeamLogoImage;
	CustomLabelField homeTeamAliasLabel, visitingTeamAliasLabel;
	CustomLabelField scoreLabel;
	CustomLabelField channelLabel;
	
	SASoccerMatchListItem m_matchItem = null;
	
	int m_index;

	private Background m_normalBack;
	private Background m_pressedBack;
	
	private boolean m_focused = false;
	
	public MatchTab(SASoccerMatchListItem match_info , int index) {
		super(Manager.FOCUSABLE);
		this.m_index = index;
		this.m_matchItem = match_info;
		init(match_info);
	}
	
	public Bitmap loadImage(String url)
	{
		Bitmap bmp;
		
		try{
			if((bmp = ImageMemory.getSharedImageBuffer().getImage(url)) != null)
				return bmp;
			
			bmp = Utils.getBitmapResource(url);
			ImageMemory.getSharedImageBuffer().cacheImage(url, bmp);
			
			return bmp;
		}
		catch(Exception ex)
		{
			return null;
		}
		
	}
	
	private void init (SASoccerMatchListItem mInfo) {
		//set Background
		if (mBgBitmap == null)
			mBgBitmap = loadImage(ResourceConstants.SA_IMG_MATCHTAB);
		
		setExtent(mBgBitmap.getWidth(), mBgBitmap.getHeight());
		
		m_normalBack = BackgroundFactory.createBitmapBackground(mBgBitmap);
		setBackground(m_normalBack);
		
		m_pressedBack = BackgroundFactory.createBitmapBackground(loadImage(ResourceConstants.SA_IMG_MATCHTAB_PRESSED));
		
		//set Match-Time-Area (month, date, time)
		CustomVerticalFieldManager matchTimeAreaManager = new CustomVerticalFieldManager(ResourceConstants.SA_VAL_MATCHTAB_DATE_WIDTH,mBgBitmap.getHeight(),Manager.FIELD_LEFT);
		{		
			matchTimeAreaManager.setMinimalWidth(ResourceConstants.SA_VAL_MATCHTAB_DATE_WIDTH);

			monthLabel = new CustomLabelField(Utils.getMonthString( mInfo.m_month ) ,Color.WHITE,Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , 0 , Utils.toDPIy(10) , ResourceConstants.SA_VAL_MATCHTAB_DATE_WIDTH , Field.FIELD_HCENTER);
			monthLabel.setTextAlign(DrawStyle.HCENTER);
			dateLabel = new CustomLabelField(mInfo.m_date,Color.WHITE,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_LARGE) , 0 , Utils.toDPIy(5) , ResourceConstants.SA_VAL_MATCHTAB_DATE_WIDTH,  Field.FIELD_HCENTER);
			dateLabel.setTextAlign(DrawStyle.HCENTER);
			timeLabel = new CustomLabelField(mInfo.m_time,Color.WHITE,Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , 0 , Utils.toDPIy(5) , ResourceConstants.SA_VAL_MATCHTAB_DATE_WIDTH,  Field.FIELD_HCENTER);
			timeLabel.setTextAlign(DrawStyle.HCENTER);
			
			matchTimeAreaManager.add(monthLabel);
			matchTimeAreaManager.add(dateLabel);
			matchTimeAreaManager.add(timeLabel);
		}
		add(matchTimeAreaManager);
		
		
		//set Match-Score-Area (logos and alias of 2 teams, score, channel)
		HorizontalFieldManager matchScoreAreaManager = new HorizontalFieldManager();
		{			
			//homeTeamLogo, homeTeamAlias
			CustomVerticalFieldManager homeTeamScroeAreaManager = new CustomVerticalFieldManager(ResourceConstants.SA_VAL_MATCHTAB_TEAM_LOGO_WIDTH, mBgBitmap.getHeight(),Manager.FIELD_LEFT);
			{				
//				Bitmap logo = Utils.getBitmapResource(mInfo.homeTeamLogo);				
				homeTeamLogoImage = new CustomBitmapField(mInfo.m_homeTeamLogo,null,0,Utils.toDPIy(20),Utils.toDPIx(50),Utils.toDPIy(50),Manager.FIELD_HCENTER|Field.FOCUSABLE,null);
				
				homeTeamAliasLabel = new CustomLabelField(mInfo.m_homeTeamAlias,Color.WHITE,Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , 0 ,Utils.toDPIy(10) , Utils.toDPIx(140),  Field.FIELD_HCENTER);
				homeTeamAliasLabel.setTextAlign(DrawStyle.HCENTER);
				
				homeTeamScroeAreaManager.add(homeTeamLogoImage);
				homeTeamScroeAreaManager.add(homeTeamAliasLabel);
			}
			
			matchScoreAreaManager.add(homeTeamScroeAreaManager);
			
			//score, channel
			CustomVerticalFieldManager scroeAreaManager = new CustomVerticalFieldManager(ResourceConstants.SA_VAL_MATCHTAB_SCORE_WIDTH, mBgBitmap.getHeight(),Manager.FIELD_HCENTER);
			
			{	
				if(mInfo.m_matchStatus.compareTo("Played") == 0)
				{
					System.out.println("Match Status = " + mInfo.m_matchStatus);
					String scoreString = new StringBuffer().append(mInfo.m_homeTeamScore).append(" - ").append(mInfo.m_visitingTeamScore).toString();
					String channelString = mInfo.m_channelInfo;
					
					scoreLabel = new CustomLabelField(scoreString,Color.WHITE,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_LARGE) , 0 , Utils.toDPIy(40) , Utils.toDPIx(145), Field.FIELD_HCENTER);
					scoreLabel.setTextAlign(DrawStyle.HCENTER);
					channelLabel = new CustomLabelField(channelString,Color.WHITE,Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_SMALL) , 0 , Utils.toDPIy(20) , Utils.toDPIx(145) ,  Field.FIELD_LEFT);
					channelLabel.setTextAlign(DrawStyle.HCENTER);
					
					scroeAreaManager.add(scoreLabel);
					scroeAreaManager.add(channelLabel);
				}
				else
				{
					System.out.println("Match Status = " + mInfo.m_matchStatus);
					String scoreString = "VS";
					String channelString = mInfo.m_channelInfo;
					
					scoreLabel = new CustomLabelField(scoreString,Color.WHITE,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_LARGE) , 0 , Utils.toDPIy(30) , Utils.toDPIx(145), Manager.FIELD_HCENTER);
					scoreLabel.setTextAlign(DrawStyle.HCENTER);
					
					channelLabel = new CustomLabelField(channelString,Color.WHITE,Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_SMALL) , 0 , Utils.toDPIy(10) , Utils.toDPIx(145) ,  Field.FIELD_LEFT);
					channelLabel.setTextAlign(DrawStyle.HCENTER);
					
					scroeAreaManager.add(scoreLabel);
					scroeAreaManager.add(channelLabel);
				}
	
			}
			
			matchScoreAreaManager.add(scroeAreaManager);
			
			//visitingTeamLogo, visitingTeamAlias
			CustomVerticalFieldManager visitingTeamScoreAreaManager = new CustomVerticalFieldManager(ResourceConstants.SA_VAL_MATCHTAB_TEAM_LOGO_WIDTH,mBgBitmap.getHeight(),Manager.FIELD_RIGHT);
			
			{		
				visitingTeamLogoImage = new CustomBitmapField(mInfo.m_visitingTeamLogo,null, 0, Utils.toDPIy(20),Utils.toDPIx(50),Utils.toDPIy(50),Manager.FIELD_HCENTER,null);
				
				visitingTeamAliasLabel = new CustomLabelField(mInfo.m_visitingTeamAlias,Color.WHITE,Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) ,0 ,Utils.toDPIy(10), Utils.toDPIx(100) , Manager.FIELD_HCENTER);
				visitingTeamAliasLabel.setTextAlign(DrawStyle.HCENTER);	
				
				visitingTeamScoreAreaManager.add(visitingTeamLogoImage);
				visitingTeamScoreAreaManager.add(visitingTeamAliasLabel);
			}
			matchScoreAreaManager.add(visitingTeamScoreAreaManager);
			
		}
		add(matchScoreAreaManager);
	}
	
	protected void sublayout(int width, int height) {
		super.sublayout(width, height);
		setExtent(mBgBitmap.getWidth(), mBgBitmap.getHeight());
	}
	
	/**
     * Event handler
     */
	
    protected boolean navigationClick(int status, int time) {
    	if (status != 0){
    		fieldChangeNotify(0);
    		clickButton();
    	}
        return true;
    }

    public boolean keyChar(char key, int status, int time) {
        if (key == Characters.ENTER) {
            fieldChangeNotify(0);
            clickButton();
            return true;
        }
        return false;
    }
    protected boolean trackwheelClick(int status, int time)
    {        
        if (status != 0) clickButton(); 
        return true;
    }
	protected boolean invokeAction(int action) 
    {
		refreshBackGround(true);
        switch( action ) {
            case ACTION_INVOKE: {
                clickButton(); 
                return true;
            }
        }
        return super.invokeAction(action);
    }
	
	protected boolean touchEvent(TouchEvent message)
	 {
		int x = message.getX( 1 );
        int y = message.getY( 1 );
        if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
            // Outside the field
        	if(m_focused)
        		refreshBackGround(false);
            return false;
        }
        else{
        	
		   	if(message.getEvent() == TouchEvent.DOWN)
		  	{	
		   		 refreshBackGround(true);
			}
		   	else if(message.getEvent() == TouchEvent.UP){
		   		refreshBackGround(false);
		   	}
		   	if(message.getEvent() == TouchEvent.CLICK){
		   		clickButton();
		   	}
		}
	    return super.touchEvent(message); 
   }

	public void refreshBackGround(boolean flag){
		if(flag == true){
			this.setBackground(m_pressedBack);
			m_focused = true;
		}else
		{	
			m_focused = false;
			this.setBackground(m_normalBack);
		}
	}
	
	public void clickButton(){
    	fieldChangeNotify(0);

        StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
        app.gotoFixtureAnalysisScreen(app.mHomeScreen, m_matchItem);
        
        refreshBackGround(false);
    }
	
	protected void onFocus(int direction)
	{
		int scrollX = mBgBitmap.getWidth() * getIndex();
		boolean bAnimation = true;
		
		
		if((direction == -1) && (getIndex() == this.getManager().getFieldCount() - 1))
		{
			// reset last item's scroll position
			scrollX -= Utils.getDisplayWidth() - mBgBitmap.getWidth();
			bAnimation = false;
		}
		else if((direction == 1) && (getIndex() == 0))
		{
			bAnimation = false;
		}
		
		this.getManager().setHorizontalScroll(scrollX, bAnimation);
		
		if(!bAnimation)
			this.getManager().getManager().setVerticalScroll(0, true);
		
		//Passion
		refreshBackGround(true);
	}
	
	protected void onUnfocus()
	{
		if(getManager().getFieldWithFocusIndex() == getIndex())
		{			
			getManager().setHorizontalScroll(0, false);
		}
		
		//Passion
		refreshBackGround(false);
	}
	public boolean isFocusable() {
		// TODO Auto-generated method stub
		return true;
	}
}
