package view.component;

import view.component.CustomBitmapField;
import view.component.CustomLabelField;
import application.ResourceConstants;
import application.Utils;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.Background;


/**
 * 
 * @author Administrator
 *	This class needs only for show cell in Analysis Screens's <Time-LineUp> Pane & <MatchEvents> Pane.
 *	This class doesn't process Event handling... 
 */
public class CustomTableCell extends HorizontalFieldManager {

	//TeamLine-up Cell
	
	/**
	 * 
	 * @param awayPosition - away Team's sports position
	 * @param awayPlayerName - away team's sports player name
	 * @param homePosition - home Team's sports position
	 * @param homePlayerName - home team's sports player name
	 * @param background
	 */
	public CustomTableCell(String awayPosition,String awayPlayerName,String homePosition,String homePlayerName,Background background){
		
		setBackground(background);
		
		int fontSize = ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL;
        int labelWidth = Utils.getTypeFace().getFont(Font.BOLD, fontSize).getBounds(homePlayerName);
        int marginX = ResourceConstants.SA_VAL_TEAMLINE_UP_PLAYERNAME_WITDH - labelWidth - 50;
        int marginY = (ResourceConstants.SA_VAL_ANALYSIS_PANE_CELL_HEIGHT - fontSize ) / 2;
		
		CustomVerticalFieldManager firstField = new CustomVerticalFieldManager(ResourceConstants.SA_VAL_TEAMLINE_UP_POSITION_WITDH,ResourceConstants.SA_VAL_ANALYSIS_PANE_CELL_HEIGHT,Manager.FIELD_LEFT);
		firstField.setBackground(background);
		CustomLabelField familyNameLabelField = new CustomLabelField(awayPosition, Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN,ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), Utils.toDPIx(30), marginY, ResourceConstants.SA_VAL_TEAMLINE_UP_POSITION_WITDH, Manager.FIELD_LEFT);
		firstField.add(familyNameLabelField);
		
		add(firstField);
		
		CustomVerticalFieldManager secondField = new CustomVerticalFieldManager(ResourceConstants.SA_VAL_TEAMLINE_UP_PLAYERNAME_WITDH,ResourceConstants.SA_VAL_ANALYSIS_PANE_CELL_HEIGHT,Manager.FIELD_LEFT);
		secondField.setBackground(background);
		CustomLabelField lastNameLabelField = new CustomLabelField(awayPlayerName, Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN,ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), 50, marginY, ResourceConstants.SA_VAL_TEAMLINE_UP_PLAYERNAME_WITDH, Manager.FIELD_LEFT);
		secondField.add(lastNameLabelField);
		
		add(secondField);
		
		
		
		CustomVerticalFieldManager thirdField = new CustomVerticalFieldManager(ResourceConstants.SA_VAL_TEAMLINE_UP_PLAYERNAME_WITDH,ResourceConstants.SA_VAL_ANALYSIS_PANE_CELL_HEIGHT,Manager.FIELD_LEFT);
		thirdField.setBackground(background);
		CustomLabelField lastNameLabelField2 = new CustomLabelField(homePlayerName, Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN,ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), marginX, marginY, ResourceConstants.SA_VAL_TEAMLINE_UP_PLAYERNAME_WITDH, Manager.FIELD_RIGHT);
		thirdField.add(lastNameLabelField2);
		
		add(thirdField);
		
		CustomVerticalFieldManager fouthField = new CustomVerticalFieldManager(ResourceConstants.SA_VAL_TEAMLINE_UP_POSITION_WITDH,ResourceConstants.SA_VAL_ANALYSIS_PANE_CELL_HEIGHT,Manager.FIELD_LEFT);
		fouthField.setBackground(background);
		CustomLabelField familyNameField2 = new CustomLabelField(homePosition, Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN,ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), Utils.toDPIx(10), marginY, ResourceConstants.SA_VAL_TEAMLINE_UP_POSITION_WITDH, Manager.FIELD_RIGHT);
		fouthField.add(familyNameField2);
		
		add(fouthField);
		
	}//Match Events
	
	
	//left and right cell
	/**
	 * 
	 * @param name - sport man's name
	 * @param lconUrl - icon url
	 * @param background
	 * @param flag - left / right
	 */
	public CustomTableCell(String name,String iconUrl,Background background,boolean flag){
		setBackground(background);

		
		int fontSize = ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL;
        int labelWidth = Utils.getTypeFace().getFont(Font.BOLD, fontSize).getBounds(name);
        int marginX = (ResourceConstants.SA_VAL_MATCH_EVENTS_NAME_WITDH - labelWidth) + 5;
        int marginY = (ResourceConstants.SA_VAL_ANALYSIS_PANE_CELL_HEIGHT - fontSize ) / 2;
        
		if(!flag)
		{
			CustomVerticalFieldManager nameField = new CustomVerticalFieldManager(ResourceConstants.SA_VAL_MATCH_EVENTS_NAME_WITDH,ResourceConstants.SA_VAL_ANALYSIS_PANE_CELL_HEIGHT,Manager.FIELD_RIGHT);
			nameField.setBackground(background);
			
			
			CustomLabelField nameLabel = new CustomLabelField(name, Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN,ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), marginX, marginY, ResourceConstants.SA_VAL_MATCH_EVENTS_NAME_WITDH, Manager.FIELD_LEFT);
			nameField.add(nameLabel);
			
				
			CustomVerticalFieldManager iconField = new CustomVerticalFieldManager(ResourceConstants.SA_VAL_MATCH_EVENTS_ICON_WITDH,ResourceConstants.SA_VAL_ANALYSIS_PANE_CELL_HEIGHT,Manager.FIELD_RIGHT);
			iconField.setBackground(background);
			
			if(iconUrl != null){
				CustomBitmapField icon = new CustomBitmapField(iconUrl, null, Utils.toDPIx(10), Utils.toDPIy(10) , Utils.toDPIx(25), Utils.toDPIy(25), Manager.FIELD_HCENTER|Manager.FIELD_VCENTER, null);
				iconField.add(icon);
			}
			add(nameField);
			add(iconField);
		}
		else
		{
			CustomVerticalFieldManager nameField = new CustomVerticalFieldManager(ResourceConstants.SA_VAL_MATCH_EVENTS_NAME_WITDH,ResourceConstants.SA_VAL_ANALYSIS_PANE_CELL_HEIGHT,Manager.FIELD_RIGHT);
			nameField.setBackground(background);
			CustomLabelField nameLabel = new CustomLabelField(name, Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN,ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), 0, marginY, ResourceConstants.SA_VAL_MATCH_EVENTS_NAME_WITDH, Manager.FIELD_LEFT);
			nameField.add(nameLabel);
			
				
			CustomVerticalFieldManager iconField = new CustomVerticalFieldManager(ResourceConstants.SA_VAL_MATCH_EVENTS_ICON_WITDH,ResourceConstants.SA_VAL_ANALYSIS_PANE_CELL_HEIGHT,Manager.FIELD_RIGHT);
			iconField.setBackground(background);
			
			if(iconUrl != null){
				CustomBitmapField icon = new CustomBitmapField(iconUrl,null, Utils.toDPIx(10), Utils.toDPIy(10), Utils.toDPIx(25), Utils.toDPIy(25), Manager.FIELD_HCENTER|Manager.FIELD_VCENTER, null);
				iconField.add(icon);
			}
			add(iconField);
			add(nameField);
		}
		
	}
	
	//middle cell
	/**
	 * 
	 * @param time - display time label text
	 * @param background
	 */
	public CustomTableCell(String time,Background background){
				
		setBackground(background);
		
		CustomVerticalFieldManager mainField = new CustomVerticalFieldManager(ResourceConstants.SA_VAL_MATCH_EVENTS_MIDDLE_CELL_WITDH,ResourceConstants.SA_VAL_ANALYSIS_PANE_CELL_HEIGHT,Manager.FIELD_HCENTER);
		mainField.setBackground(background);
		
		int fontSize = ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL;
        int labelWidth = Utils.getTypeFace().getFont(Font.BOLD, fontSize).getBounds(time);
        int marginX = (ResourceConstants.SA_VAL_MATCH_EVENTS_MIDDLE_CELL_WITDH - labelWidth) / 2 + 5;
        int marginY = (ResourceConstants.SA_VAL_ANALYSIS_PANE_CELL_HEIGHT - fontSize ) / 2;
        
		CustomLabelField timeLabelField = new CustomLabelField(time, Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN,ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), marginX, marginY, ResourceConstants.SA_VAL_MATCH_EVENTS_MIDDLE_CELL_WITDH, Manager.FIELD_RIGHT);
		mainField.add(timeLabelField);
		
		add(mainField);
	}

}
