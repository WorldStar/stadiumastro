package view.component;

import application.Utils;
import net.rim.device.api.ui.container.HorizontalFieldManager;

/**
 * 
 * @author Administrator
 *	This is only for reduce amount of Inner class.
 */
public class CustomHorizontalFieldManager extends HorizontalFieldManager {
	int m_maxWidth;
	int m_maxHeight;
	
	public CustomHorizontalFieldManager(){
		m_maxWidth = Utils.getDisplayWidth();
		m_maxHeight = Utils.getDisplayHeight();
	}
	public CustomHorizontalFieldManager(int maxWidth,int maxHeight,long style){
		super(style);
		m_maxWidth = maxWidth;
		m_maxHeight = maxHeight;
	}
	protected void sublayout(int maxWidth, int maxHeight) {
		maxWidth =  m_maxWidth;
		maxHeight = m_maxHeight;
		super.sublayout(maxWidth, maxHeight);
		this.setExtent(maxWidth , maxHeight);
	}

}
