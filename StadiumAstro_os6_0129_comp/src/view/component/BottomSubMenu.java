package view.component;

import view.component.ImageButtonField;
import application.AppSession;
import application.ResourceConstants;
import application.Utils;
import model.SASportEventContentItem;
import net.rim.device.api.ui.*;
import net.rim.device.api.ui.container.*;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

/**
 * 
 * @author Administrator
 *	This class is bottom sub menu field which consists of open/closed button and _subMenuButtons( 1. Video / News / Fixture / Standing / More  ,  2. Back / Home).
 *	HorizontalField of SubMenuButton will add/delete to/from MainManager of BaseScreen when click open/close.
 */

public class BottomSubMenu extends VerticalFieldManager{
	
	 private ImageButtonField _openButton;
	 private ImageButtonField _closeButton;
	 public HorizontalFieldManager _subMenuButtonManager;
	
	    /**
	     * @param homeFlag - is Home Screen or not
	     * @param style - field style
	     */
	    public BottomSubMenu(boolean homeFlag , long style) {
	    	
	    	super(style);
	    	
	    	AppSession _sharedSession = AppSession.sharedSession();
	    	
	        _openButton = new ImageButtonField("MENU_OPEN" , ResourceConstants.SA_IMG_SUB_MENU_BACK , 0 , Color.BLACK, Field.FIELD_LEFT | Field.FOCUSABLE);
	        _openButton.setOffPicture(Utils.getBitmapResource(ResourceConstants.SA_IMG_SUB_MENU_BACK_PRESSED));
	        this.add(_openButton);
	        
	       _closeButton = new ImageButtonField("MENU_CLOSE", ResourceConstants.SA_IMG_SUB_MENU_BACK_CLOSE , 0 , Color.BLACK, Field.FIELD_LEFT | Field.FOCUSABLE);
	       _closeButton.setOffPicture(Utils.getBitmapResource(ResourceConstants.SA_IMG_SUB_MENU_BACK_CLOSE_PRESSED)); 
	       
	       
	       _subMenuButtonManager = new HorizontalFieldManager(Field.FIELD_RIGHT){
	        	protected void sublayout(int maxWidth, int maxHeight) {
					maxWidth = Utils.getDisplayWidth();
					maxHeight = ResourceConstants.SA_VAL_BOTTOM_MENU_OTHER_MENU_BUTTON_SIZE;
					super.sublayout(maxWidth, maxHeight);
					setExtent(maxWidth, maxHeight);
				}
	        };
	        Background background = BackgroundFactory.createSolidTransparentBackground(Color.BLACK, 255);
	        _subMenuButtonManager.setBackground(background);
	        if(homeFlag)
	        {
	        	SASportEventContentItem eventContent = _sharedSession.m_sportEventContent;
	        	
	        	ImageButtonField videoMenuButton = new ImageButtonField("MENU_VIDEO", ResourceConstants.SA_ICON_MENU_VIDEO , Utils.toDPIx(30) , Color.BLACK, Field.FIELD_RIGHT | Field.FOCUSABLE);
	        	videoMenuButton.setMargin(0, 0, 0, Utils.toDPIx(50));
		        _subMenuButtonManager.add(videoMenuButton);
		        
		        ImageButtonField newsButtonField = new ImageButtonField("MENU_NEWS", ResourceConstants.SA_ICON_MENU_NEWS , Utils.toDPIx(30) ,  Color.BLACK, Field.FIELD_RIGHT | Field.FOCUSABLE);
		        _subMenuButtonManager.add(newsButtonField);
		        
		        if(eventContent == null || eventContent.m_includeFixture)
		        {
		        	ImageButtonField fixtureButtonField = new ImageButtonField("MENU_FIXTURE", ResourceConstants.SA_ICON_MENU_FIXTURE , Utils.toDPIx(30) , Color.BLACK,  Field.FIELD_RIGHT | Field.FOCUSABLE);
		        	_subMenuButtonManager.add(fixtureButtonField);
		        }
		        
		        if(eventContent == null || eventContent.m_includeStanding)
		        {
			        ImageButtonField standingButtonField = new ImageButtonField("MENU_STANDING", ResourceConstants.SA_ICON_MENU_STANDING , Utils.toDPIx(30) , Color.BLACK,  Field.FIELD_RIGHT | Field.FOCUSABLE);
			        _subMenuButtonManager.add(standingButtonField);
		        }
		        
		        ImageButtonField moreButtonField = new ImageButtonField("MENU_MORE",ResourceConstants.SA_ICON_MENU_MORE , Utils.toDPIx(30) , Color.BLACK,  Field.FIELD_RIGHT | Field.FOCUSABLE);
		        _subMenuButtonManager.add(moreButtonField);
		    }
	        else
	        {
	        	ImageButtonField backButton= new ImageButtonField("MENU_BACK",ResourceConstants.SA_ICON_MENU_BACK, 0 ,  Color.BLACK,  Manager.FIELD_RIGHT | Field.FOCUSABLE);
	        	_subMenuButtonManager.add(backButton);
	        	backButton.setMargin(0, 0, 0, Utils.toDPIx(20));
	        	
	        	ImageButtonField homeButton= new ImageButtonField("MENU_HOME", ResourceConstants.SA_ICON_MENU_HOME, 0 ,  Color.BLACK,Manager.FIELD_RIGHT | Field.FOCUSABLE);
	    		_subMenuButtonManager.add(homeButton);
	    		homeButton.setMargin(0, 0, 0, Utils.toDPIx(450));
	        }
	    }
	    
	    /**
	     * @show subMenu
	     */
	    public void showSubMenu()
	    {
	    	this.delete(_openButton);
	    	this.add(_closeButton);	
	    }
	    
	    /**
	     * @hide subMenu
	     */
	    
	    public void hideSubMenu()
	    {
	    	this.delete(_closeButton);
	    	this.add(_openButton);
	    }
	    
	    protected void sublayout(int maxWidth, int maxHeight) {
			maxWidth =  Utils.getDisplayWidth();
			maxHeight = ResourceConstants.SA_VAL_BOTTOM_MENU_OPEN_CLOSE_BUTTON_SIZE;
			super.sublayout(maxWidth, maxHeight);
			this.setExtent(maxWidth , maxHeight);
		}
}
