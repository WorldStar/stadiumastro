package view.component;

import application.ResourceConstants;
import application.Utils;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.decor.Background;

/**
 * 
 * @author Administrator
 *	This is page control that shows on homeScreen's Highlight news list ScrollBar.
 *	This control does only add dot images with it's alignment.
 */

public class CustomPageControlField extends HorizontalFieldManager {
	
	private int curPage = 0;
	private int pageCount = 0;
	private Bitmap pageControlBack;
	
	public CustomPageControlField(int pageNumber, int pageCount){
		this.curPage = pageNumber;
		this.pageCount = pageCount;
		
		this.pageControlBack = Utils.getBitmapResource(ResourceConstants.SA_IMG_PAGECONTROL);
		Background background = BackgroundFactory.createBitmapBackground(this.pageControlBack);
		this.setBackground(background);

		addDotImages();
	}
	
	public int leftMargin;
	public int topMargin;
	
	private void addDotImages(){
		
		leftMargin = (pageControlBack.getWidth() - 24 * pageCount) / 2;
		topMargin = (ResourceConstants.SA_VAL_PAGECONTROL_BACK_HEIGHT - ResourceConstants.SA_VAL_PAGECONTROL_DOT_HEIGHT) / 2;
		
		
		HorizontalFieldManager  dotsManager = new HorizontalFieldManager() {
			protected void sublayout(int maxWidth, int maxHeight) {
				maxWidth = Utils.getDisplayWidth() - leftMargin;
				maxHeight = ResourceConstants.SA_VAL_PAGECONTROL_BACK_HEIGHT - topMargin;
				super.sublayout(maxWidth, maxHeight);
				setExtent(maxWidth, maxHeight);
			}
		};
		
		dotsManager.setMargin(topMargin, 0, 0, leftMargin);
		
		for(int i = 0; i < pageCount; i++){
			CustomBitmapField dotImageField;
			if(i == curPage){
				dotImageField = new CustomBitmapField(ResourceConstants.SA_IMG_PAGECONTROL_DOT_WHITE,null,Utils.toDPIy(10),0, 0, 0,Manager.FIELD_HCENTER,null);
			}
			else{
				dotImageField = new CustomBitmapField(ResourceConstants.SA_IMG_PAGECONTROL_DOT_BLACK,null,Utils.toDPIy(10),0, 0, 0,Manager.FIELD_HCENTER,null);
			}
			dotsManager.add(dotImageField);
		}
		this.add(dotsManager);
	}
	protected void sublayout(int maxWidth, int maxHeight) {
		maxWidth = Math.max(maxWidth , pageControlBack.getWidth());
		maxHeight = Math.max(maxHeight , pageControlBack.getHeight());
		this.setExtent(maxWidth , maxHeight);
		super.sublayout(maxWidth, maxHeight);
	}
}
