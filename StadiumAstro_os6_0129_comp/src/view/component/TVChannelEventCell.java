package view.component;

import java.util.Calendar;

import application.ResourceConstants;
import application.Utils;
import model.SATVChannelEventListItem;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class TVChannelEventCell extends VerticalFieldManager {
	
	public TVChannelEventCell(SATVChannelEventListItem item,int index){
		Background background;
		if(index % 2 == 0)
			background = BackgroundFactory.createSolidTransparentBackground(0xfffff0, 255);
		else
			background = BackgroundFactory.createSolidTransparentBackground(0xeeeee0, 255);
		this.setBackground(background);
		
		CustomVerticalFieldManager titleField = new CustomVerticalFieldManager(Utils.getDisplayWidth(), Utils.toDPIy(60), Field.FIELD_VCENTER);
		CustomLabelField tvChannelEventTitleLabel = new CustomLabelField(item.m_eventName, Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN,ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), Utils.toDPIx(10), Utils.toDPIy(10), Utils.getDisplayWidth(), Field.FIELD_LEFT);
		titleField.add(tvChannelEventTitleLabel);
		add(titleField);
		
		CustomHorizontalFieldManager descField = new CustomHorizontalFieldManager(Utils.getDisplayWidth(), Utils.toDPIy(40), Field.FIELD_BOTTOM);
		
		Calendar cal = Utils.getDateFromString(item.m_dateTime);
		String dateString = Integer.toString(cal.get(Calendar.DAY_OF_MONTH)) + " " + Utils.getMonthString(cal.get(Calendar.MONTH)) + " " + Integer.toString(cal.get(Calendar.YEAR)) + " | " + Utils.getWeekDayString(cal.get(Calendar.DAY_OF_WEEK));
		
		CustomLabelField tvChannelEventDateTime = new CustomLabelField(dateString,Color.BLACK,Utils.getTypeFace().getFont(Font.PLAIN,ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), Utils.toDPIx(10), Utils.toDPIy(7), ResourceConstants.SA_VAL_TV_CHANNEL_CALENDAR_EVENT_DESC_DATETIME_WIDTH, Field.FIELD_LEFT);
		descField.add(tvChannelEventDateTime);
		CustomLabelField tvChannelEventStartEndTime = new CustomLabelField(item.m_startTime+" - " + item.m_endTime,Color.GRAY,Utils.getTypeFace().getFont(Font.PLAIN,ResourceConstants.SA_VAL_FONT_SIZE_SMALL), Utils.toDPIx(10), Utils.toDPIy(7), ResourceConstants.SA_VAL_TV_CHANNEL_CALENDAR_EVENT_DESC_STARTENDTIME_WIDTH, Field.FIELD_LEFT);
		descField.add(tvChannelEventStartEndTime);
		add(descField);
		
	}
	protected void sublayout(int maxWidth, int maxHeight) {
		// TODO Auto-generated method stub
		maxWidth = Utils.getDisplayWidth();
		maxHeight =  ResourceConstants.SA_VAL_TV_CHANNEL_CALENDAR_EVENT_ITEM_HEIGHT;
		super.sublayout(maxWidth, maxHeight);
		this.setExtent(maxWidth , maxHeight);
	}
	//TODO - Event Handler... not connected!
}
