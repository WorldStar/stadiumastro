package view.component;

import application.ResourceConstants;
import application.Utils;
import model.SASoccerMatchListItem;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;


/**
 * 
 * @author Administrator
 *
 *	This is match info view which shows in Analysis Screen's header section.
 *  This class set contents data from SAMatchList Model Class.
 */
public class AnalysisHeaderMatchInfo extends HorizontalFieldManager {
	/**
	 * 
	 * @param mInfo - Match Info Data from previous Matches Screen's table cell.
	 */
	public AnalysisHeaderMatchInfo(SASoccerMatchListItem mInfo){
		
		super();
	
		Bitmap backImage = Utils.getBitmapResource(ResourceConstants.SA_IMG_FIXTURE_MATCH_TABLE_CELL);
		Background background = BackgroundFactory.createBitmapBackground(backImage);
		this.setBackground(background);

		
		//1. home team Logo 
		
		CustomVerticalFieldManager homeTeamManager = new CustomVerticalFieldManager(ResourceConstants.SA_VAL_FIXTURE_ANALYSIS_MATCH_CELL_TEAMLOGO_WIDTH,ResourceConstants.SA_VAL_FIXTURE_MATCH_CELL_HEIGHT,Manager.FIELD_LEFT|Manager.USE_ALL_WIDTH);
		{
			//mInfo.homeTeamLogo	
			CustomBitmapField homeTeamLogoImage = new CustomBitmapField(mInfo.m_homeTeamLogo,null, 0, Utils.toDPIy(20),Utils.toDPIx(93), Utils.toDPIy(100), Manager.FIELD_HCENTER,null);
			CustomLabelField homeTeamAliasLabel = new CustomLabelField(mInfo.m_homeTeamAlias,Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , 0, Utils.toDPIy(5) , Utils.toDPIx(100),  Field.FIELD_HCENTER);
			homeTeamAliasLabel.setTextAlign(DrawStyle.HCENTER);
			homeTeamManager.add(homeTeamLogoImage);
			homeTeamManager.add(homeTeamAliasLabel);
		}
		add(homeTeamManager);
		
		//2. Score Display View 
		CustomVerticalFieldManager scoreViewManager = new CustomVerticalFieldManager(ResourceConstants.SA_VAL_FIXTURE_ANALYSIS_MATCH_CELL_SCORE_WIDTH,ResourceConstants.SA_VAL_FIXTURE_MATCH_CELL_HEIGHT,Manager.FIELD_LEFT);
		
		{
			//2-2. Score Horizontal View
			if(mInfo.m_matchStatus.compareTo("Played") == 0)
			{
				VerticalFieldManager scoreManager = new VerticalFieldManager();			
				{
					String strScore = mInfo.m_homeTeamScore + " - " + mInfo.m_visitingTeamScore;
					CustomLabelField scoreLabel = new CustomLabelField(strScore ,Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_LARGE_MIDDLE) , 0, 0 , ResourceConstants.SA_VAL_FIXTURE_ANALYSIS_MATCH_CELL_SCORE_WIDTH,  Field.FIELD_HCENTER);
					scoreLabel.setTextAlign(DrawStyle.HCENTER);
					
					scoreManager.add(scoreLabel);
				}
				
				VerticalFieldManager playedFieldManager = new VerticalFieldManager();
				CustomLabelField playedLabel = new CustomLabelField("Full Time",Color.BLACK,Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , 0, Utils.toDPIy(20) , ResourceConstants.SA_VAL_FIXTURE_ANALYSIS_MATCH_CELL_SCORE_WIDTH,  Field.FIELD_HCENTER);
				playedLabel.setTextAlign(DrawStyle.HCENTER);
				playedFieldManager.add(playedLabel);
				
				scoreViewManager.add(playedFieldManager);
				scoreViewManager.add(scoreManager);
			}
			else
			{
				VerticalFieldManager matchTimeManager = new VerticalFieldManager();
				CustomLabelField matchTimeLabel = new CustomLabelField(mInfo.m_time ,Color.BLACK,Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , 0, Utils.toDPIy(10) , ResourceConstants.SA_VAL_FIXTURE_ANALYSIS_MATCH_CELL_SCORE_WIDTH,  Field.FIELD_HCENTER);
				matchTimeLabel.setTextAlign(DrawStyle.HCENTER);
				matchTimeManager.add(matchTimeLabel);
				
				VerticalFieldManager scoreManager = new VerticalFieldManager();
				CustomLabelField scoreLabel = new CustomLabelField("vs" ,Color.BLACK,Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_BIG_LARGE) , 0, 0 , ResourceConstants.SA_VAL_FIXTURE_ANALYSIS_MATCH_CELL_SCORE_WIDTH,  Field.FIELD_HCENTER);
				scoreLabel.setTextAlign(DrawStyle.HCENTER);				
				scoreManager.add(scoreLabel);
				
				scoreViewManager.add(matchTimeManager);
				scoreViewManager.add(scoreManager);
				
			}
			
		}
		add(scoreViewManager);
				
		//3. visiting team Logo 
		CustomVerticalFieldManager opponentTeamManager = new CustomVerticalFieldManager(ResourceConstants.SA_VAL_FIXTURE_ANALYSIS_MATCH_CELL_TEAMLOGO_WIDTH,ResourceConstants.SA_VAL_FIXTURE_MATCH_CELL_HEIGHT,Manager.FIELD_RIGHT|Manager.USE_ALL_WIDTH);
		{
			CustomBitmapField visitingTeamLogoImage = new CustomBitmapField(mInfo.m_visitingTeamLogo,null, 0,Utils.toDPIy(20),Utils.toDPIx(93), Utils.toDPIy(100), Manager.FIELD_HCENTER,null);
			CustomLabelField visitingTeamAliasLabel = new CustomLabelField(mInfo.m_visitingTeamAlias,Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , 0, Utils.toDPIy(5) , Utils.toDPIx(100),  Field.FIELD_HCENTER);
			visitingTeamAliasLabel.setTextAlign(DrawStyle.HCENTER);
			opponentTeamManager.add(visitingTeamLogoImage);
			opponentTeamManager.add(visitingTeamAliasLabel);
		}
		add(opponentTeamManager);
	

	}
	protected void sublayout(int maxWidth, int maxHeight) {
		maxWidth = Utils.getDisplayWidth();
		maxHeight = ResourceConstants.SA_VAL_FIXTURE_MATCH_CELL_HEIGHT;
		super.sublayout(maxWidth, maxHeight);
		setExtent(maxWidth, maxHeight);
	}
}
