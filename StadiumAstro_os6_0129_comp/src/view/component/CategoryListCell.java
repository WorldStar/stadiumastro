package view.component;

import view.screen.BaseScreen;
import view.screen.MoreScreen;
import view.screen.NewsMainScreen;
import view.screen.TVChannelListScreen;
import view.screen.VideoMainScreen;
import application.AppSession;
import application.ResourceConstants;
import application.StadiumAstroApp;
import application.Utils;

import model.SACategoryItem;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
//import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

/**
 * 
 * @author Administrator
 *	This class is Category Table View Cell class which is the first page of Video and News Screen ' s table view.
 *	
 */
public class CategoryListCell extends HorizontalFieldManager {

	private Bitmap m_backImage;
	private CustomBitmapField m_icon;
	private String m_title;
	private boolean m_showArrow;
	
	private CustomHorizontalFieldManager m_cell;
	private Background m_background;
	private Background m_pressedBack;

	private boolean refreshed = false;
	private SACategoryItem m_itemData = null;
	
	private int cellWidth = Utils.toDPIx(560);
	/**
	 * 
	 * @param backUrl - BackImage url
	 * @param iconUrl - ICON url
	 * @param title - title of Cell
	 * @param arrowFlag - show arrow icon or not
	 * 
	 */
	public CategoryListCell(String BackUrl, String iconUrl, String title, boolean arrowFlag, long style) {
		super(style);
		this.m_title = title;
		this.m_backImage = Utils.getBitmapResource(BackUrl);
		this.m_backImage = Utils.getScaledBitmap(this.m_backImage, cellWidth, this.m_backImage.getHeight());
		if(iconUrl != null)
			this.m_icon = new CustomBitmapField(iconUrl, null, Utils.toDPIx(20), Utils.toDPIy(20), 0, 0, Manager.FIELD_HCENTER,null);
		else
			this.m_icon = null;
		this.m_showArrow = arrowFlag;
		initCellLayout();
	}
	/**
	 * 
	 * @param item - category item data
	 * @param iconUrl - iconUrl
	 * @param arrowFlag - show arrow or not
	 * @param style
	 */
	public CategoryListCell(SACategoryItem item, String iconUrl, boolean arrowFlag, long style)
	{
		super(style);
		m_itemData = item;
		
		this.m_title = item.m_name;
		this.m_backImage = Utils.getBitmapResource(item.m_bitmapURL);
		this.m_backImage = Utils.getScaledBitmap(this.m_backImage, cellWidth, this.m_backImage.getHeight());
		if(iconUrl != null)
			this.m_icon = new CustomBitmapField(iconUrl, null, Utils.toDPIx(20), Utils.toDPIy(20), 0, 0,  Manager.FIELD_HCENTER,null);
		else
			this.m_icon = null;
		this.m_showArrow = arrowFlag;
		
		initCellLayout();
	}

	public void initCellLayout()
	{
		m_cell = new CustomHorizontalFieldManager(cellWidth, m_backImage.getHeight(),Manager.FIELD_HCENTER);
		
		m_background = BackgroundFactory.createBitmapBackground(this.m_backImage);
		m_cell.setBackground(m_background);
		
		m_pressedBack = BackgroundFactory.createSolidTransparentBackground(Color.LIGHTBLUE, 200);

		if(this.m_icon != null){
			CustomHorizontalFieldManager iconField = new CustomHorizontalFieldManager(ResourceConstants.SA_VAL_CATEGORY_CELL_ICON_WIDTH, m_backImage.getHeight(),Manager.FIELD_LEFT);
			iconField.add(this.m_icon);
			m_cell.add(iconField);
			
			CustomHorizontalFieldManager labelField = new CustomHorizontalFieldManager(ResourceConstants.SA_VAL_CATEGORY_CELL_LABEL_WIDTH, m_backImage.getHeight(),Manager.FIELD_HCENTER);
			
	        CustomLabelField titleLabel = new CustomLabelField(this.m_title,Color.WHITE,Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , Utils.toDPIx(20) , Utils.toDPIy(30) , 500 , Field.FOCUSABLE);
	        labelField.add(titleLabel);
	        m_cell.add(labelField);
		}
		else
		{
			CustomHorizontalFieldManager labelField = new CustomHorizontalFieldManager(ResourceConstants.SA_VAL_CATEGORY_CELL_LABEL_WIDTH + ResourceConstants.SA_VAL_CATEGORY_CELL_ICON_WIDTH,m_backImage.getHeight(),Manager.FIELD_LEFT);
	        CustomLabelField titleLabel = new CustomLabelField(this.m_title,Color.WHITE,Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , Utils.toDPIx(20) , Utils.toDPIx(30) , 500 , Field.FOCUSABLE);
	        labelField.add(titleLabel);
	        m_cell.add(labelField);
		}
		
		
        
        if(this.m_showArrow)
        {
        	CustomHorizontalFieldManager arrowField = new CustomHorizontalFieldManager(ResourceConstants.SA_VAL_CATEGORY_CELL_ARROW_WIDTH,m_backImage.getHeight(),FIELD_RIGHT|USE_ALL_HEIGHT);        	
        	CustomBitmapField arrowBitmap  = new CustomBitmapField(ResourceConstants.SA_IMG_CELL_RIGHT_ARROW_ICON, null, 0, 0, 0, Utils.toDPIy(30), FIELD_VCENTER,null);
        	arrowField.add(arrowBitmap); 
        	m_cell.add(arrowField);
        }
		add(m_cell);
	}
	
	protected void sublayout(int maxWidth, int maxHeight) {
		maxWidth = m_backImage.getWidth();
		maxHeight = m_backImage.getHeight();
		super.sublayout(maxWidth, maxHeight);
		this.setExtent(maxWidth , maxHeight);
		
	}
	/**
	 * 
	 * @return title of me
	 */
	public String getTitle(){
		return m_title;
	}
	
	/**
     * Event handler
     */
	
    protected boolean navigationClick(int status, int time) {
    	if (status != 0){
    		fieldChangeNotify(0);
    		clickButton();
    	}
        return true;
    }

    public boolean keyChar(char key, int status, int time) {
        if (key == Characters.ENTER) {
            fieldChangeNotify(0);
            clickButton();
            return true;
        }
        return false;
    }
    protected boolean trackwheelClick(int status, int time)
    {        
        if (status != 0) clickButton(); 
        return true;
    }
	protected boolean invokeAction(int action) 
    {
        switch( action ) {
            case ACTION_INVOKE: {
                clickButton(); 
                return true;
            }
        }
        return super.invokeAction(action);
    }
	
	protected boolean touchEvent(TouchEvent message)
	 {
		int x = message.getX( 1 );
        int y = message.getY( 1 );
        if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
            // Outside the field
        	if(refreshed)
        		refreshBackGround(false);
            return false;
        }
        else{
        	
		   	if(message.getEvent() == TouchEvent.DOWN)
		  	{	
		   		 refreshBackGround(true);
			}
		   	else if(message.getEvent() == TouchEvent.UP){
		   		refreshBackGround(false);
		   	}
		   	if(message.getEvent() == TouchEvent.CLICK){
		   		refreshBackGround(false);
		   		clickButton();
		   	}
		}
	    return super.touchEvent(message); 
   }
	
	public void refreshBackGround(boolean flag){
		if(flag == true){
			m_cell.setBackground(m_pressedBack);
			refreshed = true;
		}else
		{	
			m_cell.setBackground(m_background);
			refreshed = true;
		}
		getManager().invalidate();
	}
	
	public void clickButton(){

		fieldChangeNotify(0);

        StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
        
        //TODO - goto and send index to filter list!
        BaseScreen _curScreen = (BaseScreen) app.getActiveScreen(); 
        
        if(_curScreen.getClass() == VideoMainScreen.class){
            app.gotoVideoListScreen(m_itemData.m_refKey);
        }
        else if(_curScreen.getClass() == NewsMainScreen.class)
        {
        	app.gotoNewsListScreen(m_itemData.m_refKey , m_title);
        }
        else if(_curScreen.getClass() == MoreScreen.class)
        {
        	if(getTitle() == ResourceConstants.SA_TITLE_MORE_CHANNEL)
        		app.gotoTVChannelScreen();
        	else if(getTitle() == ResourceConstants.SA_TITLE_MORE_PUSH_NOTIFICATION)
        	{
        		app.gotoPushConfigScreen();
        	}
        }else if(_curScreen.getClass() == TVChannelListScreen.class)
        {
        	TVChannelListScreen channelScreen = (TVChannelListScreen)(_curScreen);
        	AppSession.sharedSession().m_curTVChannelKey = channelScreen.getChannelRefKey(this.getIndex());
       		app.gotoTVChannelDetailScreen(getTitle());
       	 
        }
        refreshBackGround(false);
    }
	
	protected void onFocus(int direction)
	{
//		this.getManager().setVerticalScroll(position, animate)
		refreshBackGround(true);
	}
	
	protected void onUnfocus()
	{
		refreshBackGround(false);
	}
	
}
