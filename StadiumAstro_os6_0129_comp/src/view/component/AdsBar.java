package view.component;

import application.ResourceConstants;
import application.Utils;
import model.ImageLoadListener;
import model.SAAdsBannerList;
import net.rim.blackberry.api.browser.Browser;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.container.AbsoluteFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

/**
 * 
 * @author Administrator
 *	ADS BAR class. Here only show one image that specify with SA_IMG_ADS_BAR in ResourceConstants.h
 */
public class AdsBar extends AbsoluteFieldManager implements ImageLoadListener,FieldChangeListener{
	
	public static Bitmap backImage = null;
	SAAdsBannerList m_adsBannerList = null;
	ImageHighlightButton highlightRegion;
	ImageButtonField closeButton;
	int m_barWidth;
	int m_barHeight;
	
	public AdsBar(){
		super();
//		AbsoluteFieldManager adsManager = new AbsoluteFieldManager();
		
		m_adsBannerList = new SAAdsBannerList();
		if(m_adsBannerList.m_adsBannerItem != null)
		{
			String resourceStr = m_adsBannerList.m_adsBannerItem.m_mobile_file;
			
			closeButton = new ImageButtonField("ADS_BAR_CLOSE",ResourceConstants.SA_ICON_ADS_BAR_CLOSE , 0 , -1 , Field.FIELD_VCENTER | Field.FOCUSABLE);
			
			m_barWidth = ResourceConstants.SA_VAL_ADS_BAR_WIDTH;
			m_barHeight = ResourceConstants.SA_VAL_ADS_BAR_HEIGHT;
			
			highlightRegion = new ImageHighlightButton(resourceStr, 0, 0, m_barWidth, m_barHeight, Field.FIELD_LEFT|Field.FOCUSABLE);
			highlightRegion.setChangeListener( this );
			
			
//			backImage = Utils.getBitmapResource(ResourceConstants.SA_IMG_ADS_BAR);
//			Background background = BackgroundFactory.createBitmapBackground(backImage);
//			highlightRegion.setBackground(background);
			
			highlightRegion.setBackground(BackgroundFactory.createSolidBackground(Color.BLACK));
			
			add(highlightRegion);
			add(closeButton);
		
		}
	}
	
	protected void sublayout(int maxWidth, int maxHeight) {
		System.out.println("Stadium Astro Ads bar back image sub layout");
		
		maxWidth = m_barWidth;
		maxHeight = m_barHeight;
		
		super.sublayout(maxWidth, maxHeight);
		this.setExtent(maxWidth , maxHeight);
		
		if(closeButton != null)
			this.setPositionChild(closeButton, m_barWidth - closeButton.getWidth(), (m_barHeight - closeButton.getHeight()) / 2);
	}
	public void imageLoadSuccess(String url, Bitmap bmp) {
		// TODO Auto-generated method stub
		backImage = Utils.getScaledBitmap(bmp, m_barWidth, m_barHeight);
		Background background = BackgroundFactory.createBitmapBackground(backImage);
		this.setBackground(background);
		System.out.println("Ads Image Set Success");
		bmp = null;
		
	}
	
	public void imageLoadCanceled(String url)
	{
	}
	
	public void imageLoadFailed(String url, String err) {
		// TODO Auto-generated method stub
		backImage = null;
	}
	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		if(context == 1)
		{
			if(field == highlightRegion)
			{
				if(m_adsBannerList != null)
				{
					Browser.getDefaultSession().displayPage(m_adsBannerList.m_adsBannerItem.m_mobile_url);
				}
			}
		}
	}
}
