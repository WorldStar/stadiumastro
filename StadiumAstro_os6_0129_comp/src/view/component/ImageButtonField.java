package view.component;


import view.screen.BaseScreen;
import application.StadiumAstroApp;
import application.Utils;
import net.rim.device.api.ui.*;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;

/**
 * 
 * @class ImageButton
 * This class is the Image Button.
 * This class needs only for Sub Menu Buttons & Ads_bar_Close Buttons.
 *
 */
public class ImageButtonField extends Field {
	
	private String m_btnId;
	private int m_paddingX;

	private Bitmap _currentPicture;
    private Bitmap _onPicture;
    private Bitmap _offPicture;

    private int m_Color;

    
    private boolean m_refreshed = false;
    /**
     * @param ID = Button Unique ID
     * @param url = Button Image URL
     * @param paddingX = leftPadding value
     * @param color = color of Button. - This needs for remove Blue-Focus-Rect in bb6.
     * @param style = style of button field
     *
     */
    public ImageButtonField(String ID , String url, int paddingX , int color, long style) 
    {

    	super(style);
    	this.m_btnId = ID;
        this.m_paddingX = paddingX;
        this.m_Color = color;
        _onPicture = Utils.getBitmapResource(url);

        _offPicture = Utils.getScaledBitmap(_onPicture, 0.9);
        _currentPicture = _onPicture;
    }
    /**
     * @return buttons ID
     * 
     */
    public String getID(){
    	return m_btnId;
    }
    /**
     * @set _currentPicture
     */
    public void setOffPicture(Bitmap bitmap){
    	_offPicture = bitmap;
    }
    
    /**
     * Field implementation.
     * @see net.rim.device.api.ui.Field#getPreferredHeight()
     */
    public int getPreferredHeight() 
    {
    	return _onPicture.getHeight();
    }
    
    /**
     * Field implementation.
     * @see net.rim.device.api.ui.Field#getPreferredWidth()
     */
    public int getPreferredWidth() 
    {
    	return _onPicture.getWidth();
    }
    /**
     * Field implementation.
     * @see net.rim.device.api.ui.Field#layout(int, int)
     */
    protected void layout(int width, int height) 
    {
        setExtent(getPreferredWidth() + m_paddingX, getPreferredHeight());
    }
    

    /**
     * Field implementation.
     * @see net.rim.device.api.ui.Field#paint(Graphics)
     */
    protected void paint(Graphics graphics) 
    {       
        // First draw the background colour and picture
    	if(m_Color != -1){
	        graphics.setColor(m_Color);
	        graphics.fillRect(0, 0, getWidth(), getHeight());
    	}
        graphics.drawBitmap(m_paddingX, 0, getWidth(), getHeight(), _currentPicture, 0, 0);
    }
        
    /**
     * Event handler
     */
    protected void onFocus(int direction){
    	refresh(true);
    }
    protected void onUnfocus(){
    	refresh(false);
    }
    protected boolean navigationClick(int status, int time) {
    	if (status != 0){
    		fieldChangeNotify(0);
    		clickButton();
    	}
        return true;
    }

    public boolean keyChar(char key, int status, int time) {
        if (key == Characters.ENTER) {
            fieldChangeNotify(0);
            clickButton();
            return true;
        }
        return false;
    }
    
    protected boolean trackwheelClick(int status, int time)
    {        
        if (status != 0) clickButton(); 
        return true;
    }
    
    protected boolean invokeAction(int action) 
    {
        switch( action ) {
            case ACTION_INVOKE: {
                clickButton(); 
                return true;
            }
        }
        return super.invokeAction(action);
    }

    protected boolean touchEvent(TouchEvent message)
    {
        int x = message.getX( 1 );
        int y = message.getY( 1 );
        if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
            // Outside the field
        	if(m_refreshed)
        		refresh(false);
            return false;
        }
        switch( message.getEvent() ) {
       
        	case TouchEvent.DOWN:
        		refresh(true);
        		return true;
            case TouchEvent.UP:
            	refresh(false);
            	clickButton();
            	return true;
            	
        }
        return super.touchEvent( message );
    }


    public void clickButton(){
    	//Dialog.alert("Clicked!");
    	fieldChangeNotify(0);
    	StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
  		
    	if(getID() == "ADS_BAR_CLOSE"){
    		BaseScreen curScreen = (BaseScreen) app.getActiveScreen();
    		curScreen.hideAdsBar();
    	}else if(getID() == "MENU_OPEN"){
    		BaseScreen curScreen = (BaseScreen) app.getActiveScreen();
      		curScreen.ShowMenu();
    	}else if(getID() == "MENU_CLOSE"){
    		BaseScreen curScreen = (BaseScreen) app.getActiveScreen();
      		curScreen.HideMenu();
    	}else if(getID() == "MENU_VIDEO"){
    		app.gotoVideoMainScreen();
    	}else if(getID() == "MENU_NEWS"){
    		app.gotoNewsMainScreen();
    	}else if(getID() == "MENU_FIXTURE"){
    		app.gotoFixtureMatchScreen();
    	}else if(getID() == "MENU_STANDING"){
    		app.gotoStandingScreen();
    	}else if(getID() == "MENU_MORE"){
    		app.gotoMoreScreen();
    	}else if(getID() == "MENU_BACK"){
    		app.gotoBackScreen();
    	}else if(getID() == "MENU_HOME"){
    		app.gotoHomeScreen();
    	} 
    	
    }
    public void refresh(boolean flag){
    	m_refreshed = flag;
    	if(flag == true){
    		_currentPicture = _offPicture;
    		invalidate();
    	}
    	else{
    		_currentPicture = _onPicture;
    		invalidate();
    	}
    	invalidate();
    }
}
