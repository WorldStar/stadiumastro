package view.component;

import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.XYRect;

public class ImageHighlightButton extends CustomBitmapField  {
	int m_btnTag;
	boolean bFocused;
	public ImageHighlightButton(String img, int x, int y, int width,
			int height, long style) {
		
		super(img, null, x, y, width, height, style, null);
		// TODO Auto-generated constructor stub
	}
	public ImageHighlightButton(String img, String icon, int x, int y, int width,
			int height, long style, String defaultImg) {
		
		super(img, icon, x, y, width, height, style, defaultImg);
		// TODO Auto-generated constructor stub
	}
	protected boolean navigationClick(int status, int time) {
    	if (status != 0){
    		clickButton();
    	}
        return true;
    }

    public boolean keyChar(char key, int status, int time) {
        if (key == Characters.ENTER) {
            clickButton();
            return true;
        }
        return false;
    }
    protected boolean trackwheelClick(int status, int time)
    {        
        if (status != 0) clickButton(); 
        return true;
    }
	protected boolean invokeAction(int action) 
    {
		refreshBackGround(true);
        switch( action ) {
            case ACTION_INVOKE: {
                clickButton(); 
                return true;
            }
        }
        return super.invokeAction(action);
    }
	
	protected boolean touchEvent(TouchEvent message)
	 {
		int x = message.getX( 1 );
        int y = message.getY( 1 );
        if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
            // Outside the field
        	if(bFocused)
        		refreshBackGround(false);
            return false;
        }
        else{
        	
		   	if(message.getEvent() == TouchEvent.DOWN)
		  	{	
		   		 refreshBackGround(true);
			}
		   	else if(message.getEvent() == TouchEvent.UP){
		   		refreshBackGround(false);
		   	}
		   	if(message.getEvent() == TouchEvent.CLICK){
		   		clickButton();
		   		return true;
		   	}
		}
	    return super.touchEvent(message); 
   }

	public void refreshBackGround(boolean flag){
		if(flag == true)
			bFocused = true;
		else
			bFocused = false;
		invalidate();
		
	}
	protected void paint(Graphics graphics) 
	{
		super.paint(graphics);
		if(bFocused)
		{
			graphics.setGlobalAlpha(50);
			graphics.setColor(0xffffff);
			XYRect extent = this.getExtent();
			graphics.fillRect(0, 0, extent.width,extent.height);
		}
	}
	public void clickButton(){
        refreshBackGround(false);
        fieldChangeNotify(1);
     
    }
	public void setButtonTag(int tag)
	{
		m_btnTag = tag;
	}
	public int getButtonTag()
	{
		return m_btnTag;
	}
	
	protected void onFocus(int direction)
	{
		refreshBackGround(true);
	}
	
	protected void onUnfocus()
	{
		refreshBackGround(false);
	}
}
