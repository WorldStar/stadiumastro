package view.component;

import model.SACategoryItem;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;
import application.ResourceConstants;
import application.Utils;

import com.samples.toolkit.ui.component.LabeledSwitch;
import com.samples.toolkit.ui.container.JustifiedHorizontalFieldManager;

public class PushCategoryCell extends HorizontalFieldManager implements FieldChangeListener {
	private Bitmap m_backImage;	
	private String m_title;
	
	
	private CustomHorizontalFieldManager m_cell;
	private Background m_background;
	
	private int mCellWidth;
	private int mCellHeight;
	
	public String  m_refKey;
	public LabeledSwitch m_switchField;
	
	public int mOldWidth;
	public int mOldHeight;
	
	/**
	 * 
	 * @param backUrl - BackImage url
	 * @param iconUrl - ICON url
	 * @param title - title of Cell
	 * @param arrowFlag - show arrow icon or not
	 * 
	 */
	public PushCategoryCell(String BackUrl,String title, int cellWidth, int cellHeight, long style) {
		super(style);
		this.m_title = title;
		this.m_backImage = Utils.getBitmapResource(BackUrl);
		
		mCellWidth = cellWidth;
		mCellHeight = cellHeight;
		
		if(cellWidth == 0)
			mCellWidth = this.m_backImage.getWidth();
		else
		{
			mCellWidth = cellWidth;
			this.m_backImage = Utils.getScaledBitmap(this.m_backImage, mCellWidth, this.m_backImage.getHeight());
		}
		
		if(cellHeight == 0)
			mCellHeight = Utils.toDPIy(60);
		else
			mCellHeight = cellHeight;

		initCellLayout();
		
	}
	/**
	 * 
	 * @param item - category item data
	 * @param iconUrl - iconUrl
	 * @param arrowFlag - show arrow or not
	 * @param style
	 */
	
	public void initCellLayout()
	{
		
		m_cell = new CustomHorizontalFieldManager(mCellWidth, mCellHeight,Manager.FIELD_HCENTER);
		
		m_background = BackgroundFactory.createBitmapBackground(this.m_backImage, 0, 0, Background.REPEAT_SCALE_TO_FIT);
		
 		m_cell.setBackground(m_background);

        CustomLabelField titleLabel = new CustomLabelField(this.m_title,Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_BIG_MIDDLE) , Utils.toDPIx(20) , 0 , Utils.toDPIx(400) , Field.FOCUSABLE);
        
		Bitmap switch_on = Bitmap.getBitmapResource("img/switches/switch_left.png");
		Bitmap switch_off = Bitmap.getBitmapResource("img/switches/switch_right.png");
		Bitmap switch_on_focus = Bitmap.getBitmapResource("img/switches/switch_left_focus.png");
		Bitmap switch_off_focus = Bitmap.getBitmapResource("img/switches/switch_right_focus.png");
		
        m_switchField = new LabeledSwitch(switch_on, switch_off,switch_on_focus, switch_off_focus, "On", "Off",  false);
        m_switchField.setChangeListener(this);
        
        JustifiedHorizontalFieldManager fieldManager = new JustifiedHorizontalFieldManager(titleLabel, m_switchField, false, Field.FIELD_VCENTER|Field.USE_ALL_WIDTH);
        fieldManager.setPadding(Utils.toDPIy(30), Utils.toDPIx(20), 0, Utils.toDPIx(20));
        
        m_cell.add(fieldManager);
        
        add(m_cell);
	}
	
	protected void sublayout(int maxWidth, int maxHeight) {
//		this.setExtent(maxWidth , maxHeight);
		super.sublayout(maxWidth, maxHeight);
		
		
	}
	/**
	 * 
	 * @return title of me
	 */
	public String getTitle(){
		return m_title;
	}
	
	/**
	 * @author Jet
	 * @param refKey
	 * Additonal information for reference
	 */
	public void setRefKey(String refKey){
		this.m_refKey = refKey;
	}
	
	public String getRefKey()
	{
		return this.m_refKey;
	}
	
	public void setToggleState(boolean state)
	{
		m_switchField.setOn(state);
	}
	public boolean getToggleState()
	{
		return m_switchField.getOnState();
	}
	
	
	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		fieldChangeNotify(context);
	}
	
	/**
     * Event handler
     */
	
//    protected boolean navigationClick(int status, int time) {
//    	if (status != 0){
//    		fieldChangeNotify(0);
//    		clickButton();
//    	}
//        return true;
//    }
//
//    public boolean keyChar(char key, int status, int time) {
//        if (key == Characters.ENTER) {
//            fieldChangeNotify(0);
//            clickButton();
//            return true;
//        }
//        return false;
//    }
//    protected boolean trackwheelClick(int status, int time)
//    {        
//        if (status != 0) clickButton(); 
//        return true;
//    }
//	protected boolean invokeAction(int action) 
//    {
//        switch( action ) {
//            case ACTION_INVOKE: {
//                clickButton(); 
//                return true;
//            }
//        }
//        return super.invokeAction(action);
//    }
//	
//	protected boolean touchEvent(TouchEvent message)
//	 {
//		int x = message.getX( 1 );
//        int y = message.getY( 1 );
//        if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
//            // Outside the field
//        	if(refreshed)
//        		refreshBackGround(false);
//            return false;
//        }
//        else{
//        	
//		   	if(message.getEvent() == TouchEvent.DOWN)
//		  	{	
//		   		 refreshBackGround(true);
//			}
//		   	else if(message.getEvent() == TouchEvent.UP){
//		   		refreshBackGround(false);
//		   	}
//		   	if(message.getEvent() == TouchEvent.CLICK){
//		   		refreshBackGround(false);
//		   		clickButton();
//		   	}
//		}
//	    return super.touchEvent(message); 
//   }
//	
//	public void refreshBackGround(boolean flag){
//		if(flag == true){
//			m_cell.setBackground(m_pressedBack);
//			refreshed = true;
//		}else
//		{	
//			m_cell.setBackground(m_background);
//			refreshed = true;
//		}
//		getManager().invalidate();
//	}
//	
//	public void clickButton(){
//
//		fieldChangeNotify(0);
//
//        StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
//        
//        //TODO - goto and send index to filter list!
//        BaseScreen _curScreen = (BaseScreen) app.getActiveScreen(); 
//        
//        if(_curScreen.getClass() == VideoMainScreen.class){
//            app.gotoVideoListScreen(m_itemData.m_refKey);
//        }
//        else if(_curScreen.getClass() == NewsMainScreen.class)
//        {
//        	app.gotoNewsListScreen(m_itemData.m_refKey);
//        }
//        else if(_curScreen.getClass() == MoreScreen.class)
//        {
//        	if(getTitle() == "Channel")
//        		app.gotoTVChannelScreen();
//        }else if(_curScreen.getClass() == TVChannelListScreen.class)
//        {
//        	TVChannelListScreen channelScreen = (TVChannelListScreen)(_curScreen);
//        	AppSession.sharedSession().m_curTVChannelKey = channelScreen.getChannelRefKey(this.getIndex());
//       		app.gotoTVChannelDetailScreen(getTitle());
//       	 
//        }
//        refreshBackGround(false);
//    }
//	
//	protected void onFocus(int direction)
//	{
////		this.getManager().setVerticalScroll(position, animate)
//		refreshBackGround(true);
//	}
//	
//	protected void onUnfocus()
//	{
//		refreshBackGround(false);
//	}
	
}
