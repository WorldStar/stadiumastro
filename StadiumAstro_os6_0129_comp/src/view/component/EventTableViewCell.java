package view.component;


import java.util.Calendar;
import java.util.Date;

import view.screen.FixtureAnalysisScreen;
import view.screen.NewsListScreen;
import view.screen.VideoListScreen;

import application.AppSession;
import application.ResourceConstants;
import application.StadiumAstroApp;
import application.Utils;
import model.SAFeatureListItem;

import model.SANewsListItem;
import model.SAVideoListItem;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.*;
//import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

/**
 * 
 * @author Administrator
 *	This class is Event (Video or News) Cell Field class which means that comprised of image, titleLabel, dateLabel, etc...
 *	This class contains imageInfo with url and title, date.    Here date-info parsing process has not implemented yet.
 */

public class EventTableViewCell extends HorizontalFieldManager {
	private CustomBitmapField _imageThumbnail;
	private String m_title;
	private String m_time;
	private int m_type;
	private int m_index;		//Needs for show cell background between black and gray.
	
	private Background background;
	private Background pressedBack;

	private boolean refreshed = false;
	
	private Object m_dataObj = null;
	
	private int m_imageWidth = 188;
	private int m_labelWidth = 390;
	
	
	/**
	 * 
	 * @param data - SA model data
	 * @param style - super.style
	 * @param index - index of cell
	 * @param type - Video / News : type = 0 / 1
	 */
//	public EventTableViewCell(String url,String title,String time,long style,int index,int type){
//		super(style);
//		
//		//TODO - set thumbnail image with url from web Server
//		if(type == 0)
//			this._imageThumbnail = new CustomBitmapField(url, ResourceConstants.SA_ICON_VIDEO_PLAY, 10, 10, Manager.FIELD_LEFT);
//		else
//			this._imageThumbnail = new CustomBitmapField(url, null, 10, 10, Manager.FIELD_LEFT);
//		
//		this.m_title = title;
//		this.m_time = time;
//		this.m_index = index;
//		this.m_type = type;
//		
//		initCellLayout();
//	}
	
	public EventTableViewCell(Object data, long style,int index,int type)
	{
		super(style);
		if(data instanceof SAFeatureListItem){
			SAFeatureListItem item = (SAFeatureListItem)data;
			
			// set omission string
			int nLimitLength = 60;
			if(item.m_title.length() > nLimitLength)
			{
				this.m_title = item.m_title.substring(0, nLimitLength - 2).concat("..."); 
			}
			else
				this.m_title = item.m_title;
			
//			// calculate time difference in hours
//			long curTime = new Date().getTime();
//			long diffTime = (curTime - item.m_dateInt) / 1000 / 3600;
//			String strBeforeHour = diffTime + " hour ago";
//			this.m_time = strBeforeHour;
			
			// date & time string
			this.m_time = Utils.getStringFromDate(Utils.getDateFromString(item.m_date));

			if(item.m_type.equals("VIDEO"))
				type = 0;
			else if(item.m_type.equals("NEWS"))
				type = 1;
			
			this.m_index = index;
			this.m_type = type;
			
			//TODO - set thumbnail image with url from web Server
			if(type == 0)
				this._imageThumbnail = new CustomBitmapField(item.m_imageUrl, ResourceConstants.SA_ICON_VIDEO_PLAY, 10, 10, Utils.toDPIx(m_imageWidth), Utils.toDPIy(94),Manager.FIELD_LEFT | Field.FOCUSABLE,ResourceConstants.SA_IMG_VIDEO_TABLE_CELL_THUMB);
			else
				this._imageThumbnail = new CustomBitmapField(item.m_imageUrl, null, 10, 10, Utils.toDPIx(m_imageWidth), Utils.toDPIy(94), Manager.FIELD_LEFT| Field.FOCUSABLE ,ResourceConstants.SA_IMG_VIDEO_TABLE_CELL_THUMB);
		}
		else if(data instanceof SANewsListItem){
			SANewsListItem item = (SANewsListItem)data;
			
			// set omission string
			int nLimitLength = 40;
			if(item.m_title.length() > nLimitLength)
			{
				this.m_title = item.m_title.substring(0, nLimitLength - 2).concat("..."); 
			}
			else
				this.m_title = item.m_title;
			
			// produce time string
			this.m_time = Utils.getStringFromDate(Utils.getDateFromString(item.m_date));
			
			this.m_index = index;
			this.m_type = type;
			
			//TODO - set thumbnail image with url from web Server
			this._imageThumbnail = new CustomBitmapField(item.m_imageLink, null, 10, 10, Utils.toDPIx(m_imageWidth), Utils.toDPIy(94),Manager.FIELD_LEFT| Field.FOCUSABLE ,ResourceConstants.SA_IMG_VIDEO_TABLE_CELL_THUMB);
		}
		else if(data instanceof SAVideoListItem){
			SAVideoListItem item = (SAVideoListItem)data;
			
			// set omission string
			int nLimitLength = 40;
			if(item.m_title.length() > nLimitLength)
			{
				this.m_title = item.m_title.substring(0, nLimitLength - 2).concat("..."); 
			}
			else
				this.m_title = item.m_title;
			
			// produce time string
			if(item.m_creationDate != null)
				this.m_time = Utils.getStringFromDate(item.m_creationDate);
			else
				this.m_time = "";
			
			this.m_index = index;
			this.m_type = type;
			
			//TODO - set thumbnail image with url from web Server
			this._imageThumbnail = new CustomBitmapField(item.m_defaultImageUrl, ResourceConstants.SA_ICON_VIDEO_PLAY, 10, 10, Utils.toDPIx(m_imageWidth), Utils.toDPIy(94),Manager.FIELD_LEFT| Field.FOCUSABLE,ResourceConstants.SA_IMG_VIDEO_TABLE_CELL_THUMB);
		}
		initCellLayout();
		
		this.m_dataObj = data;
	}
	public void initCellLayout()
	{
								
			if(this.m_index % 2 == 0)
				background = BackgroundFactory.createSolidBackground(Color.BLACK);
			else
				background = BackgroundFactory.createSolidBackground(0x8b8b83);
			
			this.setBackground(background);

			pressedBack = BackgroundFactory.createSolidTransparentBackground(Color.LIGHTBLUE, 200);
			
			this.add(this._imageThumbnail);
			
			int labelWidth = Utils.toDPIx(m_labelWidth);
			
			//set Labels
			int nTopMargin = Utils.toDPIy(10);
			Font titleFont = Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE);
			String strTitle = Utils.getOmittedString(this.m_title, labelWidth - 10, 60, titleFont);
			CustomLabelField titleLabel = new CustomLabelField(strTitle, Color.WHITE, titleFont, Utils.toDPIx(10), nTopMargin, labelWidth, Field.FIELD_LEFT);
			
			// calculate margin
			Font dateFont = Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL);
			
			int x = Utils.toDPIy(94);
			int y = titleLabel.getPreferredHeight();
			int z = dateFont.getHeight();
			int marginY = x - y - z + nTopMargin;
			
			CustomLabelField dateLabel = new CustomLabelField(this.m_time, Color.DARKGRAY,  dateFont, Utils.toDPIx(10), marginY, labelWidth, Field.FIELD_LEFT);
			
			VerticalFieldManager cellLabelVerticalManager = new VerticalFieldManager(Field.USE_ALL_WIDTH | Field.FIELD_LEFT);
			
			cellLabelVerticalManager.add(titleLabel);
			cellLabelVerticalManager.add(dateLabel);
			
			this.add(cellLabelVerticalManager);
			this.setPadding(0, 0, 10, 0);
	}
	
	
	/**
     * Event handler
     */
	
    protected boolean navigationClick(int status, int time) {
    	if (status != 0){
    		fieldChangeNotify(0);
    		clickButton();
    	}
        return true;
    }

    public boolean keyChar(char key, int status, int time) {
        if (key == Characters.ENTER) {
            fieldChangeNotify(0);
            clickButton();
            return true;
        }
        return false;
    }
    
    protected boolean trackwheelClick(int status, int time)
    {        
        if (status != 0) clickButton(); 
        return true;
    }
    
	protected boolean invokeAction(int action) 
    {
        switch( action ) {
            case ACTION_INVOKE: {
                clickButton(); 
                return true;
            }
        }
        return super.invokeAction(action);
    }
	
	protected boolean touchEvent(TouchEvent message)
	 {
		int x = message.getX( 1 );
        int y = message.getY( 1 );
        if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
            // Outside the field
        	if(refreshed)
        		refreshBackGround(false);
            return false;
        }
        else{
        	
		   	if(message.getEvent() == TouchEvent.DOWN)
		  	{	
		   		 refreshBackGround(true);
			}
		   	else if(message.getEvent() == TouchEvent.UP){
		   		refreshBackGround(false);
		   	}
		   	if(message.getEvent() == TouchEvent.CLICK){
		   		clickButton();
		   	}
		}
	    return super.touchEvent(message); 
   }
	public void refreshBackGround(boolean flag){
		if(flag == true){
			this.setBackground(pressedBack);
			refreshed = true;
		}else
		{	
			this.setBackground(background);
			refreshed = false;
		}
	}
	
	public void clickButton(){
    	//Dialog.alert("Clicked!");
    	fieldChangeNotify(0);

        StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
        MainScreen curScreen = (MainScreen) app.getActiveScreen(); 
           if(curScreen == app.mHomeScreen)
	       {
        	   SAFeatureListItem featureListItem = (SAFeatureListItem)m_dataObj;
	    	  	if(m_type == 1)
	    	  		app.gotoNewsDetailScreen(app.mHomeScreen, featureListItem);
	    	  	else if(m_type == 0)
	    	  	{
	    	  		SAVideoListItem item = new SAVideoListItem();
	    	  		item.m_defaultImageUrl = featureListItem.m_imageUrl;
	    	  		item.m_title = featureListItem.m_title;
	    	  		item.m_description = featureListItem.m_description;
	    	  		item.m_iPhoneVideoUrl = featureListItem.m_iphoneUrl;
	    	  		item.m_iPadVideoUrl = featureListItem.m_ipadUrl;
	    	  		item.m_id = featureListItem.m_mediaID;
	    	  		item.m_creationDate = Utils.getDateFromString(featureListItem.m_date);
	    	  		
	    	  		app.gotoVideoDetailScreen(app.mHomeScreen, item);
	    	  	}
	        }
	        else if(curScreen.getClass() == VideoListScreen.class){
	        	VideoListScreen _curScreen = (VideoListScreen) curScreen;
	        	SAVideoListItem videoListItem = (SAVideoListItem)m_dataObj;
	            app.gotoVideoDetailScreen(_curScreen, videoListItem);
	        }
	        else if(curScreen.getClass() == FixtureAnalysisScreen.class){
	        	FixtureAnalysisScreen _curScreen = (FixtureAnalysisScreen) curScreen;
	        	SAVideoListItem inMatchClipsItem = (SAVideoListItem)m_dataObj;
	        	app.gotoVideoDetailScreen(_curScreen, inMatchClipsItem);
	        }
	        else if(curScreen.getClass() == NewsListScreen.class)
	        {
	        	NewsListScreen _curScreen = (NewsListScreen) curScreen;
	        	SANewsListItem newsListItem = (SANewsListItem)m_dataObj;
	        	app.gotoNewsDetailScreen(_curScreen, newsListItem);
	        }
           refreshBackGround(false);
    }
	
	protected void onFocus(int direction)
	{
		int oldPos = 0;
		StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
		if(AppSession.sharedSession().m_showAdsBar)
			oldPos = ResourceConstants.SA_VAL_ADS_BAR_HEIGHT;
		if(app.getActiveScreen() == app.mHomeScreen)
			oldPos += ResourceConstants.SA_VAL_MATCHTAB_HEIGHT +ResourceConstants.SA_VAL_HIGHLIGHT_BAR_TOTAL_HEIGHT;
		this.getManager().getManager().setVerticalScroll(oldPos + m_index * this.getHeight() , true);
		refreshBackGround(true);
	}
	
	protected void onUnfocus()
	{
//		if(getFieldWithFocusIndex() == 0)
//			this.getManager().getManager().setVerticalScroll(0 , true);
		refreshBackGround(false);
	}
}
