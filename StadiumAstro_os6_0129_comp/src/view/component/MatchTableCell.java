package view.component;

import model.SASoccerMatchListItem;
import application.ResourceConstants;
import application.StadiumAstroApp;
import application.Utils;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;

/**
 * 
 * @author Administrator
 *
 *	This class implements a cell which needs Match Screen's bottom TableView and shows match info by date.
 */
public class MatchTableCell extends HorizontalFieldManager {
	
	boolean refreshed = false;
	Background background;
	Background pressedBack;
	int m_index;
	SASoccerMatchListItem m_matchInfo = null;
	/**
	 * 
	 * @param mInfo - SAMatchList info from Web
	 * @param matchIndex - Index of cell that shall be send to Analysis page. 
	 */
	public MatchTableCell(SASoccerMatchListItem mInfo,int index){

		super(Field.FOCUSABLE);
		
		this.m_index = index;
		this.m_matchInfo = mInfo;

		initCellLayout();
		
	}
	public void initCellLayout()
	{
		try{
		Bitmap backImage = Utils.getBitmapResource(ResourceConstants.SA_IMG_FIXTURE_MATCH_TABLE_CELL);
		background = BackgroundFactory.createBitmapBackground(backImage);
		this.setBackground(background);
		
		pressedBack = BackgroundFactory.createBitmapBackground(Utils.getBitmapResource(ResourceConstants.SA_IMG_FIXTURE_MATCH_TABLE_CELL_PRESSED));

		add(new NullField(NullField.FOCUSABLE));
		//1. home team Logo 

		CustomVerticalFieldManager homeTeamManager = new CustomVerticalFieldManager(ResourceConstants.SA_VAL_FIXTURE_MATCH_CELL_OPPONENT_WIDTH,ResourceConstants.SA_VAL_FIXTURE_MATCH_CELL_HEIGHT,Manager.FIELD_LEFT);
		homeTeamManager.setMargin(0, 0, 0, Utils.toDPIx(34));
		{
			CustomBitmapField homeTeamLogoImage = new CustomBitmapField(m_matchInfo.m_homeTeamLogo ,null,0,Utils.toDPIy(10), Utils.toDPIx(93), Utils.toDPIy(100),Manager.FIELD_LEFT,null);
			
			CustomLabelField homeTeamAliasLabel = new CustomLabelField(m_matchInfo.m_homeTeamAlias,Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , -5, Utils.toDPIy(10) , Utils.toDPIx(100),  Field.FIELD_LEFT);
			homeTeamAliasLabel.setTextAlign(DrawStyle.HCENTER);
			
			homeTeamManager.add(homeTeamLogoImage);
			homeTeamManager.add(homeTeamAliasLabel);
		}
		add(homeTeamManager);
		
		//2. Score Display View 
		CustomVerticalFieldManager scoreViewManager = new CustomVerticalFieldManager(ResourceConstants.SA_VAL_FIXTURE_MATCH_CELL_SCORE_WIDTH,ResourceConstants.SA_VAL_FIXTURE_MATCH_CELL_HEIGHT,Manager.FIELD_LEFT);
		
		{
			//2-1. match Status Label
			String dateStr = Utils.getStringFromDate1(m_matchInfo.m_dateCalendar);
			if (m_matchInfo.m_matchStatus.equals("Played")) {
				CustomLabelField matchStatusLabel = new CustomLabelField("Full Time",Color.BLACK,Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , Utils.toDPIx(80), Utils.toDPIy(10) , Utils.toDPIx(300),  Field.FIELD_LEFT);
				scoreViewManager.add(matchStatusLabel);
			}else{
				CustomLabelField matchStatusLabel = new CustomLabelField(dateStr,Color.BLACK,Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , Utils.toDPIx(80), Utils.toDPIy(10) , Utils.toDPIx(300),  Field.FIELD_LEFT);
				scoreViewManager.add(matchStatusLabel);
				
			}
			//2-2. Score Horizontal View
			CustomHorizontalFieldManager scoreManager = new CustomHorizontalFieldManager(ResourceConstants.SA_VAL_FIXTURE_MATCH_CELL_SCORE_WIDTH,ResourceConstants.SA_VAL_FIXTURE_MATCH_CELL_SCORE_TOP_VIEW_HEIGHT,Manager.FIELD_LEFT);
			
			{
				String strHomeTeamScore = "";
				String strAwayTeamScore = "";
				String strCenterSymbol = "";
				
				if (m_matchInfo.m_matchStatus.equals("Played")) {
					strHomeTeamScore = m_matchInfo.m_homeTeamScore;
					strAwayTeamScore = m_matchInfo.m_visitingTeamScore;
					strCenterSymbol = "-";
					CustomLabelField homeTeamScoreLabel = new CustomLabelField(strHomeTeamScore,Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_LARGE_MIDDLE) , Utils.toDPIx(30), 0 , Utils.toDPIx(100),  Field.FIELD_LEFT);
					CustomLabelField lineLabel = new CustomLabelField(strCenterSymbol,Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_LARGE) , Utils.toDPIx(20), 0 , Utils.toDPIx(50),  Field.FIELD_LEFT);
					CustomLabelField visitingTeamScoreLabel = new CustomLabelField(strAwayTeamScore,Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_LARGE_MIDDLE) , Utils.toDPIx(45), 0 , Utils.toDPIx(150),  Field.FIELD_LEFT);
					
					scoreManager.add(homeTeamScoreLabel);
					scoreManager.add(lineLabel);
					scoreManager.add(visitingTeamScoreLabel);
				}
				else {
					CustomLabelField homeTeamScoreLabel = new CustomLabelField("vs",Color.BLACK,Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_BIG_LARGE) , Utils.toDPIx(80), Utils.toDPIx(0) , Utils.toDPIx(240),  Field.FIELD_LEFT);
					
					scoreManager.add(homeTeamScoreLabel);
				}
				
				
			}
			scoreViewManager.add(scoreManager);
			
			//2-3. Competition Stadium Place Label 
			{
//				String matchPlaceStr = mInfo.m_stadiumCity + " " + mInfo.m_stadiumName;
				String matchPlaceStr = m_matchInfo.m_channelInfo;
				int fontSize = ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE;
	            int labelWidth = Utils.getTypeFace().getFont(Font.BOLD, fontSize).getBounds(matchPlaceStr);
	            int marginX = (ResourceConstants.SA_VAL_FIXTURE_MATCH_CELL_SCORE_WIDTH - labelWidth) / 2;
	            
				CustomLabelField matchPlaceLabel = new CustomLabelField(matchPlaceStr,Color.BLACK,Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , marginX, 0 , Utils.getDisplayWidth(),  Field.FIELD_BOTTOM | Field.FIELD_HCENTER);
				scoreViewManager.add(matchPlaceLabel);
				
				
			}
			
			
		}
		add(scoreViewManager);
		
				
		//3. visiting team Logo 
		CustomVerticalFieldManager opponentTeamManager = new CustomVerticalFieldManager(ResourceConstants.SA_VAL_FIXTURE_MATCH_CELL_OPPONENT_WIDTH,ResourceConstants.SA_VAL_FIXTURE_MATCH_CELL_HEIGHT,Manager.FIELD_HCENTER);
		{
			CustomBitmapField visitingTeamLogoImage = new CustomBitmapField(m_matchInfo.m_visitingTeamLogo,null,Utils.toDPIx(40),Utils.toDPIy(10), Utils.toDPIx(93), Utils.toDPIy(100),Manager.FIELD_HCENTER,null);
			CustomLabelField visitingTeamAliasLabel = new CustomLabelField(m_matchInfo.m_visitingTeamAlias,Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , Utils.toDPIx(30), Utils.toDPIy(10) , Utils.toDPIx(150),  Field.FIELD_HCENTER);
			visitingTeamAliasLabel.setTextAlign(DrawStyle.HCENTER);
			
			opponentTeamManager.add(visitingTeamLogoImage);
			opponentTeamManager.add(visitingTeamAliasLabel);
		}
		add(opponentTeamManager);
		
		CustomBitmapField arrowBitmap  = new CustomBitmapField(ResourceConstants.SA_IMG_CELL_RIGHT_ARROW_ICON, null, 10 ,0, 0, 0, Manager.FIELD_RIGHT|Manager.FIELD_VCENTER,null);
		add(arrowBitmap);
		add(new NullField(NullField.FOCUSABLE));
		}catch(Exception e){
			add(new NullField(NullField.FOCUSABLE));
			System.out.println("aaaaaaaa");
		}
				
	}
	protected void sublayout(int maxWidth, int maxHeight) {
		maxWidth = Utils.getDisplayWidth();
		maxHeight = ResourceConstants.SA_VAL_FIXTURE_MATCH_CELL_HEIGHT;
		super.sublayout(maxWidth, maxHeight);
		setExtent(maxWidth, maxHeight);
	}
	
	/**
     * Event handler
     */
	
    protected boolean navigationClick(int status, int time) {
    	if (status != 0){
    		fieldChangeNotify(0);
    		clickButton();
    	}
        return true;
    }

    
    /*public boolean isFocusable() {
    	// TODO Auto-generated method stub
    	return true;
    }*/
    public boolean keyChar(char key, int status, int time) {
        if (key == Characters.ENTER) {
            fieldChangeNotify(0);
            clickButton();
            return true;
        }
        return false;
    }
    protected boolean trackwheelClick(int status, int time)
    {        
        if (status != 0) clickButton(); 
        return true;
    }
	protected boolean invokeAction(int action) 
    {
        switch( action ) {
            case ACTION_INVOKE: {
                clickButton(); 
                return true;
            }
        }
        return super.invokeAction(action);
    }
	
	protected boolean touchEvent(TouchEvent message)
	 {
		int x = message.getX( 1 );
        int y = message.getY( 1 );
        if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
            // Outside the field
        	if(refreshed)
        		refreshBackGround(false);
            return false;
        }
        else{
        	
		   	if(message.getEvent() == TouchEvent.DOWN)
		  	{	
		   		 refreshBackGround(true);
			}
		   	else if(message.getEvent() == TouchEvent.UP){
		   		refreshBackGround(false);
		   	}
		   	if(message.getEvent() == TouchEvent.CLICK){
		   		clickButton();
		   	}
		}
	    return super.touchEvent(message); 
   }
	
	public void refreshBackGround(boolean flag){
		if(flag == true){
			this.setBackground(pressedBack);
			refreshed = true;
		}else{
			refreshed = false;
			this.setBackground(background);
		}
	}
	
	public void clickButton(){
    	fieldChangeNotify(0);
//
        StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
        app.gotoFixtureAnalysisScreen((MainScreen) app.getActiveScreen(), m_matchInfo);
        refreshBackGround(false);
    }
	
	protected void onFocus(int direction){
		this.getManager().getManager().setVerticalScroll(ResourceConstants.SA_VAL_FIXTURE_CALENDAR_BAR_HEIGHT + ResourceConstants.SA_VAL_FIXTURE_DATE_BAR_HEIGHT + m_index * this.getHeight(),true);
		refreshBackGround(true);
	}
	protected void onUnfocus(){
		refreshBackGround(false);
	}
}
