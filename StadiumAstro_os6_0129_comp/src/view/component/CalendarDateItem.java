package view.component;

import java.util.Calendar;
import java.util.Date;

import view.screen.FixtureMatchScreen;
import application.ResourceConstants;
import application.StadiumAstroApp;
import application.Utils;
import model.SACalendarItem;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.io.http.HttpDateParser;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;

/**
 * 
 * @author Administrator
 *
 *	This class is a CalenDar Date Item which needs in Matches Screen and show date info.
 *	This class set contents data from SACalendarList Model Class.
 */

public class CalendarDateItem extends VerticalFieldManager {
	
	private String m_year;
	private String m_month;
	private String m_day;
	private String m_strID;
	private boolean m_hasMatch;
	
	int m_index;
	
	Background m_normalBack;
	Background m_pressedBack;

	boolean m_bSelected = false;
	SACalendarItem m_calendarItem = null;
	
	CustomLabelField m_monthLabel = null;
	CustomLabelField m_dayLabel = null;
	
	int m_width, m_height;
	boolean activeflg = false;
	
	public CalendarDateItem(SACalendarItem calendarItem,int index,long style, boolean activeflg){
		
		super(style);
		this.m_index = index;
		this.m_year = calendarItem.m_year;
		this.m_month = calendarItem.m_month;
		this.m_day = calendarItem.m_day;
//		this.m_isActive = calendarItem.m_isActive;
		this.m_hasMatch = calendarItem.m_hasMatch;
		this.m_strID = calendarItem.m_strId;
		this.m_calendarItem = calendarItem;
		this.activeflg = activeflg;
		initSubItems();
	}
	public void initSubItems(){
		
		Bitmap backImage;
		if(this.isToday())
			backImage = Utils.getBitmapResource(ResourceConstants.SA_IMG_FIXTURE_CALENDAR_DATE_BUTTON_2);
		else
			backImage = Utils.getBitmapResource(ResourceConstants.SA_IMG_FIXTURE_CALENDAR_DATE_BUTTON_1);
		
		m_normalBack = BackgroundFactory.createBitmapBackground(backImage);
		m_pressedBack = BackgroundFactory.createBitmapBackground(Utils.getBitmapResource(ResourceConstants.SA_IMG_FIXTURE_CALENDAR_DATE_BUTTON_2));
		
		this.setBackground(m_normalBack);
		
		m_width = backImage.getWidth();
		m_height = backImage.getHeight();

		MatchCalendarScroll.width = m_width;
		int fontSize = ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE;
        int labelWidth = Utils.getTypeFace().getFont(Font.PLAIN, fontSize).getBounds(this.m_month);
        int marginX = (m_width - labelWidth) / 2;
		m_monthLabel = new CustomLabelField(this.m_month, Color.WHITE, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , marginX, Utils.toDPIy(10), 100, Field.FIELD_LEFT);
		
		
		String dayStr = this.m_day;
		if(dayStr.charAt(0) == '0'){
			this.m_day = dayStr.substring(1);
		}else{
			this.m_day = dayStr;
		}
		
		fontSize = ResourceConstants.SA_VAL_FONT_SIZE_LARGE;
        labelWidth = Utils.getTypeFace().getFont(Font.PLAIN, fontSize).getBounds(this.m_day);
        marginX = (m_width - labelWidth) / 2;
              
        m_dayLabel = new CustomLabelField(this.m_day, m_hasMatch? Color.BLACK : Color.GRAY, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_LARGE) , marginX, Utils.toDPIy(20), 100, Field.FIELD_LEFT);
		add(m_monthLabel);
		add(m_dayLabel);
		
		marginX = (m_width - Utils.getBitmapResource(ResourceConstants.SA_ICON_FIXTURE_DOT_BLUE).getWidth())/2;
		if(this.m_hasMatch){
			CustomBitmapField matchDot;
			if(!activeflg){
				matchDot = new CustomBitmapField(ResourceConstants.SA_ICON_FIXTURE_DOT_WHITE,null, marginX, 0, 0, 0, Manager.FIELD_LEFT | Field.FOCUSABLE,null);
			}else{
				matchDot = new CustomBitmapField(ResourceConstants.SA_ICON_FIXTURE_DOT_BLUE,null, marginX, 0, 0, 0, Manager.FIELD_LEFT | Field.FOCUSABLE,null);
			}
			add(matchDot);
		}
	}
	public boolean isToday(){
		// get today calendar object
		Calendar cal = Calendar.getInstance();
		String strYear = String.valueOf(cal.get(Calendar.YEAR));
		String strMonth = Utils.getMonthString(cal.get(Calendar.MONTH));
		String strMonth1 = String.valueOf(cal.get(Calendar.MONTH)+1);
		String strDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
		/*String[] a = new String[]{"","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
		String m_sam = "";
		for(int i = 1; i < a.length; i++){
			if(a[i].equals(m_month)){
				m_sam = "" + i; break;
			}
		}
		String c = m_year+"-"+m_sam+"-"+m_day;
		String now = strYear+"-"+strMonth1+"-"+strDay;
		long timeDiff = getTimeDifference(now, c);
		*/
		
		if(m_year.equals(strYear) && m_month.equals(strMonth) && m_day.equals(strDay))
		//if(timeDiff > 0)
			return true;
		return false;		
	}

	public boolean isToday1(){
		// get today calendar object
		Calendar cal = Calendar.getInstance();
		String strYear = String.valueOf(cal.get(Calendar.YEAR));
		String strMonth = Utils.getMonthString(cal.get(Calendar.MONTH));
		String strMonth1 = String.valueOf(cal.get(Calendar.MONTH)+1);
		if(strMonth1.length() == 1){
			strMonth1 = "0"+strMonth1;
		}
		String strDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH) + 1);
		String[] a = new String[]{"","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
		String m_sam = "";
		for(int i = 1; i < a.length; i++){
			if(a[i].equals(m_month)){
				m_sam = "" + i; 
				if(m_sam.length() == 1){
					m_sam = "0"+ m_sam;
				}
				break;
			}
		}
		String c = m_year+"-"+m_sam+"-"+m_day;
		String now = strYear+"-"+strMonth1+"-"+strDay;
		long timeDiff = getTimeDifference(now, c);
		
		
		if(timeDiff > 0)
		//if(m_year.equals(strYear) && m_month.equals(strMonth) && m_day.equals(strDay))
			return false;
		else 
			return true;	
	}
	public long getTimeDifference(String timestamp1,
			   String timestamp2) {
			  long time1 = getTime(timestamp1);
			  long time2 = getTime(timestamp2);
			  return time2 - time1;
	}
	
	 public long getTime(String time) {
		  Date formatter = new Date(HttpDateParser.parse(time));
		  return formatter.getTime();
	}
	protected void sublayout(int maxWidth, int maxHeight) {
		maxWidth = m_width;
		maxHeight = m_height;
		super.sublayout(maxWidth, maxHeight);
		this.setExtent(maxWidth , maxHeight);
	}
	
	/**
     * Event handler
     */
	
    protected boolean navigationClick(int status, int time) {
    	if (status != 0){
    		fieldChangeNotify(0);
    		clickButton();
    	}
        return true;
    }

    public boolean keyChar(char key, int status, int time) {
        if (key == Characters.ENTER) {
            fieldChangeNotify(0);
            clickButton();
            return true;
        }
        return false;
    }
    protected boolean trackwheelClick(int status, int time)
    {        
        if (status != 0) clickButton(); 
        return true;
    }
	protected boolean invokeAction(int action) 
    {
        switch( action ) {
            case ACTION_INVOKE: {
                clickButton(); 
                return true;
            }
        }
        return super.invokeAction(action);
    }
	
	protected boolean touchEvent(TouchEvent message)
	 {
		if(this.m_hasMatch){
			int x = message.getX( 1 );
	        int y = message.getY( 1 );
	        if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
	            // Outside the field
	            return false;
	        }
	        if(!m_bSelected){
			   	if(message.getEvent() == TouchEvent.CLICK)
			  	{	
			   		refreshSelectedItem(false);
			   		clickButton();
				}
			}
		}

	    return super.touchEvent(message); 
   }
	
	public void refreshBackGround(boolean flag){
		try{
			if(flag == true){
				this.setBackground(m_pressedBack);
				m_dayLabel.setColor(Color.WHITE);
			}else{
				m_dayLabel.setColor(Color.BLACK);
				m_bSelected = false;
				this.setBackground(m_normalBack);
			}
		}catch(Exception e){
			
		}
	}
	public void refreshSelectedItem(boolean flag){
		StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
		FixtureMatchScreen screen = (FixtureMatchScreen)this.getScreen();
		if(screen.m_dateItemsScroll.m_curSelectedItemView != null)
			screen.m_dateItemsScroll.m_curSelectedItemView.refreshBackGround(flag);
	}
	
	public void clickButton(){
		MatchCalendarScroll.m_firstSelect = m_index;
    	fieldChangeNotify(0);
    	StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
    	FixtureMatchScreen screen = (FixtureMatchScreen)this.getScreen();
    	
    	refreshSelectedItem(false);
    	
    	screen.m_dateItemsScroll.m_curSelectedItemView = this;
    	m_bSelected = true;
    	refreshBackGround(true);
    	
    	//TODO - Filter by date processing...
    	
    	
    	screen.m_currentCalendarItem = m_calendarItem;
    	screen.showMatchTable(m_strID);
    }
	
	protected void onFocus(int direction)
	{
		int posX = m_index * m_width - Utils.getDisplayWidth() / 2;
		if(m_index <= 0) posX = 0;
				
		boolean bAnimation = true;
		
		if(((direction == -1) && (getIndex() == getManager().getFieldCount() - 1))
				 || ((direction == 1) && (getIndex() == 0)) || (getIndex() == getManager().getFieldCount() - 1))
		{
			bAnimation = false;
			if(m_index == getManager().getFieldCount() - 1) 
				posX += (m_width  - Utils.getDisplayWidth() / 2);
		}
		
		this.getManager().setHorizontalScroll(posX,bAnimation);
		MatchCalendarScroll.m_scrollSelect = m_index;
		refreshBackGround(true);
	}
	
	protected void onUnfocus()
	{
		if(!m_bSelected)
			refreshBackGround(false);
	}
}
