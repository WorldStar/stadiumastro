package view.component;



import application.ResourceConstants;
import application.Utils;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.UiApplication;

import net.rim.device.api.ui.component.progressindicator.ActivityIndicatorController;
import net.rim.device.api.ui.component.progressindicator.ActivityIndicatorModel;
import net.rim.device.api.ui.component.progressindicator.ActivityIndicatorView;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class LoadingDialog extends PopupScreen{
	
	ActivityIndicatorView m_view;
	ActivityIndicatorModel m_model;
	ActivityIndicatorController m_controller;
	CustomLabelField m_label;
	int m_width;
	public static LoadingDialog dialog;
	public LoadingDialog(String text){
		super(new CustomVerticalFieldManager(Utils.toDPIx(200), Utils.toDPIy(100), VerticalFieldManager.VERTICAL_SCROLL | VerticalFieldManager.VERTICAL_SCROLLBAR));
		
		// TODO Auto-generated constructor stub}
		m_label = new CustomLabelField(text,Color.WHITE,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) ,0 , 0 ,Utils.toDPIx(120), Field.FIELD_HCENTER);
		m_label.setTextAlign(DrawStyle.HCENTER);
		
		m_view = new ActivityIndicatorView(Field.USE_ALL_WIDTH);
		m_model = new ActivityIndicatorModel();
		m_controller = new ActivityIndicatorController(); 
        
        m_view.setController(m_controller);
        m_view.setModel(m_model);
        m_view.setMargin(Utils.toDPIy(20), 0, 0, 0);

        m_controller.setModel(m_model);
        m_controller.setView(m_view);

        m_model.setController(m_controller);        

        Bitmap bitmap = Bitmap.getBitmapResource("img/progress/spinner.png");
        m_view.createActivityImageField(bitmap, 5, Field.FIELD_HCENTER);
        
        add(m_label);
        add(m_view);
	}
	
	public static LoadingDialog showLoadingDialog(final Runnable runThis ,String text)
	{
		final LoadingDialog m_instance = new LoadingDialog(text);
		dialog = m_instance;
		new Thread(new Runnable()
		{
			public void run()
			{
				if(m_instance != null)
				{
					UiApplication app = UiApplication.getUiApplication();
					app.invokeLater(new Runnable(){
						public void run()
						{
							UiApplication.getUiApplication().pushScreen(m_instance);
						}
					});
					runThis.run();
					app.invokeLater(new Runnable(){
						public void run()
						{
							m_instance.closeDialog();
						}
					});
				}
			}
		}
		).start();
		
		return m_instance;
	}
	
	public void closeDialog()
	{
		UiApplication.getUiApplication().popScreen(this);
	}
}
