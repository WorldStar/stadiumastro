package view.component;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import application.AppSession;
import model.MoreMenuItem;
import model.SASportEventContentItem;
import model.SASportEventContentList;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Menu;
import net.rim.device.api.ui.menu.SubMenu;

/**
 * 
 * @author Star
 * 
 */
public class MainMenu
{
	SASportEventContentList m_sportEventContentList = null;
	LoadingDialog m_loadingDlg;
	public MainMenu()
	{
		LoadingDialog.showLoadingDialog(new RInitMainMenu(), "Loading");
	}
	
	public void getMenuFromModelData(Hashtable table, Menu parentMenu)
	{
		for(Enumeration e = table.keys(); e.hasMoreElements(); )
		{
			Object objSubMenuKey = e.nextElement();
			Object objSubMenu = table.get(objSubMenuKey);
			
			SubMenu subMenu = new SubMenu(null, objSubMenuKey.toString(), 0 , 0);
			
			if(objSubMenu instanceof Vector)
			{
				// create menu items
				Vector vecSubMenu = (Vector)objSubMenu;
				
				int nCount = vecSubMenu.size();
				
				for(int i = 0; i < nCount; i++)
				{
					Object objMenuItem = vecSubMenu.elementAt(i);
					
					if(objMenuItem instanceof SASportEventContentItem)
					{
						SASportEventContentItem sportEventItem = (SASportEventContentItem)objMenuItem;
						subMenu.add(new CustomMenuItem(sportEventItem.m_sportsEventName, 0, 0, sportEventItem.m_sportsEventIconUrl, sportEventItem));
						if(i != nCount - 1)
							subMenu.addSeparator();
					}
					else if(objMenuItem instanceof MoreMenuItem)
					{
						MoreMenuItem moreMenuItem = (MoreMenuItem)objMenuItem;
						subMenu.add(new CustomMenuItem(moreMenuItem.m_title, 0, 0, moreMenuItem.m_iconUrl, moreMenuItem));
						if(i != nCount - 1)
							subMenu.addSeparator();
					}
				}
			}
			else if(objSubMenu instanceof Hashtable)
			{
				// create sub menu
				getMenuFromModelData((Hashtable)objSubMenu, subMenu);
			}
			
			if(parentMenu == null)
			{
				// save data to app session
				// Add to sub menu.
				AppSession.sharedSession().m_mainSubMenus.addElement(subMenu);
			}
			else
				// Add to parent menu.
				parentMenu.add(subMenu);
		}		
	}
	
	class RInitMainMenu implements Runnable
	{
		public RInitMainMenu()
		{
		}
		public void run()
		{
			m_sportEventContentList = new SASportEventContentList();
			UiApplication.getUiApplication().invokeLater(new Runnable(){
				public void run()
				{
					getMenuFromModelData(m_sportEventContentList.m_menuItemTable, null);
					CustomMenuItem item = new CustomMenuItem("Refresh", 0, 0, "", null);
					AppSession.sharedSession().m_mainSubMenus.addElement(item);
				}
			});
			
		}
	}
}
