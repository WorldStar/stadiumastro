package view.component;

import application.Utils;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
/**
 * 
 * @author Administrator
 *	The Object of making this class is same of CustomHorizontalFieldManager.
 */
public class CustomVerticalFieldManager extends VerticalFieldManager {

	int m_maxWidth;
	int m_maxHeight;
	public CustomVerticalFieldManager(long style){
		super(style);
		m_maxWidth = Utils.getDisplayWidth();
		m_maxHeight = Utils.getDisplayHeight();
	}
	public CustomVerticalFieldManager(int maxWidth,int maxHeight,long style){
		super(style);
		m_maxWidth = maxWidth;
		m_maxHeight = maxHeight;
		//setBackground(BackgroundFactory.createSolidBackground(0xff0000));
	}
	protected void sublayout(int maxWidth, int maxHeight) {
		// TODO Auto-generated method stub
		maxWidth =  m_maxWidth;
		maxHeight = m_maxHeight;
		super.sublayout(maxWidth, maxHeight);
		this.setExtent(maxWidth , maxHeight);
	}
}
