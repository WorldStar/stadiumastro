package view.component;

import view.components.CustomButton;
import view.screen.BaseScreen;
import view.screen.FixtureMatchScreen;
import view.screen.HelpScreen;
import view.screen.InfoScreen;
import view.screen.NewsListScreen;
import view.screen.NewsMainScreen;
import view.screen.TVChannelDetailScreen;
import view.screen.TVChannelListScreen;
import view.screen.VideoListScreen;
import view.screen.VideoMainScreen;
import application.ResourceConstants;
import application.StadiumAstroApp;
import application.Utils;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.decor.*;
import net.rim.device.api.ui.*;

/**
 * 
 * @author Administrator
 *	This class is Navigation Bar that contains leftItem of BackButton and centerItem of TitleLabel.
 *	
 */

public class NavigationBar extends HorizontalFieldManager {
	
	private Bitmap _backImage = null;
	private String _title = null;
	private CustomLabelField m_titleLabel = null;
	private CustomButton m_btnHelp = null;
	private CustomButton m_btnRefresh = null;
	
	private int nRightPadding = (int)(15 * Utils.getVRatio());
	private BaseScreen parentScreen;
	public NavigationBar(String title, BaseScreen parent){
		
		super(Manager.NO_VERTICAL_SCROLL);
		parentScreen = parent;
		setTitle(title);
	}
	
	protected void sublayout(int maxWidth, int maxHeight) {
		// TODO Auto-generated method stub
		maxWidth =  Utils.getDisplayWidth();
		maxHeight = _backImage.getHeight();
		
		if (_title == " " ) {
			Field fField = this.getField(0);
			layoutChild(fField, maxWidth, maxWidth);
			setPositionChild(fField, maxWidth - nRightPadding - fField.getPreferredWidth(), (int)(maxHeight - fField.getPreferredHeight()) / 2);
		}
		else if(_title == "Analysis" || parentScreen instanceof InfoScreen
				|| _title == "Standings" || parentScreen instanceof TVChannelDetailScreen || parentScreen instanceof TVChannelListScreen
				|| parentScreen instanceof NewsListScreen || parentScreen instanceof NewsMainScreen || parentScreen instanceof FixtureMatchScreen
				|| parentScreen instanceof VideoListScreen || parentScreen instanceof VideoMainScreen) {
			Field label = this.getField(0);
			layoutChild(label, maxWidth, maxWidth);
			setPositionChild(label, 0, 0);
			
			Field button = this.getField(1);
			layoutChild(button, maxWidth, maxWidth);
			setPositionChild(button, maxWidth - nRightPadding - button.getPreferredWidth(), (int)(maxHeight - button.getPreferredHeight()) / 2);
		}
		else {
			super.sublayout(maxWidth, maxHeight);
		}

		this.setExtent(maxWidth , maxHeight);
	}
	
	public void setTitle(String title)
	{
		String backImageUrl;
		
		if((title ==  "Home") || (title == "")){
			backImageUrl = ResourceConstants.SA_IMG_BAR_TOP_BLUE_HOME;
			_title = " ";
		}
		else{
			backImageUrl = ResourceConstants.SA_IMG_BAR_TOP_BLUE_NORMAL;
			_title = title;
		}
		
		_backImage = Utils.getBitmapResource(backImageUrl);
		Background background = BackgroundFactory.createBitmapBackground(_backImage);
		this.setBackground(background);
        
		
        if(title != "Home"){
        	
        	int fontSize = ResourceConstants.SA_VAL_NAVBAR_FONT_SIZE;
            int labelWidth = Utils.getTypeFace().getFont(Font.BOLD, fontSize).getBounds(_title);
            int marginX = (ResourceConstants.SA_VAL_NAVBAR_WIDTH - labelWidth) / 2;
            int marginY = (ResourceConstants.SA_VAL_NAVBAR_HEIGHT - fontSize ) / 2;
            
            if(m_titleLabel == null)
            {
            	m_titleLabel = new CustomLabelField(_title, Color.WHITE, Utils.getTypeFace().getFont(Font.BOLD, fontSize) , marginX , marginY , Utils.getDisplayWidth(), Manager.FIELD_HCENTER);
            	add(m_titleLabel);
            }
            else
            {
            	m_titleLabel.setLabel(title);
            }
        }
        
        
        if (title == "Home" || parentScreen instanceof InfoScreen) {
        	m_btnHelp = new CustomButton(ResourceConstants.SA_IMG_BTN_HELP_SEL, ResourceConstants.SA_IMG_BTN_HELP_UNSEL){
        		public void clickButton() {
        			StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
        			
        			if (app.mHelpScreen == null) 
        				app.mHelpScreen = new HelpScreen();
        			
        			app.pushScreen(app.mHelpScreen);
        		}
        	};
        	
        	add(m_btnHelp);
        }
        else if (title == "Analysis" || title == "Matches" || title == "Standings" || parentScreen instanceof TVChannelDetailScreen || parentScreen instanceof TVChannelListScreen
        		|| parentScreen instanceof NewsListScreen || parentScreen instanceof NewsMainScreen || parentScreen instanceof FixtureMatchScreen
        		|| parentScreen instanceof VideoListScreen || parentScreen instanceof VideoMainScreen) {
        	m_btnRefresh = new CustomButton(ResourceConstants.SA_IMG_BTN_REFRESH_SEL, ResourceConstants.SA_IMG_BTN_REFRESH_UNSEL){
        		public void clickButton() {
        	    	
        	    	StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
        	    	
        	    	MainScreen curScreen = (MainScreen) app.getActiveScreen(); 
        	    	//scroll up main scroll view
        	    	if(curScreen instanceof BaseScreen)
        	    	{
        	    		BaseScreen _curScreen = (BaseScreen) curScreen;
        	        	_curScreen.refresh();
        	    	}
        		}
        	};
        	
        	add(m_btnRefresh);
        }
        
        
	}

}
