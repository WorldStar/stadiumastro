package view.component;

import application.ResourceConstants;
import application.Utils;
import model.SAStandingListItem;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

/**
 * 
 * @author Administrator
 *	This is a cell class which shows in Standing Screen.
 */
public class StandingListCell extends HorizontalFieldManager {

	/**
	 * Standing Info Class Parameters..
	 * @param m_pos - position integer value
	 * @param teamLogo - TeamLogoImage URL
	 * @param m_teamName - Team Name
	 * @param m_played - played count of team
	 * @param m_won - won count of team
	 * @param m_draw - draw count of team
	 * @param m_lost - lost count of team
	 * @param m_goalDiff - goalsDiff of team
	 * @param m_points - points of team
	 * 
	 */
	public StandingListCell(SAStandingListItem standingInfo, long style) {
		// TODO - align icon left of cell. 
		super(style);
		
		Background background = BackgroundFactory.createSolidBackground(Color.WHITE);
		setBackground(background);

//		CustomLabelField posLabel = new CustomLabelField(standingInfo.m_pos,Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_SMALL) , Utils.toDPIx(20) , Utils.toDPIy(10) , 50 , Manager.FIELD_RIGHT); 
        
        CustomBitmapField teamLogoBitmap  = new CustomBitmapField(standingInfo.m_teamLogo, null, Utils.toDPIx(10), 0, ResourceConstants.SA_VAL_STANDING_CELL_LOGO_IMG_WIDTH,ResourceConstants.SA_VAL_STANDING_CELL_HEIGHT,Manager.FIELD_RIGHT,null);
		
        CustomLabelField teamNameLabel = new CustomLabelField(standingInfo.m_teamName,Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_SMALL) , 0 , Utils.toDPIy(10) , ResourceConstants.SA_VAL_STANDING_CELL_TEAM_LABEL_WIDTH , Manager.FIELD_RIGHT);
        
        CustomLabelField playedLabel = new CustomLabelField(standingInfo.m_played,Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_SMALL) , Utils.toDPIx(5) , Utils.toDPIy(10) , ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH , Manager.FIELD_RIGHT);
        
        CustomLabelField wonLabel = new CustomLabelField(standingInfo.m_won,Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_SMALL) , Utils.toDPIx(5) , Utils.toDPIy(10) , ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH , Manager.FIELD_RIGHT);
        
        CustomLabelField drawLabel = new CustomLabelField(standingInfo.m_draw,Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_SMALL) , Utils.toDPIx(5) , Utils.toDPIy(10) , ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH , Manager.FIELD_RIGHT);
        
        CustomLabelField lostLabel = new CustomLabelField(standingInfo.m_lost,Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_SMALL) , Utils.toDPIx(5) , Utils.toDPIy(10) , ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH , Manager.FIELD_RIGHT);
        
        CustomLabelField attackLabel = new CustomLabelField(standingInfo.m_goalAgainst,Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_SMALL) , Utils.toDPIx(5) , Utils.toDPIy(10) , ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH , Manager.FIELD_RIGHT);
        CustomLabelField foulLabel = new CustomLabelField(standingInfo.m_goalScored,Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_SMALL) , Utils.toDPIx(5) , Utils.toDPIy(10) , ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH , Manager.FIELD_RIGHT);
        
        CustomLabelField pointsLabel = new CustomLabelField(standingInfo.m_points,Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_SMALL) , Utils.toDPIx(5) , Utils.toDPIy(10) , ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH , Manager.FIELD_RIGHT);
        
//        CustomHorizontalFieldManager posLabelFieldManager = new CustomHorizontalFieldManager(ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH,ResourceConstants.SA_VAL_STANDING_CELL_HEIGHT,Manager.FIELD_HCENTER);
        
		CustomHorizontalFieldManager teamLogoFieldManager = new CustomHorizontalFieldManager(Utils.toDPIx(10) + ResourceConstants.SA_VAL_STANDING_CELL_LOGO_IMG_WIDTH,ResourceConstants.SA_VAL_STANDING_CELL_HEIGHT,Manager.FIELD_HCENTER);
		
		CustomHorizontalFieldManager teamLabelFieldManager = new CustomHorizontalFieldManager(ResourceConstants.SA_VAL_STANDING_CELL_TEAM_LABEL_WIDTH,ResourceConstants.SA_VAL_STANDING_CELL_HEIGHT,Manager.FIELD_LEFT);
        
        
		CustomHorizontalFieldManager playedLabelFieldManager = new CustomHorizontalFieldManager(ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH,ResourceConstants.SA_VAL_STANDING_CELL_HEIGHT,Manager.FIELD_RIGHT);
		
		CustomHorizontalFieldManager wonLabelFieldManager = new CustomHorizontalFieldManager(ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH,ResourceConstants.SA_VAL_STANDING_CELL_HEIGHT,Manager.FIELD_HCENTER);
		
		CustomHorizontalFieldManager drawLabelFieldManager = new CustomHorizontalFieldManager(ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH,ResourceConstants.SA_VAL_STANDING_CELL_HEIGHT,Manager.FIELD_HCENTER);
		
		CustomHorizontalFieldManager lostLabelFieldManager = new CustomHorizontalFieldManager(ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH,ResourceConstants.SA_VAL_STANDING_CELL_HEIGHT,Manager.FIELD_HCENTER);
		
//		CustomHorizontalFieldManager diffLabelFieldManager = new CustomHorizontalFieldManager(ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH,ResourceConstants.SA_VAL_STANDING_CELL_HEIGHT,Manager.FIELD_HCENTER);
		CustomHorizontalFieldManager attackLabelFieldManager = new CustomHorizontalFieldManager(ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH,ResourceConstants.SA_VAL_STANDING_CELL_HEIGHT,Manager.FIELD_HCENTER);
		CustomHorizontalFieldManager foulLabelFieldManager = new CustomHorizontalFieldManager(ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH,ResourceConstants.SA_VAL_STANDING_CELL_HEIGHT,Manager.FIELD_HCENTER);
		
		CustomHorizontalFieldManager pointsLabelFieldManager = new CustomHorizontalFieldManager(ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH,ResourceConstants.SA_VAL_STANDING_CELL_HEIGHT,Manager.FIELD_HCENTER);		
		
//		posLabelFieldManager.add(posLabel);
//        add(posLabelFieldManager);
		
        teamLogoFieldManager.add(teamLogoBitmap);
        add(teamLogoFieldManager);
        teamLabelFieldManager.add(teamNameLabel);
        add(teamLabelFieldManager);
        
        playedLabelFieldManager.add(playedLabel);
        add(playedLabelFieldManager);
        wonLabelFieldManager.add(wonLabel);
        add(wonLabelFieldManager);
        drawLabelFieldManager.add(drawLabel);
        add(drawLabelFieldManager);
        lostLabelFieldManager.add(lostLabel);
        add(lostLabelFieldManager);
//        diffLabelFieldManager.add(goalsDiffLabel);
//        add(diffLabelFieldManager);

        foulLabelFieldManager.add(foulLabel);
        add(foulLabelFieldManager);
        attackLabelFieldManager.add(attackLabel);
        add(attackLabelFieldManager);
        
        pointsLabelFieldManager.add(pointsLabel);
        add(pointsLabelFieldManager);
        
	}

	
	protected void sublayout(int maxWidth, int maxHeight) {
		// TODO Auto-generated method stub
		maxWidth = ResourceConstants.SA_VAL_STANDING_CELL_WIDTH;
		maxHeight = ResourceConstants.SA_VAL_STANDING_CELL_HEIGHT;
		super.sublayout(maxWidth, maxHeight);
		this.setExtent(maxWidth , maxHeight);
		
	}
}
