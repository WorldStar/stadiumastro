package view.component;

import view.screen.BaseScreen;
import view.screen.HomeScreen;
import application.AppSession;
import application.ImageStorage;
import application.ResourceConstants;
import application.StadiumAstroApp;
import application.Utils;
import model.DataManager;
import model.ImageLoadListener;
import model.MoreMenuItem;
import model.SASportEventContentItem;
import net.rim.blackberry.api.browser.Browser;
import net.rim.blackberry.api.invoke.Invoke;
import net.rim.blackberry.api.invoke.MessageArguments;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.Screen;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.image.ImageFactory;
import net.rim.device.api.util.StringProvider;

public class CustomMenuItem extends MenuItem implements ImageLoadListener
{
	private Object m_dataObject = null;
	private String m_iconUrl;
	public CustomMenuItem(String title, int ordinal, int priority, String iconUrl, Object obj)
	{
		super(new StringProvider(title), ordinal, priority);
		
		m_iconUrl = iconUrl;
		try
		{
			if(Utils.isNetworkUrl(m_iconUrl))
			{
				DataManager.getSharedDataManager().getImageFileFromServer(m_iconUrl, this);
			}
			else
			{
				Bitmap bitmap = Utils.getBitmapResource(m_iconUrl);
				setIcon(ImageFactory.createImage(bitmap));
			}
		}
		catch(Exception ex)
		{
			
		}
		
		m_dataObject = obj;
	}

	public void run()
	{
		StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();	
		if(m_dataObject == null)
		{
			Screen activeScreen = UiApplication.getUiApplication().getActiveScreen();
			if(activeScreen instanceof BaseScreen)
			{
				((BaseScreen)activeScreen).refresh();
			}
		}
		else if(m_dataObject instanceof SASportEventContentItem)
		{
			AppSession _sharedSession = AppSession.sharedSession();
			SASportEventContentItem newItem = (SASportEventContentItem)m_dataObject;
				
			// set selection object
			SASportEventContentItem lastItem =_sharedSession.m_sportEventContent;
			if(lastItem == null || !lastItem.m_refKey.equals(newItem.m_refKey))
			{	
				_sharedSession.m_sportEventContent = newItem;
				_sharedSession.setSportEventContent((SASportEventContentItem)m_dataObject);
				
				AppSession.sharedSession().m_showAdsBar = newItem.m_includeAdsBanner;
				AppSession.sharedSession().m_AdsBar = null;
				
			}
			
			// Pop to home screen and replace

			SASportEventContentItem contentItem= AppSession.sharedSession().m_sportEventContent;
	        	if(contentItem.m_launchInAppBrowserFlag)
	        	{
	        		Browser.getDefaultSession().displayPage(contentItem.m_inAppUrlLink);
	        	}else{
	        		app.replaceHomeScreen(new HomeScreen());
	        	}
			
		}
		else if(m_dataObject instanceof MoreMenuItem)
		{
			MoreMenuItem newItem = (MoreMenuItem)m_dataObject;
			if(newItem.m_title.equals(ResourceConstants.SA_TITLE_MENUITEM_ABOUT) ||
			newItem.m_title.equals(ResourceConstants.SA_TITLE_MENUITEM_FAQ) ||
			newItem.m_title.equals(ResourceConstants.SA_TITLE_MENUITEM_TAC) ||
			newItem.m_title.equals(ResourceConstants.SA_TITLE_MENUITEM_TOS) ||
			newItem.m_title.equals(ResourceConstants.SA_TITLE_MENUITEM_PRIVACY))
				app.gotoInfoScreen(newItem.m_title);
			
			if(newItem.m_title.equals(ResourceConstants.SA_TITLE_MENUITEM_OTHER_APP))
				app.gotoOtherAppScreen();
			
			
			if(newItem.m_title.equals(ResourceConstants.SA_TITLE_MENUITEM_FEEDBACK))
			{
				MessageArguments args = new MessageArguments(MessageArguments.ARG_NEW, ResourceConstants.SA_MESSAGE_DIST_ADDRESS , ResourceConstants.SA_APP_FEEDBACK_SUBJECT, "");
				Invoke.invokeApplication(Invoke.APP_TYPE_MESSAGES, args);
			}
			if(newItem.m_title.equals(ResourceConstants.SA_TITLE_MENUITEM_REPORT))
			{
				MessageArguments args = new MessageArguments(MessageArguments.ARG_NEW,ResourceConstants.SA_MESSAGE_DIST_ADDRESS , ResourceConstants.SA_REPORT_ISSUE_SUBJECT, "");
				Invoke.invokeApplication(Invoke.APP_TYPE_MESSAGES, args);
			}
			if(newItem.m_title.equals(ResourceConstants.SA_TITLE_MENUITEM_TELL))
			{
//				MessageArguments args = new MessageArguments(MessageArguments.ARG_NEW,ResourceConstants.SA_MESSAGE_DIST_ADDRESS , ResourceConstants.SA_TELL_SUBJECT, ResourceConstants.SA_TELL_MESSAGE_TEMPLATE);
//				Invoke.invokeApplication(Invoke.APP_TYPE_MESSAGES, args);
				TellOfferDialog tellDlg = new TellOfferDialog();
				tellDlg.show();
			}
			
//			app.gotoMoreMenuScreen(newItem.m_title);
		}
	}

	public void imageLoadSuccess(String url, Bitmap bmp) {
		// TODO Auto-generated method stub
		ImageStorage.getSharedImageBuffer().saveImage(url, bmp);
		
		setIcon(ImageFactory.createImage(bmp));
	}

	public void imageLoadFailed(String url, String err) {
		// TODO Auto-generated method stub
		
	}
	public void imageLoadCanceled(String url)
	{
		
	}
}
