package view.component;

import java.util.Vector;

import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.XYRect;

/**
 * 
 * @author Administrator
 *	This class is Custom Label class which draws text rather than add , because of UI alignment problem.
 *	Any of UI component can not be placed in correct location in way user want.
 *	So this class draws Label's Text with marginX and marginY. And Calculate numberOfLines and draw Multiple line of label
 *	into several sub Sentences.
 */
public class CustomLabelField extends Field {

	private String m_label;
	private int m_foreColor;
	private int _x;
	private int _y;
	private int _width;
	private Font _font;
	
	private int line_number;
	private Vector m_lineTexts = null;
	private int textAlign;
	/**
	 * @param - label : text 
	 * @param - foreColor : textColor
	 * @param - font : Font 
	 * @param - x : marginX
	 * @param - y : marginY
	 * @param - lineNumber : count of lines
	 * @param - style : Field's style
	*/
	public CustomLabelField(String label, int foreColor, Font font, int x, int y, int width, long style){
		super(style);
		this.m_foreColor = foreColor;
		this._x = x;
		this._y = y;
		this._width = width;
		this._font = font;
//		line_number = (int)Math.floor(getPreferredWidth()/_width) + 1;
		
		if(label == null)
			m_label = "";
		else
			m_label = label;
	
		textAlign = DrawStyle.LEFT;
		splitLabelIntoLines();
	}
	public void setTextAlign(int _textalign)
	{
		textAlign = _textalign;
	}
	
	public void setColor(int foreColor){
		m_foreColor = foreColor;
		invalidate();
	}
	
	protected void layout(int width, int height) {
//		setExtent(getPreferredWidth(), getPreferredHeight());
		setExtent(_width, getPreferredHeight());
	}

	protected void paint(Graphics graphics) {
	
		graphics.setFont(this._font);
		graphics.setColor(this.m_foreColor);
		
		int high = _font.getHeight();
		
		int contentWidth = this._width - this._x; 
		for(int i = 0; i < m_lineTexts.size(); i++)
		{
			graphics.drawText(m_lineTexts.elementAt(i).toString(), this._x, i * high + this._y, textAlign, contentWidth);
		}
		
	}

	public int getPreferredHeight() {		
		return _font.getHeight() * line_number + _y;
	}

	public int getPreferredWidth() {
		return _font.getBounds(m_label) + _x;
	}
	
	/**
	 * 	Split label string into multi-lines referencing line width
	 */
	private void splitLabelIntoLines()
	{
		m_lineTexts = new Vector();
		
		// define token split character
		char chSpace = ' ';
		
		// define line width
		int lineWidth = _width - _x;
		
		int lineStartIndex = 0;
		int curLineTextWidth = 0;
		int beforeIndex = 0;
		int nextIndex = m_label.indexOf(chSpace);
		line_number = 0;
		
		while(nextIndex != -1){
			// get next token string
			String strNextToken = m_label.substring(beforeIndex, nextIndex + 1);
			int tokenLen = _font.getBounds(strNextToken);
			if(curLineTextWidth +  tokenLen > lineWidth)
			{
				// if next word ranges out of current line's width								
				m_lineTexts.addElement(m_label.substring(lineStartIndex, beforeIndex));
				line_number++;
				lineStartIndex = beforeIndex;
				curLineTextWidth = tokenLen;
				beforeIndex = nextIndex + 1;
			}
			else
			{
				// continue search word
				beforeIndex = nextIndex + 1;
				curLineTextWidth += tokenLen;
			}
			
			// search next token
			nextIndex = m_label.indexOf(chSpace, beforeIndex);
		}
		
		// if reached at string's end
		if(beforeIndex >= m_label.length())
			return;
		
		// get last token string
		String strNextToken = m_label.substring(beforeIndex);
		int tokenLen = _font.getBounds(strNextToken);
		if(curLineTextWidth +  tokenLen > lineWidth)
		{			
			m_lineTexts.addElement(m_label.substring(lineStartIndex, beforeIndex));
			line_number++;
			
			// add last token at next line
			m_lineTexts.addElement(strNextToken);
			line_number++;
		}
		else
		{	
			// add last token at current line
			m_lineTexts.addElement(m_label.substring(lineStartIndex));
			line_number++;
		}	
	}
	
	public void setLabel(String label)
	{
		
		//Locate label to center of field.
		m_label = label;
		
		// reset marginX
		_x = Math.max((_width - _font.getBounds(label)) / 2, 0);
		
		splitLabelIntoLines();
	}
	
	/**
	 * Invalidate When This Field Has Lost Focus and Remain Highlighted Background.
	 */
	protected void drawFocus(Graphics g,boolean b){
		XYRect rect = new XYRect();
		getFocusRect(rect);
		
		drawHighlightRegion(g, HIGHLIGHT_FOCUS, false, rect.x, rect.y, rect.width, rect.height);
	}
}

