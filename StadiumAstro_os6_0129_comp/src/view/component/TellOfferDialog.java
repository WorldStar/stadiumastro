package view.component;



import view.screen.FacebookShare;
import application.ResourceConstants;
import application.StadiumAstroApp;
import net.rim.blackberry.api.invoke.Invoke;
import net.rim.blackberry.api.invoke.MessageArguments;
import net.rim.device.api.synchronization.UIDGenerator;
import net.rim.device.api.system.ApplicationDescriptor;
import net.rim.device.api.system.ApplicationManager;
import net.rim.device.api.system.ApplicationManagerException;
import net.rim.device.api.system.CodeModuleManager;
import net.rim.device.api.system.ControlledAccessException;
import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.DialogFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.blackberry.api.bbm.platform.BBMPlatformApplication;
import net.rim.blackberry.api.bbm.platform.BBMPlatformContext;
import net.rim.blackberry.api.bbm.platform.BBMPlatformContextListener;
import net.rim.blackberry.api.bbm.platform.BBMPlatformManager;
import net.rim.blackberry.api.bbm.platform.io.BBMPlatformChannel;
import net.rim.blackberry.api.bbm.platform.io.BBMPlatformChannelListener;
import net.rim.blackberry.api.bbm.platform.io.BBMPlatformConnection;
import net.rim.blackberry.api.bbm.platform.io.BBMPlatformData;
import net.rim.blackberry.api.bbm.platform.profile.BBMPlatformContact;
import net.rim.blackberry.api.bbm.platform.profile.BBMPlatformContactList;
import net.rim.blackberry.api.bbm.platform.service.MessagingService;
import net.rim.blackberry.api.blackberrymessenger.BlackBerryMessenger;
import net.rim.blackberry.api.blackberrymessenger.MessengerContact;
public class TellOfferDialog extends Dialog implements FieldChangeListener{
	//private MyBBMAppPlugin myPlugin;
	String uuid1 = "";
	public TellOfferDialog()
	{
		super(Dialog.D_OK, "Tell a Friend", 0, null, 0);
		
		Background backImg = BackgroundFactory.createSolidTransparentBackground(0x00000000, 255);
		setBackground(backImg);
		
		ImageHighlightButton facebookBtn = new ImageHighlightButton(ResourceConstants.SA_FACEBOOK_ICON, 0, 0, 0, 0, FIELD_VCENTER|Manager.FOCUSABLE);
		facebookBtn.setChangeListener(this);
		facebookBtn.setButtonTag(ResourceConstants.SA_FACEBOOK_TAG);
		
		ImageHighlightButton emailBtn = new ImageHighlightButton(ResourceConstants.SA_EMAIL_ICON,  0, 0, 0, 0, FIELD_VCENTER|Manager.FOCUSABLE);
		emailBtn.setChangeListener(this);
		emailBtn.setButtonTag(ResourceConstants.SA_EMAIL_TAG);
		
		//ImageHighlightButton bbmBtn = new ImageHighlightButton(ResourceConstants.SA_BBM_ICON,  0, 0, 0, 0, FIELD_VCENTER|Manager.FOCUSABLE);
		//bbmBtn.setChangeListener(this);
		//bbmBtn.setButtonTag(ResourceConstants.SA_BBM_TAG);
		
		DialogFieldManager dialogManager = new DialogFieldManager();
		
		dialogManager.addCustomField(facebookBtn);
		dialogManager.addCustomField(emailBtn);
		//dialogManager.addCustomField(bbmBtn);
		
		add(dialogManager);
	}
	
	public void fieldChanged(Field field, int context)
	{
		super.fieldChanged(field, context);
		if(field instanceof ImageHighlightButton)
		{
			ImageHighlightButton btn = (ImageHighlightButton) field;
			
			if(btn.getButtonTag() == ResourceConstants.SA_FACEBOOK_TAG)
	        {
	        	FacebookShare shareScreen = new FacebookShare();
	        	StadiumAstroApp app = (StadiumAstroApp)UiApplication.getUiApplication();
	        	app.gotoAnyScreen(shareScreen);
	        	this.close();
	        }
	        else if(btn.getButtonTag() == ResourceConstants.SA_EMAIL_TAG)
	        {
	        	MessageArguments args = new MessageArguments(MessageArguments.ARG_NEW, "" , ResourceConstants.SA_TELL_SUBJECT, ResourceConstants.SA_TELL_MESSAGE_TEMPLATE);
				Invoke.invokeApplication(Invoke.APP_TYPE_MESSAGES, args);
				this.close();
	        }
	        else if(btn.getButtonTag() == ResourceConstants.SA_BBM_TAG)
	        {
	        	
	        	/*uuid1 = "df3c7df6-03ca-493e-ad93-f6905acaf3cf";
	        	
	        	myPlugin = new MyBBMAppPlugin();
	        	
	        	BBMPlatformContext platformContext = null;
	        	try
	        	{
		        	platformContext = BBMPlatformManager.register(myPlugin);
		        }
	        	catch (ControlledAccessException e)
	        	{
		        	// The BBM platform has been disabled
		        }
	        	if (platformContext != null)
	        	{
		        	//MyBBMPlatformContextListener platformContextListener;
		        	//platformContextListener = new MyBBMPlatformContextListener();
		        	//platformContext.setListener(platformContextListener);
		        	
		        	MessagingService msgService = platformContext.getMessagingService();
		        	msgService.sendDownloadInvitation();
		        	//BBMPlatformContactList igonoreList = new BBMPlatformContactList();
		        	BBMPlatformChannel channel = msgService.createChannel(MyChannelListener);
		            
		            if (channel != null) 
		            {
		                channel.sendInvitation("Let's play a game", "Chess App", 0);
		            }
		            //platformContext.getUIService().startBBMChat("Let's chat!");
	        	}*/
        		sendBBM();
	        }
		}
	}
	/*public static String generateGUID()
    {             
         return Integer.toString(UIDGenerator.getUID())+"-"+Integer.toString(UIDGenerator.getUID()).substring(4,8)+"-"+Integer.toString(UIDGenerator.getUID()).substring(4,8)+"-"+Integer.toString(UIDGenerator.getUID()).substring(3,7)+"-"+Long.toString(System.currentTimeMillis()).substring(0,12);
    }
	private class MyBBMPlatformContextListener extends BBMPlatformContextListener
	{
		public void accessChanged(boolean isAccessAllowed, int accessErrorCode)
		{
			if (!isAccessAllowed)
			{
			// You cannot access the BBM platform
			}
		}
		public void appInvoked(int reason, Object param)
		{
		// Code for handling different contexts for invocation
		}
	};
	private BBMPlatformChannelListener MyChannelListener = new BBMPlatformChannelListener() 
    {
        public void contactsInvited(BBMPlatformConnection conn, BBMPlatformContactList 
                                     contactList) 
        {
        }
         
        public void contactsJoined(BBMPlatformConnection conn, BBMPlatformContactList
                                   contacts, String cookie, int type) 
        {
        }
		public void contactDeclined(BBMPlatformConnection arg0,
				BBMPlatformContact arg1) {
			// TODO Auto-generated method stub
			
		}

		public void contactLeft(BBMPlatformConnection arg0,
				BBMPlatformContact arg1) {
			// TODO Auto-generated method stub
			
		}

		public void dataReceived(BBMPlatformConnection arg0,
				BBMPlatformContact arg1, BBMPlatformData arg2) {
			// TODO Auto-generated method stub
			
		}
        
   	};
   	*/
	/*protected void sendBBM() {
	    BlackBerryMessenger bbm = BlackBerryMessenger.getInstance();

	    if (bbm != null) {
	    	   MessengerContact bbmContact = bbm.chooseContact();
	    }
	}
	private class MyBBMAppPlugin extends BBMPlatformApplication
	{
		public MyBBMAppPlugin()
		{
			super( uuid1 );
	
	
	
		}
	}*/
	protected void sendBBM() {
		BlackBerryMessenger bbm= BlackBerryMessenger.getInstance();
		int moduleHandle = CodeModuleManager.getModuleHandle("net_rim_bb_qm_peer");
		ApplicationDescriptor[] apDes =  CodeModuleManager.getApplicationDescriptors(moduleHandle);
		try {
		ApplicationManager.getApplicationManager().launchApplication("net_rim_bb_qm_peer");
		} catch (ApplicationManagerException e) {
			e.printStackTrace();
		} 
	}
}
