package view.component;

import view.screen.BaseScreen;
import view.screen.FixtureAnalysisScreen;
import view.screen.TVChannelDetailScreen;
import application.ResourceConstants;
import application.StadiumAstroApp;
import application.Utils;
import model.SATVChannelCalendarListItem;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.MainScreen;


/**
 * 
 * @author Administrator
 *	This is a custom Button tab bar Button class which needs in Analysis's Screens Tab Bar and FoxSports Screen's Date Tab Bar.
 *
 */
public class CustomTabButtonField extends Field {

	private int _id = 1;
	private int paddingX;
	private String _label;
    private Font font;
    
    private Bitmap _currentPicture;
    private Bitmap _onPicture;
    private Bitmap _offPicture;

    private int defaultColor;
    private boolean refreshed = false;
    
    SATVChannelCalendarListItem m_calendarItem = null;
    
    /**
     * @param id = Button ID / index
     * @param text = Button Label text
     * @param url = Button Image URL
     * @param paddingX = leftPadding value
     * @param color = text color of Button.
     * @param font = font of button's label
     * @param style = style of button field
     *
     */
    public CustomTabButtonField(int id , String text, String url, int paddingX , int color, Font font, long style)
    {
    	 

    	super(style | Field.FOCUSABLE);
    	this._id = id;
        this._label = text;
        this.paddingX = paddingX;
        this.defaultColor = color;
        if(font != null)
        	this.font = font;
        else
        	this.font = Utils.getTypeFace().getFont(Font.PLAIN, 10);
        
        _onPicture = Utils.getBitmapResource(url);
      
        _offPicture = Utils.getBitmapResource(ResourceConstants.SA_IMG_TABBAR_TABBED_BLUE_BACK);
        _currentPicture = _onPicture;
    }
    
    /**
     * 
     * @param item - calendar date button's data
     * @param text - text string
     * @param url - back bitmap path
     * @param paddingX - padding x point
     * @param color - text color
     * @param font - text font
     * @param style
     */
    public CustomTabButtonField(SATVChannelCalendarListItem item , int id, String text, String url, int paddingX , int color, Font font, long style)
    {
    	 

    	super(style | Field.FOCUSABLE);
    	this._id = id;
    	this.m_calendarItem = item;
        this._label = text;
        this.paddingX = paddingX;
        this.defaultColor = color;
        if(font != null)
        	this.font = font;
        else
        	this.font = Utils.getTypeFace().getFont(Font.PLAIN, 10);
        
        _onPicture = Utils.getBitmapResource(url);
        _offPicture = Utils.getBitmapResource(ResourceConstants.SA_IMG_TABBAR_TABBED_BLUE_BACK);
        _currentPicture = _onPicture;
    }
    /**
     * @set _currentPicture
     */
    public void setOffPicture(Bitmap bitmap){
    	_offPicture = bitmap;
    }
    
    /**
     * @return The Text on the button
     */
    String getText()
    {
        return _label;
    }
    /**
     * @return The ID of the button
     */
    int getID()
    {
        return _id;
    }
    /**
     * Field implementation.
     * @see net.rim.device.api.ui.Field#getPreferredHeight()
     */
    public int getPreferredHeight() 
    {
    	return _currentPicture.getHeight();
    }

    /**
     * return LabelWidth
     */
    public int getLabelWidth() 
    {
    	return font.getAdvance(_label) + paddingX;
    }
    /**
     * return LabelWidth
     */
    public int getLabelHeight() 
    {
    	return font.getHeight();
    }
    
    /**
     * Field implementation.
     * @see net.rim.device.api.ui.Field#getPreferredWidth()
     */
    public int getPreferredWidth() 
    {
    	return _currentPicture.getWidth();
    }
    /**
     * Field implementation.
     * @see net.rim.device.api.ui.Field#layout(int, int)
     */
    protected void layout(int width, int height) 
    {
        setExtent(Math.max( getLabelWidth(), getPreferredWidth() + paddingX),  Math.max( getLabelHeight(), getPreferredHeight()));
    }

    /**
     * Field implementation.
     * @see net.rim.device.api.ui.Field#paint(Graphics)
     */
    protected void paint(Graphics graphics) 
    {       
        graphics.setColor(defaultColor);
    	
        graphics.fillRect(0, 0, getWidth(), getHeight());
        Bitmap cu = getScaledBitmap(_currentPicture, getWidth()-paddingX / 2, getHeight());
        graphics.drawBitmap(paddingX / 2, 0, getWidth()-paddingX / 2, getHeight(), cu, 0, 0);
        
        graphics.setFont(font);
        if(refreshed)
        	graphics.setColor(Color.WHITE);
        else
        	graphics.setColor(Color.DARKGRAY);
        graphics.drawText(getText(), paddingX + Math.abs(getPreferredWidth() - getLabelWidth())/2, Math.abs(getPreferredHeight() - getLabelHeight())/2);
        	
    }
    public Bitmap getScaledBitmap(Bitmap src, int width, int height) {
		if (src == null || width <= 0 || height <= 0) {
			return null;
		}
		int length = width * height;

		int[] argbData = new int[length];
		for(int i = 0; i < length; i++)
			argbData[i] = 0; // set as transparent    

		Bitmap dst = new Bitmap(width, height);  
		dst.setARGB(argbData, 0, width, 0, 0, width, height);
		argbData = null;

		src.scaleInto(dst, Bitmap.FILTER_BILINEAR, Bitmap.SCALE_STRETCH);    	
		src = null;
		return dst;
	}    
    /**
     * Event handler
     */
    protected boolean navigationClick(int status, int time) {
    	if (status != 0){
    		fieldChangeNotify(0);
    		if(!isClicked())
    			clickDown();
    	}
        return true;
    }

    public boolean keyChar(char key, int status, int time) {
        if (key == Characters.ENTER) {
            fieldChangeNotify(0);
            if(!isClicked())
            	clickDown();
            return true;
        }
        return false;
    }
    protected boolean trackwheelClick(int status, int time)
    {        
        if (status != 0 && !isClicked()) clickDown(); 
        return true;
    }
    
    protected boolean invokeAction(int action) 
    {
        switch( action ) {
            case ACTION_INVOKE: {
            	clickDown(); 
                return true;
            }
        }
        return super.invokeAction(action);
    }    

    protected boolean touchEvent(TouchEvent message)
    {
        int x = message.getX( 1 );
        int y = message.getY( 1 );
        if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
            // Outside the field
            return false;
        }
        
        if(!isClicked()){
        	
	        switch( message.getEvent() ) {
	       
	        	case TouchEvent.CLICK:
	        		refresh(true);
	        		clickDown();
	        		return true;
	            	
	        }
        }
        return super.touchEvent( message );
    }

    public void clickDown(){
    	fieldChangeNotify(0);
    	
    	StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
    	
    	MainScreen curScreen = (MainScreen) app.getActiveScreen(); 
    	//scroll up main scroll view
    	if(curScreen.getClass() == FixtureAnalysisScreen.class)
    	{
    		FixtureAnalysisScreen _curScreen = (FixtureAnalysisScreen) curScreen;
    		int curScrollPos = _curScreen.m_MainScroller.getVerticalScroll();
    		
        	if(curScrollPos < ResourceConstants.SA_VAL_ADS_BAR_HEIGHT || curScrollPos == 0)
        		_curScreen.m_MainScroller.setVerticalScroll(Utils.getDisplayHeight(), true);
    		//clear old tabbed button
    		if(_curScreen.m_curSelectTabButtonField != null)
        		_curScreen.m_curSelectTabButtonField.refresh(false);	
        	
        	//_curScreen.m_curSelectTabButtonField = this;
        	
        	//click processing...
        	_curScreen.readyCurPage(getID());
        	_curScreen.goCurPage();
    	}
    	
    	if(curScreen.getClass() == TVChannelDetailScreen.class)
    	{
    		TVChannelDetailScreen _curScreen = (TVChannelDetailScreen)curScreen;
    		//clear old tabbed button
    		if(_curScreen.m_curTabbedButton != null)
    			_curScreen.m_curTabbedButton.refresh(false);	
        	    	
        	
        	_curScreen.m_curTabbedButton = this;
        	
        	//click processing...
        	_curScreen.showEventList(m_calendarItem.m_id);
    	}
    }
    public boolean isClicked(){
    	StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
    	MainScreen curScreen = (MainScreen)app.getActiveScreen();
    	
    	if(curScreen.getClass() == FixtureAnalysisScreen.class){
    		FixtureAnalysisScreen _curScreen = (FixtureAnalysisScreen) curScreen;
    		if(_curScreen.m_curSelectTabButtonField != null){
        		//if(_curScreen.m_curSelectTabButtonField == this)
        			//return true;
        	}
    	}
    	
    	if(curScreen.getClass() == TVChannelDetailScreen.class)
    	{
    		TVChannelDetailScreen _curScreen = (TVChannelDetailScreen) curScreen;
    		_curScreen.curPageNumber = this.getID();
	    	if(_curScreen.m_curTabbedButton != null){
	    		if( _curScreen.m_curTabbedButton == this)
	    			return true;
	    	}
    	}
    	return false;
    }
    public void refresh(boolean flag){
    	refreshed = flag;
    	if(flag == true){
    		_currentPicture = _offPicture;
    		invalidate();
    	}
    	else{
    		_currentPicture = _onPicture;
    		invalidate();
    	}
    	invalidate();
    }

    protected void onFocus(int direction)
	{
    	StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
    	
    	MainScreen curScreen = (MainScreen) app.getActiveScreen(); 
    	//scroll up main scroll view
    	if(curScreen.getClass() == FixtureAnalysisScreen.class)
    	{
    		FixtureAnalysisScreen _curScreen = (FixtureAnalysisScreen) curScreen;

        	_curScreen.refreshButton();
    	}
    	this.getManager().setHorizontalScroll((getID() - 1) * _onPicture.getWidth() , true);
		refresh(true);
	}
	
	protected void onUnfocus()
	{
		StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
	
		MainScreen curScreen = (MainScreen) app.getActiveScreen(); 
		if(curScreen.getClass() == TVChannelDetailScreen.class)
    	{
    		TVChannelDetailScreen _curScreen = (TVChannelDetailScreen) curScreen;
    		if(_curScreen.curPageNumber != this.getID())
    			refresh(false);
    	}
	}
}
