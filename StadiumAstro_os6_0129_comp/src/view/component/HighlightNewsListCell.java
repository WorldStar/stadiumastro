package view.component;

import application.AppSession;
import application.ResourceConstants;
import application.Utils;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;

public class HighlightNewsListCell extends CustomBitmapField {
	// reference with label display
	private int m_labelPosX = 0;
	private int m_labelPosY = 0;
	private String m_title = null;
	private Font m_labelFont = null;
	private int m_labelColor = 0;
	
	private int m_labelPanelPosX = 0;
	private int m_labelPanelPosY = 0;
	private int m_labelPanelWidth = 0;
	private int m_labelPanelHeight = 0;
	
	// reference with highlight news's event name display
	private Bitmap m_eventNameBackground = null;
	private String m_strEventName = null;
	private Font m_nameFont = null; 
	private int m_nameColor = 0;
	private int m_eventNamePosX = 0;
	private int m_eventNamePosY = 0;
	
	private boolean m_bFocused = false;
	
	/**
	 * 
	 * @param url
	 * @param iconUrl
	 * @param x
	 * @param y
	 * @param style
	 * @param strLabel
	 */
	public HighlightNewsListCell(String url ,String iconUrl, int x, int y , long style, String strLabel){
		super(url, iconUrl, x, y, Utils.getDisplayWidth(), 300, style, ResourceConstants.SA_IMG_NEWS_DETAIL_SAMPLE);
		int bmpWidth = Utils.getDisplayWidth();
		int bmpHeight = 300;
		
		// set display string
		m_labelFont = Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL);
		m_labelColor = Color.WHITE;
		m_title = Utils.getOmittedString(strLabel, bmpWidth, m_labelFont);
		
		// set string's position to  bottom-center
		int marginTopBottom = 20;
		m_labelPosX = x + (bmpWidth - m_labelFont.getBounds(m_title)) / 2;
		m_labelPosY = y + bmpHeight - m_labelFont.getHeight() - marginTopBottom;
		
		// set label panel to picture's bottom
		m_labelPanelWidth = bmpWidth;
		m_labelPanelHeight = m_labelFont.getHeight() + marginTopBottom * 2;
		m_labelPanelPosX = x;
		m_labelPanelPosY = y + bmpHeight - m_labelFont.getHeight() - marginTopBottom * 2;
		
		// set event name display data
		m_eventNameBackground = Utils.getBitmapResource(ResourceConstants.SA_IMG_HSCROLL_TITLE);	
		m_nameColor = Color.WHITE;
		m_nameFont = Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL);
		int nRefWidth = m_eventNameBackground.getWidth();
		m_strEventName =  Utils.getOmittedString(AppSession.sharedSession().m_curSportsEventName, nRefWidth, m_nameFont);
		m_eventNamePosX = (nRefWidth - m_nameFont.getBounds(m_strEventName)) / 2;
		m_eventNamePosY = y + (m_eventNameBackground.getHeight() - m_nameFont.getHeight()) / 2;		
	}
	
	protected void paint(Graphics graphics)
	{
		super.paint(graphics);
		
		if(this.m_icon != null){
			graphics.setGlobalAlpha(200);
			graphics.drawBitmap((m_width - this.m_icon.getWidth()) / 2 + m_x , (m_height - this.m_icon.getHeight()) / 2 + m_y, this.m_icon.getWidth(), this.m_icon.getHeight(), this.m_icon, 0, 0);
		}
		
		if(this.m_title != null)
		{
			// draw title panel
			graphics.setGlobalAlpha(200);
			graphics.setColor(Color.BLACK);
			graphics.fillRect(this.m_labelPanelPosX, this.m_labelPanelPosY, this.m_labelPanelWidth, this.m_labelPanelHeight);
			
			// draw label string
			graphics.setGlobalAlpha(255);
			graphics.setFont(this.m_labelFont);
			graphics.setColor(this.m_labelColor);
			graphics.drawText(this.m_title, this.m_labelPosX, this.m_labelPosY);
		}
		
		// draw event name string
		if(this.m_strEventName != null)
		{
			graphics.setGlobalAlpha(255);
			graphics.drawBitmap(0, 0, this.m_eventNameBackground.getWidth(), this.m_eventNameBackground.getHeight(), this.m_eventNameBackground, 0, 0);
			graphics.setFont(this.m_nameFont);
			graphics.setColor(this.m_nameColor);
			graphics.drawText(this.m_strEventName, this.m_eventNamePosX, this.m_eventNamePosY);
		}
		
		// draw focus mask
		if(m_bFocused)
		{
			graphics.setColor(Color.GRAY);
			graphics.setGlobalAlpha(100);
			graphics.fillRect(m_x, m_y, Utils.getDisplayWidth(), m_height);
			
		}
	}	
	
	protected void onFocus(int direction)
	{
		m_bFocused = true;
		
		HighlightNewsListScroll scroll = (HighlightNewsListScroll)(this.getManager().getManager());
		
		int nIndex = -1;
		for(int i = 0; i < scroll.getFieldCount(); i++)
		{
			Field field = scroll.getField(i);
			if(field.equals(getManager()))
			{
				nIndex = i;
				break;
			}
		}
		
		
		boolean bAnimation = true;
		
		if(((direction == -1) && (nIndex == scroll.m_count - 1))
				 || ((direction == 1) && (nIndex == 0)))
		{
			bAnimation = false;
			int scrollPosX =  AppSession.sharedSession().m_showAdsBar ? ResourceConstants.SA_VAL_ADS_BAR_HEIGHT : 0;
			scroll.getManager().setVerticalScroll(scrollPosX + ResourceConstants.SA_VAL_MATCHTAB_HEIGHT, true);
		}
		scroll._curPageNumber = nIndex;
//		scroll.swipeLeft = (direction == 1);
		System.out.println("highlight focused index: " + nIndex);
//		
//		scroll.setHorizontalScroll(Utils.getDisplayWidth() * nIndex, bAnimation);
		invalidate();
	}
	
	protected void onUnfocus()
	{
		m_bFocused = false;	
		invalidate();
	}
}
