package view.component;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.XYRect;
//import application.ImageBuffer;
import application.ImageMemory;
import application.ImageStorage;
import application.Utils;
import net.rim.device.api.system.Bitmap;

import model.ImageLoadListener;
import model.DataManager;

/**
 * 
 * @author Administrator
 *	This class is Custom Bitmap class which draws Bitmap rather than add , because of UI alignment problem.
 *	Any of UI component can not be placed in correct location in way user want.
 *	So this class draws Bitmap with marginX and marginY.
 */
public class CustomBitmapField1 extends Field implements ImageLoadListener{	
	public Bitmap m_icon = null;
	public int m_x = 0;
	public int m_y = 0;	
	public int m_width = 0;
	public int m_height = 0;
	
	public boolean m_clickable;
	public boolean m_bHasFocus;
	
	public String m_defaultImg = null;
	public String m_bmpStr = null;
	public String m_iconStr = null;
	
    private Bitmap m_bgBmp = null;
    private Bitmap m_bgDefault = null;
    
	boolean canLoadScreen = true;
	boolean imageLoading;
	/**
	 * @param - url : image resource destination
	 * @param - iconUrl : icon which displays on main image, 's resource destination 
	 * @param - x : marginX
	 * @param - y : marginY
	*/
	public CustomBitmapField1(String url ,String iconUrl, int x, int y, int width, int height,long style, String defaultImg)
	{
		super(style);
		this.m_x = x;
		this.m_y = y;
		this.m_width = width;
		this.m_height = height;
		
		m_bmpStr = url;
		m_iconStr = iconUrl;
		m_defaultImg = defaultImg;
		
		canLoadScreen = true;
		imageLoading = false;

		if (m_defaultImg == null)
		{
			Bitmap bmp = null;
			bmp = loadImage(m_bmpStr);
			if(bmp != null)
			{
				if(m_width == 0)
					m_width = bmp.getWidth();
				if(m_height == 0)
					m_height = bmp.getHeight();
				
				m_bgBmp = getScaledBitmap(bmp);
			}
			else
			{
				if(Utils.isNetworkUrl(m_bmpStr))
				{
					imageLoading = true;
					requestBitmapToServer();
				}
			}
		}
		else 
		{
			m_bgDefault = getScaledBitmap(loadImage(m_defaultImg));
		}

		if(iconUrl != null)
		{
			this.m_icon = loadImage(iconUrl);
		}
		else
			this.m_icon = null;

		m_clickable = false;
	}

	public void setClickable(boolean clickable)
	{
		m_clickable = clickable;
	}
	
	public Bitmap loadImage(String url)
	{
		Bitmap bmp;

		try{
			if(!Utils.isNetworkUrl(url))
			{
				if((bmp = ImageMemory.getSharedImageBuffer().getImage(url)) != null)
					return bmp;
				
				bmp = Utils.getBitmapResource(url);
				ImageMemory.getSharedImageBuffer().cacheImage(url, bmp);
				
				return bmp;
			}
			else
			{	
				bmp = ImageStorage.getSharedImageBuffer().getImage(url);
				System.out.println(bmp == null);
				return bmp;
			}
		}
		catch(Exception ex)
		{
			return null;
		}
	}
	
	public void requestBitmapToServer()
	{
		DataManager.getSharedDataManager().getImageFileFromServer(m_bmpStr, this);
	}
	
	public Bitmap getScaledBitmap(Bitmap bmp)
	{	
		float wRatio = (float) m_width / bmp.getWidth();
		float hRatio = (float) m_height / bmp.getHeight();
		
		float maxRatio = Math.max(wRatio, hRatio);
//		return Utils.getScaledBitmap(bmp, maxRatio);
		return Utils.getScaledBitmap(bmp, m_width, m_height);
	}
	
	protected void layout(int width, int height) 
	{	
		setExtent(this.m_x + m_width, this.m_y + m_height);
	}

	protected void paint(Graphics graphics) 
	{
		if (m_bgBmp == null)
		{
			Bitmap bmp = null;
			
			System.out.println("loading image");
			bmp = loadImage(m_bmpStr);
			if(bmp != null)
			{
				if(m_width == 0)
					m_width = bmp.getWidth();
				if(m_height == 0)
					m_height = bmp.getHeight();
				
				m_bgBmp = getScaledBitmap(bmp);
			}
			else
			{
				if(canLoadScreen && !imageLoading && Utils.isNetworkUrl(m_bmpStr))
				{
					imageLoading = true;
					requestBitmapToServer();
				}
			}
		}

		Bitmap bmp = null;
		
		if (m_bgBmp != null)
		{
			bmp = m_bgBmp;
		}
		else if (m_bgDefault != null)
		{
			bmp = m_bgDefault;
		}
		else
		{
			return;
		}
		
		int nWidth = bmp.getWidth();
		int nHeight = bmp.getHeight();
		
		int offX = (nWidth - m_width) / 2;
		int offY = (nHeight - m_height) / 2;
		if(offX < 0) offX = 0;
		if(offY < 0) offY = 0;
		graphics.drawBitmap(this.m_x, this.m_y, nWidth, nHeight, bmp, offX, offY);

		if(this.m_icon != null)
		{
			int iconWidth = this.m_icon.getWidth();
			int iconHeight = this.m_icon.getHeight();

			offX = this.m_x + (m_width - iconWidth) / 2;
			offY = this.m_y + (m_height - iconHeight) / 2;
			graphics.drawBitmap(offX, offY, iconWidth,iconHeight, this.m_icon, 0, 0);
		}
	}
	
	/**
	 * Invalidate When This Field Has Lost Focus and Remain Highlighted Background.
	 */
	protected void drawFocus(Graphics g,boolean b){
		
		if(b == true)
		{
			XYRect rect = new XYRect();
			getFocusRect(rect);
			
			drawHighlightRegion(g, HIGHLIGHT_FOCUS, false, rect.x, rect.y, rect.width, rect.height);
		}
	}

	public void imageLoadSuccess(String url, Bitmap bmp) {
		// TODO Auto-generated method stub
		if(m_bmpStr == url)
		{
			System.out.println("scaling bitmap");
			ImageStorage.getSharedImageBuffer().saveImage(url, bmp);
			imageLoading = false;
		}
		invalidate();
	}

	public void imageLoadFailed(String url, String err) {
		// TODO Auto-generated method stub
		System.out.println("Image loading failed");
		canLoadScreen = false;
		imageLoading = false;
	}
	public void imageLoadCanceled(String url)
	{
		imageLoading = false;
	}
	
	protected boolean trackwheelClick(int status, int time)
    {        
        if (status != 0) clickButton(); 
        return true;
    }
	
	protected boolean invokeAction(int action) 
    {
        switch( action ) {
            case ACTION_INVOKE: {
                clickButton(); 
                return true;
            }
        }
        return super.invokeAction(action);
    }
	
    protected boolean touchEvent(TouchEvent message)
    {
        int x = message.getX( 1 );
        int y = message.getY( 1 );
        if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
            // Outside the field
        	return false;
        }
        switch( message.getEvent() ) {
        case TouchEvent.CLICK:
        	clickButton();
            return true;
            	
        }
        return super.touchEvent( message );
    }

	public void clickButton(){
    	//Dialog.alert("Clicked!");
    	fieldChangeNotify(0);

    }
}
