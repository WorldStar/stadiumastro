package view.component;

import java.util.Calendar;
import java.util.Date;

import application.ResourceConstants;
import application.Utils;
import model.SACalendarItem;
import model.SACalendarList;
import net.rim.device.api.io.http.HttpDateParser;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class MatchCalendarScroll extends HorizontalFieldManager {
	SACalendarList m_calendarList = null;
	
	public CalendarDateItem m_curSelectedItemView = null;
	public int m_totalCount = 0;
	public static int m_firstSelect = 0;
	public static int m_scrollSelect = 0;
	public boolean activeflg = false;
	public static int width = 0;
	public boolean endflg = false;
	public boolean flg = false;
	public MatchCalendarScroll(){
		super(Manager.HORIZONTAL_SCROLL | Field.FOCUSABLE);
		setMargin(10, 0, 10, 0);
		flg = false;
		//create and get calendarList from server / local
		m_calendarList = new SACalendarList();
		int nCount = m_calendarList.totalCount();
		m_totalCount = nCount;
		/*for(int i = 0; i < nCount; i++){
			SACalendarItem calendarInfo = m_calendarList.getDataAtIndex(i);
			CalendarDateItem dateItem = new CalendarDateItem(calendarInfo, i ,Manager.FIELD_LEFT);
			add(dateItem);
		}*/
		int index = 0;
		endflg = false;
		long tmp = System.currentTimeMillis();
		Calendar c1 = Calendar.getInstance();
		int y = c1.get(Calendar.YEAR);
		int m = c1.get(Calendar.MONTH)+1;
		int d = c1.get(Calendar.DAY_OF_MONTH);
		String[] a = new String[]{"","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
		
		String now = y+"-"+m+"-"+d;
		if(m < 10){
			now = y+"-0"+m+"-"+d;
		}
		boolean flg = false;
		add(new NullField(NullField.FOCUSABLE));
		int activeid = -1;
		for(int i = 0; i < nCount; i++){
			SACalendarItem calendarInfo = m_calendarList.getDataAtIndex(i);
			if(calendarInfo.m_month.equals("Dec") && calendarInfo.m_day.equals("10")){
				String a1 = "";
				String b1 = a1;
			}
			if(calendarInfo.m_isActive.toLowerCase().equals("y")){
				index = i;

				activeflg = true;
				/*activeid = i;
				try{
					int diff1 = Math.abs(d - Integer.parseInt(calendarInfo.m_day));
					int diff2 = 0;
					if(i+1 <= nCount-1){
						SACalendarItem calendarInfo1 = m_calendarList.getDataAtIndex(i+1);
						diff2 = Math.abs(d - Integer.parseInt(calendarInfo1.m_day));
					}
					if(diff1 >= diff2){
						activeid = i+1;
					}
				}catch(Exception e){}*/
			}
			//if(activeid == i)
			CalendarDateItem dateItem = new CalendarDateItem(calendarInfo, i ,Manager.FIELD_LEFT, activeflg);
			String m_sam = "";
			/*for(int k = 1; k < a.length; k++){
				if(a[k].equals(calendarInfo.m_month)){
					m_sam = "" + k; 
					if(m_sam.length() == 1)m_sam = "0"+m_sam;
					break;
				}
			}
			String c = calendarInfo.m_year+"-"+ m_sam +"-"+calendarInfo.m_day;
			long timeDiff = getTimeDifference(now, c);
			 if(!flg && timeDiff > 0){
				 index = i;
				 flg = true;
			 }*/
			/*
			if(tmp > timeDiff){
				index = i;
				tmp = timeDiff;
			}*/
			add(dateItem);
		}
		m_firstSelect = index+1;
	}
	public void setEndflg(boolean flg){
		this.endflg = flg;
	}
	public long getTimeDifference(String timestamp1,
			   String timestamp2) {
			  long time1 = getTime(timestamp1);
			  long time2 = getTime(timestamp2);
			  return time2 - time1;
	}
	 public long getTime(String time) {
		  Date formatter = new Date(HttpDateParser.parse(time));
		  return formatter.getTime();
	}
	protected void sublayout(int maxWidth, int maxHeight) {
		maxHeight = ResourceConstants.SA_VAL_FIXTURE_CALENDAR_BAR_HEIGHT;
		super.sublayout(maxWidth, maxHeight);
		setExtent(maxWidth , maxHeight);
	}
	public void selectItemAtIndex(int index)
	{
		if(this.getFieldCount() >= index)
		{
			CalendarDateItem dateItem  = (CalendarDateItem)this.getField(index);
			dateItem.clickButton();
			dateItem.setFocus();
		}
	}
	protected void onFocus(int direction){
		if(!flg) flg = true;
		else{
			if(endflg){
				endflg = false;
				if(this.getFieldCount() >= m_firstSelect+2 && m_firstSelect+2 >= 0)
				{
					CalendarDateItem dateItem  = (CalendarDateItem)this.getField(m_firstSelect+2);
					dateItem.refreshBackGround(true);
					dateItem.setFocus();
				}
			}
		}
	}
	protected void onUnfocus(){
		endflg = true;
		if(this.getFieldCount() >= m_firstSelect+2 && m_firstSelect+2 >= 0)
		{
			CalendarDateItem dateItem  = (CalendarDateItem)this.getField(m_firstSelect+2);
			dateItem.refreshBackGround(false);
		}
		if(this.getFieldCount() >= m_scrollSelect+1 && m_scrollSelect+1 >= 0)
		{
			CalendarDateItem dateItem  = (CalendarDateItem)this.getField(m_scrollSelect+1);
			if(!dateItem.m_bSelected)
				dateItem.refreshBackGround(false);
		}
		if(this.getFieldCount() >= m_scrollSelect && m_scrollSelect >= 0)
		{
			CalendarDateItem dateItem  = (CalendarDateItem)this.getField(m_scrollSelect);
			if(!dateItem.m_bSelected)
				dateItem.refreshBackGround(false);
		}
	}
}
