package view.component;


import java.util.Timer;
import java.util.TimerTask;

import application.ResourceConstants;
import application.StadiumAstroApp;
import application.Utils;
import model.SAHighlightNewsList;
import model.SAHighlightNewsListItem;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;


/**
 * 
 * @author Administrator
 * This is Match Highlight news list Scroll that shows on HomeScreen.
 * Here all of match preview Image is same. You can change this by changing static Constants SA_IMG_HSCROLL and then calc Count of this rather than using SA_VAL_TESTING_PREVIEW_IMG_COUNT.
 */

public class HighlightNewsListScroll extends HorizontalFieldManager {

	public int _curPageNumber;		//current white dot index
	public int m_count;				//Dots count which is same with image count.
	
	private int oldTouchPosX;
	public boolean swipeLeft = true;
	public int wi = 0;
	public int hi = 0;
	public SAHighlightNewsList m_highlightNewsList;
	private Timer timer;
	
	public HighlightNewsListScroll(long style){
		super(style);
		this.setScrollingInertial(false);
		
		// set model data
		m_highlightNewsList = new SAHighlightNewsList();
		
		int count = m_highlightNewsList.totalCount();
		this.m_count = count;
		
//		//add Last page at first
//		VerticalFieldManager lastPageViewManager = new VerticalFieldManager(Field.FIELD_HCENTER);
//		
//		SAHighlightNewsListItem dataItem = m_highlightNewsList.getDataAtIndex(count - 1);		
//		HighlightNewsListCell  previewField = new HighlightNewsListCell (dataItem.m_imageUrl, null, 0, 0, Manager.FIELD_LEFT | Field.FOCUSABLE, dataItem.m_title);
//	
//		CustomPageControlField pageControlField = new CustomPageControlField(count - 1, count);
//		
//		lastPageViewManager.add(previewField);
//		lastPageViewManager.add(pageControlField);
		
//		this.add(lastPageViewManager);
		
		//add medium page at middle.
		for (int i = 0; i < count; i++) {
			VerticalFieldManager eachPageViewManager = new VerticalFieldManager(Field.FIELD_HCENTER);
			
			SAHighlightNewsListItem dataItem = m_highlightNewsList.getDataAtIndex(i);		
			HighlightNewsListCell previewField = new HighlightNewsListCell (dataItem.m_imageUrl, null, 0, 0, Manager.FIELD_LEFT| Field.FOCUSABLE, dataItem.m_title);
			
			CustomPageControlField pageControlField = new CustomPageControlField(i, count);
			
			eachPageViewManager.add(previewField);
			eachPageViewManager.add(pageControlField);
			this.add(eachPageViewManager);
		}

		
//		//add First Page at last
//		VerticalFieldManager firstPageViewManager = new VerticalFieldManager(Field.FIELD_HCENTER);
//		
//		dataItem = m_highlightNewsList.getDataAtIndex(0);		
//		previewField = new HighlightNewsListCell (dataItem.m_imageUrl, null, 0, 0, Manager.FIELD_LEFT| Field.FOCUSABLE, dataItem.m_title);
//		
//		pageControlField = new CustomPageControlField(0, count);
//		
//		firstPageViewManager.add(previewField);
//		firstPageViewManager.add(pageControlField);
//		this.add(firstPageViewManager);
		
		_curPageNumber = 0;
		
		//Timer Starts...
		makeTimer();
	}
	
	public void makeTimer(){
		System.out.println("HHHHHHHH start navigation image rotating timer HHHHHHH");
		timer = new Timer();
		TimerTask task = new TimerTask() {

			public void run() {
				if(swipeLeft)
					nextPage();
				else
					beforePage();
//				StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
//				if(app._currentScreen != app.mHomeScreen)
//					this.cancel();
			}
		};
		timer.schedule(task, ResourceConstants.SA_VAL_SCROLL_TIMER_INTERVAL,ResourceConstants.SA_VAL_SCROLL_TIMER_INTERVAL);
	}
	
	public void closeTimer(){
		System.out.println("HHHHHHHH stop navigation image rotating timer HHHHHHH");
		timer.cancel();
	}
	
	public void nextPage()
	{
//		if(_curPageNumber == m_count + 1){
//			_curPageNumber = 1;
//			setHorizontalScroll(_curPageNumber * getWidth(),false);
//		}
//		_curPageNumber++;
//		setHorizontalScroll(_curPageNumber * getWidth(),true);
		_curPageNumber++;
		boolean banim = true;
		if(_curPageNumber >= m_count){
			_curPageNumber = 0;
			banim = false;
		}
		setHorizontalScroll(_curPageNumber * getWidth(), banim);
	}
	
	public void beforePage()
	{
//		if(_curPageNumber == 0){
//			_curPageNumber = m_count;
//			setHorizontalScroll(_curPageNumber * getWidth(),false);
//		}
//		_curPageNumber--;
//		setHorizontalScroll(_curPageNumber * getWidth(),true);
		boolean banim = true;
		_curPageNumber--;
		if(_curPageNumber < 0){
			_curPageNumber = m_count - 1;
			banim = false;
		}
		setHorizontalScroll(_curPageNumber * getWidth(), banim);
	}
	
	protected void sublayout(int maxWidth, int maxHeight) {
		// TODO Auto-generated method stub
		maxWidth = Utils.getDisplayWidth();
		maxHeight = ResourceConstants.SA_VAL_HIGHLIGHT_BAR_TOTAL_HEIGHT;
		this.setExtent(maxWidth , maxHeight);
		super.sublayout(maxWidth, maxHeight);
		
		this.setHorizontalScroll(_curPageNumber * this.getWidth(),false);
	}

	/**
     * Event handler
     */
	
    protected boolean navigationClick(int status, int time) {
    	if (status != 0){
    		fieldChangeNotify(0);
    		click();
    	}
        return true;
    }

    public boolean keyChar(char key, int status, int time) {
        if (key == Characters.ENTER) {
            fieldChangeNotify(0);
            click();
            return true;
        }
        return false;
    }

    protected boolean trackwheelClick(int status, int time)
    {  
        if (status != 0) click(); 
        return true;
    }

    protected boolean invokeAction(int action) 
    {
        switch( action ) {
            case ACTION_INVOKE: {
                click(); 
                return true;
            }
        }
        return super.invokeAction(action);
    }

	protected boolean touchEvent(TouchEvent message)
	 {
		int x = message.getX(1);
		int y = message.getY(1);
		if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
            // Outside the field
//        	refresh(false);
            return false;
        }
		if(message.getEvent() == TouchEvent.DOWN){
			oldTouchPosX = x;
			clickDown();
		}else if(message.getEvent() == TouchEvent.UP){
			clickUp(x,y);
		}
		if(message.getEvent() == TouchEvent.CLICK)
			click();
		return super.touchEvent(message); 
	 }
	
	public void click(){
		StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
		SAHighlightNewsListItem dataItem = m_highlightNewsList.getDataAtIndex(_curPageNumber);
		app.gotoNewsDetailScreen(app.mHomeScreen, dataItem);
	}
	
	public void clickDown(){
		fieldChangeNotify(0);
//		if(_curPageNumber == m_count + 1)
//		{
//			this.setHorizontalScroll(1 *  this.getWidth(), false);
//			_curPageNumber = 1;
//		}else if(_curPageNumber == 0){
//			this.setHorizontalScroll(m_count *  this.getWidth(), false);
//			_curPageNumber = m_count;
//		}
	}
	
	public void clickUp(int x,int y){
		fieldChangeNotify(0);
//		if(Math.abs(Math.abs(x) - Math.abs(oldTouchPosX)) >= ResourceConstants.SA_VAL_HIGHLIGHT_SCROLL_MAGNITUDE_FORCE){
//			if(x <= oldTouchPosX){
//				_curPageNumber++;
//				swipeLeft = true;
//			}else{
//				_curPageNumber--;
//				swipeLeft = false;
//			}
//			scrollToCurPage();
//		}else
//		{
//			scrollToCurPage();
//		}
	}
	
	public void scrollToCurPage(){
		this.setHorizontalScroll(_curPageNumber * this.getWidth(), true);
	}
	
//	protected void onFocus(int direction)
//	{
//		
//	}
//	
//	protected void onUnfocus()
//	{
//		makeTimer();
//	}
	
	protected int nextFocus(int dir,int axis){
		int nIndex = _curPageNumber;
		if(axis == Field.AXIS_HORIZONTAL){
			if(nIndex == 0 && dir == -1)
				return m_count-1;
			if(nIndex == m_count - 1  && dir == 1)
				return 0;
			return nIndex + dir;
		}
		else
			return -1;
	}
}

