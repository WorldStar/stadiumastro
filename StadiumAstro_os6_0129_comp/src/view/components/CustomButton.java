package view.components;

import application.Utils;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class CustomButton extends HorizontalFieldManager {
	/**
	 * Private members
	 */
	private int nTotalWidth = 0;
	private int nTotalHeight = 0;

	private String strBMPSelPath = "";
	private String strBMPUnselPath = "";
	
	private Bitmap bmpSel = null;
	private Bitmap bmpUnSel = null;

	Background bmpSel1;
	Background bmpUnSel1;
	/**
	 * Create Object: (Location Candidate Button)
	 * @param strEnableBGFile: Background File when enable
	 * @param strDisableBGFile: Background File when disable
	 * @param leftPadding: Left Padding
	 * @param rightPadding: Right Padding
	 * @param strLabelText: Location String
	 */
	public CustomButton(String strBGSelFile, String strBGUnselFile)
	{
		super(USE_ALL_WIDTH|FIELD_VCENTER);

		/**
		 * initialize
		 */
		strBMPSelPath = strBGSelFile;
		strBMPUnselPath = strBGUnselFile;

		/**
		 * set Background
		 */
		bmpSel = Utils.getBitmapResource(strBMPSelPath);
		bmpUnSel = Utils.getBitmapResource(strBMPUnselPath);
		bmpSel1 = BackgroundFactory.createBitmapBackground(bmpSel);
		bmpUnSel1 = BackgroundFactory.createBitmapBackground(bmpUnSel);
		this.setBackground(bmpUnSel1);
		add(new NullField(NullField.FOCUSABLE));
		//initialze
		nTotalWidth = bmpSel.getWidth();
		nTotalHeight = bmpSel.getHeight();
	}
	
	/**
	 * overrides
	 */
	public int getPreferredHeight() {
		return nTotalHeight;
	}

	public void refreshBackGround(boolean flag){
		if(flag == true){
			this.setBackground(bmpSel1);
		}else{
			this.setBackground(bmpUnSel1);
		}
	}
	public int getPreferredWidth() {
		return nTotalWidth;
	}
	
	public void sublayout(int nWidth, int nHeight)
	{
		super.sublayout(nWidth, nHeight);
		this.setExtent(nTotalWidth, nTotalHeight);
	}
	
	/*protected void paint(Graphics graph) {
//		super.paint(graph);
		if (isFocus())
		{
			setBackground(BackgroundFactory.createBitmapBackground(bmpSel));
		}
		else
		{
			setBackground(BackgroundFactory.createBitmapBackground(bmpUnSel));
		}
	}*/

	protected boolean navigationClick(int status, int time) 
	{
		if (status != 0){
			fieldChangeNotify(0);
			clickButton();
		}
		return true;
	}

	public boolean keyChar(char key, int status, int time) 
	{
		if (key == Characters.ENTER) {
			fieldChangeNotify(0);
			clickButton();
			return true;
		}
		return false;
	}
	protected boolean trackwheelClick(int status, int time)
	{        
		if (status != 0) clickButton();    
		return true;
	}

	protected boolean invokeAction(int action) 
	{
		switch( action ) {
		case ACTION_INVOKE: {
			clickButton(); 
			return true;
		}
		}
		return super.invokeAction(action);
	}    

	/*public boolean isFocusable() 
	{
		return true;
	}*/

	protected boolean touchEvent(TouchEvent message)
	{
		int x = message.getX( 1 );
		int y = message.getY( 1 );
		if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
			// Outside the field
			return false;
		}
		switch( message.getEvent() ) {
		case TouchEvent.UNCLICK:
			clickButton();
			return true;
		}
		return super.touchEvent( message );
	}
	
	/**
	 * User Interaction
	 * 
	 */
	public void clickButton()
	{
		
	}
	protected void onFocus(int direction)
	{
		refreshBackGround(true);
	}
	
	protected void onUnfocus()
	{
		refreshBackGround(false);
	}
	
}
