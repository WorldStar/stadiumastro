package view.screen;

import application.AppSession;
import application.CustomBrowserFieldController;
import application.ResourceConstants;
import application.Utils;
import view.component.*;
import model.SAFeatureList;
import model.SASoccerMatchListItem;
import model.SASoccerMatchList;
import model.SAFeatureListItem;
import model.SASportEventContentItem;
import net.rim.blackberry.api.browser.Browser;
import net.rim.device.api.browser.field2.BrowserField;
import net.rim.device.api.browser.field2.BrowserFieldConfig;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.container.*;
import net.rim.device.api.ui.decor.*;

import net.rim.device.api.ui.*;

/**
 * 
 * @author Administrator
 *	This is a main Home Screen.
 */
public class HomeScreen extends BaseScreen{
	
	SASoccerMatchList m_soccerMatchList = null;
	SAFeatureList m_featureList = null;
	public HighlightNewsListScroll m_matchPreviewScroll = null;
	VerticalFieldManager contentsManager;
	CustomHorizontalFieldManager matchResultsManager;
	
	public HomeScreen() {
		
//		if(AppSession.sharedSession().m_AdsBar)
		super("Home");
		setHomeScreen();
		
	}
	public void setHomeScreen()
	{
		LoadingDialog.showLoadingDialog(new RInitHomeScreen() , "Loading");
	}

	protected void sublayout(int width, int height) {
		super.sublayout(width, height);
	}
	
	public void stopHighLightNewTimer()
	{
		if(m_matchPreviewScroll != null) m_matchPreviewScroll.closeTimer();
	}
	
	class RInitHomeScreen implements Runnable
	{
		public void run()
		{
	        
	        /**
	         * Main Contents Scroll View
	         */
	        
	        SASportEventContentItem contentItem= AppSession.sharedSession().m_sportEventContent;
	        
	        if(contentItem != null)
	        {
	        	if(contentItem.m_launchInAppBrowserFlag)
	        	{
	        		//background = BackgroundFactory.createSolidTransparentBackground(Color.WHITE, 200);
	    	        //setBackground(background);
	        		/*contentsManager = new VerticalFieldManager(Field.FIELD_HCENTER | Manager.VERTICAL_SCROLL);
	        		contentsManager.setBackground(background);

	        		BrowserFieldConfig myBrowserFieldConfig = new BrowserFieldConfig();
	        		BrowserField browser = new BrowserField(myBrowserFieldConfig);

	        		myBrowserFieldConfig.setProperty(
	        				BrowserFieldConfig.NAVIGATION_MODE,
	        				BrowserFieldConfig.NAVIGATION_MODE_POINTER);
	        		myBrowserFieldConfig.setProperty(BrowserFieldConfig.CONTROLLER, new CustomBrowserFieldController(browser));

	        		//browser.requestContent(contentItem.m_inAppUrlLink);
	        		//contentsManager.add(browser);
	        		*/
	        		Browser.getDefaultSession().displayPage(contentItem.m_inAppUrlLink);
	        		/*UiApplication.getUiApplication().invokeLater(new Runnable()
	        			{
	        				public void run()
	        				{
	        					Add(contentsManager);
	        				}
	        			}
	        		);*/
	        		
	        	}
	        	else
	        	{
	    			Background background = BackgroundFactory.createSolidTransparentBackground(Color.BLACK, 200);
	    	        setBackground(background);
	        		//SASoccer MatchBar
	        		if(!AppSession.sharedSession().m_curSportEventContentKey.equals("OTHERSSPORTS"))
	        		{
	        			//    			HorizontalFieldManager matchResultsManager = new HorizontalFieldManager(Manager.HORIZONTAL_SCROLL | Manager.FOCUSABLE);
	        			
	        			if (contentItem.m_includeFixture)
	        			{
	        				matchResultsManager = new CustomHorizontalFieldManager(Utils.getDisplayWidth(), ResourceConstants.SA_VAL_MATCHTAB_HEIGHT, Manager.HORIZONTAL_SCROLL | Manager.FOCUSABLE); 
		        			m_soccerMatchList = new SASoccerMatchList();
		        			m_soccerMatchList.initArrayWithParsedData(null);
		        			
		        			int nMatchCount = m_soccerMatchList.totalCount();

		        			for (int i = 0; i < nMatchCount; i++) {
		        				SASoccerMatchListItem matchInfo = m_soccerMatchList.getDataAtIndex(i);
		        				if(matchInfo == null)
		        					break;
		        				MatchTab match = new MatchTab(matchInfo,i);
		        				matchResultsManager.add(match);
		        			}
			        		UiApplication.getUiApplication().invokeLater(new Runnable(){
			        			public void run()
			        			{
			        				Add(matchResultsManager);
			        			}
			        		});
	        			}
	        		}

	        		//Highlight News List Scroll Bar.
	        		{
	        			// Initialize the picture scroll field

	        			//HighlightNewsListScroll matchPreviewScroll = new HighlightNewsListScroll(ResourceConstants.SA_VAL_TESTING_PREVIEW_IMG_COUNT,Manager.HORIZONTAL_SCROLL | Manager.RIGHTMOST);
	        			if(m_matchPreviewScroll != null) m_matchPreviewScroll.closeTimer();
	        			m_matchPreviewScroll = new HighlightNewsListScroll(Manager.HORIZONTAL_SCROLL | Manager.RIGHTMOST);
	        		}    		

	        		//Bottom TableView in ScrollView
	        		{    			
	        			contentsManager = new VerticalFieldManager(Field.FIELD_HCENTER | Manager.VERTICAL_SCROLL);
	        			contentsManager.setBackground(background);

	        			//First , Getting video infromation and construct the EventTableViewField and add it into contentsManager.
	        			{	   //in case Video Section Cell
	        				HorizontalFieldManager sectionCell = new HorizontalFieldManager();
	        				Bitmap sectionBackBitmap = Utils.getBitmapResource(ResourceConstants.SA_IMG_TABLE_SECTION);
	        				Background sectionBackground = BackgroundFactory.createBitmapBackground(sectionBackBitmap);
	        				sectionCell.setBackground(sectionBackground);

	        				CustomLabelField sectionTitleLabel = new CustomLabelField("Top Stories", Color.WHITE, Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , Utils.toDPIx(20), Utils.toDPIy(5), 640, Field.FIELD_LEFT);
	        				sectionCell.add(sectionTitleLabel);
	        				contentsManager.add(sectionCell);
	        			}


	        			//init Feature DataSourceArrays from WEB Server!
	        			m_featureList = new SAFeatureList();

	        			//VIDEO data..
//	        			int nVideoCount = m_featureList.totalVideoCount(); 
//	        			for(int i = 0 ; i < nVideoCount; i++)
//	        			{
//	        				SAFeatureListItem data = m_featureList.getVideoDataAtIndex(i);
//	        				if(data == null)
//	        					break;
//
//	        				EventTableViewCell cell = new EventTableViewCell(data, Field.FOCUSABLE, i , 0);
//	        				contentsManager.add(cell);
//	        			}

//	        			//Second , Getting news infromation and construct the EventTableViewField and add it into contentsManager.
//	        			{	   //in case News Section Cell
//	        				HorizontalFieldManager sectionCell = new HorizontalFieldManager();
//	        				Bitmap sectionBackBitmap = Utils.getBitmapResource(ResourceConstants.SA_IMG_TABLE_SECTION);
//	        				Background sectionBackground = BackgroundFactory.createBitmapBackground(sectionBackBitmap);
//	        				sectionCell.setBackground(sectionBackground);
//
//	        				CustomLabelField sectionTitleLabel = new CustomLabelField("News", Color.WHITE, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , Utils.toDPIx(20), Utils.toDPIy(5), 640, Field.FIELD_LEFT);
//	        				sectionCell.add(sectionTitleLabel);
//	        				contentsManager.add(sectionCell);
//	        			}
	        			

	        			//NEWS data..
	        			int nNewsCount = m_featureList.totalNewsCount();
	        			for(int i = 0 ; i < nNewsCount; i++)
	        			{
	        				SAFeatureListItem data = m_featureList.getNewsDataAtIndex(i);
	        				if(data == null)
	        					break;

//	        				EventTableViewCell cell = new EventTableViewCell(data, Field.FOCUSABLE, i + nVideoCount , 1); 
	        				EventTableViewCell cell = new EventTableViewCell(data, Field.FOCUSABLE, i , 1); 
	        				contentsManager.add(cell);
	        			}

	        			
	        		}
	        		UiApplication.getUiApplication().invokeLater(new Runnable(){
	        			public void run()
	        			{
	        				
	        				Add(m_matchPreviewScroll);
	        				Add(contentsManager);
	        			}
	        		});
	        	}
	        }
		}
	}
}
