package view.screen;

import view.component.*;
import model.SAVideoList;
import model.SAVideoListItem;
import net.rim.device.api.ui.container.*;
import net.rim.device.api.ui.decor.*;
import net.rim.device.api.ui.*;

public class VideoListScreen extends BaseScreen {
	private SAVideoList m_videoList = null;
	String m_strCategoryKey;
	public VideoListScreen(String strCategoryKey){
		super("Videos");
		Background background = BackgroundFactory.createSolidTransparentBackground(Color.BLACK, 200);
        this.setBackground(background);
        
        m_strCategoryKey = strCategoryKey;
        //Main Table View
        {
        	RVideoLoader videoListLoader = new RVideoLoader();
        	LoadingDialog.showLoadingDialog(videoListLoader, "Loading");
        }
      
     }
	
	public void deleteGarbage(){
		if(m_videoList != null){
			m_videoList.m_array.removeAllElements();
			m_videoList = null;
		}
		
		DeleteAll();
	}
	public void refresh()
	{
		deleteGarbage();
		RVideoLoader videoListLoader = new RVideoLoader();
    	LoadingDialog.showLoadingDialog(videoListLoader, "Loading");
	}
	class RVideoLoader implements Runnable
	{
		VerticalFieldManager mainTableView;
		public void run()
		{
			mainTableView = new VerticalFieldManager(Manager.VERTICAL_SCROLL | Manager.FIELD_HCENTER | Manager.FIELD_TOP);
        	
        	m_videoList = new SAVideoList(m_strCategoryKey); 
        	
        	int nCount = m_videoList.totalCount();
        	
        	for(int i = 0; i < nCount; i++)
        	{
        		SAVideoListItem item = m_videoList.getDataAtIndex(i);
        		EventTableViewCell mainTableCell = new EventTableViewCell(item, Field.FOCUSABLE,i , 0);
        		mainTableView.add(mainTableCell);
        	}
        	UiApplication.getUiApplication().invokeLater(new Runnable()
        	{
        		public void run()
        		{
        			Add(mainTableView);
        		}
        	});
        	
		}
	}
}