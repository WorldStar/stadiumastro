package view.screen;

import view.component.*;
import model.SAVideoCategoryList;
import model.SACategoryItem;
import net.rim.device.api.ui.container.*;
import net.rim.device.api.ui.decor.*;
import net.rim.device.api.ui.*;

public class VideoMainScreen extends BaseScreen {
	private SAVideoCategoryList m_videoCategoryList = null;
	
	public VideoMainScreen(){
		super("Videos");
		Background background = BackgroundFactory.createSolidTransparentBackground(Color.BLACK, 255);
        this.setBackground(background);
        
        //Main Table View
        LoadingDialog.showLoadingDialog(new RVideoCategoryLoader(), "Loading");
        
	}
	
	public void deleteGarbage(){
		if(m_videoCategoryList != null){
			m_videoCategoryList.m_array.removeAllElements();
			m_videoCategoryList = null;
		}
		
		DeleteAll();
	}
	public void refresh()
	{
		deleteGarbage();
		LoadingDialog.showLoadingDialog(new RVideoCategoryLoader(), "Loading");
	}
	class RVideoCategoryLoader implements Runnable
	{
		VerticalFieldManager mainTableView;
		public void run()
		{
			mainTableView = new VerticalFieldManager(Manager.VERTICAL_SCROLL | Manager.FIELD_HCENTER | Manager.FIELD_TOP);
        	mainTableView.setPadding(20, 10, 10, 10);
        	
        	m_videoCategoryList = new SAVideoCategoryList(); 
        	m_videoCategoryList.initArrayWithParsedData();
        	
        	int nCount = m_videoCategoryList.totalCount();
        	for(int i = 0; i < nCount; i++)
        	{
        		SACategoryItem item = m_videoCategoryList.getDataAtIndex(i);
        		CategoryListCell mainTableCell = new CategoryListCell(item, null , true, Field.FOCUSABLE);
        		mainTableView.add(mainTableCell);
        	}
        	UiApplication.getUiApplication().invokeLater(new Runnable()
        	{
        		public void run()
        		{
        			Add(mainTableView);
        		}
        	}
        	);
        	
		}
	}
}
