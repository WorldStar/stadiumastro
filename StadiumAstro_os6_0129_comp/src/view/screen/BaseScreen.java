package view.screen;

import java.util.Vector;

import application.AppSession;
import application.ResourceConstants;
import application.Utils;
import view.component.AdsBar;
import view.component.BottomSubMenu;
import view.component.NavigationBar;
import model.SASportEventContentItem;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.component.Menu;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.menu.SubMenu;


/**
 * 
 * @author Administrator
 *	This is template Screen of Our Program.
 *	This consists of 
 *						1.	Navigation Bar.
 *						2.  Ads Banner List Bar.
 *						3.	MainScroll View of Any Screen. - You can add anyone into this container by call Add().
 * 						4.	Bottom Sub Menu Bar.
 * 						5.	Main Menu.
 * 
 *	Other screens extends this and Add their own children component themselves.
 */
public class BaseScreen extends MainScreen {
	
	NavigationBar m_NavBar = null;
	AdsBar m_Ads_Bar = null;
	BottomSubMenu m_BottomSubMenu = null;
	
	public VerticalFieldManager m_MainScroller;
	private int mainScrollHeight;
	
	public BaseScreen(String title){
		super(VERTICAL_SCROLL);
		
		setBaseScreen(title,false,true);
	}
	
	public BaseScreen(String title,boolean bHideNavBar){
		super(VERTICAL_SCROLL);
		setBaseScreen(title, bHideNavBar, true);
	}
	public void setBaseScreen(String title,boolean bHideNavBar, boolean bShowAdsBar)
	{
		
		if(!bHideNavBar)
			mainScrollHeight = Utils.getDisplayHeight() - ResourceConstants.SA_VAL_NAVBAR_HEIGHT - ResourceConstants.SA_VAL_BOTTOM_MENU_OPEN_CLOSE_BUTTON_SIZE;
		else
			mainScrollHeight = Utils.getDisplayHeight() - ResourceConstants.SA_VAL_BOTTOM_MENU_OPEN_CLOSE_BUTTON_SIZE;
		
		m_MainScroller = new VerticalFieldManager(VERTICAL_SCROLL){
	    	protected void sublayout(int maxWidth, int maxHeight) {
				maxWidth = Utils.getDisplayWidth();
				maxHeight = mainScrollHeight;
				super.sublayout(maxWidth, maxHeight);
				setExtent(maxWidth, maxHeight);
			}
	    };
	    
		Background background = BackgroundFactory.createSolidTransparentBackground(Color.BLACK, 255);
		super.setBackground(background);
        this.setBackground(background);
		m_MainScroller.setBackground(background);
		
		//ADD NAVIGATION BAR
		if(!bHideNavBar)
		{
			m_NavBar = new NavigationBar(title, this);
	  		add(m_NavBar);
		}
  		
  		//ADD ADS_BAR
		if(bShowAdsBar)
			showAdsBar();
  		
  		System.out.println("Stadium Astro Main Scroll bar");
  		//ADD MAIN SCROLLER
  		add(m_MainScroller);

  		System.out.println("Stadium Astro Bottom sub menu");
  		//ADD BOTTOM SUB MENU
  		boolean flag;
		if(title == "Home")
			flag = true;
		else
			flag = false;
		
		SASportEventContentItem eventContent = AppSession.sharedSession().m_sportEventContent;
		if(eventContent == null || !eventContent.m_launchInAppBrowserFlag)
		{
			//If we have to launch browser, we needn't make bottom menu.
			m_BottomSubMenu = new BottomSubMenu(flag,Field.FIELD_HCENTER | Field.USE_ALL_WIDTH);
			add(m_BottomSubMenu);
		}
		 
	}
	/**
	 * public Method for add Field to MainScroller.
	 * @param field
	 */
	public void Add(Field field){
		m_MainScroller.add(field);
	}
	
	/**
	 * public Method for delete Field from MainScroller.
	 * @param field
	 */
	public void Delete(Field field){
		m_MainScroller.delete(field);
	}
	/**
	 * public Method for delete Field from MainScroller.
	 * @param field
	 */
	public void DeleteAll(){
		//Delete all field except Ads bar.
		m_MainScroller.deleteAll();
		if(m_Ads_Bar != null)
			m_MainScroller.add(m_Ads_Bar);
	}
	
	/**
	 * Show bottom sub Menu.
	 */
	public void ShowMenu(){
		m_BottomSubMenu.showSubMenu();
		add(m_BottomSubMenu._subMenuButtonManager);
		
		getMainManager().setVerticalScroll(Utils.getDisplayHeight(), true);
//		Dialog.alert(" " + mainContainer.getVerticalScroll());	
	}
	public void HideMenu(){
		m_BottomSubMenu.hideSubMenu();
		delete(m_BottomSubMenu._subMenuButtonManager);
	}
	/**
	 * if did not close AdsBar then show AdsBar
	 */
	public void showAdsBar(){
		if(!AppSession.sharedSession().m_showAdsBar)
			return;
		if(m_Ads_Bar == null)
			m_Ads_Bar = new AdsBar();
		Add(m_Ads_Bar);
		System.out.println("Import ads bar Success");
	}
	/**
	 * @function Hide ads bar. 
	 */
	public void hideAdsBar(){
		if(m_Ads_Bar != null)
		{
			Delete(m_Ads_Bar);
			m_Ads_Bar = null;
		}
	}
	/**
	 * Make Main Menu.
	 */
	
	protected void makeMenu(Menu menu, int context)
    {
		//StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
		//app.showMainMenu(menu,context);
		Vector menuVec = AppSession.sharedSession().m_mainSubMenus;
		for(int i = 0; i < menuVec.size(); i++)
		{
			Object menuItem= menuVec.elementAt(i);
			if(menuItem instanceof SubMenu)
			{
				menu.add((SubMenu) menuItem);
			}
			else if(menuItem instanceof MenuItem)
				menu.add((MenuItem) menuItem);
			
		}
		menu.addSeparator();
    }
	
	protected void SetTitleString(String title)
	{
		if(m_NavBar != null)
			m_NavBar.setTitle(title);
	}
	public void deleteGarbage()
	{
		
	}
	
	public void refresh()
	{
		
	}
}
