package view.screen;

import application.ResourceConstants;
import view.component.CategoryListCell;
import view.component.LoadingDialog;
import model.SATVChannelList;
import model.SATVChannelListItem;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class TVChannelListScreen extends BaseScreen {
	
	SATVChannelList m_tvChannelList = null;
	
	public TVChannelListScreen(){
		super("Channels");
		Background background = BackgroundFactory.createSolidTransparentBackground(Color.BLACK, 255);
		setBackground(background);
		LoadingDialog.showLoadingDialog(new RChannelLoader(), "Loading");
		 //Main Table View
	}
	public String getChannelRefKey(int index)
	{
		SATVChannelListItem tvChannelItem = m_tvChannelList.getDataAtIndex(index);
		return tvChannelItem.m_id;
		
	}
	
	public void deleteGarbage()
	{
		if(m_tvChannelList != null){
			m_tvChannelList.m_array.removeAllElements();
			m_tvChannelList = null;
		}

		DeleteAll();
	}
	class RChannelLoader implements Runnable
	{
		VerticalFieldManager mainTableView;
		
		public void run()
		{
			mainTableView = new VerticalFieldManager(Manager.VERTICAL_SCROLL | Manager.FIELD_HCENTER | Manager.FIELD_TOP);
        	mainTableView.setPadding(20, 10, 10, 10);
        	
        	m_tvChannelList = new SATVChannelList();
        	
        	int nCount = m_tvChannelList.totalCount();
        	for(int i = 0; i < nCount; i++){
        		SATVChannelListItem tvChannelItem = m_tvChannelList.getDataAtIndex(i);
        		CategoryListCell channelTableCell = new CategoryListCell(tvChannelItem.m_bitmapURL, ResourceConstants.SA_ICON_MORE_CHANNEL , tvChannelItem.m_name ,true,Field.FOCUSABLE);
        		mainTableView.add(channelTableCell);
        	}
        	UiApplication.getUiApplication().invokeLater(new Runnable()
        	{
        		public void run()
        		{
        			Add(mainTableView);
        		}
        	});
        	
		}
	}
	public void refresh()
	{
		deleteGarbage();
		LoadingDialog.showLoadingDialog(new RChannelLoader(), "Loading");
	}
}
