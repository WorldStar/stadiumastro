package view.screen;

import application.ResourceConstants;
import view.component.CategoryListCell;

import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Manager;

import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

/**
 * 
 * @author Administrator
 *	This is More Screen.
 */
public class MoreScreen extends BaseScreen {
	public MoreScreen(){
		super("More");
		Background background = BackgroundFactory.createSolidTransparentBackground(Color.BLACK, 255);
        this.setBackground(background);
        
		 //Main Table View
        {
        	VerticalFieldManager mainTableView = new VerticalFieldManager(Manager.VERTICAL_SCROLL | Manager.FIELD_HCENTER | Manager.FIELD_TOP);
        	mainTableView.setPadding(20, 10, 10, 10);
        	mainTableView.setBackground(background);
        	
        	        	
        	CategoryListCell channelTableCell = new CategoryListCell(ResourceConstants.SA_IMG_CELL_BACK_TOP,  ResourceConstants.SA_ICON_MORE_CHANNEL , ResourceConstants.SA_TITLE_MORE_CHANNEL ,true,Field.FOCUSABLE);
        	mainTableView.add(channelTableCell);
        	
        	CategoryListCell alertTableCell = new CategoryListCell(ResourceConstants.SA_IMG_CELL_BACK_MIDDLE, ResourceConstants.SA_ICON_MORE_ALERT , ResourceConstants.SA_TITLE_MORE_PUSH_NOTIFICATION ,true,Field.FOCUSABLE);
        	mainTableView.add(alertTableCell);
        	
        	CategoryListCell appTableCell = new CategoryListCell(ResourceConstants.SA_IMG_CELL_BACK_BOTTOM_WITHOUT_ARROW, ResourceConstants.SA_ICON_MORE_APP_VERSION , ResourceConstants.SA_TITLE_MORE_APP_VERSION ,true,Field.FOCUSABLE);
        	mainTableView.add(appTableCell);
        	
        	Add(mainTableView);

        }
	}
	
	public void deleteGarbage(){
		DeleteAll();
	}
}
