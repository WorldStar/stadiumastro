package view.screen;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import view.component.CustomBitmapField1;
import view.component.MainMenu;
import application.AppSession;
import application.ResourceConstants;
import application.StadiumAstroApp;
import application.Utils;
import model.DataManager;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.container.AbsoluteFieldManager;
import net.rim.device.api.ui.container.FullScreen;
import net.rim.device.api.ui.extension.container.EyelidFieldManager;

public class PartnerLogoScreen extends FullScreen implements FieldChangeListener {

	AbsoluteFieldManager manager;

	public PartnerLogoScreen() {
		super();

		MainMenu mainMenu = new MainMenu();

    	System.out.println("Partner Logo Screen");

		////////////////////////////////////////////////////////
		// TODO Auto-generated method stub
		String strResponse;
		String strImageUrl = "";

		// get response data from server
		if(AppSession.useLocalDataForTest)
			strResponse = ResourceConstants.SA_JSON_DATA_ADS_BANNER_LIST;
		else
		{
			String strRequest = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + ResourceConstants.SA_ADS_BANNER_LIST_API_PATH + "FULL_ADS";
			strResponse = DataManager.getResponseDataFromServer(strRequest);
		}
		
		try{
			// parse json data
			JSONObject objAdsBannerList = new JSONObject(strResponse);
			JSONArray arrAdsBannerContent = objAdsBannerList.getJSONArray(ResourceConstants.SA_KEY_ADS_BANNER);
			// use only first ads banner content
			JSONObject objAdsBanner = (JSONObject)arrAdsBannerContent.get(0);

			// determine image file path
			strImageUrl = ResourceConstants.SA_PRODUCTION_SERVER_ROOT_URL + objAdsBanner.get(ResourceConstants.SA_KEY_ADS_BANNER_FULL_FILE).toString();
			
			objAdsBannerList = null;
		}
		catch(JSONException e)
		{
			System.out.println(e.toString());
		}
		////////////////////////////////////////////////
    	CustomBitmapField1 bgImage = new CustomBitmapField1(strImageUrl, null, 0, 0, Utils.getDisplayWidth(), Utils.getDisplayHeight(), Manager.FIELD_HCENTER, ResourceConstants.SA_IMG_SPLASH);

    	// get menu data and confirm menu
    	manager = new AbsoluteFieldManager();
    	manager.add(bgImage);

    	add(manager);
    
    	UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {
				showSkipAdsButton();
			}
		}, Utils.SKIPADS_APPEAR_AFTER_TIME, false);
	}
	
	protected void sublayout(int width, int height) {
		// TODO Auto-generated method stub
		super.sublayout(width, height);
	}
	
    void showSkipAdsButton() {
    	EyelidFieldManager eyemanager = new EyelidFieldManager();
    	ButtonField mSkipAdsLabelField = new ButtonField("Skip Ads>>", FIELD_RIGHT);
    	mSkipAdsLabelField.setChangeListener(this);
    	eyemanager.addBottom(mSkipAdsLabelField);
    	manager.add(eyemanager);
    }

	public void fieldChanged(Field field, int context) {
		StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
		AppSession.sharedSession().m_curSportEventContentKey = "BPL";
		app.gotoHomeScreenFromPartnerLogoScreen();
	}
}