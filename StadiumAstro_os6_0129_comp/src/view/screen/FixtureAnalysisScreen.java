package view.screen;

import java.util.Calendar;


import application.ResourceConstants;
import application.StadiumAstroApp;
import application.Utils;
import view.component.*;

import model.SAInMatchClipsList;
import model.SAMatchEventItem;
import model.SAMatchEventList;
import model.SAMatchPreview;
import model.SAMatchReport;

import model.SASoccerMatchListItem;
import model.SATeamLineUpListItem;
import model.SATeamLineUpList;
import model.SAVideoListItem;
import net.rim.device.api.browser.field2.BrowserField;
import net.rim.device.api.browser.field2.BrowserFieldConfig;
import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.*;
import net.rim.device.api.ui.decor.*;


/**
 * 
 * @author Administrator
 *	This is a Analysis Screen.
 */
public class FixtureAnalysisScreen extends BaseScreen  {

	public int curPageNumber = 0;
	public CustomTabButtonField1 m_curSelectTabButtonField = null;
	public static int select_id = 1;
	CustomHorizontalFieldManager tabButtonsManager;
	
	VerticalFieldManager scrollPane;
	
	VerticalFieldManager pane1;		//preview
	VerticalFieldManager pane2;		//report
	VerticalFieldManager pane3;		//team line-up
	VerticalFieldManager pane4;		//match event
	VerticalFieldManager pane5;		//video highlights
	VerticalFieldManager pane6;		//in match clips
	
	VerticalFieldManager oldPane = null;
	VerticalFieldManager newPane = null;
			
	CustomTabButtonField1 previewButton;
	CustomTabButtonField1 reportButton;
	CustomTabButtonField1 teamLineUpButton;
	CustomTabButtonField1 matchEventButton;
	CustomTabButtonField1 videoHighlightsButton;
	CustomTabButtonField1 inMatchClipsButton;
	
	SAMatchPreview m_matchPreview = null;
	SAMatchReport m_matchReport = null;
	SATeamLineUpList m_teamLineUpList = null;
	SAMatchEventList m_matchEventList = null;
	SAInMatchClipsList m_inMatchClipList = null;
	
	String strRefKey = "";
	String dateStr;
	
	private boolean m_bStartUp = false;
	
	public FixtureAnalysisScreen(SASoccerMatchListItem matchItem){
		super("Analysis");
		
		strRefKey = matchItem.m_strID;
		
		//set background with black color.
		Background background = BackgroundFactory.createSolidTransparentBackground(Color.BLACK, 255);
        this.setBackground(background);
        
		//Date bar
      //Date bar
  		{
  			HorizontalFieldManager dateBar = new HorizontalFieldManager()
  			{
  				protected void sublayout(int maxWidth, int maxHeight) {
  					maxWidth = Utils.getDisplayWidth();
  					maxHeight = ResourceConstants.SA_VAL_FIXTURE_DATE_BAR_HEIGHT;
  					super.sublayout(maxWidth, maxHeight);
  					setExtent(maxWidth, maxHeight);
  				}
  			};
  			Background dateBarbackground = BackgroundFactory.createSolidTransparentBackground(Color.GRAY, 200);
  			dateBar.setBackground(dateBarbackground);
  			
  			dateStr = Utils.getStringFromDate(matchItem.m_dateCalendar);
  			
  			int fontSize = ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE;
              int labelWidth = Utils.getTypeFace().getFont(Font.BOLD, fontSize).getBounds(dateStr);
              int marginX = (Utils.getDisplayWidth() - labelWidth) / 2;
  			CustomLabelField dateLabel = new CustomLabelField(dateStr, Color.WHITE, Utils.getTypeFace().getFont(Font.PLAIN,ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), marginX, Utils.toDPIy(5), 500, Manager.FIELD_HCENTER);
  			dateBar.add(dateLabel);
  			
  			Add(dateBar);
  		}
		//One of MatchTableCell
  		{
			
			AnalysisHeaderMatchInfo topMatchCell = new AnalysisHeaderMatchInfo(matchItem);
			Add(topMatchCell);
		}
		//Scroll Tab Bar
		{
			HorizontalFieldManager tabBarManager = new HorizontalFieldManager(NO_HORIZONTAL_SCROLL);
			
			//1.left Arrow button
			{
				ImageButtonField leftArrowButton = new ImageButtonField("TAB_BAR_LEFT",ResourceConstants.SA_IMG_TABBAR_LEFT_BUTTON, 0,Color.ALICEBLUE, Manager.FIELD_LEFT){
					public void clickButton(){
						scrollLeft();
	        		}
				};
				tabBarManager.add(leftArrowButton);
			}
			
			//2.tabBarScrollField
			{
				tabButtonsManager = new CustomHorizontalFieldManager(ResourceConstants.SA_VAL_ANAYSIS_TAB_BAR_WIDTH,ResourceConstants.SA_VAL_ANAYSIS_TAB_BAR_HEIGHT,Manager.HORIZONTAL_SCROLL){

				    
				};
				
				previewButton = new CustomTabButtonField1(1,"Preview", ResourceConstants.SA_IMG_TABBAR_NORMAL_GRAY_BACK, 0, Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD,ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), Manager.FIELD_LEFT);
				previewButton.setOffPicture(Utils.getBitmapResource(ResourceConstants.SA_IMG_TABBAR_TABBED_BLUE_BACK));
				previewButton.refresh(true);
				
				reportButton = new CustomTabButtonField1(2,"Report", ResourceConstants.SA_IMG_TABBAR_NORMAL_GRAY_BACK, 0, Color.BLACK, Utils.getTypeFace().getFont(Font.BOLD,ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL),Manager.FIELD_LEFT);
				reportButton.setOffPicture(Utils.getBitmapResource(ResourceConstants.SA_IMG_TABBAR_TABBED_BLUE_BACK));
				
				teamLineUpButton = new CustomTabButtonField1(3,"Team Line-up", ResourceConstants.SA_IMG_TABBAR_NORMAL_GRAY_BACK, 0, Color.BLACK, Utils.getTypeFace().getFont(Font.BOLD,ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL),Manager.FIELD_LEFT);
				teamLineUpButton.setOffPicture(Utils.getBitmapResource(ResourceConstants.SA_IMG_TABBAR_TABBED_BLUE_BACK));
				
				matchEventButton = new CustomTabButtonField1(4,"Match Events", ResourceConstants.SA_IMG_TABBAR_NORMAL_GRAY_BACK, 0, Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD,ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), Manager.FIELD_LEFT);
				matchEventButton.setOffPicture(Utils.getBitmapResource(ResourceConstants.SA_IMG_TABBAR_TABBED_BLUE_BACK));
				
				videoHighlightsButton = new CustomTabButtonField1(5,"Video Highlights", ResourceConstants.SA_IMG_TABBAR_NORMAL_GRAY_BACK, 0, Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD,ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), Manager.FIELD_LEFT);
				videoHighlightsButton.setOffPicture(Utils.getBitmapResource(ResourceConstants.SA_IMG_TABBAR_TABBED_BLUE_BACK));
				
				inMatchClipsButton = new CustomTabButtonField1(6,"In-Match Clips", ResourceConstants.SA_IMG_TABBAR_NORMAL_GRAY_BACK, 0, Color.BLACK,Utils.getTypeFace().getFont(Font.BOLD,ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), Manager.FIELD_LEFT);
				inMatchClipsButton.setOffPicture(Utils.getBitmapResource(ResourceConstants.SA_IMG_TABBAR_TABBED_BLUE_BACK));
				//tabButtonsManager.add(new NullField(Field.FOCUSABLE));
				tabButtonsManager.add(previewButton);
				tabButtonsManager.add(reportButton);
				tabButtonsManager.add(teamLineUpButton);
				tabButtonsManager.add(matchEventButton);
				tabButtonsManager.add(videoHighlightsButton);
				tabButtonsManager.add(inMatchClipsButton);
				
				tabBarManager.add(tabButtonsManager);
				
			}
			
			//3.rightArrowButton
			{
				ImageButtonField rightArrowButton = new ImageButtonField("TAB_BAR_RIGHT",ResourceConstants.SA_IMG_TABBAR_RIGHT_BUTTON, 0,Color.ALICEBLUE, Manager.FIELD_RIGHT){
					public void clickButton(){	
	        			scrollRight();
	        		}
				};
				tabBarManager.add(rightArrowButton);
			}
			
			Add(tabBarManager);
		}
		
		//init Content Scroll View
		{
			scrollPane = new VerticalFieldManager();//(Utils.getDisplayWidth() * 2,Utils.getDisplayHeight() / 2,Manager.FIELD_BOTTOM);			
			Add(scrollPane);
		}

		curPageNumber = 1;
		initContentPanes();
		
		
	}

	public void refreshButton1() {
		previewButton.refresh(false);
		reportButton.refresh(false);
		teamLineUpButton.refresh(false);
		matchEventButton.refresh(false);
		videoHighlightsButton.refresh(false);
		inMatchClipsButton.refresh(false);
	}
	public void initContentPanes(){
		LoadingDialog.showLoadingDialog(new RFixtureInitPannel(), "Loading");
	}	
	
	public void initPane1(){
		pane1 =  new VerticalFieldManager(Manager.NO_HORIZONTAL_SCROLL);//(Utils.getDisplayWidth() ,Utils.getDisplayHeight() / 2,Manager.VERTICAL_SCROLL);
		Background background = BackgroundFactory.createSolidTransparentBackground(Color.WHITE, 255);
		pane1.setBackground(background);
		
		CustomLabelField titleLabel = new CustomLabelField(m_matchPreview.m_title, Color.BLACK, Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Utils.toDPIx(20), Utils.toDPIy(20), 600, Manager.FIELD_LEFT);
		CustomLabelField dateLabel = new CustomLabelField(dateStr, Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Utils.toDPIx(20), Utils.toDPIy(20), 400, Manager.FIELD_LEFT);
		
		pane1.add(titleLabel);
		pane1.add(dateLabel);
		
		if(m_matchPreview.m_body.compareTo("Not Yet Available!") == 0)
		{
			CustomLabelField notAvail = new CustomLabelField("Match Preview will be available within 24 hours before the match starts", Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_SMALL), Utils.toDPIx(20), Utils.toDPIy(20), Utils.getDisplayWidth() - Utils.toDPIx(20), Manager.FIELD_LEFT);
			pane1.add(notAvail);
		}
		else
		{
			BrowserFieldConfig browserConfig = new BrowserFieldConfig();
			BrowserField webBrowser = new BrowserField(browserConfig);
			try{
				webBrowser.setMargin(Utils.toDPIy(20), Utils.toDPIx(20), Utils.toDPIy(10),  Utils.toDPIx(10));
	//			webBrowser.displayContent("<span style='font-size:8pt'>" + m_matchPreview.m_body + "</span>", "");
				//m_matchPreview.m_body = Utils.replaceString(m_matchPreview.m_body,"<br />","<br>");
				//m_matchPreview.m_body = Utils.replaceString(m_matchPreview.m_body,"<br/>","<br>");
				String a = m_matchPreview.m_body;
				a = Utils.replaceString(a,"&lt;","<");
				a = Utils.replaceString(a,"&gt;",">");
				a = Utils.replaceString(a,"/&gt;",">");
				a = Utils.replaceString(a,"&#39;","'");
				a = Utils.replaceString(a,"&#163;","f");
				a = Utils.replaceString(a,"&quot;","\\\"");
				a = Utils.replaceString(a,"\r","");
				a = Utils.replaceString(a,"\n","");
				webBrowser.displayContent(Utils.setHTMLContentSize(a, 6, "white", "black"), "");
			}catch(Exception e){}
			pane1.add(webBrowser);
		}
		
		if (!m_bStartUp)
			previewButton.refresh(true);
		
	}
	
	public void refreshButton() {
		previewButton.refresh(false);
		reportButton.refresh(false);
	}
	
	public void initPane2(){
		pane2 = new VerticalFieldManager(Manager.NO_HORIZONTAL_SCROLL);//new CustomVerticalFieldManager(Utils.getDisplayWidth() ,Utils.getDisplayHeight() / 2,Manager.VERTICAL_SCROLL);
		Background background = BackgroundFactory.createSolidTransparentBackground(Color.WHITE, 255);
        pane2.setBackground(background);
        
		CustomLabelField titleLabel = new CustomLabelField(m_matchReport.m_title, Color.BLACK, Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Utils.toDPIx(20), Utils.toDPIy(20), 600, Manager.FIELD_LEFT);
//		Calendar cal = Utils.getDateFromString(m_matchReport.m_publishedDate);
//		String dateString = cal.get(Calendar.DAY_OF_MONTH) + " " + Utils.getMonthString(cal.get(Calendar.MONTH)) + " " + cal.get(Calendar.YEAR) + " " + cal.get(Calendar.HOUR_OF_DAY)+":"+cal.get(Calendar.MINUTE);
//		CustomLabelField dateLabel = new CustomLabelField(dateString, Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Utils.toDPIx(20), Utils.toDPIy(20), 400, Manager.FIELD_LEFT);
		CustomLabelField dateLabel = new CustomLabelField(dateStr, Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Utils.toDPIx(20), Utils.toDPIy(20), 500, Manager.FIELD_LEFT);
		
		pane2.add(titleLabel);
		pane2.add(dateLabel);
		if(m_matchReport.m_body.compareTo("Not Yet Available!") == 0)
		{
			CustomLabelField notAvail = new CustomLabelField("Match Report will be available within 24 hours before the match starts", Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_SMALL), Utils.toDPIx(20), Utils.toDPIy(20), Utils.getDisplayWidth() - Utils.toDPIx(20), Manager.FIELD_LEFT);
			pane2.add(notAvail);
		}
		else
		{
			BrowserFieldConfig browserConfig = new BrowserFieldConfig();
			
			BrowserField decription = new BrowserField();
			try{
				decription.setMargin(Utils.toDPIy(20), Utils.toDPIx(20), Utils.toDPIy(10), Utils.toDPIx(10));
				    
	//			decription.displayContent("<span style='font-size:8pt'>" + m_matchReport.m_body + "</span>", "");
				String a = m_matchReport.m_body;
				a = Utils.replaceString(a,"&lt;","<");
				a = Utils.replaceString(a,"&gt;",">");
				a = Utils.replaceString(a,"/&gt;",">");
				a = Utils.replaceString(a,"&#39;","'");
				a = Utils.replaceString(a,"&#163;","f");
				a = Utils.replaceString(a,"&quot;","\\\"");
				a = Utils.replaceString(a,"\r","");
				a = Utils.replaceString(a,"\n","");
				decription.displayContent(Utils.setHTMLContentSize(a, 6, "white", "black"),"");
				
			}catch(Exception e){}
			
			pane2.add(decription);
		}
		
		if (!m_bStartUp)
			reportButton.refresh(true);
	}
	
	/**
	 * When left / right arrow click.
	 */
	public void scrollRight(){
		int movement = 150;
		int curScroll = tabButtonsManager.getHorizontalScroll();
		if(curScroll >= tabButtonsManager.getContentWidth())
			movement = 0;
		tabButtonsManager.setHorizontalScroll(curScroll + movement, true);
	}
	public void scrollLeft(){
		int movement = 150;
		int curScroll = tabButtonsManager.getHorizontalScroll();
		if(curScroll == 0)
			movement = 0;
		tabButtonsManager.setHorizontalScroll(curScroll - movement, true);
	}	
	
	/**
	 * 
	 * @param index - index of page which show next time
	 */
	/**
	 * @param index
	 */
	public void readyCurPage(int index){
		if (!m_bStartUp) {
			refreshButton();
		}
		
		switch(index){
		case 1:
			//KJH
			if (!m_bStartUp)
				initPane1();
			
			newPane = pane1;
			break;
		case 2:
			//KJH
			if (!m_bStartUp)
				initPane2();
			
			newPane = pane2;
			break;
		case 3:
			newPane = pane3;
			break;
		case 4:
			newPane = pane4;
			break;
		case 5:
			newPane = pane5;
			break;
		case 6:
			newPane = pane6;
			break;
		}
		curPageNumber = index;
		scrollPane.add(newPane);
		
		if (m_bStartUp) {
			oldPane = newPane;
			m_bStartUp = false;
		}
	}
	
	public void goCurPage()
	{
		newPane.setVerticalScroll(0);
		//scrollPane.setHorizontalScroll(Utils.getDisplayWidth(),true);
		
		if(oldPane != null) {
			scrollPane.delete(oldPane);
		}
			
		//scrollPane.setHorizontalScroll(0,false);
		oldPane = newPane;	
	}
	
	
	public void deleteGarbage(){
		tabButtonsManager.deleteAll();
		tabButtonsManager = null;
		
		scrollPane.deleteAll();
		scrollPane = null;
		
		m_matchPreview = null;
		m_matchReport = null;
		
		if(m_teamLineUpList != null){
			m_teamLineUpList.m_arrayAwayTeam.removeAllElements();
			m_teamLineUpList.m_arrayHomeTeam.removeAllElements();
			m_teamLineUpList = null;
		}
		if(m_matchEventList != null){
			m_matchEventList.m_array.removeAllElements();
			m_matchEventList = null;
		}
		if(m_inMatchClipList != null){
			m_inMatchClipList.m_array.removeAllElements();
			m_inMatchClipList = null;
		}
	}
	
	public void refresh()
	{
		if(oldPane != null)
		{
			scrollPane.delete(oldPane);
		}
		initContentPanes();
		
	}

	class RFixtureInitPannel implements Runnable
	{
		public void run()
		{
			//KJH
			m_bStartUp = true;
			
			Background background = BackgroundFactory.createSolidTransparentBackground(Color.WHITE, 255);
			
			//preview pane
			m_matchPreview = new SAMatchPreview(strRefKey);
			{
				//m_bStartUp = false;
				initPane1();
			}
			//report pane
			m_matchReport = new SAMatchReport(strRefKey);
			{
				//m_bStartUp = false;
				initPane2();
			}
			//team line-up pane
			m_teamLineUpList = new SATeamLineUpList(strRefKey);
			{
				pane3 = new VerticalFieldManager(Manager.NO_HORIZONTAL_SCROLL);//new CustomVerticalFieldManager(Utils.getDisplayWidth() ,Utils.getDisplayHeight() / 2,Manager.VERTICAL_SCROLL);
		        pane3.setBackground(background);
		        
		        int nCount = m_teamLineUpList.totalAwayTeamCount();
		        if(nCount > 0)
		        {
			        for(int i = 0; i < nCount; i++){
			        	SATeamLineUpListItem awayTeamItem = m_teamLineUpList.getAwayTeamDataAtIndex(i);
			        	SATeamLineUpListItem homeTeamItem = m_teamLineUpList.getHomeTeamDataAtIndex(i);
			        	
			        	Background back;
			        	if(i % 2 == 0)
			        		back = BackgroundFactory.createSolidTransparentBackground(Color.WHITE, 255);
			        	else
			        		back = BackgroundFactory.createSolidTransparentBackground(Color.LIGHTGRAY, 255);
			        	
			        	CustomTableCell cell = new CustomTableCell(awayTeamItem.m_position,awayTeamItem.m_playerName,homeTeamItem.m_position,homeTeamItem.m_playerName , back);
			        	pane3.add(cell);
			        }
		        }
		        else
		        {
		    		CustomLabelField titleLabel = new CustomLabelField(m_matchPreview.m_title, Color.BLACK, Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Utils.toDPIx(20), Utils.toDPIy(20), 600, Manager.FIELD_LEFT);
//		    		Calendar cal = Utils.getDateFromString(m_matchReport.m_publishedDate);
//		    		String dateString = cal.get(Calendar.DAY_OF_MONTH) + " " + Utils.getMonthString(cal.get(Calendar.MONTH)) + " " + cal.get(Calendar.YEAR) + " " + cal.get(Calendar.HOUR_OF_DAY)+":"+cal.get(Calendar.MINUTE);
//		    		CustomLabelField dateLabel = new CustomLabelField(dateString, Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Utils.toDPIx(20), Utils.toDPIy(20), 400, Manager.FIELD_LEFT);
		    		CustomLabelField dateLabel = new CustomLabelField(dateStr, Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Utils.toDPIx(20), Utils.toDPIy(20), 500, Manager.FIELD_LEFT);

		    		pane3.add(titleLabel);
		    		pane3.add(dateLabel);
		    		
		    		CustomLabelField notAvail = new CustomLabelField("The data will be available when the match starts", Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_SMALL), Utils.toDPIx(20), Utils.toDPIy(20), Utils.getDisplayWidth() - Utils.toDPIx(20), Manager.FIELD_LEFT);
		    		pane3.add(notAvail);
		        }
	       
			}
			//match events pane
			m_matchEventList = new SAMatchEventList(strRefKey);
			
			{
				pane4 = new VerticalFieldManager(Manager.NO_HORIZONTAL_SCROLL);//new CustomVerticalFieldManager(Utils.getDisplayWidth() ,Utils.getDisplayHeight() / 2,Manager.VERTICAL_SCROLL);
		        pane4.setBackground(background);
		        
		        int nCount = m_matchEventList.totalCount();
		        if(nCount > 0)
		        {
			        for(int i = 0; i < nCount; i++){
			        	SAMatchEventItem matchItem = m_matchEventList.getDataAtIndex(i);
			        	
			        	CustomHorizontalFieldManager eachCell = new CustomHorizontalFieldManager(Utils.getDisplayWidth(),ResourceConstants.SA_VAL_ANALYSIS_PANE_CELL_HEIGHT,Manager.FIELD_HCENTER);
				        
				        Background back;
				        Background backMiddle;
			        	if(i % 2 == 0){
			        		back = BackgroundFactory.createSolidTransparentBackground(Color.WHITE, 255);
			        		backMiddle = BackgroundFactory.createBitmapBackground(Utils.getBitmapResource(ResourceConstants.SA_IMG_TABBAR_MIDDLE_CELL_WHITE));
			        	}
			        	else{
			        		back = BackgroundFactory.createSolidTransparentBackground(Color.LIGHTGRAY, 255);
			        		backMiddle = BackgroundFactory.createBitmapBackground(Utils.getBitmapResource(ResourceConstants.SA_IMG_TABBAR_MIDDLE_CELL_GRAY));
			        	}
			        	
			        	String iconUrl = null;
			        	String strDisplay = matchItem.m_playerName;
			        	if(matchItem.m_eventType.equals("G")){
			        		iconUrl = ResourceConstants.SA_ICON_FIXTURE_STATUS_GOAL;
			        	}else if(matchItem.m_eventType.equals("AS")){
			        		iconUrl = null;
			        	}else if(matchItem.m_eventType.equals("OG")){
			        		iconUrl = ResourceConstants.SA_ICON_FIXTURE_STATUS_GOAL;
			        		strDisplay += "(OG)";
			        	}else if(matchItem.m_eventType.equals("YC")){
			        		iconUrl = ResourceConstants.SA_ICON_FIXTURE_STATUS_YELLOW_CARD;
			        	}else if(matchItem.m_eventType.equals("RC")){
			        		iconUrl = ResourceConstants.SA_ICON_FIXTURE_STATUS_RED_CARD;
			        	}
			        	
			        	
			        	if(matchItem.m_teamType.equals("AWAY")){		//right cell
			        		
			        		CustomTableCell cellLeft = new CustomTableCell("", null, back, false);
				        	eachCell.add(cellLeft);
				        	
				        	String strMinutes = matchItem.m_minute + "'";
				        	CustomTableCell cellMiddle = new CustomTableCell(strMinutes, backMiddle);
				        	eachCell.add(cellMiddle);
				        
	
				        	if(matchItem.m_eventType.equals("SO")){
				        		iconUrl = ResourceConstants.SA_ICON_FIXTURE_STATUS_AWAY_SUB_OUT;
				        	}else if(matchItem.m_eventType.equals("SI")){
				        		iconUrl = ResourceConstants.SA_ICON_FIXTURE_STATUS_AWAY_SUB_IN;
				        	}
				        	
				        	CustomTableCell cellRight = new CustomTableCell(strDisplay, iconUrl, back, true);
				        	eachCell.add(cellRight);
			        	}
			        	else if(matchItem.m_teamType.equals("HOME")){	//left cell
			        		
			        		if(matchItem.m_eventType.equals("SO")){
				        		iconUrl = ResourceConstants.SA_ICON_FIXTURE_STATUS_HOME_SUB_OUT;
				        	}else if(matchItem.m_eventType.equals("SI")){
				        		iconUrl = ResourceConstants.SA_ICON_FIXTURE_STATUS_HOME_SUB_IN;
				        	}
			        		
			        		CustomTableCell cellLeft = new CustomTableCell(strDisplay, iconUrl, back, false);
				        	eachCell.add(cellLeft);
				        	
				        	String strMinutes = matchItem.m_minute + "'";
				        	CustomTableCell cellMiddle = new CustomTableCell(strMinutes, backMiddle);
				        	eachCell.add(cellMiddle);
				        
				        	CustomTableCell cellRight = new CustomTableCell("", null, back, true);
				        	eachCell.add(cellRight);
			        	}
	
			        	pane4.add(eachCell);	        	
			        }
		        }
		        else
		        {
		    		CustomLabelField titleLabel = new CustomLabelField(m_matchPreview.m_title, Color.BLACK, Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Utils.toDPIx(20), Utils.toDPIy(20), 600, Manager.FIELD_LEFT);
//		    		Calendar cal = Utils.getDateFromString(m_matchReport.m_publishedDate);
//		    		String dateString = cal.get(Calendar.DAY_OF_MONTH) + " " + Utils.getMonthString(cal.get(Calendar.MONTH)) + " " + cal.get(Calendar.YEAR) + " " + cal.get(Calendar.HOUR_OF_DAY)+":"+cal.get(Calendar.MINUTE);
//		    		CustomLabelField dateLabel = new CustomLabelField(dateString, Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Utils.toDPIx(20), Utils.toDPIy(20), 400, Manager.FIELD_LEFT);
		    		CustomLabelField dateLabel = new CustomLabelField(dateStr, Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Utils.toDPIx(20), Utils.toDPIy(20), 500, Manager.FIELD_LEFT);

		    		pane4.add(titleLabel);
		    		pane4.add(dateLabel);
		    		
		    		CustomLabelField notAvail = new CustomLabelField("The data will be available when the match starts", Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_SMALL), Utils.toDPIx(20), Utils.toDPIy(20), Utils.getDisplayWidth() - Utils.toDPIx(20), Manager.FIELD_LEFT);
		    		pane4.add(notAvail);
		        }
			}
			//video highlights pane - now this is same as in-Match Clips.
			m_inMatchClipList = new SAInMatchClipsList(strRefKey);
			int nCount = m_inMatchClipList.totalCount();
			{
				pane5 = new VerticalFieldManager(Manager.NO_HORIZONTAL_SCROLL);//new CustomVerticalFieldManager(Utils.getDisplayWidth() ,Utils.getDisplayHeight() / 2,Manager.VERTICAL_SCROLL);
		        pane5.setBackground(background);
		        if(nCount > 0)
		        {
		        	for(int i = 0; i < nCount; i++){
		        		SAVideoListItem clipsItem = m_inMatchClipList.getDataAtIndex(i);

		        		EventTableViewCell cell = new EventTableViewCell(clipsItem, Manager.FIELD_LEFT, i, 0);
		        		pane5.add(cell);
		        	}
		        }
		        else
		        {
		        	CustomLabelField titleLabel = new CustomLabelField(m_matchPreview.m_title, Color.BLACK, Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Utils.toDPIx(20), Utils.toDPIy(20), 600, Manager.FIELD_LEFT);
//		    		Calendar cal = Utils.getDateFromString(m_matchReport.m_publishedDate);
//		    		String dateString = cal.get(Calendar.DAY_OF_MONTH) + " " + Utils.getMonthString(cal.get(Calendar.MONTH)) + " " + cal.get(Calendar.YEAR) + " " + cal.get(Calendar.HOUR_OF_DAY)+":"+cal.get(Calendar.MINUTE);
//		    		CustomLabelField dateLabel = new CustomLabelField(dateString, Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Utils.toDPIx(20), Utils.toDPIy(20), 400, Manager.FIELD_LEFT);
		        	CustomLabelField dateLabel = new CustomLabelField(dateStr, Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Utils.toDPIx(20), Utils.toDPIy(20), 500, Manager.FIELD_LEFT);

		    		pane5.add(titleLabel);
		    		pane5.add(dateLabel);
		    		
		    		CustomLabelField notAvail = new CustomLabelField("The video will be available within 24 hours after the match", Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_SMALL), Utils.toDPIx(20), Utils.toDPIy(20), Utils.getDisplayWidth() - Utils.toDPIx(20), Manager.FIELD_LEFT);
		    		pane5.add(notAvail);
		        }
				
			}
			//in match clips pane

			{
				pane6 = new VerticalFieldManager(Manager.NO_HORIZONTAL_SCROLL);//new CustomVerticalFieldManager(Utils.getDisplayWidth() ,Utils.getDisplayHeight() / 2,Manager.VERTICAL_SCROLL);
		        pane6.setBackground(background);
		        if(nCount > 0)
		        {
		        	for(int i = 0; i < nCount; i++){
		        		SAVideoListItem clipsItem = m_inMatchClipList.getDataAtIndex(i);

		        		EventTableViewCell cell = new EventTableViewCell(clipsItem, Manager.FIELD_LEFT, i, 0);
		        		pane6.add(cell);
		        	}
		        }
		        else
		        {
		        	CustomLabelField titleLabel = new CustomLabelField(m_matchPreview.m_title, Color.BLACK, Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Utils.toDPIx(20), Utils.toDPIy(20), 600, Manager.FIELD_LEFT);
//		    		Calendar cal = Utils.getDateFromString(m_matchReport.m_publishedDate);
//		    		String dateString = cal.get(Calendar.DAY_OF_MONTH) + " " + Utils.getMonthString(cal.get(Calendar.MONTH)) + " " + cal.get(Calendar.YEAR) + " " + cal.get(Calendar.HOUR_OF_DAY)+":"+cal.get(Calendar.MINUTE);
//		    		CustomLabelField dateLabel = new CustomLabelField(dateString, Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Utils.toDPIx(20), Utils.toDPIy(20), 400, Manager.FIELD_LEFT);
		        	CustomLabelField dateLabel = new CustomLabelField(dateStr, Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Utils.toDPIx(20), Utils.toDPIy(20), 500, Manager.FIELD_LEFT);

		    		pane6.add(titleLabel);
		    		pane6.add(dateLabel);
		    		
		    		CustomLabelField notAvail = new CustomLabelField("The video will be available only during the match", Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_SMALL), Utils.toDPIx(20), Utils.toDPIy(20), Utils.getDisplayWidth() - Utils.toDPIx(20), Manager.FIELD_LEFT);
		    		pane6.add(notAvail);
		        }
			}
			UiApplication.getUiApplication().invokeLater(new Runnable()
			{
				public void run()
				{
					readyCurPage(curPageNumber);
				}
			});
		}
		
	}
	
}