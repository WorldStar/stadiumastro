package view.screen;


import view.component.CustomHorizontalFieldManager;
import view.component.CustomLabelField;
import view.component.CustomVerticalFieldManager;
import view.component.ImageButtonField;
import view.component.ImageHighlightButton;
import application.Utils;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;
import com.samples.toolkit.ui.component.BitmapGaugeField;

public class VideoPlayerScreen extends BaseScreen implements FieldChangeListener {

	private String m_videoUrl = "";
	
	private String m_mediaPlayImg = "icon/icon_play_start.png";
	private String m_mediaPauseImg = "icon/icon_play_pause.png";
	
	BitmapGaugeField gaugeField;
	CustomLabelField curTime;
	
	public VideoPlayerScreen(String url, String title){
		
		super("Videos", true);
        super.hideAdsBar();
        
        
		Background background = BackgroundFactory.createSolidTransparentBackground(Color.BLACK, 255);
		this.setBackground(background);
        
        m_videoUrl = url;
        HorizontalFieldManager videoTimeBar = new HorizontalFieldManager();
        
        Bitmap gaugeBack = Bitmap.getBitmapResource( "img/gauge/gauge_back_2.png" );
        Bitmap gaugeProgress = Bitmap.getBitmapResource( "img/gauge/gauge_progress_2.png" );
        
        gaugeField = new BitmapGaugeField(gaugeBack, gaugeProgress, 100, 0, 7, 7, 3, 3, true);
        
        CustomHorizontalFieldManager progressArea = new CustomHorizontalFieldManager(Utils.toDPIx(480), Utils.toDPIy(40), Manager.FIELD_LEFT);
        progressArea.add(gaugeField);
        
        curTime = new CustomLabelField("00:00", Color.WHITE, Utils.getTypeFace().getFont(Font.PLAIN, Utils.toDPIx(20)), 20, 0, Utils.toDPIx(100), Field.FIELD_RIGHT);
        videoTimeBar.setMargin(Utils.toDPIy(20),Utils.toDPIx(30), Utils.toDPIy(30), Utils.toDPIx(50));
        videoTimeBar.add(progressArea);
        videoTimeBar.add(curTime);
        
        CustomHorizontalFieldManager titleArea = new CustomHorizontalFieldManager(Utils.getDisplayWidth(), Utils.toDPIy(60), Manager.FIELD_LEFT);
        CustomLabelField videoTitle = new CustomLabelField(title, Color.WHITE, Utils.getTypeFace().getFont(Font.BOLD, Utils.toDPIx(30)), Utils.toDPIx(20), 0, Utils.toDPIx(450), Field.FIELD_RIGHT);
        titleArea.add(videoTitle);
        
        CustomVerticalFieldManager playScreen = new CustomVerticalFieldManager(Utils.getDisplayWidth(), Utils.toDPIy(250),Manager.NO_VERTICAL_SCROLL|Manager.FIELD_HCENTER);
        CustomLabelField playerHello = new CustomLabelField("Invalid video format", Color.WHITE, Utils.getTypeFace().getFont(Font.PLAIN, 30), 20, 30, Utils.getDisplayWidth(), Field.FIELD_HCENTER);
        playScreen.add(playerHello);
        
        ImageHighlightButton button = new ImageHighlightButton(m_mediaPlayImg, 0, 0, Utils.toDPIx(70), Utils.toDPIy(70), Field.FIELD_HCENTER|Field.FOCUSABLE);

       	Add(videoTimeBar);
       	Add(titleArea);
       	Add(playScreen);
       	Add(button);
       	
       	button.setChangeListener(this);
      	
	}
	public void setMediaDuration(int duration)
	{
		if(duration <= 0)
			duration = 1;
		gaugeField.setNumValues(duration);		
	}
	public void setCurPlayPos(int pos)
	{
		gaugeField.setValue(pos);
		int min = pos / 60;
		int second = pos % 60;
		String timeStr = String.valueOf(min) + ":" + String.valueOf(second);
		curTime.setLabel(timeStr);
	}
	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		if(context == 1)
		{
			//Play Button Clicked.
		}
	}
}
