package view.screen;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.FullScreen;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;
import application.ResourceConstants;
import application.StadiumAstroApp;
import application.Utils;


/**
 * A class extending the MainScreen class, which provides default standard
 * behavior for BlackBerry GUI applications.
 */
public final class SplashScreen extends FullScreen
{
	/**
     * Creates a new SpalshScreen object
     */
    public SplashScreen()
    {        
    	super();
    	showSplashImage();
    } 

    void showSplashImage() {
    	
    	Bitmap bootup_bmp = Utils.getBitmapResource(ResourceConstants.SA_IMG_SPLASH);
    	Background bg = BackgroundFactory.createBitmapBackground(bootup_bmp);
    	setBackground(bg);

    	//run Splash thread and after 2s push PartnerScreen
    	
    	UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {
				StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
				app.gotoPartnerScreen();
			}
		}, Utils.LOGO_SCREEN_DURATION, false);
    }
}
