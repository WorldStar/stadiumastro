package view.screen;

import view.component.CustomBitmapField;
import view.component.CustomLabelField;
import application.AppSession;
import application.ResourceConstants;
import application.Utils;
import model.SAFeatureListItem;
import model.SAHighlightNewsListItem;
import model.SANewsListItem;
import model.SAVideoListItem;
import net.rim.device.api.browser.field.RenderingOptions;
import net.rim.device.api.browser.field.RenderingSession;
import net.rim.device.api.browser.field2.BrowserField;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;


public class NewsDetailScreen extends BaseScreen {
	private String m_title = "";
	private String m_date = "";
	private String m_imagePath = "";
	private String m_detailContent = "";

	public NewsDetailScreen(Object dataItem){
		super("News");
		
		// get data from param
		if(dataItem instanceof SANewsListItem)
        {
        	SANewsListItem item = (SANewsListItem)dataItem;
        	
        	m_title = item.m_title;
        	m_date = Utils.getStringFromDate(item.m_publishDate);
        	m_imagePath = item.m_imageLink;
        	m_detailContent = Utils.setHTMLContentSize(item.m_fullBody, 6, "#292929", "white");
        }
        else if(dataItem instanceof SAFeatureListItem)
        {
        	SetTitleString("Latest News");
        	SAFeatureListItem item = (SAFeatureListItem)dataItem;
        	
        	m_title = item.m_title;
        	m_date = Utils.getStringFromDate(Utils.getDateFromString(item.m_date));// m_date = item.m_date.substring(0, 19);
        	m_imagePath = item.m_imageUrl;
        	m_detailContent = Utils.setHTMLContentSize(item.m_fullBody, 4, "#292929", "white");
        }
        else if(dataItem instanceof SAHighlightNewsListItem){
        	SetTitleString("");
        	SAHighlightNewsListItem item = (SAHighlightNewsListItem)dataItem;
        	
        	m_title = item.m_title;
        	m_date = Utils.getStringFromDate(Utils.getDateFromString(item.m_publishDate));// m_date = item.m_publishDate.substring(0,19);
        	
        	m_imagePath = item.m_imageUrl;
        	m_detailContent = Utils.setHTMLContentSize(item.m_fullBody, 6, "#292929", "white");
        	
        }
		
		if(AppSession.useLocalDataForTest)
		{
			m_imagePath = ResourceConstants.SA_IMG_NEWS_DETAIL_SAMPLE;
		}
		
		// create view components
        Background background = BackgroundFactory.createSolidTransparentBackground(Color.BLACK, 255);
		this.setBackground(background);
		
		VerticalFieldManager blackBackManager = new VerticalFieldManager();
		blackBackManager.setBackground(background);
		
		VerticalFieldManager detailContentsManager = new VerticalFieldManager(Manager.NO_HORIZONTAL_SCROLL);
		Background contentsBackground = BackgroundFactory.createSolidBackground(0x292929);
		
        detailContentsManager.setBackground(contentsBackground);
        detailContentsManager.setMargin(0, 10, 0, 10);
       
        // define content width
        int nWidth = Utils.getDisplayWidth() - Utils.toDPIx(40);
        
       	//CustomLabelField title = new CustomLabelField(m_title,Color.WHITE,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_LARGE - 1) , Utils.toDPIx(10) , Utils.toDPIy(40) , nWidth, Field.FIELD_LEFT);
       	//CustomLabelField date = new CustomLabelField(m_date,Color.WHITE,Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_BIG_MIDDLE) , Utils.toDPIx(10) , Utils.toDPIy(10), nWidth, Field.FIELD_LEFT);
       	
        //Passion
        CustomLabelField title = new CustomLabelField(m_title,Color.WHITE,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_BIG_MIDDLE - 1) , Utils.toDPIx(10) , Utils.toDPIy(40) , nWidth, Field.FIELD_LEFT);
       	CustomLabelField date = new CustomLabelField(m_date,Color.WHITE,Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , Utils.toDPIx(10) , Utils.toDPIy(10), nWidth, Field.FIELD_LEFT);
        
       	CustomBitmapField image = new CustomBitmapField(m_imagePath, null, 10, 10 , nWidth, Utils.toDPIy(324), Manager.FIELD_LEFT, ResourceConstants.SA_IMG_NEWS_DETAIL_SAMPLE );
       	
       	//CustomLabelField detail = new CustomLabelField(m_detailContent, Color.WHITE, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , Utils.marginX(10) , Utils.marginY(10) , nWidth, Field.FIELD_LEFT);
       	BrowserField detail = new BrowserField();
       	
       	detail.setMargin(Utils.toDPIy(10), Utils.toDPIy(20), Utils.toDPIy(10), 0);
       	
//       	//Passion for Browser field's minimum font size process
//       	int fontSize = (int)(8 * Utils.getVRatio());
//       	
//       	detail.getRenderingOptions().setProperty(RenderingOptions.CORE_OPTIONS_GUID, RenderingOptions.MINIMUM_FONT_SIZE, fontSize);
//       	detail.getRenderingOptions().setProperty(RenderingOptions.CORE_OPTIONS_GUID, RenderingOptions.MINIMUM_FONT_SIZE_DEFAULT, fontSize);
//       	RenderingSession renderingSession = RenderingSession.getNewInstance();
//       	renderingSession.getRenderingOptions().setProperty(RenderingOptions.CORE_OPTIONS_GUID, RenderingOptions.MINIMUM_FONT_SIZE, fontSize);
//       	renderingSession.getRenderingOptions().setProperty(RenderingOptions.CORE_OPTIONS_GUID, RenderingOptions.MINIMUM_FONT_SIZE_DEFAULT, fontSize);
       	
       	Utils.setFontSizeForBrowserField(detail, ResourceConstants.SA_VAL_FONT_SIZE_NEWS_DETAIL);
       	
       	detail.displayContent(m_detailContent, "");
       	
       	detailContentsManager.add(title);
       	detailContentsManager.add(date);
       	detailContentsManager.add(image);
       	detailContentsManager.add(detail);
       	
       	blackBackManager.add(detailContentsManager);
       	
       	Add(blackBackManager);
      
	}
	
	public void deleteGarbage(){
		DeleteAll();
	}

}
