package view.screen;



import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import application.AppSession;
import application.ResourceConstants;
import application.Utils;

import view.component.CustomLabelField;
import view.component.CustomVerticalFieldManager;
import view.component.PushCategoryCell;

import model.SAPushSetting;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class PushConfigScreen extends BaseScreen implements FieldChangeListener{
	public static String IMG_TOP = "img/push_listing_top.png";
	public static String IMG_MID = "img/push_listing_mid.png";
	public static String IMG_BOTTOM = "img/push_listing_bottom.png";
	public static String IMG_BACKGROUND = "img/push_listing_background.png";
	
//	String [] data = {"MSTART", "Kick Off", "MHALF", "Half Time", "MFULL", "Full Time", "MGOAL", "Goal", "MDISCIPLINE","Yellow/Red Card"};
	String [] data = {"MFULL", "Full Time", "MGOAL", "Goal", "MHALF", "Half Time", "MSTART", "Kick Off", "MDISC", "Yellow/Red Card"};
	String m_eventKey;
	
	boolean bFieldVisible;
	boolean bInOperation;
	
	Vector fieldToHide;
	Hashtable keyField;
	
	VerticalFieldManager m_fieldArea;
	SAPushSetting m_setting;
	
	private Field createGroupLabel(String strLabel)
	{
		CustomVerticalFieldManager labelManager;
		labelManager = new CustomVerticalFieldManager(Utils.getDisplayWidth(), Utils.toDPIy(100), Manager.USE_ALL_WIDTH);
		
		CustomLabelField label = new CustomLabelField(strLabel, 0x4c566c, Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_BIG_MIDDLE), Utils.toDPIx(30), Utils.toDPIy(40), Utils.getDisplayWidth(), Manager.FIELD_VCENTER);
		labelManager.add(label);
		return labelManager;
	}
	
	public PushConfigScreen(){
		super("Push Notification");
		hideAdsBar();
		
		bInOperation = true;
		m_eventKey = AppSession.sharedSession().m_curSportEventContentKey;
		fieldToHide = new Vector();
		keyField = new Hashtable();
		
		
		m_fieldArea = new VerticalFieldManager(Field.USE_ALL_WIDTH);
		m_fieldArea.setPadding(Utils.toDPIx(10),Utils.toDPIx(5),10,Utils.toDPIx(5));
		
		Bitmap backBmp = Utils.getBitmapResource(IMG_BACKGROUND);
		Background mainBack = BackgroundFactory.createBitmapBackground(backBmp, Background.POSITION_X_LEFT, Background.POSITION_Y_TOP, Background.REPEAT_SCALE_TO_FIT);
		m_MainScroller.setBackground(mainBack);
		
		m_setting = new SAPushSetting(m_eventKey);
		
		boolean pEnabledFlag;
		pEnabledFlag = stringToBoolean((String)m_setting.getSettingForKey("pushEnableFlag"));
		
		Add(m_fieldArea);
		
		PushCategoryCell cell = new PushCategoryCell(PushConfigScreen.IMG_MID, "Enable Push Alert", Utils.getDisplayWidth() - Utils.toDPIx(20), ResourceConstants.SA_PUSH_CATEGORY_CELL_HEIGHT, Field.FIELD_VCENTER|FIELD_HCENTER);
		
		m_fieldArea.add(cell);
		cell.setToggleState(pEnabledFlag);
		cell.setChangeListener(this);
		cell.setRefKey("pushEnableFlag");
		keyField.put("pushEnableFlag", cell);
		
		bFieldVisible = pEnabledFlag;
		
		CustomVerticalFieldManager labelManager;
		labelManager = (CustomVerticalFieldManager) createGroupLabel("General");
		fieldToHide.addElement(labelManager);
		
		if(bFieldVisible)
			m_fieldArea.add(labelManager);
		
		//Populate general setting.
		String img;
		int index = 0;
		for(int i = 0; i < 5; i++)
		{
			if(i == 0)
				img = PushConfigScreen.IMG_TOP;
			else if(i == 4)
				img = PushConfigScreen.IMG_BOTTOM;
			else
				img = PushConfigScreen.IMG_MID;
			
			pEnabledFlag = stringToBoolean((String)m_setting.getSettingForKey(data[index]));
			
			cell = new PushCategoryCell(img, data[index + 1], Utils.getDisplayWidth() - Utils.toDPIx(20), ResourceConstants.SA_PUSH_CATEGORY_CELL_HEIGHT, Field.FIELD_VCENTER|FIELD_HCENTER);
			if(bFieldVisible)
				m_fieldArea.add(cell);
			cell.setToggleState(pEnabledFlag);
			cell.setRefKey(data[index]);
			cell.setChangeListener(this);
			
			//Register field.
			fieldToHide.addElement(cell);
			keyField.put(data[index], cell);
			index += 2;
		}
		
		//Populate team setting
		labelManager = (CustomVerticalFieldManager) createGroupLabel("Team");
		if(bFieldVisible)
			m_fieldArea.add(labelManager);
		fieldToHide.addElement(labelManager);
		
		Vector teamSettingList = (Vector)m_setting.getSettingForKey("teamsetting");
		Enumeration teamEnum = teamSettingList.elements();
		
		String statusVal;
		boolean bFirstCell = true;
		while(teamEnum.hasMoreElements())
		{
			Hashtable teamStatus = (Hashtable)teamEnum.nextElement();
			
			
			if(teamStatus != null)
			{
				if(bFirstCell)
				{
					img = PushConfigScreen.IMG_TOP;
					bFirstCell = false;
				}
				else if(teamEnum.hasMoreElements() == false)
					img = PushConfigScreen.IMG_BOTTOM;
				else
					img = PushConfigScreen.IMG_MID;
				
				statusVal = (String)teamStatus.get("teamName");
				
				cell = new PushCategoryCell(img, statusVal, Utils.getDisplayWidth() - Utils.toDPIx(20), ResourceConstants.SA_PUSH_CATEGORY_CELL_HEIGHT, Field.FIELD_VCENTER|FIELD_HCENTER);
				if(bFieldVisible)
					m_fieldArea.add(cell);
				cell.setRefKey((String)teamStatus.get("teamId"));
				cell.setToggleState(stringToBoolean((String)teamStatus.get("state")));
				cell.setChangeListener(this);
				//Register field.
				keyField.put((String)teamStatus.get("teamId"), cell);
				fieldToHide.addElement(cell);
				
			}
		}
		
		bInOperation = false;
	}
	
	public void resetConfigScreen()
	{
		boolean pEnabledFlag;
		
		
		Enumeration fieldEnum;
		bInOperation = true;
		//Hide field first.
		
		pEnabledFlag = stringToBoolean((String)m_setting.getSettingForKey("pushEnableFlag"));
		PushCategoryCell cell = (PushCategoryCell) keyField.get("pushEnableFlag"); 
		cell.setToggleState(pEnabledFlag);
		
		if(pEnabledFlag == true)
		{
			if(!bFieldVisible)
			{
				fieldEnum = fieldToHide.elements();
				while(fieldEnum.hasMoreElements())
				{
					Field field = (Field)fieldEnum.nextElement();
					m_fieldArea.add(field);
				}
				bFieldVisible = true;
			}
			
			
				
			//Populate general setting.
			int index = 0;
			for(int i = 0; i < 5; i++)
			{
				pEnabledFlag = stringToBoolean((String)m_setting.getSettingForKey(data[index]));
				
				cell = (PushCategoryCell) keyField.get(data[index]);
				cell.setToggleState(pEnabledFlag);
				
				index += 2;
			}
			
			Vector teamSettingList = (Vector)m_setting.getSettingForKey("teamsetting");
			Enumeration teamEnum = teamSettingList.elements();
			
			while(teamEnum.hasMoreElements())
			{
				Hashtable teamStatus = (Hashtable)teamEnum.nextElement();
				if(teamStatus != null)
				{
					cell = (PushCategoryCell) keyField.get((String)teamStatus.get("teamId"));
					cell.setToggleState(stringToBoolean((String)teamStatus.get("state")));
				}
			}
			
		}
		
		else if(!pEnabledFlag)
		{
			if(bFieldVisible)
			{
				fieldEnum = fieldToHide.elements();
				while(fieldEnum.hasMoreElements())
				{
					Field field = (Field)fieldEnum.nextElement();
					m_fieldArea.delete(field);
				}
				bFieldVisible = false;
			}
		}
		
		bInOperation = false;
	}
	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		boolean bSuccess;
		boolean curToggleState;
		if(bInOperation)
			return;
		PushCategoryCell cell = (PushCategoryCell) field;
		curToggleState = cell.getToggleState();
		if(cell != null)
		{
			String refKey = cell.getRefKey();
			if(refKey.compareTo("pushEnableFlag") == 0)
			{
				bSuccess = m_setting.enablePush(curToggleState);
			}
			else
			{
				bSuccess = m_setting.enablePushForEvent(refKey, curToggleState);	
			}
			
			if(bSuccess)
				m_setting.init(m_eventKey);
			else
				cell.setToggleState(!curToggleState);
			resetConfigScreen();
		}
		
	}
	boolean stringToBoolean(String str)
	{
		if(str == null)
			return false;
		if(str.compareTo("Y") == 0)
			return true;
		return false;
	}
	String booleanToString(boolean bEnabled)
	{
		if(bEnabled)
			return "Y";
		else 
			return "N";
	}
}
