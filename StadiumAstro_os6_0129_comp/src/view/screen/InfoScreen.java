package view.screen;

import application.ResourceConstants;
import application.Utils;
import model.SAInfoContent;
import net.rim.device.api.browser.field2.BrowserField;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import view.component.CustomLabelField;
import view.component.LoadingDialog;

public class InfoScreen extends BaseScreen{
	
	public InfoScreen(String title)
	{
		super(title);
		
		RInfoInit infoLoader = new RInfoInit();
		infoLoader.setInfoTitle(title);
		LoadingDialog.showLoadingDialog(infoLoader, "Loading");
		
	}
	class RInfoInit implements Runnable
	{
		String m_title;
		CustomLabelField titleLabel;
		BrowserField contentField;
		
		public void setInfoTitle(String title)
		{
			m_title = title;
		}
		public void run()
		{
			try{
				if (m_title.equals(ResourceConstants.SA_TITLE_MENUITEM_ABOUT))
				{
					final Bitmap bmpIntro = Utils.getBitmapResource(ResourceConstants.SA_IMG_INFO_INTRO_TITLE);
					//
					UiApplication.getUiApplication().invokeLater(new Runnable(){
						public void run()
						{
		//					Add(titleLabel);
							Add(new BitmapField(bmpIntro, FIELD_HCENTER));
						}
					});
				}
				
				String refKey = null;
				if(m_title.equals(ResourceConstants.SA_TITLE_MENUITEM_ABOUT))
					refKey = ResourceConstants.SA_INFO_KEY_ABOUT;
				else if(m_title.equals(ResourceConstants.SA_TITLE_MENUITEM_FAQ))
					refKey = ResourceConstants.SA_INFO_KEY_FAQ;
				else if(m_title.equals(ResourceConstants.SA_TITLE_MENUITEM_PRIVACY))
					refKey = ResourceConstants.SA_INFO_KEY_PRIVACYPOLICY;
				else if(m_title.equals(ResourceConstants.SA_TITLE_MENUITEM_TOS))
					refKey = ResourceConstants.SA_INFO_KEY_TERMSOFSERVICE;
				else if(m_title.equals(ResourceConstants.SA_TITLE_MENUITEM_TAC))
					refKey = ResourceConstants.SA_INFO_KEY_TERMSANDCONDITIONS;
				
				SAInfoContent content = new SAInfoContent();
				if(refKey != null)
					content.initArrayWithParsedData(refKey);
				
				
	//			titleLabel = new CustomLabelField("Stadium Astro", Color.WHITE, Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), 0, 0, 300, FIELD_HCENTER);
	//			titleLabel.setLabel("Stadium Astro");
				
				contentField = new BrowserField();
				contentField.displayContent(content.getContent(), "");
				UiApplication.getUiApplication().invokeLater(new Runnable(){
					public void run()
					{
	//					Add(titleLabel);
						Add(contentField);
					}
				});
			}catch(Exception e){
					contentField = new BrowserField();
					String m_content = "Server connection failed. Please try again.";
					m_content = "<html>"
							+ "<body bgcolor='BLACK' style='font-size:24px;'>"
							+ "<font color = 'white'>"
							+ m_content
							+ "</font>"
							+ "</body>"
							+ "</html>";
					contentField.displayContent(m_content, "");
					UiApplication.getUiApplication().invokeLater(new Runnable(){
						public void run()
						{
		//					Add(titleLabel);
							Add(contentField);
						}
					});
			}
		}
	}
}
