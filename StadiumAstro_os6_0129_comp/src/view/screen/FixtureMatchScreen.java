package view.screen;

import application.ResourceConstants;
import application.Utils;
import view.component.*;
import model.SACalendarItem;
import model.SASoccerMatchListItem;
import model.SASoccerMatchList;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

/**
 * 
 * @author Administrator
 *	This is a Matches Screen.
 */
public class FixtureMatchScreen extends BaseScreen {
	
	SASoccerMatchList m_matchList = null;
	
	public SACalendarItem m_currentCalendarItem = null;
	
	public MatchCalendarScroll m_dateItemsScroll = null;
	public CalendarDateItem m_curSelectedItem = null;
	
	CustomHorizontalFieldManager m_dateBar = null;
	VerticalFieldManager m_matchTableView = null;
	public FixtureMatchScreen(){
		super("Matches");
		LoadingDialog.showLoadingDialog(new RGetMatchCalender(), "Loading");
		
	}
	public void showMatchTable(String strID)
	{
		if(m_dateBar != null){
			Delete(m_dateBar);
			m_dateBar = null;
		}
		
		if(m_matchTableView != null){
			Delete(m_matchTableView);
			m_matchTableView = null;
		}
		RGetMatchTable matchLoader = new RGetMatchTable();
		matchLoader.setStrKey(strID);
		LoadingDialog.showLoadingDialog(matchLoader, "Loading...");
	}
	
	public void deleteGarbage(){
		m_dateItemsScroll.deleteAll();
		Delete(m_dateItemsScroll);
		m_dateItemsScroll = null;
				
		if(m_matchList != null){
			m_matchList.m_array.removeAllElements();
			m_matchList = null;
		}
	}
	
	public void refresh()
	{
		DeleteAll();
		m_dateItemsScroll = null;
		m_dateBar = null;
		m_matchTableView = null;
		
		LoadingDialog.showLoadingDialog(new RGetMatchCalender(), "Loading");
	}
	
	class RGetMatchCalender implements Runnable
	{
		public void run()
		{
			Background background = BackgroundFactory.createSolidTransparentBackground(Color.GRAY, 255);
	        setBackground(background);
	        	
			//Match Calendar Scroll
			m_dateItemsScroll = new MatchCalendarScroll();
			UiApplication.getUiApplication().invokeLater(new Runnable()
			{
				public void run()
				{
					Add(m_dateItemsScroll);
					m_dateItemsScroll.selectItemAtIndex(m_dateItemsScroll.m_firstSelect);
					m_dateItemsScroll.setEndflg(true);
					//m_dateItemsScroll.selectItemAtIndex(m_dateItemsScroll.m_totalCount-1);
				}
			}
			);

		}
	}
	class RGetMatchTable implements Runnable
	{
		String m_strID = "";
		public void setStrKey(String id)
		{
			m_strID = id;
		}
		
		public void run()
		{

			if(m_strID !=  null)
			{
				/// matches table view
				
				m_matchTableView = new VerticalFieldManager();
				m_matchTableView.add(new NullField());
				
				m_matchList = new SASoccerMatchList();
				m_matchList.initArrayWithParsedData(m_strID);
				int mk = 0;
				try{
					for(int i = 0; i < m_matchList.totalCount(); i++){
						mk = i;
						SASoccerMatchListItem mInfo = m_matchList.getDataAtIndex(i);
						if(mInfo != null){
							MatchTableCell matchCell = new MatchTableCell(mInfo,i);
							m_matchTableView.add(matchCell);
						}
					}
				}catch(Exception e){
					m_matchTableView = null;
					m_matchTableView = new VerticalFieldManager();
					m_matchTableView.add(new NullField());
					if(mk > 0){
						try{
							for(int k = 0; k < mk -1 ; k++){
								SASoccerMatchListItem mInfo = m_matchList.getDataAtIndex(k);
								if(mInfo != null){
									MatchTableCell matchCell = new MatchTableCell(mInfo,k);
									m_matchTableView.add(matchCell);
								}
							}
						}catch(Exception e1){
							m_matchTableView = null;
							m_matchTableView = new VerticalFieldManager();
							m_matchTableView.add(new NullField());
						}
					}
				}
				
				//Date Bar
				m_dateBar = new CustomHorizontalFieldManager(Display.getWidth(),ResourceConstants.SA_VAL_FIXTURE_DATE_BAR_HEIGHT,Manager.FIELD_TOP);
				
				Background dateBarbackground = BackgroundFactory.createSolidTransparentBackground(Color.GRAY, 200);
				m_dateBar.setBackground(dateBarbackground);
				
				
				String dateStr = Utils.getStringFromDate(m_matchList.getDataAtIndex(0).m_dateCalendar);
							
				int fontSize = ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE;
	            int labelWidth = Utils.getTypeFace().getFont(Font.BOLD, fontSize).getBounds(dateStr);
	            int marginX = (Utils.getDisplayWidth() - labelWidth) / 2;
	            
				CustomLabelField dateLabel = new CustomLabelField(dateStr, Color.WHITE, Utils.getTypeFace().getFont(Font.PLAIN,ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), marginX, Utils.toDPIy(5), 600, Manager.FIELD_HCENTER);
				m_dateBar.add(dateLabel);
				
				UiApplication.getUiApplication().invokeLater(new Runnable(){
					public void run(){
						Add(m_dateBar);
						//if(m_matchTableView != null)
							Add(m_matchTableView);
					}
				});	
				
			}
		}
	}
	
}
