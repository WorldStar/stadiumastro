package view.screen;

import application.ResourceConstants;
import application.Utils;
import view.component.*;
import model.SAStandingListItem;
import model.SAStandingList;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class StandingScreen extends BaseScreen {
	
	SAStandingList m_standingList = null;
	String m_groupName = "";
	public StandingScreen(){
		super("Standings");
		LoadingDialog.showLoadingDialog(new RStandingListInit(), "Loading"); 
	}
	
	public void deleteGarbage(){
		DeleteAll();
		if(m_standingList != null){
			m_standingList.m_array.removeAllElements();
			m_standingList = null;
		}
	}
	class RStandingListInit implements Runnable
	{
		VerticalFieldManager blackBackManager;
		public void run()
		{
			Background background = BackgroundFactory.createSolidTransparentBackground(Color.BLACK, 255);
	        setBackground(background);
	        

			
			//Vertical Field Manager
			{
				blackBackManager = new VerticalFieldManager(VERTICAL_SCROLL);
				blackBackManager.setBackground(background);
				
				VerticalFieldManager standingInfoTableManager = new VerticalFieldManager(Manager.FIELD_HCENTER);
				standingInfoTableManager.setBackground(background);
				standingInfoTableManager.setMargin(20, 10, 0, 10);
				
				//get groupName from first StandingItem.
				m_standingList = new SAStandingList();
				int nCount = m_standingList.totalCount();
				
				SAStandingListItem standingItem = m_standingList.getDataAtIndex(0);
				m_groupName = standingItem.m_groupName;
				
				//Top RoundRectangle Bitmap Field
				{
					HorizontalFieldManager topBitmapFieldManager = new HorizontalFieldManager();		
					
					Background backTopImage = BackgroundFactory.createBitmapBackground(Utils.getBitmapResource(ResourceConstants.SA_IMG_STANDING_TOP_BAR));
					topBitmapFieldManager.setBackground(backTopImage);
					
					CustomVerticalFieldManager headerFieldManager = new CustomVerticalFieldManager(Utils.getDisplayWidth(),ResourceConstants.SA_VAL_STANDING_HEADER_HEIGHT,Manager.FIELD_HCENTER);
					CustomLabelField titleLabel = new CustomLabelField(m_groupName, Color.WHITE, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Utils.toDPIx(30), Utils.toDPIy(30), Utils.getDisplayWidth(), Manager.FIELD_LEFT);
//					CustomLabelField blankLabel = new CustomLabelField(" ", Color.WHITE, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , Utils.marginX(600), Utils.marginY(5), 640, Field.FIELD_LEFT);
					headerFieldManager.add(titleLabel);
//					headerFieldManager.add(blankLabel);
//					topBitmapFieldManager.add(titleLabel);
//					topBitmapFieldManager.add(blankLabel);
					topBitmapFieldManager.add(headerFieldManager);
					standingInfoTableManager.add(topBitmapFieldManager);
				}
				
				//Title view
				{
					CustomHorizontalFieldManager titleFieldManager = new CustomHorizontalFieldManager(ResourceConstants.SA_VAL_STANDING_TITLE_WIDTH,ResourceConstants.SA_VAL_STANDING_TITLE_HEIGHT,Manager.FIELD_HCENTER);
					
					Background backTopImage = BackgroundFactory.createSolidBackground(Color.GRAY);
					titleFieldManager.setBackground(backTopImage);
					
					CustomLabelField teamLabel = new CustomLabelField("Team", Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), Utils.toDPIx(30), Utils.toDPIy(10), Utils.toDPIx(240), Field.FIELD_VCENTER);
					CustomLabelField playedlLabel = new CustomLabelField("P", Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), Utils.toDPIx(10), Utils.toDPIy(10), ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH, Field.FIELD_VCENTER);
					CustomLabelField wonLabel = new CustomLabelField("W", Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), Utils.toDPIx(5), Utils.toDPIy(10), ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH, Field.FIELD_VCENTER);
					CustomLabelField drawLabel = new CustomLabelField("D", Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), Utils.toDPIx(5), Utils.toDPIy(10), ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH, Field.FIELD_VCENTER);
					CustomLabelField lostLabel = new CustomLabelField("L", Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), Utils.toDPIx(5), Utils.toDPIy(10), ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH, Field.FIELD_VCENTER);
					
					CustomLabelField foulLabel = new CustomLabelField("F", Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), Utils.toDPIx(5), Utils.toDPIy(10), ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH, Field.FIELD_VCENTER);
					CustomLabelField awardLabel = new CustomLabelField("A", Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), Utils.toDPIx(5), Utils.toDPIy(10), ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH, Field.FIELD_VCENTER);
					CustomLabelField pointsLabel = new CustomLabelField("Pts", Color.BLACK, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL), Utils.toDPIx(5), Utils.toDPIy(10), ResourceConstants.SA_VAL_STANDING_CELL_NORMAL_LABEL_WIDTH, Field.FIELD_VCENTER);
					
					titleFieldManager.add(teamLabel);
					titleFieldManager.add(playedlLabel);
					titleFieldManager.add(wonLabel);
					titleFieldManager.add(drawLabel);
					titleFieldManager.add(lostLabel);
					titleFieldManager.add(foulLabel);
					titleFieldManager.add(awardLabel);
					titleFieldManager.add(pointsLabel);
					
					standingInfoTableManager.add(titleFieldManager);
				}
				
				//No grid border Table view
				{
					
					for(int i = 0; i < nCount; i++){
						SAStandingListItem standingInfo = m_standingList.getDataAtIndex(i);
						StandingListCell sCell = new StandingListCell(standingInfo, Manager.FIELD_HCENTER);
						standingInfoTableManager.add(sCell);
					}
				}
				
				blackBackManager.add(standingInfoTableManager);
				
				UiApplication.getUiApplication().invokeLater(new Runnable()
				{
					public void run()
					{
						Add(blackBackManager);
					}
				}
				);
				
			}
		}
	}
	
	public void refresh()
	{
		deleteGarbage();
		LoadingDialog.showLoadingDialog(new RStandingListInit(), "Loading"); 
	}
}
