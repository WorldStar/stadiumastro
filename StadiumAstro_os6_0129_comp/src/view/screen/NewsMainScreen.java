package view.screen;

import view.component.CategoryListCell;
import view.component.LoadingDialog;
import model.SACategoryItem;
import model.SANewsCategoryList;
import net.rim.device.api.ui.*;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class NewsMainScreen extends BaseScreen {
	private SANewsCategoryList m_newsCategoryList = null;
	
	public NewsMainScreen(){
		super("News");
		LoadingDialog.showLoadingDialog(new RNewsCategoryInit(), "Loading");
	}
	
	public void deleteGarbage(){
		if(m_newsCategoryList != null){
			m_newsCategoryList.m_array.removeAllElements();
			m_newsCategoryList = null;
		}
		
		DeleteAll();
	}
	class RNewsCategoryInit implements Runnable
	{
		VerticalFieldManager mainTableView;
		public void run()
		{
			Background background = BackgroundFactory.createSolidTransparentBackground(Color.BLACK, 255);
	        setBackground(background);
	        
	        //Main Table View
	        {
	        	mainTableView = new VerticalFieldManager(Manager.VERTICAL_SCROLL | Manager.FIELD_HCENTER | Manager.FIELD_TOP);
	        	mainTableView.setPadding(20, 10, 10, 10);
	        	mainTableView.setBackground(background);
	        	
	        	m_newsCategoryList = new SANewsCategoryList(); 
	        	
	        	int nCount = m_newsCategoryList.totalCount();
	        	for(int i = 0; i < nCount; i++)
	        	{
	        		SACategoryItem item = m_newsCategoryList.getDataAtIndex(i);
	        		CategoryListCell mainTableCell = new CategoryListCell(item, null , true, Field.FOCUSABLE);
	        		mainTableView.add(mainTableCell);
	        	}
	        	
	        	UiApplication.getUiApplication().invokeLater(new Runnable()
	        	{
	        		public void run()
	        		{
	        			Add(mainTableView);
	        		}
	        	}
	        	);
	        	
	        }
		}
	}
	public void refresh()
	{
		deleteGarbage();
		LoadingDialog.showLoadingDialog(new RNewsCategoryInit(), "Loading");
	}
}
