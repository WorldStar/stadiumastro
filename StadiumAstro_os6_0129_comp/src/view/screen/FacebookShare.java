package view.screen;
import javax.microedition.io.InputConnection;

import application.CustomBrowserFieldController;


import application.ResourceConstants;
import application.StadiumAstroApp;
import application.Utils;

import view.component.ImageButtonField;
import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.browser.field2.BrowserField;
import net.rim.device.api.browser.field2.BrowserFieldConfig;
import net.rim.device.api.browser.field2.BrowserFieldRequest;

import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class FacebookShare extends MainScreen{
	
	String shareUrl = "https://m.facebook.com/dialog/feed?";
	String appId = "711593568858016";
	String link = "http://www.stadiumastro.com";
	String redirect_uri = "http://www.stadiumastro.com";
	
	public FacebookShare(){
		super();
		VerticalFieldManager manager = new VerticalFieldManager();
		
		
		Background background = BackgroundFactory.createSolidTransparentBackground(Color.BLACK, 255);
		
		BrowserFieldConfig config = new BrowserFieldConfig();
		config.setProperty(BrowserFieldConfig.USER_AGENT, "MyApplication 1.0");
		BrowserField browser = new BrowserField(config);
		CustomBrowserFieldController controller = new ShareBrowserController(browser, redirect_uri);
		config.setProperty(BrowserFieldConfig.CONTROLLER, controller);
		browser.setMargin(Utils.toDPIy(10), 0, Utils.toDPIy(10), 0);
		
		manager.add(browser);
		
		this.setBackground(background);
		manager.setBackground(background);
		browser.setBackground(0x000000);
		
		//Request url.
		URLEncodedPostData encoder = new URLEncodedPostData(URLEncodedPostData.DEFAULT_CHARSET, false);
		encoder.append("app_id", appId);
		encoder.append("link", link);
		encoder.append("redirect_uri", redirect_uri);
		encoder.append("name", ResourceConstants.SA_TELL_SUBJECT);
		encoder.append("caption", ResourceConstants.SA_TELL_MESSAGE_TEMPLATE);
		String requestUrl = shareUrl + encoder.toString();
		
		browser.requestContent(requestUrl);
		
		ImageButtonField btnField = new ImageButtonField("MENU_BACK","icon/icon_arrow_back.png", 20, 0x00000000, Field.FIELD_LEFT|Field.FOCUSABLE);
		manager.add(btnField);
		
		this.add(manager);
	}
}

class ShareBrowserController extends CustomBrowserFieldController
{
	BrowserField mBrowser;
	String mSuccessUrl;
	public ShareBrowserController(BrowserField browser,String successUrl)
	{
		super(browser);
		mBrowser = browser;
		mSuccessUrl = successUrl;
	}
	public InputConnection handleResourceRequest(BrowserFieldRequest request)
	{
		if(request.getURL().startsWith(mSuccessUrl))
		{
			
			UiApplication app = UiApplication.getUiApplication();
			app.invokeLater(new Runnable(){
				public void run()
				{
					StadiumAstroApp app = (StadiumAstroApp) UiApplication.getUiApplication();
					app.gotoBackScreen();
				}
				
			}
			);
			return null;
		}
		return super.handleResourceRequest(request);
	}
}
