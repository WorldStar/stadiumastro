package view.screen;

import view.component.EventTableViewCell;
import view.component.LoadingDialog;
import model.SANewsList;
import model.SANewsListItem;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class NewsListScreen extends BaseScreen {
	private SANewsList m_newsList = null;
	String m_strRefKey;
	public NewsListScreen(String strRefKey , String categoryName){
		super(categoryName);
		m_strRefKey = strRefKey;
		
		RNewsListInit newsLoader = new RNewsListInit();
		LoadingDialog.showLoadingDialog(newsLoader, "Loading");
		
	}
	
	public void deleteGarbage(){
		if(m_newsList != null){
			m_newsList.m_array.removeAllElements();
			m_newsList = null;
		}
		DeleteAll();
	}
	class RNewsListInit implements Runnable
	{
		VerticalFieldManager mainTableView;
		
		public void run()
		{
			Background background = BackgroundFactory.createSolidTransparentBackground(Color.BLACK, 200);
			setBackground(background);
	        
	        //Main Table View
	        {
	        	mainTableView = new VerticalFieldManager(Manager.VERTICAL_SCROLL | Manager.FIELD_HCENTER | Manager.FIELD_TOP);
	        	mainTableView.setBackground(background);
	        	
	        	// create model data
	        	m_newsList = new SANewsList(m_strRefKey);
	        	
	        	int nCount = m_newsList.totalCount();
	        	for(int i = 0; i < nCount; i++)
	        	{
	        		// create view components with each model data 
	        		SANewsListItem itemData = m_newsList.getDataAtIndex(i);
	        		EventTableViewCell mainTableCell = new EventTableViewCell(itemData, Field.FOCUSABLE, i, 1);
	        		mainTableView.add(mainTableCell);
	        	}
	        	UiApplication.getUiApplication().invokeLater(new Runnable()
	        	{
	        		public void run()
	        		{
	        			Add(mainTableView);
	        		}
	        	}
	        	);
	        	
	        }
		}
	}
	public void refresh()
	{
		deleteGarbage();
		RNewsListInit newsLoader = new RNewsListInit();
		LoadingDialog.showLoadingDialog(newsLoader, "Loading");
	}
}
