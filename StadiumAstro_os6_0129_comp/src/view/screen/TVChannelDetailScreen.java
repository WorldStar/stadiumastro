package view.screen;

import application.ResourceConstants;
import application.Utils;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;
import view.component.CustomHorizontalFieldManager;
import view.component.CustomVerticalFieldManager;
import view.component.CustomTabButtonField;
import view.component.LoadingDialog;
import view.component.TVChannelEventCell;
import model.SATVChannelCalendarList;
import model.SATVChannelCalendarListItem;
import model.SATVChannelEventList;
import model.SATVChannelEventListItem;

public class TVChannelDetailScreen extends BaseScreen {
	
	SATVChannelCalendarList m_tvCalendarList = null;
	SATVChannelEventList m_tvEventList = null;
	public int curPageNumber = 0;
	public CustomTabButtonField m_curTabbedButton = null;
	CustomVerticalFieldManager m_tvChannelEventField = null;
	
	public TVChannelDetailScreen(String title){
		super(title);
		
		//TV Channel Calendar List
		{
			LoadingDialog.showLoadingDialog(new RTVCalendarLoader(), "Loading");
		}
	}
	public void showEventList(String strID){
		if(m_tvChannelEventField != null){
			Delete(m_tvChannelEventField);
			m_tvChannelEventField = null;
		}
		
		//TV Channel Event Table View
		RTVEventLoader eventLoader = new RTVEventLoader();
		eventLoader.setStrID(strID);
		LoadingDialog.showLoadingDialog(eventLoader, "Loading");
				
	}
	public void deleteGarbage(){
		if(m_tvCalendarList != null){
			m_tvCalendarList.m_array.removeAllElements();
			m_tvCalendarList = null;
		}
		
		if(m_tvEventList != null){
			m_tvEventList.m_array.removeAllElements();
			m_tvEventList = null;
		}
		
		DeleteAll();
	}
	
	class RTVCalendarLoader implements Runnable
	{
		CustomHorizontalFieldManager tvChannelCalendarField;
		
		public void run()
		{
			tvChannelCalendarField = new CustomHorizontalFieldManager(Utils.getDisplayWidth(), ResourceConstants.SA_VAL_TV_CHANNEL_CALENDAR_ITEM_HEIGHT, Manager.HORIZONTAL_SCROLL | Manager.USE_ALL_WIDTH);
			int backColor = 0x7B7979;
			Background background = BackgroundFactory.createSolidBackground(backColor);
			tvChannelCalendarField.setBackground(background);
			m_tvCalendarList = new SATVChannelCalendarList();
			int nCount = m_tvCalendarList.totalCount();
			for(int i = 0; i < nCount; i++){
				SATVChannelCalendarListItem item = m_tvCalendarList.getDataAtIndex(i);
				CustomTabButtonField button = new CustomTabButtonField(item, i + 1, item.m_displayDay+item.m_displayMonth, ResourceConstants.SA_IMG_TV_CHANNEL_CALENDAR_ITEM_NORMAL, 10, backColor, Utils.getTypeFace().getFont(Font.BOLD,ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Manager.FIELD_HCENTER);
				button.setOffPicture(Utils.getBitmapResource(ResourceConstants.SA_IMG_TV_CHANNEL_CALENDAR_ITEM_DOWN));
				tvChannelCalendarField.add(button);
				
				if (i == 0)
				{
					m_curTabbedButton = button;
				}
			}
			curPageNumber = 0;
			UiApplication.getUiApplication().invokeLater(new Runnable()
			{
				public void run()
				{
					Add(tvChannelCalendarField);
					if (m_tvCalendarList.totalCount() > 0)
					{
						SATVChannelCalendarListItem item = m_tvCalendarList.getDataAtIndex(0);
						
						showEventList(item.m_id);
						CustomTabButtonField cu = (CustomTabButtonField)tvChannelCalendarField.getField(0);
						cu.refresh(true);
					}
				}
			});
			
		}
	}
	class RTVEventLoader implements Runnable
	{
		String m_strID;
		public void setStrID(String strKey)
		{
			m_strID = strKey;
		}
		public void run()
		{
			m_tvChannelEventField = new CustomVerticalFieldManager(Manager.VERTICAL_SCROLL);
			Background background = BackgroundFactory.createSolidTransparentBackground(Color.LIGHTGRAY, 250);
			m_tvChannelEventField.setBackground(background);
			m_tvEventList = new SATVChannelEventList(m_strID);
			int nCount = m_tvEventList.totalCount();
			for(int i = 0; i < nCount; i++){
				SATVChannelEventListItem item = m_tvEventList.getDataAtIndex(i);
				TVChannelEventCell cell = new TVChannelEventCell(item, i);
				cell.setMargin(0, 0, 2, 0);
				m_tvChannelEventField.add(cell);
			}
			
			UiApplication.getUiApplication().invokeLater(new Runnable()
			{
				public void run()
				{
					Add(m_tvChannelEventField);
				}
			});
			
		}
	}
	public void refresh()
	{
		deleteGarbage();
		LoadingDialog.showLoadingDialog(new RTVCalendarLoader(), "Loading"); 		
	}
}
