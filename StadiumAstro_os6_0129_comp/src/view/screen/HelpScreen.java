package view.screen;

import view.component.HelpListScroll;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.container.MainScreen;

public class HelpScreen extends MainScreen {

	/**
	 * 
	 */
	public HelpScreen() {
		super(MainScreen.HORIZONTAL_SCROLL | MainScreen.VERTICAL_SCROLLBAR);
		// TODO Auto-generated constructor stub
		
		HelpListScroll helpList = new HelpListScroll(Manager.HORIZONTAL_SCROLL);
		add(helpList);
	}

}
