package view.screen;

import view.component.CustomBitmapField;
import view.component.CustomLabelField;
import view.component.ImageHighlightButton;
import application.AppSession;
import application.ResourceConstants;
import application.StadiumAstroApp;
import application.Utils;

import model.SAFeatureListItem;
import model.SAHighlightNewsListItem;
import model.SANewsListItem;
import model.SAVideoListItem;
import net.rim.device.api.browser.field.RenderingOptions;
import net.rim.device.api.browser.field.RenderingSession;
import net.rim.device.api.browser.field2.BrowserField;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;


public class VideoDetailScreen extends BaseScreen implements FieldChangeListener {
	private String m_title = "";
	private String m_date = "";
	private String m_imagePath = "";
	private String m_detailContent = "";
	private String m_videoUrl = "";
	public VideoDetailScreen(Object dataItem){
//		super("Video");
		super("");
		
		// get data from param
		SAVideoListItem item = (SAVideoListItem)dataItem;
        m_title = item.m_title;
        m_date = Utils.getStringFromDate(item.m_creationDate);
        m_imagePath = item.m_defaultImageUrl;
       	m_detailContent = item.m_description;
       	m_videoUrl = item.m_iPhoneVideoUrl;
		// create view components
        Background background = BackgroundFactory.createSolidTransparentBackground(Color.BLACK, 255);
		this.setBackground(background);
		
		VerticalFieldManager blackBackManager = new VerticalFieldManager();
		blackBackManager.setBackground(background);
		
		VerticalFieldManager detailContentsManager = new VerticalFieldManager(Manager.NO_HORIZONTAL_SCROLL);
		Background contentsBackground = BackgroundFactory.createSolidTransparentBackground(0x292929, 100);
		
        detailContentsManager.setBackground(contentsBackground);
        detailContentsManager.setMargin(0, 10, 0, 10);
       
        // define content width
        int nWidth = Utils.getDisplayWidth() - Utils.toDPIx(40);
        
       	CustomLabelField title = new CustomLabelField(m_title,Color.WHITE,Utils.getTypeFace().getFont(Font.BOLD, ResourceConstants.SA_VAL_FONT_SIZE_BIG_MIDDLE) , Utils.toDPIx(10) , Utils.toDPIy(40) , nWidth, Field.FIELD_LEFT);
//       	CustomLabelField date = new CustomLabelField(m_date,Color.WHITE,Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_BIG_SMALL) , Utils.toDPIx(10) , Utils.toDPIy(20), nWidth, Field.FIELD_LEFT);
       	
       	ImageHighlightButton btn = new ImageHighlightButton(m_imagePath, "icon/icon_video_play.png", 10, 10 , nWidth, Utils.toDPIy(324), Manager.FIELD_LEFT|Manager.FOCUSABLE, ResourceConstants.SA_IMG_VIDEO_TABLE_CELL_THUMB);
       	btn.setChangeListener(this);
       	
       	//CustomLabelField detail = new CustomLabelField(m_detailContent, Color.WHITE, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE) , Utils.marginX(10) , Utils.marginY(10) , nWidth, Field.FIELD_LEFT);
       	BrowserField detail = new BrowserField();
       	
       	detail.setMargin(Utils.toDPIy(10), 0, Utils.toDPIy(10), 0);
       	
//       	//Passion for Browser field's minimum font size process
//       	int fontSize = (int)(10 * Utils.getVRatio());
//       	
//       	detail.getRenderingOptions().setProperty(RenderingOptions.CORE_OPTIONS_GUID, RenderingOptions.MINIMUM_FONT_SIZE, fontSize);
//       	detail.getRenderingOptions().setProperty(RenderingOptions.CORE_OPTIONS_GUID, RenderingOptions.MINIMUM_FONT_SIZE_DEFAULT, fontSize);
//       	RenderingSession renderingSession = RenderingSession.getNewInstance();
//       	renderingSession.getRenderingOptions().setProperty(RenderingOptions.CORE_OPTIONS_GUID, RenderingOptions.MINIMUM_FONT_SIZE, fontSize);
//       	renderingSession.getRenderingOptions().setProperty(RenderingOptions.CORE_OPTIONS_GUID, RenderingOptions.MINIMUM_FONT_SIZE_DEFAULT, fontSize);

       	Utils.setFontSizeForBrowserField(detail, ResourceConstants.SA_VAL_FONT_SIZE_VIDEOS_DETAIL);
       	
       	detail.displayContent(Utils.setHTMLContentSize(m_detailContent, 6, "#292929", "white"), "");
       	detailContentsManager.add(title);
//       	detailContentsManager.add(date);
       	detailContentsManager.add(btn);
       	detailContentsManager.add(detail);
       	
       	blackBackManager.add(detailContentsManager);
       	
       	Add(blackBackManager);
      
	}
	
	public void deleteGarbage(){
		DeleteAll();
	}

	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		if(context == 1)
		{
			StadiumAstroApp mainApp = (StadiumAstroApp) UiApplication.getUiApplication();
			mainApp.gotoVideoPlayerScreen((MainScreen) mainApp.getActiveScreen(), m_title, m_videoUrl);
		}
	}
	
}
