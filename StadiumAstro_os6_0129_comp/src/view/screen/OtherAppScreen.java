package view.screen;



import java.io.IOException;

import javax.microedition.content.ContentHandler;
import javax.microedition.content.ContentHandlerException;
import javax.microedition.content.Invocation;
import javax.microedition.content.Registry;

import application.ResourceConstants;
import application.Utils;
import view.component.CustomHorizontalFieldManager;
import view.component.CustomLabelField;
import view.component.ImageHighlightButton;
import view.component.LoadingDialog;
import model.SAOtherAppList;
import model.SAOtherAppListItem;
import net.rim.blackberry.api.browser.Browser;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class OtherAppScreen extends BaseScreen implements FieldChangeListener{
	private SAOtherAppList m_otherAppList = null;
	public OtherAppScreen(){
		super("Other Apps");
		hideAdsBar();
		ROtherAppInit appLoader = new ROtherAppInit(this);
		LoadingDialog.showLoadingDialog(appLoader, "Loading");
	}
	
	public void deleteGarbage(){
		if(m_otherAppList != null)
			m_otherAppList.m_array.removeAllElements();
		DeleteAll();
	}

	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		if(context == 1)
		{
			ImageHighlightButton button = (ImageHighlightButton)field;
			int tag = button.getButtonTag();
			SAOtherAppListItem item = m_otherAppList.getDataAtIndex(tag);
//			Browser.getDefaultSession().displayPage(item.m_appIOSUrl);//appBBURL
			Registry registry = Registry.getRegistry(this.getClass().getName());
			Invocation invocation = new Invocation(null, null, 
			                                       "net.rim.bb.appworld.Content", 
			                                       false, ContentHandler.ACTION_OPEN);
			String contentID = item.m_appIOSUrl.substring(item.m_appIOSUrl.indexOf("content/")+"content/".length(), item.m_appIOSUrl.indexOf("/?lang"));
			invocation.setArgs(new String[] { contentID });
			try {
				registry.invoke(invocation);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Browser.getDefaultSession().displayPage(item.m_appIOSUrl);//appBBURL
			} catch (ContentHandlerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Browser.getDefaultSession().displayPage(item.m_appIOSUrl);//appBBURL
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Browser.getDefaultSession().displayPage(item.m_appIOSUrl);//appBBURL
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Browser.getDefaultSession().displayPage(item.m_appIOSUrl);//appBBURL
			}
		}
	}
	class ROtherAppInit implements Runnable
	{
		OtherAppScreen m_target;
		CustomHorizontalFieldManager manager;
		public ROtherAppInit(OtherAppScreen target)
		{
			m_target = target;
		}
		public void run()
		{
			m_otherAppList = new SAOtherAppList();
			{
				int totalCount = m_otherAppList.totalCount();
				int imgSize = Utils.toDPIx(100);
				for(int i = 0 ; i < totalCount; i++)
				{
					manager = new CustomHorizontalFieldManager(Utils.getDisplayWidth(), imgSize+10 ,Manager.FIELD_HCENTER);
					
					SAOtherAppListItem item = m_otherAppList.getDataAtIndex(i);
					
					ImageHighlightButton appLogo = new ImageHighlightButton(item.m_appLogoURL, Utils.toDPIx(10), 0, imgSize, imgSize, Manager.FIELD_LEFT|Manager.FOCUSABLE);
					appLogo.setChangeListener(m_target);
					appLogo.setButtonTag(i);
					manager.add(appLogo);
					
					VerticalFieldManager descLayout = new VerticalFieldManager();
					
					int width = Utils.getDisplayWidth() - imgSize; 
					CustomLabelField appNameField = new CustomLabelField(item.m_appName, Color.WHITE, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_MIDDLE), Utils.toDPIx(10), 0, width, Field.FIELD_TOP);
					CustomLabelField appDescField = new CustomLabelField(item.m_appDesc, Color.WHITE, Utils.getTypeFace().getFont(Font.PLAIN, ResourceConstants.SA_VAL_FONT_SIZE_SMALL), Utils.toDPIx(10), Utils.toDPIy(20), width, Field.FIELD_TOP);
					descLayout.add(appNameField);
					descLayout.add(appDescField);
					
					manager.add(descLayout);
					UiApplication.getUiApplication().invokeAndWait(new Runnable()
					{
						public void run()
						{
							Add(manager);
						}
					}
					);
					
				}
			}
		}
	}

}
