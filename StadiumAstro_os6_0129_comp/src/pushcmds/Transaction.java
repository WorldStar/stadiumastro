package pushcmds;

interface TransactionListener
{
	public void onTransactionEnded();
}

public abstract class Transaction extends Thread {
	
	boolean bStopRunning;
	String cmdType;
	TransactionListener delegate;
	public Transaction(String cmd)
	{
		cmdType = cmd;
	}
	
	public void setTransactionListener(TransactionListener listener)
	{
		delegate = listener;
	}
	
	public void stopTransaction()
	{
		bStopRunning = true;
	}
	
	public void run()
	{	
		
	}
	
	public void onEndTransaction()
	{
		if(delegate != null)
		{
			delegate.onTransactionEnded();
		}
	}
	
	protected abstract void execute() throws Exception;
}
