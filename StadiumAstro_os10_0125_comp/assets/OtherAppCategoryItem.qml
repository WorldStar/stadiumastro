import bb.cascades 1.0
import my.library 1.0
// view model of highlight video & news info
Container {
    id: itemRoot

    topPadding: 20.0
    bottomPadding: 20.0

    background: Color.Black
    preferredWidth: 768

    Container {
        layout: DockLayout {
        }

        verticalAlignment: VerticalAlignment.Fill
        horizontalAlignment: HorizontalAlignment.Fill

        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }

            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Left

            // must be replaced with WebImageView!!!
            Container {
                layout: DockLayout {
                }

                WebImageViewContainer {
                    url: ListItemData.appLogoURL
                    preferredWidth: 150
                    preferredHeight: 150
                    minWidth: 150.0
                    maxWidth: 150.0
                }
            }

            Container {
                layout: DockLayout {
                }

                leftMargin: 20.0
                verticalAlignment: VerticalAlignment.Fill

                // subject title
                Label {
                    text: ListItemData.appName
                    multiline: true

                    textStyle {
                        textAlign: TextAlign.Left
                        fontSize: FontSize.PointValue
                        fontSizeValue: 7
                        color: Color.White
                        fontWeight: FontWeight.Bold
                    }
                    verticalAlignment: VerticalAlignment.Top
                    horizontalAlignment: HorizontalAlignment.Left

                }

                Label {
                // description
                    text: ListItemData.appDesc
                    verticalAlignment: VerticalAlignment.Bottom
                    horizontalAlignment: HorizontalAlignment.Left
                    multiline: true
                    textStyle {
                        textAlign: TextAlign.Left
                        fontSize: FontSize.PointValue
                        fontSizeValue: 4
                        color: Color.White
                    }
                }
            }
        }
    }
}