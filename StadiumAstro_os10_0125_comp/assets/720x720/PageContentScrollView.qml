import bb.cascades 1.0


// For the 720x720 resolution a ScrollView is used, for the standard
// resolution this would lead to rubberbanding of a full screen UI
// which we do not want
ScrollView {
    scrollViewProperties {
        scrollMode: ScrollMode.Vertical        
        overScrollEffectMode: OverScrollEffectMode.OnScroll
        pinchToZoomEnabled: false        
    }
}