// Default empty project template
import bb.cascades 1.0
import my.library 1.0

// creates one page
Page {
    property int nTime: 5
    Container {  
    	id: splashScreen
    
        layout: DockLayout {

        }
        
        background: Color.Black
        
        // BootUp image view
        ImageView {
            id: splash
            imageSource: "asset:///images/bootup.png"
            loadEffect: ImageViewLoadEffect.FadeZoom
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
            
            attachedObjects: [
                QTimer {
                    id: bootupTimer
                    interval: 2000
                    onTimeout: {
                        // display ads screen after 2 seconds
                        requestAds();
                        bootupTimer.stop();
                    }
                }
            ]
            
            onCreationCompleted: {
                bootupTimer.start();
            }
        }        
        
        // Ads image view
        WebImageView {
            id: ads
            objectName:"ads"
            imageSource: "asset:///images/bg_black.png" // to be replaced
            visible: false
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
            
            //scalingMethod: ScalingMethod.AspectFill
            
            attachedObjects: [
                QTimer {
                    id: goHomeTimer
                    interval: 1000
                    onTimeout: {
                        // display ads screen after 2 seconds
                        //requestDisplayMainPage();
                    }
                }
            ]
            onTouch:
            {
                if (! event.isUp()) return;
                // go to home page
                mainInstance.openAdsLink();
            }
        }        

        // skip ads button
        Label {
            id: skipButton
            text: "skip in 5" //Skip Ads>>
            visible: true
            enabled: false
            preferredWidth: 170.0
            opacity: 0
            
            textStyle {
                color: Color.White
                fontSize: FontSize.PointValue
                fontSizeValue: 8
                textAlign: TextAlign.Left
            }

            verticalAlignment: VerticalAlignment.Bottom
            horizontalAlignment: HorizontalAlignment.Right

            focusPolicy: FocusPolicy.Touch
            onTouch: {
                if (! event.isUp()) return;

                if (nTime != 0) return;

                touchAnim.play();

                // go to home page
                requestDisplayMainPage();
            }

            animations: [
                FadeTransition {
                    id: displaySkipButton
                    //delay: 4500
                    duration: 500
                    toOpacity: 1.0
                    fromOpacity: 0.0
                },

                SequentialAnimation {
                    id: touchAnim
                    
                    FadeTransition {
                        duration: 150
                        toOpacity: 1.0
                        fromOpacity: 0.3
                        easingCurve: StockCurve.BackIn
                    }
                }

            ]
        }

        attachedObjects: [
            QTimer {
                id: timer
                interval: 1000
                onTimeout: {
                    // One second after page loads make the http get request
                    nTime = nTime - 1;
                    if (nTime == 0) {
                        skipButton.enabled = true;
                        skipButton.text = "skip ad>>"
                        timer.stop();
                    } else {
                        skipButton.enabled = false;
                        skipButton.text = "skip in " + nTime
                        timer.stop();
                        timer.start();
                    }
                }
            }
       ]       
    }

	// display ads screen automatically after splash screen
	function requestAds()
	{
	    mainInstance.getAdsBannerListInfo();
    }
	
	function replaceToAdsScreen()
	{
        splash.visible = false;
        ads.visible = true;
        skipButton.visible = true;
        displaySkipButton.play();
        
        timer.start();
        //goHomeTimer.start();
    }
	
	// request displaying of main page
	function requestDisplayMainPage()
	{
	    eventHandler.ShowHome();
	}
   
}
