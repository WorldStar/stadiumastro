import bb.cascades 1.0

Container {
    id: rootItem
    
    preferredWidth: 100
    preferredHeight: 42
    rightMargin: 20
    
    background: ListItem.selected ? selectedBackground.imagePaint: null
    
    Label {
        id: lblDate
        text: ListItemData.displayDay + ListItemData.displayMonth
        verticalAlignment: VerticalAlignment.Center
        horizontalAlignment: HorizontalAlignment.Center
        textStyle.fontSize: FontSize.Small
        textStyle.textAlign: TextAlign.Left
        textStyle.color: Color.Gray 
    }

    attachedObjects: [
        ImagePaintDefinition {
            id: selectedBackground
            imageSource: "asset:///images/img_channel_calendar_down.png"
            repeatPattern: RepeatPattern.Fill
        }
    ]

    // Highlight function for the highlight Container
    function setHighlight(highlighted) {
        if (highlighted) {
            lblDate.textStyle.color = Color.White;
        } else {
            lblDate.textStyle.color = Color.Gray;
        }
    }

    // Signal handler for ListItem selection
    ListItem.onSelectionChanged: {
        setHighlight(ListItem.selected);
    }
}
