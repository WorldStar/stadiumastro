import bb.cascades 1.0
import my.library 1.0

Page {
    id: mainPage
    property real screenWidth: 768
    property real screenHeight: 1280

    //! [0]
    Container {
        layout: DockLayout {
        }

        background: Color.Black

        //! Help contents
        PageContentScrollView {
            id: mainContents

            Container {
                layout: DockLayout {

                }

                preferredHeight: screenHeight

                
         
                ListView
                {
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Center
                    dataModel: XmlDataModel {
                        source: "models/helpItems.xml"
                    }
                    listItemComponents: [
                        ListItemComponent {
                            type: "item"
                            ImageView {
                                horizontalAlignment: HorizontalAlignment.Fill
                                verticalAlignment: VerticalAlignment.Fill
                                preferredWidth: 768.0
                                preferredHeight: 1280.0
                                imageSource: ListItemData.image  
                            }
                        }
                    ]
                    layout: StackListLayout {
                        orientation: LayoutOrientation.LeftToRight
                        headerMode: ListHeaderMode.None

                    }
                    snapMode: SnapMode.LeadingEdge
                    preferredWidth: 768.0
                    preferredHeight: 1280.0
                    flickMode: FlickMode.SingleItem
                    focusAutoShow: FocusAutoShow.Default
                    leadingVisualSnapThreshold: 1.0
                }

            
                ImageButton {

                    onClicked: {
                        eventHandler.popTop();
                    }
                    horizontalAlignment: HorizontalAlignment.Right
                    verticalAlignment: VerticalAlignment.Top
                    defaultImageSource: "asset:///images/icon_ads_close.png"
                }
            }
            

        }
    } 
}