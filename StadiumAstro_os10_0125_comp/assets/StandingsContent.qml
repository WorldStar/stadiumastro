import bb.cascades 1.0

Container {
    background: Color.Black
    
    layout: DockLayout{}
    
    topPadding: 20
    leftPadding: 10
    rightPadding: 10
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    
    Container {
        layout: StackLayout{
            orientation: LayoutOrientation.TopToBottom
        }
        horizontalAlignment: HorizontalAlignment.Fill
        
        // Standing Content's title area
        Container {
            layout: DockLayout {}
            
            preferredHeight: 80
            
            background: standingTopBackground.imagePaint

            horizontalAlignment: HorizontalAlignment.Fill
            leftPadding: 40

            attachedObjects: [
                ImagePaintDefinition {
                    id: standingTopBackground                    
                    imageSource: "asset:///images/bar_standing-top.png"
                }
            ]
            
            // group title label
            Label {
                id: groupTitle
                objectName: "lblGroupTitle"
                verticalAlignment: VerticalAlignment.Center
                textStyle.color: Color.White
                textStyle.fontWeight: FontWeight.Bold
            }            
        }
        
        // standing table header
        Container {
            id: standingsContainer

            layout: DockLayout{}
            horizontalAlignment: HorizontalAlignment.Fill
            background: Color.LightGray
            preferredHeight: 60
            
            Container {
                leftPadding: 30
                
                layout: DockLayout{}

                verticalAlignment: VerticalAlignment.Fill
                
                Label {
                    text: "Team"
                    textStyle.fontSize: FontSize.XSmall
                    verticalAlignment: VerticalAlignment.Center
                    textStyle.color: Color.Black
                }
            }

            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.RightToLeft
                }

                horizontalAlignment: HorizontalAlignment.Right
                verticalAlignment: VerticalAlignment.Fill
                
                rightPadding: 10
                
                Label {
                    preferredWidth: 40
                    text: "Pts"
                    textStyle.textAlign: TextAlign.Center
                    textStyle.color: Color.Black
                    textStyle.fontSize: FontSize.XSmall
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                }

                Label {
                    preferredWidth: 40
                    text: "A"
                    textStyle.textAlign: TextAlign.Center
                    textStyle.color: Color.Black
                    textStyle.fontSize: FontSize.XSmall
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                }

                Label {
                    preferredWidth: 40
                    text: "F"
                    textStyle.textAlign: TextAlign.Center
                    textStyle.color: Color.Black
                    textStyle.fontSize: FontSize.XSmall
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                }

                Label {
                    preferredWidth: 40
                    text: "L"
                    textStyle.textAlign: TextAlign.Center
                    textStyle.color: Color.Black
                    textStyle.fontSize: FontSize.XSmall
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                }

                Label {
                    preferredWidth: 40
                    text: "D"
                    textStyle.textAlign: TextAlign.Center
                    textStyle.color: Color.Black
                    textStyle.fontSize: FontSize.XSmall
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                }

                Label {
                    preferredWidth: 40
                    text: "W"
                    textStyle.textAlign: TextAlign.Center
                    textStyle.color: Color.Black
                    textStyle.fontSize: FontSize.XSmall
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                }

                Label {
                    preferredWidth: 40
                    text: "P"
                    textStyle.textAlign: TextAlign.Center
                    textStyle.color: Color.Black
                    textStyle.fontSize: FontSize.XSmall
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                }
            }
        }

        // standing table body
        ListView {
            id: standingList
            objectName: "standingList"

            layout: StackListLayout {
                orientation: LayoutOrientation.TopToBottom
                headerMode: ListHeaderMode.None
            }

            horizontalAlignment: HorizontalAlignment.Fill

            listItemComponents: [
                ListItemComponent {
                    type: "item"
                    StandingTableListItem {
                        
                        preferredWidth: 768

                    }
                }
            ]
            scrollIndicatorMode: ScrollIndicatorMode.None
        }
    }
}
