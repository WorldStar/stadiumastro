import bb.cascades 1.0
import my.library 1.0
Container {
    property bool bMatchAvail
    property bool bHasHighLight
    property bool bHasMatchClip
    property string strMatchID
    layout: StackLayout {
        orientation: LayoutOrientation.TopToBottom
    }

	//! [1] Date String panel
    Container {        
        background: dateDisplayBackground.imagePaint

        horizontalAlignment: HorizontalAlignment.Fill

		topPadding: 2
		bottomPadding: 6
        attachedObjects: [
            ImagePaintDefinition {
                id: dateDisplayBackground
                imageSource: "asset:///images/bar_main_section_gray.png"
            }
        ]
        
        layout: DockLayout {
            
        }
        
        Label {
            id: lblDateStr
            objectName: "lblDateStr"
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
            textStyle.color: Color.White
            textStyle.fontSize: FontSize.PointValue
            textStyle.fontSizeValue: 6.0
        }
    }//! [1]
    
    //! [2] Match result subview
    Container {
        layout: DockLayout {
        }
        horizontalAlignment: HorizontalAlignment.Fill

        preferredHeight: 197.0

        ImageView {
            imageSource: "asset:///images/bg_match_box.png"
            horizontalAlignment: HorizontalAlignment.Fill
        }

        Container {
            leftPadding: 50.0
            rightPadding: 50.0

            preferredHeight: 197.0            

            layout: DockLayout {
            }

            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill

            // home team logo image
            // must be replaced with WebImageView!!!
            

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 20.0
                bottomPadding: 20.0
                layout: DockLayout {
                }

                horizontalAlignment: HorizontalAlignment.Fill

                // home team alias
                Container
                {
                    preferredHeight: 100.0
                    topPadding: 20.0
                    WebImageView {
                        id: imgHomeTeamLogo
                        objectName: "imgHomeTeamLogo"
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                        preferredWidth: 80.0
                        preferredHeight: 80.0
                        minHeight: 80.0
                    }
                    Label {
                        id: lblHomeTeamAlias
                        objectName: "lblHomeTeamAlias"
                        textStyle.fontSize: FontSize.PointValue
                        textStyle.fontSizeValue: 7.0
                        textStyle.color: Color.Black    
                        textStyle.fontWeight: FontWeight.Bold
                        
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center

                    }
                }

                Container
                {
                    horizontalAlignment: HorizontalAlignment.Right

                    layout: StackLayout {

                    }
                    preferredHeight: 100.0
                    topPadding: 20.0
                    // away team logo image
	                // must be replaced with WebImageView!!!
	                WebImageView {
	
	                    id: imgVisitingTeamLogo
	                    objectName: "imgVisitingTeamLogo"
	                    verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                        preferredWidth: 80.0
	                    preferredHeight: 80.0
                        minHeight: 80.0
                    }
	                
                    // away team alias
                    Label {
                        id: lblVisitingTeamAlias
                        objectName: "lblVisitingTeamAlias"
                        textStyle.fontSize: FontSize.PointValue
                        textStyle.fontSizeValue: 7.0
                        textStyle.color: Color.Black
                        textStyle.fontWeight: FontWeight.Bold
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center

                    }
                }
            }

            Container {
                leftPadding: 200.0
                rightPadding: 200.0

                layout: DockLayout {
                }

                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Center

                Label {
                    id: lblMatchScore
                    objectName: "lblMatchScore"
                    textStyle.color: Color.Black
                    horizontalAlignment: HorizontalAlignment.Center
                    textStyle.fontSize: FontSize.PointValue
                    
                    verticalAlignment: VerticalAlignment.Top
                    textStyle.fontSizeValue: 20.0
                    text: qsTr("")
                }

                
            }
            
            Container {
                leftPadding: 200.0
                rightPadding: 200.0
                topPadding: 20.0

                layout: DockLayout {
                }

                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Top

                Label {
                    id: lblMatchTime
                    objectName: "lblMatchTime"
                    textStyle.color: Color.DarkGray
                    horizontalAlignment: HorizontalAlignment.Center
                    textStyle.fontSize: FontSize.PointValue

                    verticalAlignment: VerticalAlignment.Top
                    textStyle.fontSizeValue: 6.0
                    text: qsTr("")
                }
            }

            Container {
                topPadding: 20.0
                bottomPadding: 20.0
                layout: DockLayout {

                }
                verticalAlignment: VerticalAlignment.Fill
                horizontalAlignment: HorizontalAlignment.Fill

                // match status display
                Label {
                    id: lblMatchStaus
                    objectName: "lblMatchStatus"
                    horizontalAlignment: HorizontalAlignment.Center
                    textStyle.fontSize: FontSize.XSmall
                    textStyle.textAlign: TextAlign.Center
                    textStyle.color: Color.create("#ff505050")

                }
            }
        }
    }//! [2]
    
    //! [3] Tab Panel
    Container {
        id: tabPanel
        layout: DockLayout {}

        horizontalAlignment: HorizontalAlignment.Fill
        
        property variant selectedTab: null
        
        Container {
            background: tabPanelBackground.imagePaint

            horizontalAlignment: HorizontalAlignment.Fill

            preferredHeight: 86.0

            attachedObjects: [
                ImagePaintDefinition {
                    id: tabPanelBackground
                    imageSource: "asset:///images/bg_tab_full.png"
                },
                ImagePaintDefinition {
                    id: tabSelectedBackground
                    imageSource: "asset:///images/bg_tab_dblue207.png"
                }
            ]

            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            
         
            ScrollView {
                scrollViewProperties.scrollMode: ScrollMode.Horizontal
                Container
                {
                    
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight

                    }
                    clipContentToBounds: false
                    horizontalAlignment: HorizontalAlignment.Left
                    preferredWidth: 1262.0
                    minWidth: 1262.0
                    maxWidth: 1262.0
                    // Preview button
                    Container {
                        id: btnPreview
                        objectName : "btnPreview"
                        preferredWidth: 207
                        preferredHeight: 86.0
                        verticalAlignment: VerticalAlignment.Fill
                        
                        property variant foreColor: Color.Black
                        
                        layout: DockLayout {
                        }
                        
                        minWidth: 207.0
                        maxWidth: 207.0
                        Label {
                            text: "Preview"
                            textStyle.color: btnPreview.foreColor
                            horizontalAlignment: HorizontalAlignment.Center
                            verticalAlignment: VerticalAlignment.Center
                            textStyle.textAlign: TextAlign.Center
                            textStyle.fontSize: FontSize.XSmall
                            overlapTouchPolicy: OverlapTouchPolicy.Allow
                            textStyle.fontWeight: FontWeight.Bold
                        }
                        
                        onTouch: {
                            if (event.isUp()) onSelected(btnPreview);
                        }
                    }
                    
                    // Report button
                    Container {
                        id: btnReport
                        preferredWidth: 207
                        preferredHeight: 86.0
                        verticalAlignment: VerticalAlignment.Fill
                        
                        property variant foreColor: Color.Black
                        
                        layout: DockLayout {
                        }
                        
                        minWidth: 207.0
                        maxWidth: 207.0
                        Label {
                            text: "Report"
                            textStyle.color: btnReport.foreColor
                            horizontalAlignment: HorizontalAlignment.Center
                            verticalAlignment: VerticalAlignment.Center
                            textStyle.textAlign: TextAlign.Center
                            textStyle.fontSize: FontSize.XSmall
                            overlapTouchPolicy: OverlapTouchPolicy.Allow
                            textStyle.fontWeight: FontWeight.Bold
                        }
                        
                        onTouch: {
                            if (event.isUp()) onSelected(btnReport);
                        }
                    }
                    
                    // Team Line-up button
                    Container {
                        id: btnTeamLineUp
                        preferredWidth: 207
                        preferredHeight: 86.0
                        verticalAlignment: VerticalAlignment.Fill
                        
                        property variant foreColor: Color.Black
                        
                        layout: DockLayout {
                        }
                        
                        minWidth: 207.0
                        maxWidth: 207.0
                        Label {
                            text: "Team Line-up"
                            textStyle.color: btnTeamLineUp.foreColor
                            horizontalAlignment: HorizontalAlignment.Center
                            verticalAlignment: VerticalAlignment.Center
                            textStyle.textAlign: TextAlign.Center
                            textStyle.fontSize: FontSize.XSmall
                            overlapTouchPolicy: OverlapTouchPolicy.Allow
                            textStyle.fontWeight: FontWeight.Bold
                        }
                        
                        onTouch: {
                            if (event.isUp()) onSelected(btnTeamLineUp);
                        }
                    }
                    
                    // Match Events button
                    Container {
                        id: btnMatchEvents
                        preferredWidth: 207
                        preferredHeight: 86.0
                        verticalAlignment: VerticalAlignment.Fill
                        
                        property variant foreColor: Color.Black
                        
                        layout: DockLayout {
                        }
                        
                        minWidth: 207.0
                        maxWidth: 207.0
                        Label {
                            text: "Match Events"
                            textStyle.color: btnMatchEvents.foreColor
                            horizontalAlignment: HorizontalAlignment.Center
                            verticalAlignment: VerticalAlignment.Center
                            textStyle.textAlign: TextAlign.Center
                            textStyle.fontSize: FontSize.XSmall
                            overlapTouchPolicy: OverlapTouchPolicy.Allow
                            textStyle.fontWeight: FontWeight.Bold
                        }
                        
                        onTouch: {
                            if (event.isUp()) onSelected(btnMatchEvents);
                        }
                    }
                    
                    // Video highlights button
                    Container {
                        id: btnVideoHighlights
                        preferredWidth: 227
                        preferredHeight: 86.0
                        verticalAlignment: VerticalAlignment.Fill
                        
                        property variant foreColor: Color.Black
                        
                        layout: DockLayout {
                        }
                        
                        minWidth: 227.0
                        maxWidth: 227.0
                        Label {
                            text: "Video Highlights"
                            textStyle.color: btnVideoHighlights.foreColor
                            horizontalAlignment: HorizontalAlignment.Center
                            verticalAlignment: VerticalAlignment.Center
                            textStyle.textAlign: TextAlign.Center
                            textStyle.fontSize: FontSize.XSmall
                            overlapTouchPolicy: OverlapTouchPolicy.Allow
                            textStyle.fontWeight: FontWeight.Bold
                            
                        }
                        
                        onTouch: {
                            if (event.isUp()) onSelected(btnVideoHighlights);
                        }
                    }
                    
                    // In-Match clips button
                    Container {
                        id: btnInMatchClips
                        preferredWidth: 207
                        preferredHeight: 86.0
                        verticalAlignment: VerticalAlignment.Fill
                        
                        property variant foreColor: Color.Black
                        
                        layout: DockLayout {
                        }
                        
                        minWidth: 207.0
                        maxWidth: 207.0
                        Label {
                            text: "In-Match Clips"
                            textStyle.color: btnInMatchClips.foreColor
                            horizontalAlignment: HorizontalAlignment.Center
                            verticalAlignment: VerticalAlignment.Center
                            textStyle.textAlign: TextAlign.Center
                            textStyle.fontSize: FontSize.XSmall
                            overlapTouchPolicy: OverlapTouchPolicy.Allow
                            textStyle.fontWeight: FontWeight.Bold
                        }
                        
                        onTouch: {
                            if (event.isUp()) onSelected(btnInMatchClips);
                        }
                    }
                }
            }
        }
        
    }//! [3]
    
    //! [4]
    Container {
        objectName: "analysisContentViewer"
        layout: DockLayout {}
        
        background: Color.White

        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill

		//! [5] preview content viewer
        ScrollView {
            id: previewContent
            objectName: "previewView"
            
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            scrollViewProperties.scrollMode: ScrollMode.Vertical
            scrollViewProperties.initialScalingMethod: ScalingMethod.AspectFill
            scrollViewProperties.overScrollEffectMode: OverScrollEffectMode.OnScroll
            scrollViewProperties.pinchToZoomEnabled: false
            
            Container {
                objectName: "previewContentContainer"
                
                property alias previewDetail: lblPreviewDetail.html
                property alias previewTitle: lblPreviewTitle.text
                property alias previewDate: lblPreviewDate.text
                leftPadding: 20
                rightPadding: 20
                topPadding: 20
                
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                
                Container {
                    leftPadding: 10
                    horizontalAlignment: HorizontalAlignment.Fill
                    
                    Label {
                        id: lblPreviewTitle
                        objectName: "lblPreviewTitle"
                        textStyle.fontSize: FontSize.Small
                        textStyle.color: Color.Black
                        
                        textStyle.textAlign: TextAlign.Left
                        textStyle.fontWeight: FontWeight.Bold
                        multiline: true
                    }
                }
                
                Container {
                    leftPadding: 10
                    horizontalAlignment: HorizontalAlignment.Fill
                    
                    Label {
                        id: lblPreviewDate
                        objectName: "lblPreviewDate"
                        textStyle.fontSize: FontSize.Small
                        textStyle.color: Color.DarkGray
                        textStyle.textAlign: TextAlign.Left
                    }

                }

                ScrollView {

                    scrollViewProperties.scrollMode: ScrollMode.Both
                    scrollViewProperties.initialScalingMethod: ScalingMethod.AspectFill

                    scrollViewProperties.overScrollEffectMode: OverScrollEffectMode.OnPinchAndScroll
                    scrollViewProperties.maxContentScale: 2.0
                    scrollViewProperties.pinchToZoomEnabled: true
                    scrollViewProperties.minContentScale: 1.0
                    Container {
                        topPadding: 30
                        horizontalAlignment: HorizontalAlignment.Fill
                        
                        layout: DockLayout {
                            
                        }
                        
                        WebView {
                            id: lblPreviewDetail
                            verticalAlignment: VerticalAlignment.Top
                        }
                        WebView {
                            id: lblTempView
                            verticalAlignment: VerticalAlignment.Top
                            visible: false
                        }
                        

                    }
                }

				                
            }
            
        }//! [5]
        
        //! [6] report viewer
        ScrollView {
            id: reportContent
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            scrollViewProperties.scrollMode: ScrollMode.Vertical
            scrollViewProperties.initialScalingMethod: ScalingMethod.AspectFill
            scrollViewProperties.overScrollEffectMode: OverScrollEffectMode.OnScroll
            scrollViewProperties.pinchToZoomEnabled: false
            
            Container {
                objectName: "reportContentContainer"
                
                property alias reportTitle: lblReportTitle.text
                property alias reportDate: lblReportDate.text
                property alias reportBody: reportBodyView.html
                
                topPadding: 20
                leftPadding: 20
                rightPadding: 20

                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                
                layout: StackLayout{
                    orientation: LayoutOrientation.TopToBottom
                }

                Container {
                    leftPadding: 10
                    horizontalAlignment: HorizontalAlignment.Fill
                    Label {
                        id: lblReportTitle
                        objectName: "lblReportTitle"
                        textStyle.fontSize: FontSize.Small
                        textStyle.color: Color.Black

                        textStyle.textAlign: TextAlign.Left
                        textStyle.fontWeight: FontWeight.Bold
                        multiline: true
                    }
                }
                
                Container {
                    leftPadding: 10
                    horizontalAlignment: HorizontalAlignment.Fill

                    Label {
                        id: lblReportDate
                        objectName: "lblReportDate"
                        textStyle.fontSize: FontSize.Small
                        textStyle.color: Color.DarkGray
                        
                        textStyle.textAlign: TextAlign.Left
                    }
                }

                ScrollView {

                    scrollViewProperties.scrollMode: ScrollMode.Both
                    scrollViewProperties.initialScalingMethod: ScalingMethod.AspectFill

                    scrollViewProperties.overScrollEffectMode: OverScrollEffectMode.OnPinchAndScroll
                    scrollViewProperties.maxContentScale: 2.0
                    scrollViewProperties.pinchToZoomEnabled: true
                    scrollViewProperties.minContentScale: 1.0
                    Container {
                        topPadding: 30
                        horizontalAlignment: HorizontalAlignment.Fill
                        WebView {
                            id: reportBodyView
                            objectName: "reportBodyView"

                        }
                    }
                }
                
                
            }
        }//! [6]
        //! [7] team line-up viewer
        ListView {
            id: lineUpContent
            objectName: "lineUpListView"

            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill

            layout: StackListLayout {
                orientation: LayoutOrientation.TopToBottom
                headerMode: ListHeaderMode.None
            }

            scrollIndicatorMode: ScrollIndicatorMode.None

            listItemComponents: [
                ListItemComponent {
                    type: "item"

                    LineUpListItem {

                    }
                }
            ]

        } //! [7]
        
        //! [8] match event viewer
        ListView {
            id: eventContent
            objectName: "eventListView"

            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill

            layout: StackListLayout {
                orientation: LayoutOrientation.TopToBottom
                headerMode: ListHeaderMode.None
            }

            scrollIndicatorMode: ScrollIndicatorMode.None

            listItemComponents: [
                ListItemComponent {
                    type: "item"

                    MatchEventListItem {

                    }
                }
            ]
        } //! [8]

        
        //! [9] video highlights viewer
  
        ListView {
            id: videoHighlightContent
            objectName: "videoHighlightListView"

            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill

            layout: StackListLayout {
                orientation: LayoutOrientation.TopToBottom
                headerMode: ListHeaderMode.None
            }

            scrollIndicatorMode: ScrollIndicatorMode.None

            listItemComponents: [
                ListItemComponent {
                    type: "item"

                    VideoFeedListItem {
                        preferredWidth: 768
                    }
                }
            ]

            onTriggered: {
                // get video's url
                var videoData = videoHighlightContent.dataModel.data(indexPath);

                // go to video play screen
                //eventHandler.ShowVideoPlayScreen(videoData);
                eventHandler.ShowNewsViewPage(videoData);

            }
        } //! [9]
        
        
        //! [10] in-match clips viewer
        Container
        {
            id: inMatchClipsContainer
            ListView {
                id: inMatchClipsContent
                objectName: "inMatchClipsListView"

                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill

                preferredWidth: 768

                layout: StackListLayout {
                    orientation: LayoutOrientation.TopToBottom
                    headerMode: ListHeaderMode.None
                }

                scrollIndicatorMode: ScrollIndicatorMode.None

                listItemComponents: [
                    ListItemComponent {
                        type: "item"

                        VideoFeedListItem {
                            preferredWidth: 768

                        }
                    }
                ]

                onTriggered: {
                    // get video's url
                    var clipData = inMatchClipsContent.dataModel.data(indexPath);

                    // go to video play screen
                    eventHandler.ShowVideoPlayScreen(clipData);
                }
            } //! [10]
        }
        
        
        
    }//! [4]

    onCreationCompleted: {
        hideAllContents();
    }

    // select animation implementaion
    function onSelected(id){
        var beforeSelected = tabPanel.selectedTab;
//        if(id != beforeSelected){

            // reset previous selected item's property
            if(beforeSelected){
            	beforeSelected.background = null;
            	beforeSelected.foreColor = Color.Black;
            }
            
	        // set newly selected item's property
	        id.background = tabSelectedBackground.imagePaint;
	        id.foreColor = Color.White;
	        
         	// save current selected item to property
            tabPanel.selectedTab = id;
            
            // display suitable content
            displayContent(id);
//        }
    }
    
    // display content when pressed tab buttons
    function displayContent(id){
        
        if(bMatchAvail)
        {
            previewContent.visible = (id == btnPreview);
            videoHighlightContent.visible = (id == btnVideoHighlights);
            inMatchClipsContent.visible = (id == btnInMatchClips);
            reportContent.visible = (id == btnReport);
            lineUpContent.visible = (id == btnTeamLineUp);
            eventContent.visible = (id == btnMatchEvents);
            
            //passion
            if (id == btnPreview){
                previewContent.visible = true;
                lblPreviewDetail.visible = true;
                lblTempView.visible = false;
            }
            else {
                previewContent.visible = false;
                lblPreviewDetail.visible = false;
                lblTempView.visible = false;
            }

            

            if (id == btnVideoHighlights) {
                if (bHasHighLight == false) {
                    videoHighlightContent.visible = false;
                    previewContent.visible = true;
                    lblPreviewDetail.visible = false;
                    lblTempView.visible = true;
                    lblTempView.html = "<html><font size=5> The video will be available within 24 hours after the match</font></html>"
                } else {
                    previewContent.visible = false;
                    lblPreviewDetail.visible = false;
                    lblTempView.visible = false;
                    
                    videoHighlightContent.visible = true;
                }
            }

            //Passion
            if (id == btnInMatchClips) {
                if (bHasMatchClip == false) {
                    inMatchClipsContent.visible = false;
                    previewContent.visible = true;
                    lblPreviewDetail.visible = false;
                    lblTempView.visible = true;
                    lblTempView.html = "<html><font size=5> The video will be available only during the match</font></html>"
                } else {
                    previewContent.visible = false;
                    lblPreviewDetail.visible = false;
                    lblTempView.visible = false;
                    
                    inMatchClipsContent.visible = true;
                }
            }
        }
        else
        {
            previewContent.visible = false;
            reportContent.visible = false;
            lineUpContent.visible = false;
            eventContent.visible = false;
            videoHighlightContent.visible = false;
            inMatchClipsContent.visible = false;
            
            if(id == btnPreview)
        	{
        		previewContent.visible = true;
                lblPreviewDetail.html = "<html><font size=5>Match Preview will be available within 24 hours before the match starts</font></html>"
            }
            	
            else if(id == btnReport)
            {
                reportContent.visible = true;
                reportBodyView.html = "<html><font size=5>Match Report will be available within 24 hours before the match starts</font></html>"
            }
            else if(id==btnTeamLineUp || id == btnMatchEvents)
            {
                previewContent.visible = true;
                lblPreviewDetail.html = "<html><font size =5>The data will be available when the match starts</font></html>"
	        }
	        else if(id==btnVideoHighlights)
	        {
                previewContent.visible = true;
                lblPreviewDetail.html = "<html><font size=5> The video will be available within 24 hours after the match</font></html>"
	        }
	        else if(id == btnInMatchClips)
	        {
                previewContent.visible = true;
                lblPreviewDetail.html = "<html><font size=5> The video will be available only during the match</font></html>"
	        }
        }
        
    }

    
    function hideAllContents()
    {
        previewContent.visible = false;
        reportContent.visible = false;
        lineUpContent.visible = false;
        eventContent.visible = false;
        videoHighlightContent.visible = false;
        inMatchClipsContent.visible = false;
    }
    function selectFirstTab()
    {
        onSelected(btnPreview);
    }
}
