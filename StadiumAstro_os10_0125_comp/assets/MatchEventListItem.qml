import bb.cascades 1.0

Container {
    id: itemRoot

    layout: DockLayout {
    }

    horizontalAlignment: HorizontalAlignment.Fill
    preferredWidth: 768.0
    
    property variant backColor: (itemRoot.ListItem.indexPath[0] % 2) ? Color.White : Color.create("#ffe0e0e0")
    
    background: itemRoot.backColor

    preferredHeight: 60.0
    
    minHeight: 60.0
    maxHeight: 60.0
    
    // home team event display
    Container {
        preferredWidth: 320
        verticalAlignment: VerticalAlignment.Fill

        background: Color.LightGray
        
        rightPadding: 3.0
        layout: DockLayout {}
        
        Container {
            background: itemRoot.backColor
            
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
            
            rightPadding: 20.0
            
            layout : StackLayout{
                orientation: LayoutOrientation.RightToLeft
            }

			// event image
            ImageView {
                imageSource: (ListItemData.teamType == "HOME") ? getImageForEventType(ListItemData.eventType, true) : ""
                verticalAlignment: VerticalAlignment.Center
            }
            
            // player name
            Label {
                text: (ListItemData.teamType == "HOME") ? ListItemData.playerName + (ListItemData.eventType == "OG" ? " (OG)" : "")  : ""
                verticalAlignment: VerticalAlignment.Center
                textStyle.color: Color.Black
                textStyle.fontSize: FontSize.XSmall
                textStyle.textAlign: TextAlign.Right
            }
        }
    }
    
    // away team event display
    Container {
        preferredWidth: 320
        verticalAlignment: VerticalAlignment.Fill

        background: Color.LightGray

        leftPadding: 3.0
        layout: DockLayout {}

        horizontalAlignment: HorizontalAlignment.Right
        Container {
            background: itemRoot.backColor

            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill

            leftPadding: 20.0

            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            
            // event type image
            ImageView {
                imageSource: (ListItemData.teamType == "AWAY") ? getImageForEventType(ListItemData.eventType, false)  : "" 
                verticalAlignment: VerticalAlignment.Center
            }
            
            // player name
            Label {
                text: (ListItemData.teamType == "AWAY") ? ListItemData.playerName + (ListItemData.eventType == "OG" ? " (OG)" : "") : ""
                verticalAlignment: VerticalAlignment.Center
                textStyle.color: Color.Black
                textStyle.fontSize: FontSize.XSmall
                textStyle.textAlign: TextAlign.Left
            }
        }
    }
    
    // event time display
    Label {
        text: ListItemData.minute
        verticalAlignment: VerticalAlignment.Center
        textStyle.color: Color.Black
        textStyle.fontSize: FontSize.XSmall
        textStyle.textAlign: TextAlign.Center
        horizontalAlignment: HorizontalAlignment.Center
    }

    function getImageForEventType(type, bHomeEvent) {

        var YELLOW_CARD_IMAGE = "icon_stats_yellow-card.png";
        var RED_CARD_IMAGE = "icon_stats_red-card.png";
        var GOALIN_IMAGE = "icon_stats_goal.png";
        var HOME_TEAM_SUBIN_IMAGE = "icon_stats_sub-green01.png";
        var HOME_TEAM_SUBOUT_IMAGE = "icon_stats_sub-red02.png";
        var AWAY_TEAM_SUBIN_IMAGE = "icon_stats_sub-green02.png";
        var AWAY_TEAM_SUBOUT_IMAGE = "icon_stats_sub-red01.png";

        var result = "";

        if (type == "YC") {
            result = YELLOW_CARD_IMAGE;
        } else if (type == "RC") {
            result = RED_CARD_IMAGE;
        } else if ((type == "G") || (type == "OG")) {
            result = GOALIN_IMAGE;
        } else if (type == "SI") {
            if (bHomeEvent) result = HOME_TEAM_SUBIN_IMAGE;
            else result = AWAY_TEAM_SUBIN_IMAGE;
        } else if (type == "SO") {
            if (bHomeEvent) result = HOME_TEAM_SUBOUT_IMAGE;
            else result = AWAY_TEAM_SUBOUT_IMAGE;
        }else
        	return "";

        result = "asset:///images/" + result;
        return result;
    }
}
