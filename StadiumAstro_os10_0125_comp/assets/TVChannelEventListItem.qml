import bb.cascades 1.0

Container {
    id: itemRoot
    preferredWidth: 768  
    preferredHeight: 144

    background: (itemRoot.ListItem.indexPath[0] % 2) ? Color.create("#ffeeeee0") : Color.create("#fffffff0")

    Container {
         

        layout: DockLayout {
        }

		topPadding: 10
		bottomPadding: 10
        leftPadding: 10
        rightPadding: 10
        
        preferredHeight: 144

        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Center
        
        // event name
        Container{
            maxHeight: 80.0
            horizontalAlignment: HorizontalAlignment.Fill
            
            Label {
	            id: lblEventName
	            text: ListItemData.eventName
	            textStyle.fontSize: FontSize.XSmall
                textStyle.textAlign: TextAlign.Left
	            textStyle.color: Color.Black
	            overlapTouchPolicy: OverlapTouchPolicy.Allow
	            multiline: true
	            
	        }
        }
        // event date & time
        Label {
            id: lblEventDate
            text: ListItemData.strEventDate
            textStyle.fontSize: FontSize.XSmall
            textStyle.textAlign: TextAlign.Left
            textStyle.color: Color.Black
            verticalAlignment: VerticalAlignment.Bottom
            overlapTouchPolicy: OverlapTouchPolicy.Allow
            textStyle.fontWeight: FontWeight.Bold

        }

        // event period
        Label {
            id: lblEventPeriod
            text: ListItemData.startTime + "  -  " + ListItemData.endTime
            textStyle.fontSize: FontSize.XSmall
            textStyle.textAlign: TextAlign.Right
            textStyle.color: Color.create("#ff222222")
            verticalAlignment: VerticalAlignment.Bottom
            horizontalAlignment: HorizontalAlignment.Right
            overlapTouchPolicy: OverlapTouchPolicy.Allow
        }
    }
    
}
