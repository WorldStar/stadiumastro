import bb.cascades 1.0

Page {
    id: framePage
    property real screenWidth: 768
    property real screenHeight: 1280
    property real menuPaneHeight: 125
}
