import bb.cascades 1.0

Container {
    id: itemRoot

    preferredWidth: 726
    //preferredHeight: 114

    // determine background image
    background: (itemRoot.ListItem.indexInSection == 0) ? topItemBackground.imagePaint : ((itemRoot.ListItem.indexInSection == itemRoot.ListItem.sectionSize - 1) ? bottomItemBackground.imagePaint : midItemBackground.imagePaint)

    layout: DockLayout {
    }
    horizontalAlignment: HorizontalAlignment.Center

    leftPadding: 50
    rightPadding: 50
    topPadding: 26
    bottomPadding: 26

    // category title
    Label {
        horizontalAlignment: HorizontalAlignment.Left
        verticalAlignment: VerticalAlignment.Center
        multiline: true
        text: ListItemData.categoryName
        textStyle {
            textAlign: TextAlign.Left
            fontSize: FontSize.PointValue
            fontSizeValue: 8
            color: Color.White
            fontWeight: FontWeight.Default
        }
    }

    // right arrow image
    ImageView {
        imageSource: "asset:///images/icon_arrow_right.png"
        verticalAlignment: VerticalAlignment.Center
        horizontalAlignment: HorizontalAlignment.Right
    }

    // for set background image
    attachedObjects: [
        ImagePaintDefinition {
            id: topItemBackground
            imageSource: "asset:///images/bar_listing_top.png"
            repeatPattern: RepeatPattern.Fill
        },

        ImagePaintDefinition {
            id: midItemBackground
            imageSource: "asset:///images/bar_listing_mid.png"
            repeatPattern: RepeatPattern.Fill
        },

        ImagePaintDefinition {
            id: bottomItemBackground
            imageSource: "asset:///images/bar_listing_btm.png"
            repeatPattern: RepeatPattern.Fill
        }
    ]
}