import bb.cascades 1.0
import my.library 1.0

// view model of highlight video & news info
Container {
    id: itemRoot

    background: Color.DarkGray
    
    Container {
        leftPadding: 20.0
        rightPadding: 20.0

        background: (itemRoot.ListItem.indexPath[0] % 2) ? Color.create("#ff8b8b83") : Color.Black

        preferredWidth: 768

        Container {
            layout: DockLayout {
            }

            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill

            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }

                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Left

                // must be replaced with WebImageView!!!
                Container {
                    layout: DockLayout {
                    }
                    topPadding: 20.0
                    bottomPadding: 20.0

                    WebImageViewContainer {
                        url: ListItemData.image
                        preferredWidth: 190
                        minWidth: 190.0
                        maxWidth: 190.0
                        preferredHeight: 102.0
                    }
                }

                Container {
                    layout: DockLayout {
                    }

                    leftMargin: 20.0
                    verticalAlignment: VerticalAlignment.Fill
                    topPadding: 10.0
                    bottomPadding: 10.0

                    // subject title
                    Label {
                        text: ListItemData.title
                        multiline: true

                        textStyle {
                            textAlign: TextAlign.Left
                            fontSize: FontSize.PointValue
                            fontSizeValue: 7
                            color: Color.White
                            fontWeight: FontWeight.Bold
                        }
                        verticalAlignment: VerticalAlignment.Top
                        horizontalAlignment: HorizontalAlignment.Left

                    }

                    // time
                    Label {
                        text: ListItemData.time
                        verticalAlignment: VerticalAlignment.Bottom
                        horizontalAlignment: HorizontalAlignment.Left
                        textStyle {
                            textAlign: TextAlign.Left
                            fontSize: FontSize.PointValue
                            fontSizeValue: 6
                            color: Color.White
                        }
                    }
                }
            }
        }
    }
}