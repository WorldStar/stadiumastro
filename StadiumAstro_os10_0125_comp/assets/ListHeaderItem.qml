import bb.cascades 1.0

Container {
    horizontalAlignment: HorizontalAlignment.Fill
    topPadding: 5.0
    leftPadding: 20.0
    bottomPadding: 5.0

    background: backgroundPaint.imagePaint

    Label {
        text: ListItemData.title

        textStyle {
            textAlign: TextAlign.Left
            fontSize: FontSize.PointValue
            fontSizeValue: 6
            color: Color.White
            fontWeight: FontWeight.Bold
        }
    }

    attachedObjects: [
        ImagePaintDefinition {
            id: backgroundPaint
            imageSource: "asset:///images/bar_main_section_gray.png"
            repeatPattern: RepeatPattern.Fill
        }
    ]
}