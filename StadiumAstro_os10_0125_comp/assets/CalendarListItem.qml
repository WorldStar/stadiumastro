import bb.cascades 1.0

// calendar item for matches 
Container {
    id: calendarItem
    
    property string backgroundImage: (ListItemData.isToday ? "asset:///images/btn_cal_date03.png" : "asset:///images/btn_cal_date01.png")
    property bool bActive: ListItemData.strId ? true : false
    property variant dayColor: bActive ? Color.Black : Color.DarkGray

    preferredWidth: 131
    preferredHeight: 158
    
    layout: DockLayout {
        
    }
    
    enabled: calendarItem.bActive

    minWidth: 131.0
    minHeight: 158.0
    maxWidth: 131.0
    maxHeight: 158.0
    
    ImageView {
        id:backImg
        imageSource: calendarItem.backgroundImage

    }
    Container {
        topPadding: 3.0
        bottomPadding: 30.0

        verticalAlignment: VerticalAlignment.Fill
        horizontalAlignment: HorizontalAlignment.Fill

        overlapTouchPolicy: OverlapTouchPolicy.Allow

        Label {
            id: lblMonth
            text: ListItemData.displayMonth
            textStyle.color: Color.White
            textStyle.textAlign: TextAlign.Center
            horizontalAlignment: HorizontalAlignment.Center
            overlapTouchPolicy: OverlapTouchPolicy.Allow
        }

        Label {
            id: lblDay
            text: ListItemData.displayDay
            textStyle.color: calendarItem.dayColor
            textStyle.textAlign: TextAlign.Center
            textStyle.fontSize: FontSize.PointValue
            textStyle.fontSizeValue: 12.0
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Bottom
            textStyle.fontWeight: FontWeight.Bold
            overlapTouchPolicy: OverlapTouchPolicy.Allow
        }
    }
    
    Container {
        verticalAlignment: VerticalAlignment.Bottom
        horizontalAlignment: HorizontalAlignment.Fill
        
        bottomPadding: 8.0

        overlapTouchPolicy: OverlapTouchPolicy.Allow
        
        ImageView {
            id: imgDot
            verticalAlignment: VerticalAlignment.Bottom
            horizontalAlignment: HorizontalAlignment.Center
            imageSource:  ListItemData.sinceToday? "asset:///images/dot_calendar01.png": "asset:///images/dot_calendar03.png" 
//            visible: calendarItem.bActive
            overlapTouchPolicy: OverlapTouchPolicy.Allow            
        } 
    }    

	// background image define 
   
    // Highlight function for the highlight Container
    function setHighlight(highlighted) {
        if (highlighted) {
            
            backImg.imageSource = "asset:///images/btn_cal_date02.png";
//            imgDot.visible = false;
            lblDay.textStyle.color = Color.White;
        } else {
            backImg.imageSource = calendarItem.backgroundImage;
//            imgDot.visible = calendarItem.bActive;
            lblDay.textStyle.color = calendarItem.dayColor;
        }
    }

    // Signal handler for ListItem selection
    ListItem.onSelectionChanged: {
        setHighlight(ListItem.selected);
    }
}
