import bb.cascades 1.0
import my.library 1.0
// Definition for each match preview item at main page
Container {
    layout: StackLayout {
        orientation: LayoutOrientation.LeftToRight
    }
    
    background: backImage.imagePaint

    preferredHeight: 141

    attachedObjects: [
        ImagePaintDefinition {
            id: backImage
            imageSource: "asset:///images/bg_match_tab.png"
        }
    ]
    // display date / time info
    Container {
        layout: DockLayout {
        }

        verticalAlignment: VerticalAlignment.Fill
        preferredWidth: 141

        // month
        Label {
            verticalAlignment: VerticalAlignment.Top
            horizontalAlignment: HorizontalAlignment.Center
            text: ListItemData.month
            textStyle {
                textAlign: TextAlign.Center
                fontSize: FontSize.PointValue
                fontSizeValue: 5
                color: Color.White
            }
        }

        // date
        Label {
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
            text: ListItemData.date
            textStyle {
                textAlign: TextAlign.Center
                fontSize: FontSize.PointValue
                fontSizeValue: 10
                color: Color.White
            }
        }

        // time
        Label {
            verticalAlignment: VerticalAlignment.Bottom
            horizontalAlignment: HorizontalAlignment.Center
            text: ListItemData.time
            textStyle {
                textAlign: TextAlign.Center
                fontSize: FontSize.PointValue
                fontSizeValue: 5
                color: Color.White
            }
        }
    }

    // display match info
    Container {
        layout: DockLayout {
        }
        
        leftPadding: 30.0
        rightPadding: 30.0
        topPadding: 15.0
        bottomPadding: 15.0

        verticalAlignment: VerticalAlignment.Fill
        preferredWidth: 370

        // home team logo image
        // must be replaced with WebImageView!!!
        Container
        {
            layout: StackLayout {

            }
            WebImageView {
                url: ListItemData.homeTeamLogo
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                preferredWidth: 50.0
                preferredHeight: 50.0
                maxWidth: 50.0
                maxHeight: 50.0
                minWidth: 50.0
                minHeight: 50.0
            }

            Container {
                preferredHeight: 60.0
                // home team alias
	            Label {
	                text: ListItemData.homeTeamAlias
	                horizontalAlignment: HorizontalAlignment.Center
	                verticalAlignment: VerticalAlignment.Top
	
	                preferredHeight: 50.0
	                textStyle {
	                    fontSize: FontSize.PointValue
	                    fontSizeValue: 6
	                    color: Color.White
	                    fontWeight: FontWeight.Bold
	                    textAlign: TextAlign.Center
	                }
	            }
            }

        }
        
        // match result
        Label {
            text: ListItemData.matchStatus == "Fixture" ? "vs" : ListItemData.homeTeamScore + " - " + ListItemData.visitingTeamScore
            verticalAlignment: VerticalAlignment.Top
            horizontalAlignment: HorizontalAlignment.Center

            textStyle {
                textAlign: TextAlign.Center
                fontSize: FontSize.PointValue
                fontSizeValue: 12
                color: Color.White
                fontWeight: FontWeight.Bold
            }
        }

        // additive info
        Label {
            text: ListItemData.channelInfo ? ListItemData.channelInfo : ""
            verticalAlignment: VerticalAlignment.Bottom
            horizontalAlignment: HorizontalAlignment.Center

            textStyle {
                textAlign: TextAlign.Center
                fontSize: FontSize.PointValue
                fontSizeValue: 4
                color: Color.White
            }
        }

        // away team logo image
        // must be replaced with WebImageView!!!
        Container
        {
            horizontalAlignment: HorizontalAlignment.Right
            layout: StackLayout {

            }
            WebImageView {
                url: ListItemData.visitingTeamLogo
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Top
                preferredWidth: 50.0
                preferredHeight: 50.0
                maxWidth: 50.0
                maxHeight: 50.0
                minWidth: 50.0
                minHeight: 50.0
            }

            // home team alias
            Container
            {
                preferredHeight: 60.0
                
                Label {
	                text: ListItemData.visitingTeamAlias
	                horizontalAlignment: HorizontalAlignment.Center
	                verticalAlignment: VerticalAlignment.Center

                    textStyle.fontStyle: FontStyle.Default
	                textStyle {
	                    textAlign: TextAlign.Center
	                    fontSize: FontSize.PointValue
	                    fontSizeValue: 6.0
	                    color: Color.White
	                    fontWeight: FontWeight.Bold
	                }
	            }
            }
        }     
    }
}