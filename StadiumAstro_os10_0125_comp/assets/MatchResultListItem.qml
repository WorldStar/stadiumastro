import bb.cascades 1.0
import my.library 1.0
// define a list item of matches for specific date
Container {
    layout: DockLayout{}
    horizontalAlignment: HorizontalAlignment.Fill
    
    preferredHeight: 197.0
    
    //Passion
    
    ImageView {
        imageSource: "asset:///images/bg_match_box.png"
        horizontalAlignment: HorizontalAlignment.Fill        
    }    
    
    
    Container {
        preferredHeight: 197.0

        layout: DockLayout {}

        horizontalAlignment: HorizontalAlignment.Fill

        //Home team mark and alias and score
        Container {
            leftPadding: 40
            
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Left
            
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            
            Container {
                //
                
                layout: StackLayout {
                    orientation: LayoutOrientation.TopToBottom

                }

                verticalAlignment: VerticalAlignment.Center
                
                // home team logo image
                // must be replaced with WebImageView!!!
                WebImageView {
                    id: imgHomeTeamLogo
                    url: ListItemData.homeTeamLogo
                    horizontalAlignment: HorizontalAlignment.Center
                    preferredWidth: 100.0
                    preferredHeight: 100.0
                }

                // home team alias
                Label {
                    id: lblHomeTeamAlias
                    horizontalAlignment: HorizontalAlignment.Center
                    text: ListItemData.homeTeamAlias
                    textStyle.fontSize: FontSize.PointValue
                    textStyle.fontSizeValue: 6.0
                    textStyle.color: Color.Black
                    multiline: false
                    textStyle.fontStyle: FontStyle.Default
                    textStyle.fontWeight: FontWeight.Bold

                }
            }
            
            Container {
                verticalAlignment: VerticalAlignment.Center

                leftPadding: 60.0
                // home team score
                Label {
                    id: lblHomeTeamScore
                    text: ListItemData.matchStatus == "Played" ? ListItemData.homeTeamScore : ""
                    textStyle.color: Color.Black
                    horizontalAlignment: HorizontalAlignment.Left
                    textStyle.fontSize: FontSize.PointValue
                    textStyle.textAlign: TextAlign.Left
                    verticalAlignment: VerticalAlignment.Top
                    textStyle.fontSizeValue: 20.0
                }
            }

        }
        
        //Away team mark and alias and score
        Container {
            rightPadding: 40

            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Right

            layout: StackLayout {
                orientation: LayoutOrientation.RightToLeft
            }

            Container {
                //

                layout: StackLayout {
                    orientation: LayoutOrientation.TopToBottom

                }

                verticalAlignment: VerticalAlignment.Center

                // away team logo image
                // must be replaced with WebImageView!!!
                WebImageView {
                    id: imgVisitingTeamLogo
                    url: ListItemData.visitingTeamLogo
                    horizontalAlignment: HorizontalAlignment.Center
                    preferredWidth: 100.0
                    preferredHeight: 100.0
                }

                // away team alias
                Label {
                    id: lblVisitingTeamAlias
                    text: ListItemData.visitingTeamAlias
                    textStyle.fontSize: FontSize.PointValue
                    textStyle.fontSizeValue: 6.0
                    horizontalAlignment: HorizontalAlignment.Center
                    textStyle.color: Color.Black
                    multiline: false
                    textStyle.fontWeight: FontWeight.Bold
                }
            }
            
            Container {
                verticalAlignment: VerticalAlignment.Center
                rightPadding: 60.0
                // away team score
                Label {
                    
                    id: lblVisitingTeamScore
                    text: ListItemData.matchStatus == "Played" ? ListItemData.visitingTeamScore : ""
                    textStyle.color: Color.Black
                    textStyle.fontSize: FontSize.PointValue
                    textStyle.textAlign: TextAlign.Right
                    verticalAlignment: VerticalAlignment.Top
                    textStyle.fontSizeValue: 20.0
                }
            }
        }
        
        
        //Center Information
        Container {
            topPadding: 20
            bottomPadding: 20
            
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Center
            
            layout: DockLayout {
                
            }

			//VS and -
            Label {
                text: ListItemData.matchStatus == "Played" ? "-" : "vs"
                textStyle.color: Color.Black
                horizontalAlignment: HorizontalAlignment.Center
                textStyle.fontSize: FontSize.PointValue
                textStyle.textAlign: TextAlign.Center
                verticalAlignment: VerticalAlignment.Center
                textStyle.fontSizeValue: 12.0
                textStyle.fontWeight: FontWeight.Bold
            }

            
            
            //Stadium name
            Label {
                id: lblStadiumName
                //text: ListItemData.stadiumCity + " " + ListItemData.stadiumName
                text: ListItemData.channelInfo
                horizontalAlignment: HorizontalAlignment.Center
                textStyle.fontSize: FontSize.XSmall
                textStyle.textAlign: TextAlign.Center
                textStyle.color: Color.create("#ff505050")
                verticalAlignment: VerticalAlignment.Bottom
            }

			Container {
				layout: StackLayout {
                    orientation: LayoutOrientation.TopToBottom
                }

                verticalAlignment: VerticalAlignment.Top
                
                // match status display
                Label {
                    id: lblMatchStaus
                    text: ListItemData.matchStatus == "Played" ? "Full Time" : ListItemData.matchTime
                    horizontalAlignment: HorizontalAlignment.Center
                    textStyle.fontSize: FontSize.XSmall
                    textStyle.textAlign: TextAlign.Center
                    textStyle.color: Color.create("#ff505050")
                    
                }
                
//                ImageView {
//                    id: imgLiveStatus
//                    imageSource: "asset:///images/icon_live_match.png"
//                    visible: ListItemData.channelInfo
//                    horizontalAlignment: HorizontalAlignment.Center
//                }
            }
            

        }

		Container {
            // right arrow image

            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Right
            rightPadding: 10.0
            
            ImageView {
                imageSource: "asset:///images/icon_arrow_right.png"
                
            }
        }
        
    }
}

