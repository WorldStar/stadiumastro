import bb.cascades 1.0
import my.library 1.0
Page {
   id: viewPage
    property string pageTitle
    property real screenWidth: 768
    property real screenHeight: 1280
    property real menuPaneHeight: 125
    property string identifier
    property string pageType
    
    //! [0]
    Container {
        background: Color.Black
        layout: DockLayout {

        }

        verticalAlignment: VerticalAlignment.Fill
        horizontalAlignment: HorizontalAlignment.Fill

        //! [1] title and contents
        PageContentScrollView {
            id: mainContents

            Container {
                horizontalAlignment: HorizontalAlignment.Fill

                layout: StackLayout {
                    orientation: LayoutOrientation.TopToBottom
                }

                preferredHeight: screenHeight;

                //! [2] title
                Container {
                    horizontalAlignment: HorizontalAlignment.Fill
                    verticalAlignment: VerticalAlignment.Top

                    layout: DockLayout {
                    }

                    ImageView {
                        imageSource: "asset:///images/bar_top_blue.png"
                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Fill
                        minHeight: 100.0
                    }

                    Label {
                        id: lblTitle
                        text: pageTitle
                        objectName: "pageTitle"

                        textStyle {
                            fontSizeValue: 10.0
                            textAlign: TextAlign.Center
                            color: Color.White
                            fontSize: FontSize.PointValue
                            fontWeight: FontWeight.Bold
                        }
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                    }

					Container {
                        horizontalAlignment: HorizontalAlignment.Right
                        verticalAlignment: VerticalAlignment.Center
                        
                        layout: DockLayout {
                            
                        }
                        
                        rightPadding: 20
                        
                        //Refresh Button
                        ImageButton {
                            
                            objectName: "pageRefresh"
                            defaultImageSource: "asset:///images/icon_refresh.png"
                            onClicked: {
                                eventHandler.refreshCurPage(lblTitle.text, identifier, pageType);
                            }
                            
                            visible: false
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Center
                        }

                        // help button(must be replaced!!!)
                        ImageButton {
                            
                            objectName: "showHelp"
                            defaultImageSource: "asset:///images/icon_help.png"
                            onClicked: {
                                eventHandler.showHelpPage();
                            }

                            visible: false
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Center
                        }
                    }

                } //! [2]

                //! [3] admob area
                Container {
                    id: admob
                    objectName: "admob"
                    layout: DockLayout {

                    }
                    horizontalAlignment: HorizontalAlignment.Fill

                    verticalAlignment: VerticalAlignment.Top
                    // admob image(must be replaced!!!)
                    WebImageView {
                        id: imgAdmob
                        objectName: "imgAdmob"
                        imageSource: "asset:///images/ads_bar_black.png"
                        horizontalAlignment: HorizontalAlignment.Fill
                        preferredHeight: 100.0
                        onTouch: {
                            if (event.isUp) {
                                eventHandler.onTouchAdmob();
                            }
                        }
                    }

                    // admob close button image
                    ImageView {
                        imageSource: "asset:///images/icon_ads_close.png"
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Right
                        scalingMethod: ScalingMethod.None
                        rightMargin: 0.0
                        onTouch: {
                            // hide admob area
                            if (event.isUp) {
                                admob.visible = false;
                                eventHandler.onHideAdmob();
                            }

                        }
                    }
                } //! [3]

                //! [3] contents
                Container {
                    id: contentContainer
                    objectName: "contentContainer"

                    bottomPadding: menuPaneHeight

                    layout: DockLayout {
                    }
                    horizontalAlignment: HorizontalAlignment.Fill

                    //Intro view
                    Container {
                        id: introView
                        objectName: "introView"
                        topPadding: 20.0
                        ImageView {
                            imageSource: "asset:///images/intro_title.png"
                            bottomMargin: 20.0
                            scalingMethod: ScalingMethod.AspectFit
                            horizontalAlignment: HorizontalAlignment.Fill
                            preferredHeight: 50.0
                            verticalAlignment: VerticalAlignment.Center
                            objectName: "introImage"
                        }
                        ScrollView {
                            objectName: "introScroller"
                            scrollViewProperties.scrollMode: ScrollMode.Both
                            scrollViewProperties.initialScalingMethod: ScalingMethod.AspectFill

                            scrollViewProperties.overScrollEffectMode: OverScrollEffectMode.OnPinchAndScroll
                            scrollViewProperties.maxContentScale: 2.0
                            scrollViewProperties.pinchToZoomEnabled: true
                            scrollViewProperties.minContentScale: 1.0
                            WebView {
                                objectName: "introContent"
                                settings.background: Color.Black
                                focusPolicy: FocusPolicy.Default
                                horizontalAlignment: HorizontalAlignment.Fill
                                settings.defaultFontSize: 36
                            }
                        }
                    }

                    // Other apps by Astro
                    ListView {
                        id: otherApps
                        objectName: "otherApps"

                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Fill

                        scrollIndicatorMode: ScrollIndicatorMode.None

                        layout: StackListLayout {
                            orientation: LayoutOrientation.TopToBottom
                            headerMode: ListHeaderMode.None
                        }

                        topPadding: 20.0
                        leftPadding: 20.0
                        rightPadding: 20.0

                        listItemComponents: [
                            ListItemComponent {
                                type: "item"

                                OtherAppCategoryItem {

                                }
                            }
                        ]

                        onTriggered: {
                            // enter into video feed list screen
                            var appData = otherApps.dataModel.data(indexPath);
                            eventHandler.triggerAppItem(appData.appBBURL);
                        }
                    }

                    //Push notification setting
                    Container {
                        objectName: "PushSetting"
                        background: bgPushNotification.imagePaint

                        visible: false

                        verticalAlignment: VerticalAlignment.Fill
                        horizontalAlignment: HorizontalAlignment.Fill
                        ScrollView {
                            scrollViewProperties.scrollMode: ScrollMode.Vertical
                            horizontalAlignment: HorizontalAlignment.Fill
                            Container {
                                objectName : "PushList"
                                horizontalAlignment: HorizontalAlignment.Fill
                                topPadding: 40.0
                                bottomPadding: 100.0
                                leftPadding: 50.0
                                rightPadding: 50.0
                            }

                        }
                        attachedObjects: [
                            ImagePaintDefinition {
                                id: bgPushNotification
                                imageSource: "asset:///images/bg_pushnotification.png"
                            }
                        ]

                    }
                    // -- push notification setting view

                    // Video Category list
                    ListView {
                        id: videoCategoryList
                        objectName: "videoCategoryList"

                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Fill

                        scrollIndicatorMode: ScrollIndicatorMode.None

                        layout: StackListLayout {
                            orientation: LayoutOrientation.TopToBottom
                            headerMode: ListHeaderMode.None
                        }

                        topPadding: 50.0
                        leftPadding: 20.0
                        rightPadding: 20.0

                        listItemComponents: [
                            ListItemComponent {
                                type: "item"

                                VideoAndNewsCategoryItem {

                                }
                            }
                        ]

                        onTriggered: {
                            // enter into video feed list screen
                            var categoryData = videoCategoryList.dataModel.data(indexPath);
                            eventHandler.ShowVideoFeedListPage(categoryData.refKey);
                        }
                    }

                    // Video Feed List
                    ListView {
                        id: videoFeedList
                        objectName: "videoFeedList"

                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Fill

                        scrollIndicatorMode: ScrollIndicatorMode.None

                        layout: StackListLayout {
                            orientation: LayoutOrientation.TopToBottom
                            headerMode: ListHeaderMode.None
                        }

                        listItemComponents: [
                            ListItemComponent {
                                type: "item"

                                VideoFeedListItem {

                                }
                            }
                        ]

                        onTriggered: {
                            // play selected video file
                            var feedData = videoFeedList.dataModel.data(indexPath);
                            eventHandler.ShowNewsViewPage(feedData);
                        }
                    }

                    // News Category list
                    ListView {
                        id: newsCategoryList
                        objectName: "newsCategoryList"

                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Fill

                        layout: StackListLayout {
                            orientation: LayoutOrientation.TopToBottom
                            headerMode: ListHeaderMode.None
                        }

                        topPadding: 50.0
                        leftPadding: 20.0

                        scrollIndicatorMode: ScrollIndicatorMode.None

                        listItemComponents: [
                            ListItemComponent {
                                type: "item"

                                VideoAndNewsCategoryItem {

                                }
                            }
                        ]

                        onTriggered: {
                            // enter into news feed list screen
                            var categoryData = newsCategoryList.dataModel.data(indexPath);
                            eventHandler.ShowNewsFeedListPage(categoryData.refKey, categoryData.categoryName);
                        }
                    }

                    // News Feed List
                    ListView {
                        id: newsFeedList
                        objectName: "newsFeedList"

                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Fill

                        scrollIndicatorMode: ScrollIndicatorMode.None

                        layout: StackListLayout {
                            orientation: LayoutOrientation.TopToBottom
                            headerMode: ListHeaderMode.None
                        }

                        listItemComponents: [
                            ListItemComponent {
                                type: "item"

                                NewsFeedListItem {

                                }
                            }
                        ]

                        onTriggered: {
                            // show news detail info
                            var feedData = newsFeedList.dataModel.data(indexPath);
                            eventHandler.ShowNewsViewPage(feedData);
                        }
                    }

                    // Matches
                    Container {
                        id: matchesContent
                        objectName: "matchesContent"
                        
                        property int nSelectItemIndex: 0
                        property alias dateStr: lblDateStr.text
                        
                        background: Color.Black

                        layout: StackLayout {
                            orientation: LayoutOrientation.TopToBottom
                        }

                        Container {
                            background: Color.DarkGray

                            preferredHeight: 158


                            // calendar list
                            ListView {
                                id: calendarList
                                objectName: "calendarList"

                                scrollIndicatorMode: ScrollIndicatorMode.None

                                layout: StackListLayout {
                                    orientation: LayoutOrientation.LeftToRight
                                    headerMode: ListHeaderMode.None
                                }

                                listItemComponents: [
                                    ListItemComponent {
                                        type: "item"

                                        CalendarListItem {

                                        }
                                    }
                                ]

                                onTriggered: {
                                    var calendarData = calendarList.dataModel.data(indexPath);
                                    if (calendarData.strId) {
                                        // set selection
                                        clearSelection();
                                        select(indexPath);

                                        // show info for selected day
                                        //lblDateStr.text = calendarData.date.toDateString();
                                        eventHandler.getMatchResultListData(calendarData.strId);
                                    }
                                }
                                
//                                function onInitializeComplete() {
//                                    calendarList.scrollToPosition(ScrollPosition.End, ScrollAnimation.None);
//                                    var indexPath = [ calendarList.dataModel.size() - 1 ];
//                                    var calendarData = calendarList.dataModel.data(indexPath);
//
//                                    if (calendarData.strId) {
//                                        // set selection
//                                        clearSelection();
//                                        select(indexPath);
//
//                                        // show info for selected day
//                                        lblDateStr.text = calendarData.date.toDateString();
//                                        eventHandler.getMatchResultListData(calendarData.strId);
//                                    }
//                                }

                                function onInitializeComplete() {
                                    //calendarList.scrollToPosition(ScrollPosition.Beginning, ScrollAnimation.None);
                                    var indexPath = [ matchesContent.nSelectItemIndex ];
                                    calendarList.scrollToItem(indexPath, ScrollAnimation.None);
                                    var calendarData = calendarList.dataModel.data(indexPath);

                                    if (calendarData.strId) {
                                        // set selection
                                        clearSelection();
                                        select(indexPath);

                                        // show info for selected day
                                        //lblDateStr.text = calendarData.date.toDateString();
                                        eventHandler.getMatchResultListData(calendarData.strId);
                                    }
                                }

                            }
                        }

                        // match date string
                        Container {
                            //topPadding: 10
                            bottomPadding: 6

                            horizontalAlignment: HorizontalAlignment.Fill

                            background: Color.create("#ff555555")
                            //                            attachedObjects: [
//                                ImagePaintDefinition {
//                                    id: dateDisplayBackground
//                                    imageSource: "asset:///images/bar_main_section.png"
//                                }
//                            ]
                            Label {
                                id: lblDateStr
                                
                                horizontalAlignment: HorizontalAlignment.Center
                                textStyle.color: Color.White
                                textStyle.fontSize: FontSize.Small
                            }
                        }

                        // match result list for speicific date
                        ListView {
                            id: matchResultList
                            objectName: "matchResultList"

//                            leftPadding: 20
//                            rightPadding: 20
//                            topPadding: 30

                            horizontalAlignment: HorizontalAlignment.Fill

                            scrollIndicatorMode: ScrollIndicatorMode.None

                            layout: StackListLayout {
                                orientation: LayoutOrientation.TopToBottom
                                headerMode: ListHeaderMode.None
                            }

                            listItemComponents: [
                                ListItemComponent {
                                    type: "item"

                                    MatchResultListItem {

                                    }
                                }
                            ]

                            onTriggered: {
                                // replace to analysis screen
                                var matchResultData = matchResultList.dataModel.data(indexPath);
                                eventHandler.ShowAnalysisPage(matchResultData);
                            }
                        }
                    }

                    // Analysis page content
                    MatchAnalysis {
                        id: analysisContent
                        objectName: "analysisContent"

                    }

                    // standings page content
                    StandingsContent {
                        id: standingsContent
                        objectName: "standingsContent"
                    }

                    // more page content
                    MorePage {
                        id: moreContent
                        objectName: "moreContent"
                    }

                    // TV Channels category content
                    ListView {
                        id: channelList
                        objectName: "channelList"

                        horizontalAlignment: HorizontalAlignment.Center

                        layout: StackListLayout {
                            orientation: LayoutOrientation.TopToBottom
                            headerMode: ListHeaderMode.None
                        }

                        topPadding: 50.0
                        leftPadding: 20.0

                        scrollIndicatorMode: ScrollIndicatorMode.None

                        listItemComponents: [
                            ListItemComponent {
                                type: "item"

                                TVChannelListItem {

                                }
                            }
                        ]

                        onTriggered: {
                            // enter into news feed list screen
                            var channelListData = channelList.dataModel.data(indexPath);
                            eventHandler.ShowChannelEventPage(channelListData);
                        }
                    }

                    // TV channel Events content
                    Container {
                        id: tvChannelEventContent
                        objectName: "tvChannelEventContent"

                        background: Color.Black

                        horizontalAlignment: HorizontalAlignment.Fill

                        Container {
                            topPadding: 14
                            bottomPadding: 14
                            preferredHeight: 70

                            background: Color.create("#ff646464")

                            horizontalAlignment: HorizontalAlignment.Fill

                            layout: StackLayout {
                                orientation: LayoutOrientation.TopToBottom
                            }

                            ListView {
                                id: channelCalendarList
                                objectName: "channelCalendarList"

                                leftPadding: 10
                                preferredHeight: 100

                                scrollIndicatorMode: ScrollIndicatorMode.None

                                layout: StackListLayout {
                                    orientation: LayoutOrientation.LeftToRight
                                    headerMode: ListHeaderMode.None
                                }

                                listItemComponents: [
                                    ListItemComponent {
                                        type: "item"

                                        TVChannelCalendarListItem {

                                        }
                                    }
                                ]
                                onDataModelChanged: {
                                    var indexPath = [ 0 ];
                                    var channelCalendarData = channelCalendarList.dataModel.data(indexPath);

                                    clearSelection();
                                    select(indexPath);
                                    eventHandler.getChannelEventListData(channelCalendarData.id);
                                }
                                onTriggered: {
                                    var channelCalendarData = channelCalendarList.dataModel.data(indexPath);

                                    // set selection
                                    clearSelection();
                                    select(indexPath);

                                    eventHandler.getChannelEventListData(channelCalendarData.id);
                                }
                            }
                        }

                        ListView {
                            id: channelEventList
                            objectName: "channelEventList"

                            scrollIndicatorMode: ScrollIndicatorMode.None
                            horizontalAlignment: HorizontalAlignment.Fill

                            layout: StackListLayout {
                                orientation: LayoutOrientation.TopToBottom
                                headerMode: ListHeaderMode.None
                            }

                            listItemComponents: [
                                ListItemComponent {
                                    type: "item"

                                    TVChannelEventListItem {

                                    }
                                }
                            ]
                        }
                    }
                } //! [3]

                onCreationCompleted: {
                    // hide all content views
                    otherApps.visible = false;
                    introView.visible = false;
                    videoCategoryList.visible = false;
                    videoFeedList.visible = false;
                    newsCategoryList.visible = false;
                    newsFeedList.visible = false;
                    matchesContent.visible = false;
                    analysisContent.visible = false;
                    standingsContent.visible = false;
                    moreContent.visible = false;
                    channelList.visible = false;
                    tvChannelEventContent.visible = false;
                }
            }
        } //! [1]

        //! [5] menu bar
        Container {
            id: menuBar

            layout: DockLayout {
            }

            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Bottom

            topPadding: 3.0
            background: Color.DarkGray

            preferredHeight: menuPaneHeight

            Container {
                background: Color.create("#ff222222")

                layout: DockLayout {
                }

                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill

                // static back button(need to modify!!!)
                Container {
                    id: staticBackButton
                    preferredWidth: 120
                    background: Color.DarkGray
                    layout: DockLayout {
                    }

                    verticalAlignment: VerticalAlignment.Fill
                    horizontalAlignment: HorizontalAlignment.Left

                    topPadding: 15.0
                    bottomPadding: 15.0

                    onTouch: {
                        // processing display menu at left-top side
                        if (event.isUp())
                        	eventHandler.popTop();
                    }

                    ImageView {
                        imageSource: "asset:///images/icon_arrow_back.png"
                        verticalAlignment: VerticalAlignment.Top
                        horizontalAlignment: HorizontalAlignment.Center
                        overlapTouchPolicy: OverlapTouchPolicy.Allow
                    }

                    Label {
                        text: "Back"
                        textStyle {
                            color: Color.White
                            fontSize: FontSize.PointValue
                            fontSizeValue: 5
                            textAlign: TextAlign.Center
                        }
                        verticalAlignment: VerticalAlignment.Bottom
                        horizontalAlignment: HorizontalAlignment.Center
                        overlapTouchPolicy: OverlapTouchPolicy.Allow
                    }
                }

                // home button
                ImageView {
                    id: homeBtn
                    imageSource: "asset:///images/icon_red_home.png"
                    horizontalAlignment: HorizontalAlignment.Right
                    verticalAlignment: VerticalAlignment.Center
                    onTouch: {
                        // go to main page
                        if (event.isUp())
                        {
                            homeBtnTouchAnim.play();
                            eventHandler.popToRootPage();
                        }

                    }
                    animations: [
                        SequentialAnimation {
                            id: homeBtnTouchAnim

                            ParallelAnimation {
                                ScaleTransition {
                                    toX: 1.2
                                    toY: 1.2
                                    duration: 100
                                }

                                FadeTransition {
                                    fromOpacity: 1.0
                                    toOpacity: 0.3
                                    duration: 100
                                }
                            }

                            ParallelAnimation {
                                ScaleTransition {
                                    toX: 1.0
                                    toY: 1.0
                                    duration: 300
                                }

                                FadeTransition {
                                    toOpacity: 1.0
                                    duration: 300
                                }
                            }
                        }
                    ]
                }
            }
        } //! [6]
    }
}

