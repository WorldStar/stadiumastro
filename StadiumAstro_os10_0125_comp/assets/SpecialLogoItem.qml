import bb.cascades 1.0
import my.library 1.0
Container
{
    layout: DockLayout {

    }
    
    WebImageViewContainer {
        url: ListItemData.imageLink
        horizontalAlignment: HorizontalAlignment.Left
        verticalAlignment: VerticalAlignment.Top
        preferredWidth: 768
        preferredHeight: 370
    }

    Container
    {
        horizontalAlignment: HorizontalAlignment.Fill
        background: Color.create("#7e000000")

		//By PASSION
		topPadding: 16
		bottomPadding: 16
		leftPadding: 20.0
        rightPadding: 20.0
        verticalAlignment: VerticalAlignment.Bottom
        Label {
            horizontalAlignment: HorizontalAlignment.Fill
            
            preferredHeight: 70.0
            textStyle.color: Color.White
            textStyle.fontWeight: FontWeight.Bold
            text: ListItemData.title
            textStyle.fontSizeValue: 1.0
            textStyle.lineHeight: 1.0
            textStyle.fontSize: FontSize.Small
            textStyle.textAlign: TextAlign.Left
            preferredWidth: 720.0
            maxHeight: 70.0
            minHeight: 70.0
            
            multiline: true
            
        }
    }
    
    
}



