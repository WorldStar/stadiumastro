import bb.cascades 1.0

// confirm layout with manual mode
ScrollView {
    scrollViewProperties.scrollMode: ScrollMode.Vertical
    scrollViewProperties.overScrollEffectMode: OverScrollEffectMode.OnScroll

    scrollViewProperties.pinchToZoomEnabled: false

    //! [0] main menu object
    Container {

        background: Color.Black

        layout: StackLayout {
            orientation: LayoutOrientation.TopToBottom
        }

        layoutProperties: StackLayoutProperties {
            spaceQuota: 1.0
        }

        //! [1] football sub menu
        Container {
            preferredWidth: 520
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }

            background: Color.create("#FF404040")

            leftPadding: 20
            topPadding: 20
            bottomPadding: 20

            ImageView {
                property bool downState: true

                imageSource: "asset:///images/icon_arrow-blue_down.png"
                verticalAlignment: VerticalAlignment.Center
                onTouch: {
                    if (! event.isUp()) return;

                    if (downState) {
                        rotateUpFootball.play();
                    } else {
                        rotateDownFootball.play();
                    }

                    downState = ! downState;
                    footballSubView.visible = downState;
                }
                animations: [
                    RotateTransition {
                        id: rotateUpFootball
                        toAngleZ: -90.0
                        duration: 100
                    },
                    RotateTransition {
                        id: rotateDownFootball
                        toAngleZ: 0.0
                        duration: 100
                    }
                ]
            }

            Label {
                text: "Football"
                verticalAlignment: VerticalAlignment.Center
                textStyle {
                    color: Color.White
                    fontSize: FontSize.PointValue
                    fontSizeValue: 8
                    textAlign: TextAlign.Left
                }
            }
        } //! [1]

        //! [1-1] football's sub menu
        Container {
            property int selected:-1
            
            id: footballSubView
            objectName: "footballSubMenu"
            
            layout: StackLayout {
                orientation: LayoutOrientation.TopToBottom
            }

        }

        //! [2] others sub menu
        Container {
            preferredWidth: 520
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }

            background: Color.create("#FF404040")

            leftPadding: 20
            topPadding: 20
            bottomPadding: 20

            ImageView {
                property bool downState: true

                imageSource: "asset:///images/icon_arrow-blue_down.png"
                verticalAlignment: VerticalAlignment.Center
                onTouch: {
                    if (! event.isUp()) return;

                    if (downState) {
                        rotateUpOthers.play();
                    } else {
                        rotateDownOthers.play();
                    }

                    downState = ! downState;
                    othersSubView.visible = downState;
                }
                animations: [
                    RotateTransition {
                        id: rotateUpOthers
                        toAngleZ: -90.0
                        duration: 100
                    },
                    RotateTransition {
                        id: rotateDownOthers
                        toAngleZ: 0.0
                        duration: 100
                    }
                ]
            }

            Label {
                text: "Others"
                verticalAlignment: VerticalAlignment.Center
                textStyle {
                    color: Color.White
                    fontSize: FontSize.PointValue
                    fontSizeValue: 8
                    textAlign: TextAlign.Left
                }
            }
        } //! [2]

        //! [2-1] others's sub menu
        Container {
            property int selected: -1
            
            id: othersSubView
            objectName: "otherSubMenu"
            
            layout: StackLayout {
                orientation: LayoutOrientation.TopToBottom
            }
 
        }

        //! [3] more sub menu
        Container {
            preferredWidth: 520
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }

            background: Color.create("#FF404040")

            leftPadding: 20
            topPadding: 20
            bottomPadding: 20

            ImageView {
                property bool downState: true

                imageSource: "asset:///images/icon_arrow-blue_down.png"
                verticalAlignment: VerticalAlignment.Center
                onTouch: {
                    if (! event.isUp()) return;

                    if (downState) {
                        rotateUpMore.play();
                    } else {
                        rotateDownMore.play();
                    }

                    downState = ! downState;
                    moreSubView.visible = downState;
                }
                animations: [
                    RotateTransition {
                        id: rotateUpMore
                        toAngleZ: -90.0
                        duration: 100
                    },
                    RotateTransition {
                        id: rotateDownMore
                        toAngleZ: 0.0
                        duration: 100
                    }
                ]
            }

            Label {
                text: "More"
                verticalAlignment: VerticalAlignment.Center
                textStyle {
                    color: Color.White
                    fontSize: FontSize.PointValue
                    fontSizeValue: 8
                    textAlign: TextAlign.Left
                }
            }
        } //! [3]


        //! [3-1] more's sub menu
        Container {
            id: moreSubView
            objectName:"moreSubView"
            
            layout: StackLayout {
                orientation: LayoutOrientation.TopToBottom
            }

            //! [3-1-1] Information
            Container {
                preferredWidth: 520

                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }

                leftPadding: 50
                topPadding: 20
                bottomPadding: 20

                background: Color.Black

                Label {
                    text: "Information"
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Left
                    textStyle {
                        color: Color.White
                        fontSize: FontSize.PointValue
                        fontSizeValue: 7
                        textAlign: TextAlign.Left
                    }
                }
            }
            
            //! [3-1-1-1]
            Container
            {
                objectName: "information"
            }
        

            //! [3-1-2] Contacts
            Container {
                preferredWidth: 520

                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }

                leftPadding: 50
                topPadding: 20
                bottomPadding: 20

                background: Color.Black

                Label {
                    text: "Contacts"
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Left
                    textStyle {
                        color: Color.White
                        fontSize: FontSize.PointValue
                        fontSizeValue: 7
                        textAlign: TextAlign.Left
                    }
                }
            }
            Container
            {
                objectName:"contacts"
            }
            
            //! [3-1-3] Others
            Container {
                preferredWidth: 520

                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }

                leftPadding: 50
                topPadding: 20
                bottomPadding: 20

                background: Color.Black

                Label {
                    text: "Others"
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Left
                    textStyle {
                        color: Color.White
                        fontSize: FontSize.PointValue
                        fontSizeValue: 7
                        textAlign: TextAlign.Left
                    }
                }
            }
            Container
            {
                objectName:"others"
            
            }
        }
    } //! [0]
}
