import bb.cascades 1.0

Container {
    id: itemRoot

    layout: DockLayout {
    }
   
    horizontalAlignment: HorizontalAlignment.Fill
    preferredWidth: 768.0
    
    background: (itemRoot.ListItem.indexPath[0] % 2) ? Color.White : Color.create("#ffe0e0e0") 

    leftPadding: 20.0
    rightPadding: 20.0
    
    preferredHeight: 60.0
    minHeight: 60.0
    maxHeight: 60.0
    
    // home team player's position
    Label {
    	text: ListItemData.homeTeamPlayerPosition
        horizontalAlignment: HorizontalAlignment.Left
        verticalAlignment: VerticalAlignment.Center
        textStyle.color: Color.Black
        textStyle.textAlign: TextAlign.Center
        textStyle.fontSize: FontSize.Small
    }
    
    // away team player's position
    Label {
        text: ListItemData.awayTeamPlayerPosition
        horizontalAlignment: HorizontalAlignment.Right
        verticalAlignment: VerticalAlignment.Center
        textStyle.color: Color.Black
        textStyle.textAlign: TextAlign.Center
        textStyle.fontSize: FontSize.Small
    }
    
    Container {
        leftPadding: 60.0
        rightPadding: 60.0
        layout: DockLayout {

        }

        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Center
        
        // home team player's name
        Label {
            text: ListItemData.homeTeamPlayerName
            horizontalAlignment: HorizontalAlignment.Left
            verticalAlignment: VerticalAlignment.Center
            textStyle.color: Color.Black
            textStyle.textAlign: TextAlign.Left
            textStyle.fontSize: FontSize.Small
        }
        
        // away team player's name
        Label {
            
            text: ListItemData.awayTeamPlayerName + "  " + ListItemData.awayTeamPlayerNo
            horizontalAlignment: HorizontalAlignment.Right
            verticalAlignment: VerticalAlignment.Center
            textStyle.color: Color.Black
            textStyle.textAlign: TextAlign.Right
            textStyle.fontSize: FontSize.Small
            maxWidth: 300.0
        }
    }
}
