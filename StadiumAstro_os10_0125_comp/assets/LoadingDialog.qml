import bb.cascades 1.0
Dialog {
    Container
    {
            background: Color.create("#78000000")
            layout: DockLayout {
            
            }
            
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
            Container {
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                
                preferredHeight: 300
                
                leftPadding: 40
                
                
                layout: StackLayout {
                    orientation: LayoutOrientation.TopToBottom
                }
                
                ActivityIndicator {
                    id: loadingIndicator
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                	preferredWidth: 100.0
                    preferredHeight: 100.0
                    scaleX: 1.0
                    scaleY: 1.0
                
                }
                
                Label {
                    text: "Loading"
                    verticalAlignment: VerticalAlignment.Center
                    
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 50.0
                    }

                	horizontalAlignment: HorizontalAlignment.Center
	                textStyle {
	                        color: Color.White
	                        fontSize: FontSize.PointValue
	                        fontSizeValue: 8
	                        textAlign: TextAlign.Center
                }
	                }
                 }    
    }
    onOpened:{
        loadingIndicator.running = true;
    }
    onClosed:{
        loadingIndicator.running = false;   
    }
}
