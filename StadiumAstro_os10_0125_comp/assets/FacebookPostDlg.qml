import bb.cascades 1.0
import my.library 1.0

Dialog {
    id: facebookDlg
    Container{
        preferredWidth: 768
        preferredHeight: 1280
        verticalAlignment: VerticalAlignment.Fill
        horizontalAlignment: HorizontalAlignment.Fill
        background: Color.White
        layout: DockLayout {

        }
        Container{
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Top
            background: Color.create("#ff405d9a")
            preferredHeight: 100.0

            layout: DockLayout {

            }
            Label{
                horizontalAlignment: HorizontalAlignment.Fill
                textStyle.fontWeight: FontWeight.Bold
                textStyle.color: Color.White
                text: "Facebook"
                textStyle.textAlign: TextAlign.Center
                verticalAlignment: VerticalAlignment.Center
            }
        }

         
                     
        Container
        {
            preferredWidth: 600
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
            TextArea {
                id : publishText
                hintText: "Type your text here"
                preferredHeight: 300.0

            }
            Container
            {
                leftPadding: 30.0
                rightPadding: 30.0

                layout: DockLayout {

                }

                horizontalAlignment: HorizontalAlignment.Fill
                ImageButton {
                    preferredWidth: 108
                    preferredHeight: 48
                    
                    onClicked: {
                        facebookDlg.close();
                    }
                    defaultImageSource: "asset:///images/faceCancel.png"

                }
                ImageButton {
                    preferredWidth: 108.0
                    preferredHeight: 48.0
                    
                    onClicked:{
                        facebook.publishNote("I want to tell my friend!", publishText.text);
                        facebookDlg.close();
                    }
                    horizontalAlignment: HorizontalAlignment.Right
                    defaultImageSource: "asset:///images/facepost.png"
                }
            }
        }
    }
}
