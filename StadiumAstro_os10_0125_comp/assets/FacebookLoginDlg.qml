import bb.cascades 1.0

Dialog {
    id : facebookDlg
    
    property alias url : facebookView.url
    Container {
        layout: DockLayout {

        }
        verticalAlignment: VerticalAlignment.Fill
        horizontalAlignment: HorizontalAlignment.Fill
        WebView {
            id:facebookView
            objectName: "facebookView"
            
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
            settings.zoomToFitEnabled: false
            settings.background: Color.White
        }
        ImageButton{
            horizontalAlignment: HorizontalAlignment.Right
            defaultImageSource: "asset:///images/icon_ads_close.png"
            onClicked:{
                facebookDlg.close();
            }
        }

    }
    onCreationCompleted:{
        facebookView.storage.clear();
    }
}
