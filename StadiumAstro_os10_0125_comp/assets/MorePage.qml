import bb.cascades 1.0

// define content displaying when clicks 'More' button
Container {
    property alias channelEnabled : channelContainer.enabled
    background: Color.Black
    preferredWidth: 768
    
    leftPadding: 20
    rightPadding: 20
    topPadding: 30
    horizontalAlignment: HorizontalAlignment.Fill
    
    layout: StackLayout{
        orientation: LayoutOrientation.TopToBottom
    }
    
    // Channel item
    Container {
        preferredHeight: 84

        // determine background image
        background: topItemBackground.imagePaint

        layout: DockLayout {
        }

        horizontalAlignment: HorizontalAlignment.Fill

        rightPadding: 50
        topPadding: 24
        bottomPadding: 24

        Container {
            leftPadding: 50
            id: channelContainer
            
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }

            overlapTouchPolicy: OverlapTouchPolicy.Deny

            enabled: true
            
            Container {
                verticalAlignment: VerticalAlignment.Center
                rightMargin: 30

                preferredWidth: 68.0
                preferredHeight: 68.0
                
                layout: DockLayout {
                    
                }
                
                ImageView {
                    imageSource: "asset:///images/icon_more_channel.png"

                    preferredWidth: 58.0

                    overlapTouchPolicy: OverlapTouchPolicy.Allow
                    maxHeight: 62.0
                    minHeight: 62.0
                    opacity: 1.0
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center

                }
            }

            Label {
                horizontalAlignment: HorizontalAlignment.Left
                verticalAlignment: VerticalAlignment.Center
                text: "Channels"
                overlapTouchPolicy: OverlapTouchPolicy.Allow
                textStyle {
                    textAlign: TextAlign.Left
                    fontSize: FontSize.PointValue
                    fontSizeValue: 8
                    color: Color.White
                    fontWeight: FontWeight.Bold
                }
            }            
        }        

        // right arrow image
        ImageView {
            imageSource: "asset:///images/icon_arrow_right.png"
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Right
            overlapTouchPolicy: OverlapTouchPolicy.Allow
        }

        onTouch: {
            if (! event.isUp()) return;
            eventHandler.ShowTVChannelListPage();
        }
    }

    // push notification item
    Container {
        preferredWidth: 726
        preferredHeight: 84

        // determine background image
        background: midItemBackground.imagePaint

        layout: DockLayout {
        }

        horizontalAlignment: HorizontalAlignment.Center

        rightPadding: 50
        topPadding: 24
        bottomPadding: 24

        Container {
            leftPadding: 50

            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }

            Container {
                verticalAlignment: VerticalAlignment.Center
                rightMargin: 30

                preferredWidth: 68.0
                preferredHeight: 68.0

                layout: DockLayout {

                }
                ImageView {
                    imageSource: "asset:///images/icon_more_alert.png"
                    preferredWidth: 66.0
                    preferredHeight: 62.0
                    verticalAlignment: VerticalAlignment.Center
                    maxHeight: 62.0
                    minHeight: 62.0
                    horizontalAlignment: HorizontalAlignment.Center

                }

            }
            
            Label {
                horizontalAlignment: HorizontalAlignment.Left
                verticalAlignment: VerticalAlignment.Center
                text: "Push Notification"
                textStyle {
                    textAlign: TextAlign.Left
                    fontSize: FontSize.PointValue
                    fontSizeValue: 8
                    color: Color.White
                    fontWeight: FontWeight.Bold
                }
            }
        }

        // right arrow image
        ImageView {
            imageSource: "asset:///images/icon_arrow_right.png"
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Right
        }
        onTouch:{
            if(event.isUp())
            {
                eventHandler.showPushSetting();
            }
        }
    }

    // app version item
    Container {
        preferredWidth: 726
        preferredHeight: 84

        // determine background image
        background: bottomItemBackground.imagePaint

        layout: DockLayout {
        }

        horizontalAlignment: HorizontalAlignment.Center

        rightPadding: 50
        topPadding: 24
        bottomPadding: 24

        Container {
            leftPadding: 50

            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            Container {
                verticalAlignment: VerticalAlignment.Center
                rightMargin: 30

                preferredWidth: 68.0
                preferredHeight: 68.0

                layout: DockLayout {

                }

                ImageView {
                    imageSource: "asset:///images/icon_more_app-ver.png"
                    rightMargin: 26
                    preferredWidth: 68.0
                    preferredHeight: 68.0
                    verticalAlignment: VerticalAlignment.Center
                    minHeight: 68.0
                    maxHeight: 68.0
                    horizontalAlignment: HorizontalAlignment.Center
                }
            }

            

            Label {
                horizontalAlignment: HorizontalAlignment.Left
                verticalAlignment: VerticalAlignment.Center
                text: "App Version v1.0.1"
                textStyle {
                    textAlign: TextAlign.Left
                    fontSize: FontSize.PointValue
                    fontSizeValue: 8
                    color: Color.White
                    fontWeight: FontWeight.Bold
                }
            }
        }        
    }

    // define for set background image
    attachedObjects: [
        ImagePaintDefinition {
            id: topItemBackground
            imageSource: "asset:///images/bar_listing_top.png"
            repeatPattern: RepeatPattern.Fill
        },

        ImagePaintDefinition {
            id: midItemBackground
            imageSource: "asset:///images/bar_listing_mid.png"
            repeatPattern: RepeatPattern.Fill
        },

        ImagePaintDefinition {
            id: bottomItemBackground
            imageSource: "asset:///images/bar_listing_btm.png"
            repeatPattern: RepeatPattern.Fill
        }
    ]
}
