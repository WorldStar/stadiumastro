import bb.cascades 1.0

Container {
    property alias text : headerTitle.text
    property string itemKey
    preferredHeight: 120.0
    horizontalAlignment: HorizontalAlignment.Fill
    layout: DockLayout {

    }
    background: Color.create("#00b4b4b4")
    Label {
        id: headerTitle
        layoutProperties: StackLayoutProperties {

        }
        
        textStyle.fontWeight: FontWeight.Bold
        verticalAlignment: VerticalAlignment.Center
        textStyle.fontSize: FontSize.Large
        textStyle.color: Color.create("#ff4c566c")
    }
}
