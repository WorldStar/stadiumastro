import bb.cascades 1.0

// Channel item
Container {
    id: itemRoot
    
    preferredHeight: 84
    preferredWidth: 720

    // determine background image
    background: (itemRoot.ListItem.indexInSection == 0) ? topItemBackground.imagePaint : ((itemRoot.ListItem.indexInSection == itemRoot.ListItem.sectionSize - 1) ? bottomItemBackground.imagePaint : midItemBackground.imagePaint)

    layout: DockLayout {
    }

    horizontalAlignment: HorizontalAlignment.Fill

    rightPadding: 50
    topPadding: 16
    bottomPadding: 20

    Container {
        leftPadding: 50
        
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        
        ImageView {
            imageSource: "asset:///images/icon_more_channel.png"
            rightMargin: 30
            preferredWidth: 64
            
            verticalAlignment: VerticalAlignment.Center
            preferredHeight: 66.0
            minHeight: 66.0
            maxHeight: 66.0

        }

        Label {
            horizontalAlignment: HorizontalAlignment.Left
            verticalAlignment: VerticalAlignment.Center
            text: ListItemData.name
            textStyle {
                textAlign: TextAlign.Left
                fontSize: FontSize.PointValue
                fontSizeValue: 7
                color: Color.White
                fontWeight: FontWeight.Bold
            }
        }
    }

    // right arrow image
    ImageView {
        imageSource: "asset:///images/icon_arrow_right.png"
        verticalAlignment: VerticalAlignment.Center
        horizontalAlignment: HorizontalAlignment.Right
    }

    // for set background image
    attachedObjects: [
        ImagePaintDefinition {
            id: topItemBackground
            imageSource: "asset:///images/bar_listing_top.png"
            repeatPattern: RepeatPattern.Fill
        },

        ImagePaintDefinition {
            id: midItemBackground
            imageSource: "asset:///images/bar_listing_mid.png"
            repeatPattern: RepeatPattern.Fill
        },

        ImagePaintDefinition {
            id: bottomItemBackground
            imageSource: "asset:///images/bar_listing_btm.png"
            repeatPattern: RepeatPattern.Fill
        }
    ]
}