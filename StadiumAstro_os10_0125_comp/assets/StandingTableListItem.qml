import bb.cascades 1.0
import my.library 1.0
// standing table list item
Container {
    layout: DockLayout {
    }
    horizontalAlignment: HorizontalAlignment.Fill
    background: Color.White
    preferredHeight: 70

    Container {        
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }

        verticalAlignment: VerticalAlignment.Fill
        
        leftPadding: 20
        
//        // team position
//        Label {
//            preferredWidth: 40
//            text: ListItemData.pos
//            textStyle.fontSize: FontSize.XSmall
//            verticalAlignment: VerticalAlignment.Center
//            textStyle.color: Color.Black
//            textStyle.textAlign: TextAlign.Center
//        }
        
        // team logo   
        // must be replaced with WebImageView
        WebImageView {
            preferredWidth: 70
            preferredHeight: 70
            url: ListItemData.teamLogo
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
        }
        
        // team name
        Label {
            text: ListItemData.teamName
            textStyle.textAlign: TextAlign.Left
            textStyle.color: Color.Black
            textStyle.fontSize: FontSize.XSmall
            verticalAlignment: VerticalAlignment.Center
            multiline: true
            preferredHeight: 70.0
            preferredWidth: 170.0
            horizontalAlignment: HorizontalAlignment.Left
            textFormat: TextFormat.Plain
            minWidth: 150.0
        }
    }

    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.RightToLeft
        }

        horizontalAlignment: HorizontalAlignment.Right
        verticalAlignment: VerticalAlignment.Fill
        
        rightPadding: 10
        
        // total points
        Label {
            preferredWidth: 40
            text: ListItemData.points
            textStyle.textAlign: TextAlign.Center
            textStyle.color: Color.Black
            textStyle.fontSize: FontSize.XSmall
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
        }
        
        // goal (a)
        Label {
            preferredWidth: 40
            text: ListItemData.goalAgainst
            textStyle.textAlign: TextAlign.Center
            textStyle.color: Color.Black
            textStyle.fontSize: FontSize.XSmall
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
        }

        // goal (f)
        Label {
            preferredWidth: 40
            text: ListItemData.goalScored
            textStyle.textAlign: TextAlign.Center
            textStyle.color: Color.Black
            textStyle.fontSize: FontSize.XSmall
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
        }

        // lose match count
        Label {
            preferredWidth: 40
            text: ListItemData.lost
            textStyle.textAlign: TextAlign.Center
            textStyle.color: Color.Black
            textStyle.fontSize: FontSize.XSmall
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
        }
        
        // draw match count
        Label {
            preferredWidth: 40
            text: ListItemData.draw
            textStyle.textAlign: TextAlign.Center
            textStyle.color: Color.Black
            textStyle.fontSize: FontSize.XSmall
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
        }
        
        // win match count
        Label {
            preferredWidth: 40
            text: ListItemData.won
            textStyle.textAlign: TextAlign.Center
            textStyle.color: Color.Black
            textStyle.fontSize: FontSize.XSmall
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
        }
        
        // total played match count
        Label {
            preferredWidth: 40
            text: ListItemData.played
            textStyle.textAlign: TextAlign.Center
            textStyle.color: Color.Black
            textStyle.fontSize: FontSize.XSmall
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
        }
    }
}