import bb.cascades 1.0

Dialog{
    id: mailDialog
    
    Container{
        preferredWidth: 768
        preferredHeight: 1280
        background: Color.create(0.0, 0.0, 0.0, 0.8)
        layout: DockLayout {

        }
        
        Container {
            topPadding: 30
            leftPadding: 30
            
            ImageButton {
                defaultImageSource: "asset:///images/btn_close.png"
                pressedImageSource: "asset:///images/btn_close.png"
                
                onClicked: {
                    mailDialog.close();
                }
            }
        }
        Container{
            preferredWidth: 660
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center

            
            layout: DockLayout {

            }
            
            background: Color.Transparent
            ImageButton {
                defaultImageSource: "asset:///images/facebook-icon.png"
                onClicked:
                {
                    eventHandler.openFacebook();
                }
                preferredWidth: 200.0
                preferredHeight: 200.0
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Left

            }
            
            ImageButton {
                defaultImageSource: "asset:///images/email-icon.png"
                onClicked:
                {
                    eventHandler.tellFriendByEmail();
                }
                horizontalAlignment: HorizontalAlignment.Center
                preferredWidth: 200.0
                preferredHeight: 200.0
                verticalAlignment: VerticalAlignment.Center
            }
            
            ImageButton {
                defaultImageSource: "asset:///images/share_bmm_bbz10.png"
                
                onClicked: 
                {
                    eventHandler.tellFriendByBBM();
                }
                preferredWidth: 200.0
                preferredHeight: 200.0
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Right
            }
        }

    }    
}