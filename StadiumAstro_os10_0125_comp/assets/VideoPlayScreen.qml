/* Copyright (c) 2012 Research In Motion Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.0
import bb.multimedia 1.0
import my.library 1.0

Page {
    
    id: videoPage
    property alias videoUrl: player.sourceUrl
    property bool bVideoPlay : false
    property real fRatio:1.8
    property bool pagevalid: true
    Container {
        layout: DockLayout {
        }

        background: Color.Black
        //! [1]
        Container
        {
            topPadding: 100.0
            bottomPadding: 100.0

            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Fill
            layout: DockLayout {

            }
            background: Color.Transparent
            ForeignWindowControl {
                id: fwcVideoSurface
                windowId: "myVideoSurface"
                visible: boundToWindow
                updatedProperties: WindowProperty.Size | WindowProperty.Position | WindowProperty.Visible
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Fill
                preferredHeight: 400.0
            }
            Container{
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
                ActivityIndicator {
                    id: bufferIndicator
                    preferredWidth: 150.0
                    preferredHeight: 150.0
                    visible: false
                    running: true
                }
                Label {
                    text:""
                    id: bufferingLabel
                    visible: true
                    textStyle.color: Color.White
                }
            }
            
            
        }
        
        //[2]
        Container {
            layout: DockLayout {

            }

            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Top

            preferredHeight: 100.0
           
            ImageView {
                imageSource: "asset:///images/blackbar.png"
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
            }
            Container
            {
                verticalAlignment: VerticalAlignment.Fill
                horizontalAlignment: HorizontalAlignment.Fill
                rightPadding: 40.0
                leftPadding: 40.0
                layout: DockLayout {

                }
                ProgressBar {
                    id: playBar
                    horizontalAlignment: HorizontalAlignment.Right
                    verticalAlignment: VerticalAlignment.Center

                    duration: nowPlaying.duration
                    position: nowPlaying.position
                }

                Container {
                    // "Down" button
                    id: downButton

                    layout: DockLayout {
                    }

                    ImageView {
                        id: downBtnImage
                        imageSource: "asset:///images/back.png"
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                        overlapTouchPolicy: OverlapTouchPolicy.Allow
                        preferredWidth: 50.0
                        preferredHeight: 50.0
                    }

                    onTouch: {
                        if (event.isUp() && videoPage.pagevalid == true) {
                            nowPlaying.stop();
                            eventHandler.popTop();
                            videoPage.pagevalid = false;
                        }
                    }
                    horizontalAlignment: HorizontalAlignment.Left
                    verticalAlignment: VerticalAlignment.Center
                }
            }
            
        }

        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            layout: DockLayout {

            }
            verticalAlignment: VerticalAlignment.Bottom
            ImageView {
                horizontalAlignment: HorizontalAlignment.Fill
                imageSource: "asset:///images/blackbar.png"
                preferredHeight: 100.0
                verticalAlignment: VerticalAlignment.Fill
            }
            Container {
                topPadding: 20.0
                bottomPadding: 20.0
                verticalAlignment: VerticalAlignment.Center
                layout: DockLayout {

                }
                horizontalAlignment: HorizontalAlignment.Fill
                ImageButton {
                    property string playImg : "asset:///images/play.png"
                    property string pauseImg: "asset:///images/pause.png"
                    id: playButton
                    onClicked: {
                        videoPlay();
                    }
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Top
                    defaultImageSource: "asset:///images/play.png"
                    preferredWidth: 80.0
                    preferredHeight: 80.0

                    function videoPlay() {
                        if (videoPage.bVideoPlay) {
                            nowPlaying.pause();
                        } else {
                            bufferingLabel.text = "Loading";
                            bufferingLabel.visible = true;
                            bufferIndicator.visible = true;
                            timer.start();
                        }
                    }
                    onCreationCompleted: {
                        eventHandler.playVideo.connect(videoPlay);
                    }
                }   
            }
        }
    }

    attachedObjects: [
        //! [2]
        MediaPlayer {
            id: player
            sourceUrl:""
            videoOutput: VideoOutput.PrimaryDisplay
            windowId: fwcVideoSurface.windowId
            onBufferStatusChanged: {
                if (player.bufferStatus == BufferStatus.Buffering) {
                    // Show the ProgressBar when loading started.
                    bufferIndicator.visible = true;
                    bufferingLabel.visible = true;
                    bufferingLabel.text = "buffering";
                    //  myForeignWindow.visible = false;
                } else if (player.bufferStatus == BufferStatus.Playing) {
                    // Hide the ProgressBar when loading is complete.
                    bufferIndicator.visible = false
                    bufferingLabel.visible = false
                    
                } else if (player.bufferStatus == BufferStatus.Idle) {
                    bufferIndicator.visible = false
                    bufferingLabel.visible = false
                }
            }
        },

        //! [2]
        //! [3]

        NowPlayingConnection {
            id: nowPlaying

            duration: player.duration
            position: player.position
            mediaState: player.mediaState

            onAcquired: {
                nowPlaying.mediaState = MediaState.Started
                nowPlaying.metaData = player.metaData
            }
            onMediaStateChanged:{
                if(nowPlaying.mediaState == MediaState.Paused)
                {
                    videoPage.bVideoPlay = false;
                    playButton.defaultImageSource = playButton.playImg;
                }
                else if(nowPlaying.mediaState == MediaState.Unprepared)
                {
                }
                else if(nowPlaying.mediaState == MediaState.Prepared)
                {

                }
                else if(nowPlaying.mediaState == MediaState.Started)
                {
                    
                    //Update UI
                    videoPage.bVideoPlay = true;
                    playButton.defaultImageSource = playButton.pauseImg;
                }
                else if(nowPlaying.mediaState == MediaState.Stopped)
                {
                    videoPage.bVideoPlay = false;
                    playButton.defaultImageSource = playButton.playImg;
                    player.sourceUrl = "";
                }
            }
            
            onPause: {
                player.pause()
            }

            onPlay: {
                player.play()
            }
            
            onStop:{
                videoPage.bVideoPlay = false;
                playButton.defaultImageSource = playButton.playImg;
                player.stop();
            }
            
            onRevoked: {
                videoPage.bVideoPlay = false;
                playButton.defaultImageSource = playButton.playImg;
                player.stop()
            }
            onPositionChanged:
            {
            	
            }
        },
        QTimer {
                id: timer
                interval: 10
                onTimeout: {
                    // One second after page loads make the http get request
                timer.stop();
                nowPlaying.play();
            }
        }       
    ]
    

    
    
    
    
}
