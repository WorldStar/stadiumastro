import bb.cascades 1.0

Container {
    id: itemRoot
    preferredHeight: 100.0
    horizontalAlignment: HorizontalAlignment.Fill
    background: Color.DarkGray
    property alias itemTitle: lblPushTitle.text
    property alias itemChecked: lblToggleBtn.checked
    property bool itemOdd 
    property string itemKey
    
    layout: DockLayout {

    }
    bottomPadding: 1.0
    Container {
        id : itemBody     
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        background : itemRoot.itemOdd ? Color.LightGray: Color.White
        
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        leftPadding: 44.0
        rightPadding: 10.0
        
        Label {
            id: lblPushTitle
            layoutProperties: StackLayoutProperties {

            }
            textStyle.fontWeight: FontWeight.Bold
            verticalAlignment: VerticalAlignment.Center
            preferredWidth: 516.0
            textStyle.color: Color.Black

        }
        ToggleButton {
            id: lblToggleBtn
            verticalAlignment: VerticalAlignment.Center
            preferredWidth: 150.0
            onCheckedChanged: {
                eventHandler.enablePushForItem(itemRoot.itemKey, itemRoot.itemChecked);
            }
        }
    }
}