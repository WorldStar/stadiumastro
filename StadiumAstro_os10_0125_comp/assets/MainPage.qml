import bb.cascades 1.0
import my.library 1.0
NavigationPane
{
    id:navPane
    backButtonsVisible: false
    
    property real screenWidth: 768
    property real screenHeight: 1280
    Page {
        id: mainPage
        
        property real menuPaneHeight: 125

        //! [0]

        Container {
            layout: DockLayout {
            }

            animations: [
                FadeTransition {
                    id: showPageEffect
                    duration: 500
                    toOpacity: 1.0
                    fromOpacity: 0.0
                }
            ]

            background: Color.Black

            //! [1] main contents
            PageContentScrollView {
                id: mainContents

                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.TopToBottom
                    }

                    preferredHeight: navPane.screenHeight

                    //! [2] main title area
                    Container {
                        layout: DockLayout {

                        }

                        horizontalAlignment: HorizontalAlignment.Fill

                        // title background image
                        ImageView {
                            imageSource: "asset:///images/bar_top_blue_home.png"
                            verticalAlignment: VerticalAlignment.Top
                            horizontalAlignment: HorizontalAlignment.Fill
                            scalingMethod: ScalingMethod.AspectFill
                            preferredHeight: 100.0
                            minHeight: 100.0
                        }

                        // help button(must be replaced!!!)
                        Container {
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Right
                            
                            rightPadding: 20
                            ImageButton {
                                defaultImageSource: "asset:///images/icon_help.png"
                                onClicked: {
                                    eventHandler.showHelpPage();
                                }
                            }
                        }
                        
                        

                    } //! [2]

                    //[1.2] - WebView
                    Container {
                        preferredHeight: navPane.screenHeight - 235

                        id: fantasyview
                        objectName: "fantasyview"

                        layout: StackLayout {

                        }
                        horizontalAlignment: HorizontalAlignment.Fill
                        ScrollView {
                            horizontalAlignment: HorizontalAlignment.Fill
                            WebView {
                                objectName: "fantasybrowser"
                                id: fantasybrowser
                                verticalAlignment: VerticalAlignment.Top
                                horizontalAlignment: HorizontalAlignment.Fill
                                settings.webInspectorEnabled: true
                                settings.zoomToFitEnabled: true
                            }
                        }
                    }
                    //[1.1] - Main Content
                    Container {
                        objectName: "maincontent"

                        //! [3] admob area
                        Container {
                            id: admob
                            objectName: "admob"
                            layout: DockLayout {

                            }
                            horizontalAlignment: HorizontalAlignment.Fill

                            verticalAlignment: VerticalAlignment.Top
                            // admob image(must be replaced!!!)
                            WebImageView {
                                id: imgAdmob
                                objectName: "imgAdmob"
                                imageSource: "asset:///images/ads_bar_black.png"
                                horizontalAlignment: HorizontalAlignment.Fill
                                loadEffect: ImageViewLoadEffect.FadeZoom
                                preferredHeight: 100.0
                                onTouch: {
                                    if (event.isUp) {
                                        eventHandler.onTouchAdmob();
                                    }
                                }
                            }

                            // admob close button image
                            ImageView {
                                imageSource: "asset:///images/icon_ads_close.png"
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Right
                                scalingMethod: ScalingMethod.None
                                rightMargin: 0.0
                                onTouch: {
                                    // hide admob area
                                    if (event.isUp) {
                                        admob.visible = false;
                                        eventHandler.onHideAdmob();
                                    }

                                }
                            }
                        } //! [3]
                        ScrollView
                        {
                            objectName: "homeScroll"
                            Container
	                        {
	                        	//! [4] Date/Time & Match result
		                        ListView {
		                            id: matchListForToday
		                            objectName: "matchListForToday"
		
		                            preferredHeight: 120
		
		                            layout: StackListLayout {
		                                headerMode: ListHeaderMode.None
		                                orientation: LayoutOrientation.LeftToRight
		                            }
		
		                            listItemComponents: [
		                                ListItemComponent {
		                                    type: "item"
		                                    MatchPreviewItem {
		
		                                    }
		                                }
		                            ] // listItemComponents
		
		                            onTriggered: {
		                                // replace to analysis screen
		                                var matchResultData = matchListForToday.dataModel.data(indexPath);
		                                eventHandler.ShowAnalysisPage(matchResultData);
		                            }
		
		                            scrollIndicatorMode: ScrollIndicatorMode.None
		                            flickMode: FlickMode.Default
		                            snapMode: SnapMode.Default
		                            bufferedScrollingEnabled: true
		                        }
		                        //! [4]
		
		                        //! [5] Special Logo
		                        Container {
		                            id: speicalLogo
		                            layout: DockLayout {
		                            }
		
		                            horizontalAlignment: HorizontalAlignment.Fill
		
		                            // highlight news list
		                            ListView {
		                                id: specialLogoList
		                                objectName: "specialLogoList"
		
		                                property int prevSelectedIndex: 0
		
		                                //Aspect ratio 16:8
		
		                                preferredHeight: 370
		
		                                layout: StackListLayout {
		                                    headerMode: ListHeaderMode.None
		                                    orientation: LayoutOrientation.LeftToRight
		                                }
		
		                                listItemComponents: [
		                                    ListItemComponent {
		                                        type: "item"
		                                        SpecialLogoItem {
		
		                                        }
		                                    }
		                                ] // listItemComponents
		
		                                onTriggered: {
		                                    // replace to news view screen
		                                    var hightlightNewsData = specialLogoList.dataModel.data(indexPath);
		                                    eventHandler.ShowNewsViewPage(hightlightNewsData);
		                                }
		
		                                onTouch: {
		                                    // synchronize with scroll status bar
		                                    if (event.isDown()) {
		                                        // record down status
		                                        specialLogoList.prevSelectedIndex = scrollStateHandler.firstVisibleItem[0];
		                                        timer.stop();
		                                    } else if (event.isUp()) {
		                                        // get index of image that currently displayed
		                                        var nCount = scrollStatusBar.count();
		                                        var oldIndex = scrollStatusBar.curIndex;
		                                        var newIndex = scrollStateHandler.firstVisibleItem[0];
		
		                                        // compare with up status
		                                        if (newIndex < specialLogoList.prevSelectedIndex) newIndex --;
		                                        // because when scrolled right, firstvisibleitem would not be changed!!!
		                                        newIndex = (newIndex + nCount) % nCount;
		
		                                        if (oldIndex != newIndex) {
		                                            // update property value
		                                            scrollStatusBar.curIndex = newIndex;
		
		                                            // swap highlight point
		                                            scrollStatusBar.swap(oldIndex, newIndex);
		                                        }
		
		                                        timer.start();
		                                    }
		                                }
		
		                                onDataModelChanged: {
		                                    timer.start();
		                                    // display "second"(really, it's first logo image!) logo image
		                                    specialLogoList.scrollToItem([ 1 ], ScrollAnimation.None);
		                                    scrollStatusBar.curIndex = 0;
		                                }
		
		                                attachedObjects: [
		                                    // This handler is tracking the scroll state of the ListView.
		                                    ListScrollStateHandler {
		                                        id: scrollStateHandler
		
		                                        onScrollingChanged: {
		                                            if (! scrolling) {
		                                                // when scrolling finished
		                                                var nCount = scrollStatusBar.count();
		                                                var newIndex = scrollStateHandler.firstVisibleItem[0];
		                                                if (newIndex == nCount + 1) {
		                                                    // after displayed last backup logo image, then replace to first image
		                                                    specialLogoList.scrollToItem([ 1 ], ScrollAnimation.None);
		                                                } else if (newIndex == 0) {
		                                                    // after displayed first backup logo image, then replace to last image
		                                                    specialLogoList.scrollToItem([ nCount ], ScrollAnimation.None);
		                                                }
		                                            }
		                                        }
		                                    }
		                                ]
		
		                                flickMode: FlickMode.SingleItem
		                                snapMode: SnapMode.None
		                                scrollIndicatorMode: ScrollIndicatorMode.None
		                                focusPolicy: FocusPolicy.Touch
		                            }
		
		                            //! [6] logo string
		                            Container {
		                                layout: DockLayout {
		                                }
		
		                                ImageView {
		                                    imageSource: "asset:///images/bar_blue-title_side.png"
		                                    verticalAlignment: VerticalAlignment.Top
		                                    horizontalAlignment: HorizontalAlignment.Left
		                                }
		
		                                Label {
		                                    id: logoString
		                                    objectName: "logoString"
		
		                                    verticalAlignment: VerticalAlignment.Center
		                                    horizontalAlignment: HorizontalAlignment.Center
		                                    textStyle.color: Color.White
                                            textStyle.fontSize: FontSize.XSmall
                                            textStyle.fontWeight: FontWeight.Bold
                                        }
		                            } //! [6]
		
		                            onCreationCompleted: {
		                                // Start progress indicator and timer
		                                //progressIndicator.active = true;
		
		                            }
		
		                        } //! [5]
		
		                        //! [9] custom control to display scroll state's image list
		                        Container {
		                            horizontalAlignment: HorizontalAlignment.Fill
		
		                            topPadding: 10.0
		                            bottomPadding: 10.0
		
		                            Container {
		                                id: scrollStatusBar
		                                property int curIndex: 0
		
		                                objectName: "scrollStatusBar"
		
		                                horizontalAlignment: HorizontalAlignment.Center
		
		                                layout: StackLayout {
		                                    orientation: LayoutOrientation.LeftToRight
		                                }
		                            }
		
		                            attachedObjects: [
		                                QTimer {
		                                    id: timer
		                                    interval: 5000
		                                    onTimeout: {
		                                        // One second after page loads make the http get request
		                                        //eventHandler.ChangeLogoFrame();
		
		                                        // get next image's index
		                                        var curIndex = scrollStatusBar.curIndex;
		                                        var nextIndex = curIndex + 1;
		                                        var indexPath;
		
		                                        if (nextIndex == scrollStatusBar.count()) {
		                                            // if reached at end, then scroll to begin
		                                            nextIndex = 0;
		
		                                        }
		
		                                        // scroll logo image
		                                        indexPath = [ nextIndex ];
		                                        specialLogoList.scrollToItem(indexPath, ScrollAnimation.None);
		
		                                        // swap highlight point
		                                        scrollStatusBar.swap(curIndex, nextIndex);
		
		                                        // set property value
		                                        scrollStatusBar.curIndex = nextIndex;
		                                    }
		                                }
		                            ]
		                        } //! [9]
		
		                        // ![10] feature video and news list
		                        ListView {
		                            id: highlightVideosAndNews
		                            objectName: "highlightVideosAndNews"
		
									
		                            bottomPadding: mainPage.menuPaneHeight
		
		                            layout: StackListLayout {
		                                headerMode: ListHeaderMode.None
		                                orientation: LayoutOrientation.TopToBottom
		                            }
		
		                            scrollIndicatorMode: ScrollIndicatorMode.None
		
		                            listItemComponents: [
		                                ListItemComponent {
		                                    type: "item"
		                                    // list item
		                                    HighlightVideoNewsItem {
		
		                                    }
		                                },
		                                ListItemComponent {
		                                    type: "header"
		
		                                    // list title
		                                    ListHeaderItem {
		                                        preferredWidth: 768
		                                    }
		                                }
		                            ] // listItemComponents
		
		                            // Item type-mapping
		                            function itemType(data, indexPath) {
		                                if (data.type == "header") return "header";
		                                else return 'item';
		                            }
		
		                            onTriggered: {
		                                // replace screen based on 'type' property
		                                var featureData = highlightVideosAndNews.dataModel.data(indexPath);
		                                //	                        if(featureData.type == "VIDEO")
		                                //	                        	eventHandler.ShowVideoViewPage(featureData);
		                                //	                        else if(featureData.type == "NEWS")
		                                eventHandler.ShowNewsViewPage(featureData);
		                            }
		                            snapMode: SnapMode.LeadingEdge
                                    
                                    preferredHeight: 2690
                                } //! [10]
	                        }
	                    }
                    }
                } //! [1]
            }

            // main menu
            Container {
                id: mainMenu
                objectName: "mainMenuArea"
                visible: false

                property bool status: false;

                //background: Color.Black
                layout: DockLayout {
                }

                verticalAlignment: VerticalAlignment.Fill
                horizontalAlignment: HorizontalAlignment.Fill

                preferredWidth: 520.0
                maxWidth: 520.0
                Container {

                    objectName: "mainMenu"
                    MainMenu {

                    }
                }

            }

            Container {
                id: maskScreen
                background: Color.Transparent
                opacity: 0.1
                visible: false
                verticalAlignment: VerticalAlignment.Fill
                horizontalAlignment: HorizontalAlignment.Right
                preferredWidth: navPane.screenWidth - 520

                onTouch: {
                    if (event.isUp() && mainMenu.status) hideMainMenu();
                }
            }

            //! [8] menu buttons
            Container {
                id: menuPane

                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Bottom

                topPadding: 3.0
                background: Color.DarkGray
 
                preferredHeight: mainPage.menuPaneHeight

                layout: DockLayout {
                }

                Container {
                    background: Color.create("#ff222222")

                    layout: DockLayout {
                    }

                    horizontalAlignment: HorizontalAlignment.Fill
                    verticalAlignment: VerticalAlignment.Fill

                    // static menu button(need to modify!!!)
                    Container {
                        id: staticMenuButton
                        preferredWidth: 120

                        horizontalAlignment: HorizontalAlignment.Left
                        verticalAlignment: VerticalAlignment.Fill

                        layout: DockLayout {
                        }

                        Container {
                            background: Color.DarkGray
                            layout: DockLayout {
                            }

                            horizontalAlignment: HorizontalAlignment.Fill
                            verticalAlignment: VerticalAlignment.Fill

                            topPadding: 12.0
                            bottomPadding: 12.0

                            ImageView {
                                imageSource: "asset:///images/icon_main_menu.png"
                                verticalAlignment: VerticalAlignment.Top
                                horizontalAlignment: HorizontalAlignment.Center

                                animations: [
                                    ParallelAnimation {
                                        id: moveContentsToRight
                                        TranslateTransition {
                                            target: mainContents
                                            toX: 520.0
                                            duration: 500
                                            easingCurve: StockCurve.CircularOut
                                        }
                                        TranslateTransition {
                                            target: mainMenu
                                            fromX: -520.0
                                            toX: 0
                                            duration: 500
                                            easingCurve: StockCurve.CircularOut
                                        }
                                    },
                                    ParallelAnimation {
                                        id: moveContentsToLeft
                                        TranslateTransition {
                                            target: mainContents
                                            toX: 0.0
                                            duration: 500
                                            easingCurve: StockCurve.CircularOut
                                        }
                                        TranslateTransition {
                                            target: mainMenu
                                            toX: -520.0
                                            duration: 500
                                            easingCurve: StockCurve.CircularOut
                                        }
                                        onEnded: {
                                            mainMenu.visible = false;
                                        }
                                    }
                                ]
                                scalingMethod: ScalingMethod.Fill

                                onTouch: {
                                    // processing display menu at left-top side
                                    if (! event.isUp()) return;

                                    if (mainMenu.status) {
                                        hideMainMenu();
                                    } else {
                                        showMainMenu();
                                    }
                                }
                            }

                            Label {
                                text: "Menu"
                                textStyle {
                                    color: Color.White
                                    fontSize: FontSize.PointValue
                                    fontSizeValue: 5
                                    textAlign: TextAlign.Center
                                }
                                verticalAlignment: VerticalAlignment.Bottom
                                horizontalAlignment: HorizontalAlignment.Center
                                overlapTouchPolicy: OverlapTouchPolicy.Allow
                            }
                        }

                    }

                    Container {
                        layout: AbsoluteLayout {

                        }

                        horizontalAlignment: HorizontalAlignment.Fill
                        overlapTouchPolicy: OverlapTouchPolicy.Allow

                        ImageView {
                            id: videoMenu
                            imageSource: "asset:///images/icon_red_video.png"
                            verticalAlignment: VerticalAlignment.Center
                            //horizontalAlignment: HorizontalAlignment.Right
                            visible: false

                            onTouch: {
                                if (! event.isUp()) return;
                                hideDynamicMenuButtons();
                                videoTouchAnim.play();
                                initializeDynamicMenuStatus();
                                eventHandler.ShowVideoCategoryPage();
                            }

                            animations: [
                                SequentialAnimation {
                                    id: videoTouchAnim
                                    ScaleTransition {
                                        toX: 1.6
                                        toY: 1.6
                                        duration: 300
                                    }

                                    FadeTransition {
                                        fromOpacity: 1.0
                                        toOpacity: 0.0
                                        duration: 300
                                    }
                                }
                            ]
                            preferredWidth: 80.0
                            preferredHeight: 80.0
                            layoutProperties: AbsoluteLayoutProperties {
                                positionY: 20.0

                            }

                        }

                        ImageView {
                            id: newsMenu
                            imageSource: "asset:///images/icon_red_news.png"
                            verticalAlignment: VerticalAlignment.Center
                            //horizontalAlignment: HorizontalAlignment.Right
                            visible: false

                            onTouch: {
                                if (! event.isUp()) return;
                                hideDynamicMenuButtons();
                                initializeDynamicMenuStatus();
                                newsTouchAnim.play();
                                eventHandler.ShowNewsCategoryPage();
                            }

                            animations: [
                                SequentialAnimation {
                                    id: newsTouchAnim
                                    ScaleTransition {
                                        toX: 1.6
                                        toY: 1.6
                                        duration: 300
                                    }

                                    FadeTransition {
                                        fromOpacity: 1.0
                                        toOpacity: 0.0
                                        duration: 300
                                    }
                                }
                            ]
                            preferredWidth: 80.0
                            preferredHeight: 80.0
                            layoutProperties: AbsoluteLayoutProperties {
                                positionY: 20.0

                            }
                        }

                        ImageView {
                            id: fixtureMenu
                            objectName: "fixtureMenu"
                            imageSource: "asset:///images/icon_red_fixtures.png"
                            verticalAlignment: VerticalAlignment.Center
                            //horizontalAlignment: HorizontalAlignment.Right
                            visible: false
                            property bool bshow;

                            onTouch: {
                                if (! event.isUp()) return;
                                hideDynamicMenuButtons();
                                initializeDynamicMenuStatus();
                                fixtureTouchAnim.play();
                                eventHandler.ShowMatchesPage();
                            }

                            animations: [
                                SequentialAnimation {
                                    id: fixtureTouchAnim
                                    ScaleTransition {
                                        toX: 1.6
                                        toY: 1.6
                                        duration: 300
                                    }

                                    FadeTransition {
                                        fromOpacity: 1.0
                                        toOpacity: 0.0
                                        duration: 300
                                    }
                                }
                            ]
                            preferredWidth: 80.0
                            preferredHeight: 80.0
                            layoutProperties: AbsoluteLayoutProperties {
                                positionY: 20.0

                            }
                        }

                        ImageView {
                            id: standingsMenu
                            imageSource: "asset:///images/icon_red_standings.png"
                            verticalAlignment: VerticalAlignment.Center
                            //horizontalAlignment: HorizontalAlignment.Right
                            visible: false

                            onTouch: {
                                if (! event.isUp()) return;
                                hideDynamicMenuButtons();
                                initializeDynamicMenuStatus();
                                standingsTouchAnim.play();
                                eventHandler.ShowStandingsPage();
                            }

                            animations: [
                                SequentialAnimation {
                                    id: standingsTouchAnim
                                    ScaleTransition {
                                        toX: 1.6
                                        toY: 1.6
                                        duration: 300
                                    }

                                    FadeTransition {
                                        fromOpacity: 1.0
                                        toOpacity: 0.0
                                        duration: 300
                                    }
                                }
                            ]
                            preferredWidth: 80.0
                            preferredHeight: 80.0
                            layoutProperties: AbsoluteLayoutProperties {
                                positionY: 20.0

                            }
                        }

                        ImageView {
                            id: moreMenu
                            imageSource: "asset:///images/icon_red_more.png"
                            verticalAlignment: VerticalAlignment.Center
                            //horizontalAlignment: HorizontalAlignment.Right
                            visible: false

                            onTouch: {
                                if (! event.isUp()) return;
                                hideDynamicMenuButtons();
                                initializeDynamicMenuStatus();
                                moreTouchAnim.play();
                                eventHandler.ShowMorePage();
                            }

                            animations: [
                                SequentialAnimation {
                                    id: moreTouchAnim
                                    ScaleTransition {
                                        toX: 1.6
                                        toY: 1.6
                                        duration: 300
                                    }

                                    FadeTransition {
                                        fromOpacity: 1.0
                                        toOpacity: 0.0
                                        duration: 300
                                    }
                                }
                            ]
                            preferredWidth: 80.0
                            preferredHeight: 80.0
                            layoutProperties: AbsoluteLayoutProperties {
                                positionX: 0.0
                                positionY: 20.0

                            }
                        }

                        ImageView {
                            id: dynamicMainMenu

                            imageSource: "asset:///images/icon_red_main.png"
                            horizontalAlignment: HorizontalAlignment.Right
                            verticalAlignment: VerticalAlignment.Center

                            property bool showStatus: false

                            property real initialPosX: navPane.screenWidth - 100
                            property real lastPosX: 150.0

                            animations: [
                                ParallelAnimation {
                                    id: menuShowAnimation
                                    // main menu
                                    RotateTransition {
                                        target: dynamicMainMenu
                                        toAngleZ: 0.0
                                        fromAngleZ: 360.0
                                        duration: 500
                                    }

                                    // video menu
                                    RotateTransition {
                                        id: videoMenuShowRotate
                                        target: videoMenu
                                        fromAngleZ: 360.0
                                        toAngleZ: 0.0
                                        duration: 500
                                    }
                                    TranslateTransition {
                                        id: videoMenuShowTranslate
                                        target: videoMenu
                                        fromX: dynamicMainMenu.initialPosX
                                        toX: dynamicMainMenu.lastPosX
                                        duration: 500
                                        easingCurve: StockCurve.BackOut
                                    }

                                    // news menu
                                    RotateTransition {
                                        id: newsMenuShowRotate
                                        target: newsMenu
                                        fromAngleZ: 360.0
                                        toAngleZ: 0.0
                                        duration: 500
                                    }
                                    TranslateTransition {
                                        id: newsMenuShowTranslate
                                        target: newsMenu
                                        fromX: dynamicMainMenu.initialPosX
                                        toX: dynamicMainMenu.lastPosX + (dynamicMainMenu.initialPosX - dynamicMainMenu.lastPosX) / 5
                                        duration: 500
                                        easingCurve: StockCurve.BackOut
                                    }

                                    // fixture menu
                                    RotateTransition {
                                        id: fixtureMenuShowRotate
                                        target: fixtureMenu
                                        fromAngleZ: 360.0
                                        toAngleZ: 0.0
                                        duration: 500
                                    }
                                    TranslateTransition {
                                        id: fixtureMenuShowTranslate
                                        target: fixtureMenu
                                        fromX: dynamicMainMenu.initialPosX
                                        toX: dynamicMainMenu.lastPosX + (dynamicMainMenu.initialPosX - dynamicMainMenu.lastPosX) / 5 * 2
                                        duration: 500
                                        easingCurve: StockCurve.BackOut
                                    }

                                    // standing menu
                                    RotateTransition {
                                        id: standingsMenuShowRotate
                                        target: standingsMenu
                                        fromAngleZ: 360.0
                                        toAngleZ: 0.0
                                        duration: 400
                                    }
                                    TranslateTransition {
                                        id: standingsMenuShowTranslate
                                        target: standingsMenu
                                        fromX: dynamicMainMenu.initialPosX
                                        toX: dynamicMainMenu.lastPosX + (dynamicMainMenu.initialPosX - dynamicMainMenu.lastPosX) / 5 * 3
                                        duration: 500
                                        easingCurve: StockCurve.BackOut
                                    }

                                    // more menu
                                    RotateTransition {
                                        id: moreMenuShowRotate
                                        target: moreMenu
                                        fromAngleZ: 360.0
                                        toAngleZ: 0.0
                                        duration: 500
                                    }
                                    TranslateTransition {
                                        id: moreMenuShowTranslate
                                        target: moreMenu
                                        fromX: dynamicMainMenu.initialPosX
                                        toX: dynamicMainMenu.lastPosX + (dynamicMainMenu.initialPosX - dynamicMainMenu.lastPosX) / 5 * 4
                                        duration: 500
                                        easingCurve: StockCurve.BackOut
                                    }
                                },
                                ParallelAnimation {
                                    id: menuHideAnimation

                                    // main menu
                                    RotateTransition {
                                        target: dynamicMainMenu
                                        toAngleZ: 360.0
                                        duration: 300
                                    }

                                    // video menu
                                    RotateTransition {
                                        id: videoMenuHideRotate

                                        target: videoMenu
                                        toAngleZ: 360.0
                                        duration: 300
                                    }
                                    TranslateTransition {
                                        id: videoMenuHideTranslate

                                        target: videoMenu
                                        toX: dynamicMainMenu.initialPosX
                                        duration: 300
                                        easingCurve: StockCurve.BackIn
                                    }

                                    // news menu
                                    RotateTransition {
                                        id: newsMenuHideRotate
                                        target: newsMenu
                                        toAngleZ: 360.0
                                        duration: 300
                                    }
                                    TranslateTransition {
                                        id: newMenuHideTranslate
                                        target: newsMenu
                                        toX: dynamicMainMenu.initialPosX
                                        duration: 300
                                        easingCurve: StockCurve.BackIn
                                    }

                                    // fixture menu
                                    RotateTransition {
                                        id: fixtureMenuHideRotate
                                        target: fixtureMenu
                                        toAngleZ: 360.0
                                        duration: 300
                                    }
                                    TranslateTransition {
                                        id: fixtureMenuHideTranslate
                                        target: fixtureMenu
                                        toX: dynamicMainMenu.initialPosX
                                        duration: 300
                                        easingCurve: StockCurve.BackIn
                                    }

                                    // standings menu
                                    RotateTransition {
                                        id: standingsMenuHideRotate
                                        target: standingsMenu
                                        toAngleZ: 360.0
                                        duration: 300
                                    }
                                    TranslateTransition {
                                        id: standingsMenuHideTranslate
                                        target: standingsMenu
                                        toX: dynamicMainMenu.initialPosX
                                        duration: 300
                                        easingCurve: StockCurve.BackIn
                                    }

                                    // more menu
                                    RotateTransition {
                                        id: moreMenuHideRotate
                                        target: moreMenu
                                        toAngleZ: 360.0
                                        duration: 300
                                    }
                                    TranslateTransition {
                                        id: moreMenuHideTranslate
                                        target: moreMenu
                                        toX: dynamicMainMenu.initialPosX
                                        duration: 300
                                        easingCurve: StockCurve.BackIn
                                    }
                                }
                            ]
                            onTouch: {
                                // dynamic menu animation processing
                                if (! event.isUp()) return;

                                resetDynamicMenuButtons();

                                if (! showStatus) {
                                    videoMenu.visible = true;
                                    newsMenu.visible = true;
                                    if(fixtureMenu.bshow == true)
                                    	fixtureMenu.visible = true;
                                    else
                                        fixtureMenu.visible = false;
                                    standingsMenu.visible = true;
                                    moreMenu.visible = true;
                                    menuShowAnimation.play();
                                } else {
                                    menuHideAnimation.play();
                                }

                                showStatus = ! showStatus;
                            }
                            preferredWidth: 100.0
                            preferredHeight: 100.0
                            layoutProperties: AbsoluteLayoutProperties {
                            	positionX: dynamicMainMenu.initialPosX-10
                                positionY: 10.0
                            }
                        }
                    }
                }

            } //! [8]
        } //! [0]
        
        onCreationCompleted: {
            fantasyview.visible = false;
            showPageEffect.play();
        }

        
    }
    
    onPopTransitionEnded: {
        delete page;
    }
    onNavigateToTransitionEnded: {
        var i;
    	for(i = 0; i < pages.count(); i++)
    	{
    	    delete pages[i];
    	}
    }
    
    peekEnabled: false
    // show/hide animation menu buttons
    function resetDynamicMenuButtons() {
        videoMenu.opacity = 1;
        newsMenu.opacity = 1;
        fixtureMenu.opacity = 1;
        standingsMenu.opacity = 1;
        moreMenu.opacity = 1;

        videoMenu.scaleX = 1;
        videoMenu.scaleY = 1;

        newsMenu.scaleX = 1;
        newsMenu.scaleY = 1;

        fixtureMenu.scaleX = 1;
        fixtureMenu.scaleY = 1;

        standingsMenu.scaleX = 1;
        standingsMenu.scaleY = 1;

        moreMenu.scaleX = 1;
        moreMenu.scaleY = 1;
    }
    function hideDynamicMenuButtons() {
        menuHideAnimation.play();
    }

    function resetMenuForFootballSports() {
        hideDynamicMenuButtons();
        
        var translates;
        if(fixtureMenu.bshow == true){
            translates = [ videoMenuShowTranslate, newsMenuShowTranslate, fixtureMenuShowTranslate, standingsMenuShowTranslate,
                moreMenuShowTranslate ];
        }else{
            translates = [ videoMenuShowTranslate, newsMenuShowTranslate, standingsMenuShowTranslate,
                moreMenuShowTranslate ];
        }

        var i = 0;
        fixtureMenuShowRotate.fromAngleZ = 360;
        standingsMenuShowRotate.fromAngleZ = 360;
        fixtureMenuHideRotate.toAngleZ = 360;
        standingsMenuHideRotate.toAngleZ = 360;

         if(fixtureMenu.bshow == true){
            for (i = 0; i < 5; i ++) {
                translates[i].toX = dynamicMainMenu.lastPosX + (dynamicMainMenu.initialPosX - dynamicMainMenu.lastPosX) / 5 * i;
            }
		}else{
            for (i = 0; i < 4; i ++) {
                translates[i].toX = dynamicMainMenu.lastPosX + (dynamicMainMenu.initialPosX - dynamicMainMenu.lastPosX) / 4 * i;
            }
		}
    }

    function resetMenuForOtherSports() {
        hideDynamicMenuButtons();
        //var rotates = [videoMenuShowRotate, newsMenuShowRotate, moreMenuShowRotate ];
        var translates = [ videoMenuShowTranslate, newsMenuShowTranslate, moreMenuShowTranslate ];

        var i = 0;
        fixtureMenuShowRotate.fromAngleZ = 0;
        standingsMenuShowRotate.fromAngleZ = 0;

        fixtureMenuHideRotate.toAngleZ = 0;
        standingsMenuHideRotate.toAngleZ = 0;
        fixtureMenuShowTranslate.toX = dynamicMainMenu.initialPosX;
        standingsMenuShowTranslate.toX = dynamicMainMenu.initialPosX;

        for (i = 0; i < 3; i ++) {
            translates[i].toX = dynamicMainMenu.lastPosX + (dynamicMainMenu.initialPosX - dynamicMainMenu.lastPosX) / 3 * i;
        }
    }

    // show/hide left-side menu bar
    function showMainMenu() {
        timer.stop();
        staticMenuButton.background = Color.Gray
        mainMenu.visible = true;
        mainMenu.status = true;
        mainMenu.preferredWidth = 520;
        moveContentsToRight.play();
        menuPane.visible = false;
        maskScreen.visible = true;
        mainPage.menuPaneHeight = 0;
    }

    function hideMainMenu() {
        timer.start();
        staticMenuButton.background = Color.DarkGray;
        mainMenu.status = false;
        moveContentsToLeft.play();
        menuPane.visible = true;
        maskScreen.visible = false;
        mainPage.menuPaneHeight = 125;
    }

    function initializeDynamicMenuStatus() {
        dynamicMainMenu.showStatus = false;
    }
}
