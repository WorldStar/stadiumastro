import bb.cascades 1.0
import my.library 1.0

Container {
    id: rootContainer
    property alias url: webImageView.url
    property alias imageSource: webImageView.imageSource
    property alias scalingMethod: webImageView.scalingMethod

    layout: DockLayout {

    }

    WebImageView {
        id: webImageView
        url: ListItemData.imageLink
        loadEffect: ImageViewLoadEffect.FadeZoom
        preferredWidth: rootContainer.preferredWidth
        preferredHeight: rootContainer.preferredHeight
        defaultImageSource: "asset:///images/defaultFeed.jpg"

        function startActivity() {
            loadingIndicator.start();
            loadingIndicator.visible = true
        }
        
        function stopActivity() {
            loadingIndicator.stop();
            loadingIndicator.visible = false
        }

        verticalAlignment: VerticalAlignment.Center
        horizontalAlignment: HorizontalAlignment.Center
    }

    ActivityIndicator {
        id: loadingIndicator
        visible: false
        verticalAlignment: VerticalAlignment.Center
        horizontalAlignment: HorizontalAlignment.Center
        preferredWidth: rootContainer.preferredWidth / 5
    }

    onCreationCompleted: {
        webImageView.urlChanged.connect(webImageView.startActivity());
        webImageView.imageDownloaded.connect(webImageView.stopActivity());
    }

}