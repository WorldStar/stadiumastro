import bb.cascades 1.0
import my.library 1.0
Page {
    id: viewPage
    objectName: "viewPage"
    property alias title: newsArticleTitle.text
    property alias dateTime: newsArticleDateTime.text
    property alias image: newsArticleImage.imageSource
    property alias url: newsArticleImage.url
    property alias newsContent: newsArticleContent.text
    property alias caption: caption.text
    property alias showPlayMark : playMark.visible
    property string videoUrl
    property bool pagevalid : true
    property alias showBanner : admob.visible
    property string identifier
    property string pageType
    
    //! [0] background
    Container {
        background: Color.Black        
        
        layout: StackLayout{
            orientation: LayoutOrientation.TopToBottom
        }

        //! [1] page title
        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Top

            layout: DockLayout {
            }

            ImageView {
                imageSource: viewPage.showPlayMark ? "asset:///images/bar_top_blue_home.png" : "asset:///images/bar_top_blue.png"
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
            }

            Label {
            	id: caption
                text: "News"

                textStyle {
                    fontSizeValue: 10.0
                    textAlign: TextAlign.Center
                    color: Color.White
                    fontSize: FontSize.PointValue
                    fontWeight: FontWeight.Bold
                }
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
            }

            Container {
                horizontalAlignment: HorizontalAlignment.Right
                verticalAlignment: VerticalAlignment.Center

                layout: DockLayout {

                }

                rightPadding: 20
            }
        } //! [1]

        //! [3] admob area
        Container {
            id: admob
            objectName: "admob"
            
            layout: DockLayout {

            }
            
            horizontalAlignment: HorizontalAlignment.Fill

            verticalAlignment: VerticalAlignment.Top
            // admob image(must be replaced!!!)
            WebImageView {
                id: imgAdmob
                objectName: "imgAdmob"
                imageSource: "asset:///images/ads_bar_black.png"
                horizontalAlignment: HorizontalAlignment.Fill
                preferredHeight: 100.0
                onTouch: {
                    if (event.isUp) {
                        admob.visible = false;
                        eventHandler.onTouchAdmob();
                    }
                }
            }

            // admob close button image
            ImageView {
                imageSource: "asset:///images/icon_ads_close.png"
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Right
                scalingMethod: ScalingMethod.None
                rightMargin: 0.0
                onTouch: {
                    // hide admob area
                    if (event.isUp) {
                        admob.visible = false;
                        eventHandler.onHideAdmob();
                    }

                }
            }
        } //! [3]

        //! [2] Article viewer
        Container {
            leftPadding: 20.0
            rightPadding: 20.0
            topPadding:10.0
            background: Color.create("#ff222222")

            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            ScrollView {
                scrollViewProperties.scrollMode: ScrollMode.Vertical

                scrollViewProperties.initialScalingMethod: ScalingMethod.AspectFill
                scrollViewProperties.pinchToZoomEnabled: false
                scrollViewProperties.overScrollEffectMode: OverScrollEffectMode.OnScroll

                verticalAlignment: VerticalAlignment.Fill

                horizontalAlignment: HorizontalAlignment.Fill
                Container {
                    topPadding: 20.0
                    leftPadding: 20
                    rightPadding: 20
                    
                    

                    layout: StackLayout{
                        orientation: LayoutOrientation.TopToBottom
                    }

                    background: Color.Transparent
                    horizontalAlignment: HorizontalAlignment.Fill
                    
                    // title
                    Label {
                        id: newsArticleTitle
                        objectName: "newsArticleTitle"
                        
                        textStyle.fontSize: FontSize.Large
                        textStyle.fontWeight: FontWeight.Bold
                        textStyle.textAlign: TextAlign.Left
                        multiline: true
                        textStyle.color: Color.White
                        horizontalAlignment: HorizontalAlignment.Fill
                    }
                    
                    // date time
                    Label {
                        id: newsArticleDateTime
                        objectName: "newsArticleDateTime"
                        visible: viewPage.showPlayMark ? false: true

                        textStyle.fontSize: FontSize.Small
                        textStyle.textAlign: TextAlign.Left
                        textStyle.color: Color.White
                        horizontalAlignment: HorizontalAlignment.Fill
                    }
                    
                    // image view
                    Container
                    {
                        horizontalAlignment: HorizontalAlignment.Fill
                        layout: DockLayout {

                        }
                        WebImageViewContainer {
	                        id: newsArticleImage
	                        objectName: "newsArticleImage"
	                        //imageSource: "asset:///images/icon_video_news_item.png"
//	                        horizontalAlignment: HorizontalAlignment.Fill
	                        preferredHeight: 312.0
                            preferredWidth: 680.0
//                            scalingMethod: ScalingMethod.AspectFill
                        }
                        
                        //By PASSION
                        ImageView {
                            id:playMark
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Center
                            imageSource: "asset:///images/icon_video_play.png"

                        }
                        onTouch:{
                            if(event.isUp())
                            {
                                if (viewPage.showPlayMark)
                                {
                                    eventHandler.ShowVideoPlayScreen(viewPage.videoUrl);
                                }
                                
                            }
                         	   
                        }
                        background: Color.Transparent

                    }
                    
                    Container {
                        topPadding: 40
                        Label {
                            id: newsArticleContent
                            objectName: "newsArticleContent"
                            textStyle.fontSize: viewPage.showPlayMark ? FontSize.Medium: FontSize.Small
                            textStyle.fontWeight: viewPage.showPlayMark ? FontWeight.Bold: FontWeight.Default
                            textStyle.color: Color.White
                            multiline: true
                            horizontalAlignment: HorizontalAlignment.Fill
                            //textStyle.textAlign: TextAlign.Left
                        }

                    }
					
                }                
            }
        }

        //! [6] menu bar
        Container {
            id: menuBar

            layout: DockLayout {
            }

            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Bottom

            topPadding: 3.0            

            preferredHeight: 125

            Container {
                background: Color.create("#ff333333")

                layout: DockLayout {
                }

                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill

                // back button(need to modify!!!)
                Container {
                    id: staticMenuButton
                    preferredWidth: 120
                    background: Color.create("#ff222222")
                    
                    layout: DockLayout {
                    }

                    verticalAlignment: VerticalAlignment.Fill
                    horizontalAlignment: HorizontalAlignment.Left

                    topPadding: 30.0
                    bottomPadding: 15.0

                    onTouch: {
                        // processing display menu at left-top side
                        if (! event.isUp()) return;
                        if(viewPage.pagevalid == true)
                        	eventHandler.popTop();
                        viewPage.pagevalid = false;
                        
                    }

                    ImageView {
                        imageSource: "asset:///images/icon_arrow_back.png"
                        verticalAlignment: VerticalAlignment.Top
                        horizontalAlignment: HorizontalAlignment.Center
                        overlapTouchPolicy: OverlapTouchPolicy.Allow
                        scalingMethod: ScalingMethod.AspectFill
                    }

                    Label {
                        text: "Back"
                        textStyle {
                            color: Color.White
                            fontSize: FontSize.PointValue
                            fontSizeValue: 5
                            textAlign: TextAlign.Center
                        }
                        verticalAlignment: VerticalAlignment.Bottom
                        horizontalAlignment: HorizontalAlignment.Center
                        overlapTouchPolicy: OverlapTouchPolicy.Allow
                    }
                }

                // home button
                ImageView {
                    id: homeBtn
                    imageSource: "asset:///images/icon_red_home.png"
                    horizontalAlignment: HorizontalAlignment.Right
                    verticalAlignment: VerticalAlignment.Center
                    onTouch: {
                        // go to main page
                        if (! event.isUp()) return;
                        if (viewPage.pagevalid == true)
                        {
		                    homeBtnTouchAnim.play();
		                    eventHandler.popToRootPage();
                        }
                        viewPage.pagevalid = false;
                    }
                    animations: [
                        SequentialAnimation {
                            id: homeBtnTouchAnim

                            ParallelAnimation {
                                ScaleTransition {
                                    toX: 1.2
                                    toY: 1.2
                                    duration: 100
                                }

                                FadeTransition {
                                    fromOpacity: 1.0
                                    toOpacity: 0.3
                                    duration: 100
                                }
                            }

                            ParallelAnimation {
                                ScaleTransition {
                                    toX: 1.0
                                    toY: 1.0
                                    duration: 300
                                }

                                FadeTransition {
                                    toOpacity: 1.0
                                    duration: 300
                                }
                            }
                        }
                    ]

                }
            }
        } //! [6]
    }//! [0]
}
