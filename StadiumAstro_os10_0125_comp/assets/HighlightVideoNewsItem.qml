import bb.cascades 1.0
import my.library 1.0
// view model of highlight video & news info 

//preferredWidth: 768
Container {
//    topPadding: 12.0
    leftPadding: 20.0
//    bottomPadding: 12.0
    rightPadding: 20.0
    
    background: (ListItem.indexPath[0] % 2) ? Color.Black: Color.create("#ff8b8b83")      
    
    preferredWidth: 768
    
    Container {
        layout: DockLayout {}

        verticalAlignment: VerticalAlignment.Fill
        horizontalAlignment: HorizontalAlignment.Fill
        
        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }

            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Left
            
            // must be replaced with WebImageView!!!
            // useful property is 'imageLink'
            Container
            {
                layout: DockLayout {

                }
                
                topPadding: 12
                bottomPadding: 12
                WebImageViewContainer {
                    url: ListItemData.imageUrl
                    minWidth: 180
                    preferredWidth: 180
                    maxWidth: 180
//                    verticalAlignment: VerticalAlignment.Fill
                    preferredHeight: 100
//                    scalingMethod: ScalingMethod.AspectFill
                }
                ImageView{
                    imageSource: "asset:///images/icon_video_play.png"
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    visible: ListItemData.itemType == "VIDEO" ? true : false
                }
            }
            

			Container {
                layout: DockLayout {
                }                    
                
                leftMargin: 20.0
                verticalAlignment: VerticalAlignment.Fill
                
                // subject title
                Container {
                    verticalAlignment: VerticalAlignment.Top
                    horizontalAlignment: HorizontalAlignment.Fill
                    
                    topPadding: 4
                    Label {
                        text: ListItemData.subject
                        multiline: true

                        textStyle {
                            textAlign: TextAlign.Left
                            fontSize: FontSize.PointValue
                            fontSizeValue: 7
                            color: Color.White
                            fontWeight: FontWeight.Bold
                        }
                    }
                }
                                
                // time
                Container {
                    verticalAlignment: VerticalAlignment.Bottom
                    horizontalAlignment: HorizontalAlignment.Left

					bottomPadding: 12
                    Label {

                        text: ListItemData.time

                        textStyle {
                            textAlign: TextAlign.Left
                            fontSize: FontSize.PointValue
                            fontSizeValue: 5
                            color: Color.White
                        }
                    }
                }
                                    
            }
        }
    }               
}    
