import bb.cascades 1.0
import my.library 1.0
// view model of highlight video & news info
Container {
    id: itemRoot


    background: Color.DarkGray

    horizontalAlignment: HorizontalAlignment.Fill
    
    layout: DockLayout {}
    
    Container {
        leftPadding: 20.0
        rightPadding: 20.0

        preferredWidth: 768

        layout: DockLayout {}

        horizontalAlignment: HorizontalAlignment.Fill

        background: (itemRoot.ListItem.indexPath[0] % 2) ? Color.create("#ff8b8b83") : Color.Black
        Container {
            layout: DockLayout {
            }

            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill

            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }

                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Left

                Container {
                    layout: DockLayout {}

                    topPadding: 20.0
                    bottomPadding: 20.0

                    // must be replaced with WebImageView!!! and useful data is 'defaultImage128x96' property
                    WebImageViewContainer {
                        url:ListItemData.image
                        preferredWidth: 170
                        minWidth: 170.0
                        maxWidth: 170.0
                        preferredHeight: 90
                    }
                    
                    ImageView {
                        imageSource: "asset:///images/icon_video_play.png"
                        opacity: 0.8
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                    }
                }
                

                Container {
                    layout: DockLayout {
                    }

                    topPadding: 10.0
                    bottomPadding: 10.0
                    leftMargin: 20.0
                    verticalAlignment: VerticalAlignment.Fill
                    horizontalAlignment: HorizontalAlignment.Fill
                    
                    // subject title
                    Label {
                        text: ListItemData.title
                        multiline: true

                        textStyle {
                            textAlign: TextAlign.Left
                            fontSize: FontSize.PointValue
                            fontSizeValue: 6
                            color: Color.White
                            fontWeight: FontWeight.Bold
                        }
                        verticalAlignment: VerticalAlignment.Top
                        horizontalAlignment: HorizontalAlignment.Left
                        
                    }

                    // time
                    Label {
                        text: ListItemData.time
                        verticalAlignment: VerticalAlignment.Bottom
                        horizontalAlignment: HorizontalAlignment.Left
                        textStyle {
                            textAlign: TextAlign.Left
                            fontSize: FontSize.PointValue
                            fontSizeValue: 6
                            color: Color.White
                        }
                    }
                }
            }
        }
    }
}