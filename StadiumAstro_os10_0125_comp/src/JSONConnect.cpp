/* Copyright (c) 2012 Research In Motion Limited.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


#include <bb/data/JsonDataAccess>
#include "JSONConnect.hpp"
using namespace bb::cascades;
using namespace bb::data;
using namespace bb::pim::contacts;

//! [0]
JSONConnect::JSONConnect(QObject *parent)
	: QObject(parent)
{
    //if(NetworkCheck()){
    	//RequestServer(QString("http://223.25.247.242/api/1/venues/areas/"));
    //}
	networkAccessManager = new QNetworkAccessManager(this);
	SPORT_EVENTLIST = "http://customer.appxtream.com/astro.apps.cms/jsonFeed.action?service=astroSportsDataService&action=grabJsonText&p1=SportsEventList&mimeType=application/json";

	JSON_URL = "http://customer.appxtream.com/astro.apps.cms/jsonFeed.action";
	SITE_URL = "http://customer.appxtream.com/astro.apps.cms";
	PUSH_SERVER_URL = "http://customer.appxtream.com/m.push.svr2/jsonFeed.action";
	m_pParent = parent;
//! [0]
}
JSONConnect::~JSONConnect()
{
	networkAccessManager->deleteLater();
}
//! [1]
//network connect check function
bool JSONConnect::NetworkCheck(){
	QNetworkConfigurationManager m;
	if(m.isOnline()){
		return true;
	}else{
		bool available = false;
		foreach(QNetworkInterface interface,QNetworkInterface::allInterfaces())
		{
			if(interface.flags().testFlag(QNetworkInterface::IsUp))
			{
				available = true;
			}
		}
		return available;
	}
	return true;
}
//Retrieve list of all areas

//Retrieve Carlsberg Promos

bool JSONConnect::SendGetRequest(QMap<QString, QString> &params,QObject * target, const char *slot)
{
	//Make request url;
	QString url = JSON_URL;

	url.append("?");

	QMap<QString,QString>::iterator i = params.begin();
	while(i != params.end())
	{
		url.append(i.key());
		url.append("=");
		url.append(i.value());
		url.append("&");
		i++;
	}
	if(slot != NULL)
		return SendGetRequest(url, m_pParent, slot);
}
bool JSONConnect::SendGetRequest(QString url,QObject * target, const char *slot)
{
	qDebug() << "AFS  " << url;

	if(NetworkCheck())
	{
		QNetworkRequest request;
		request.setUrl( QUrl(url) );

		// Sends the HTTP request.
		QNetworkReply * reply = networkAccessManager->get(request);
		connect(reply, SIGNAL(finished()), target, slot);
		return true;
	}
	return false;
}

bool JSONConnect::SendPushRequest(QMap<QString, QString> &params,QObject * target, const char *slot)
{
	//Make request url;
	QString url = PUSH_SERVER_URL;

	url.append("?");

	QMap<QString,QString>::iterator i = params.begin();
	while(i != params.end())
	{
		url.append(i.key());
		url.append("=");
		url.append(i.value());
		url.append("&");
		i++;
	}

	if(slot != NULL)
		return SendGetRequest(url, target, slot);


}

const QString& JSONConnect::GetJsonUrl()
{
	return JSON_URL;
}
const QString& JSONConnect::GetSiteUrl()
{
	return SITE_URL;
}
//bool JSONConnect::RequestSportEventList(){
//	//network connect check
//
//	QMap<QString,QString> params;
//	params["service"] = "astroSportsDataService";
//	params["action"] = "grabJsonText";
//	params["p1"] = "SportsEventList";
//	params["mimeType"] = "application/json";
//}
