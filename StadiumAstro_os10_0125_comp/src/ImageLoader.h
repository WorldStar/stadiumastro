/*
 * ImageLoader.h
 *
 *  Created on: Jul 31, 2013
 *      Author: team2
 */

#ifndef IMAGELOADER_H_
#define IMAGELOADER_H_

#include <QNetworkAccessManager>
#include <QObject>
#include <QUrl>
class ImageLoader:public QObject {
	Q_OBJECT
public:
	ImageLoader();
	virtual ~ImageLoader();

	void pushImageUrl(QString &);

	QUrl redirectUrl(const QUrl&,
            const QUrl&);

public:
	static ImageLoader * getSharedImageLoader();

private:
	void loadImage();

private Q_SLOTS:
	void imageLoaded();
signals:
	void loadingDone();
	void loadingStart();
private:
	QNetworkAccessManager *mNetManager;
	QList<QString> urls;
	QUrl curLoadingUrl;
};

#endif /* IMAGELOADER_H_ */
