// Default empty project template
#ifndef STADIUMASTRO_H_
#define STADIUMASTRO_H_

#include <QObject>
#include <bb/cascades/AbstractPane>
#include <bb/system/InvokeRequest>
#include <bb/network/PushPayload>
#include <bb/network/PushStatus>
#include <bb/system/InvokeManager>
#include <bb/platform/NotificationDialog>

#include "PushNotificationService.hpp"
namespace bb { namespace cascades { class Application; }}

using namespace bb::cascades;
using namespace bb::system;
using namespace bb::platform;
/*!
 * @brief Application pane object
 *
 *Use this object to create and init app UI, to create context objects, to register the new meta types etc.
 */

class JSONConnect;

class StadiumAstro : public QObject
{
    Q_OBJECT
public:
    StadiumAstro(Application *app);
    virtual ~StadiumAstro();
public:
    Q_INVOKABLE void getAdsBannerListInfo();
    Q_SLOT 	void setAdsBannerListInfo();

    Q_SIGNAL void replaceToAdsScreen();
    Q_SLOT void onImageDownloaded();
    Q_INVOKABLE void openAdsLink();

    void pushNotificationHandler(PushPayload &payload);
    void openPush(QByteArray &data);
    bool appWasLaunchedFromRibbon();
public Q_SLOTS:
	void onCreateSessionCompleted(const bb::network::PushStatus &status);
	void onCreateChannelCompleted(const bb::network::PushStatus &status, const QString &token);
	void onDestroyChannelCompleted(const bb::network::PushStatus &status);
	void onRegisterToLaunchCompleted(const bb::network::PushStatus &status);
	void onUnregisterFromLaunchCompleted(const bb::network::PushStatus &status);
	void onNoPushServiceConnection();
	void onSimChanged();
    void onPushTransportReady(bb::network::PushCommand::Type command);
	void onInvoked(const bb::system::InvokeRequest &request);

public://Static functions.
	static void callBBWorld(QString &);
    static void callBrowser(QString &);
    InvokeManager *invokeManager;
private:
    NotificationDialog notificationDialog;
    JSONConnect *connector;
    AbstractPane *root;
    QVariantMap adsMap;

    PushNotificationService m_pushNotificationService;

};


#endif /* ApplicationUI_HPP_ */
