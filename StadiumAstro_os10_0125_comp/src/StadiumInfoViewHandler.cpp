#include <bb/cascades/Application>
#include <bb/cascades/Container>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/QmlDocument>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/Label>
#include <bb/cascades/ListView>
#include <bb/cascades/ScrollView>
#include <bb/cascades/ImageView>
#include <bb/cascades/StackLayout>
#include <bb/cascades/StackLayoutProperties>
#include <bb/cascades/WebView>
#include <bb/device/DisplayInfo>
#include <bb/PpsObject>
#include <bb/cascades/ScrollAnimation>

#include <QtCore/QDateTime>

#include "facebook/Facebook.hpp"
#include "facebook/FBUser.h"
#include "facebook/FBPlace.h"
#include "facebook/FBPost.h"
#include "facebook/FBComment.h"
#include "facebook/FacebookObject.h"

#include "WebImageView.h"
#include "SportsEventTab.hpp"
#include "SportsEventBar.hpp"
#include "ImageCache.hpp"
#include "ImageLoader.h"
#include "StadiumEventHandler.h"


using namespace bb::cascades;
using namespace bb::data;
using namespace bb::system;

//About, Terms of service, Privacy Policy, Other apps implementation
//
void StadiumEventHandler::onInfoSelectionChanged(SportsEventTab *old, SportsEventTab *cur)
{
	//Add select mark.
	if(old != NULL && old != cur)
	{
		old->unselect();
	}
	cur->select();

	if(cur->refKey().compare("about") == 0)
	{
		showAboutPage();
	}
	else if(cur->refKey().compare("faq") == 0)
	{
		showFAQPage();
	}
	else if(cur->refKey().compare("termservice") == 0)
	{
		showTermsOfServicePage();
	}
	else if(cur->refKey().compare("termcondition") == 0)
	{
		showTermsAndConditionPage();
	}
	else if(cur->refKey().compare("policy") == 0)
	{
		showPrivacyPolicyPage();
	}
	else if(cur->refKey().compare("otherapp") == 0)
	{
		showOtherAppPage();
	}
	emit onMenuItemTouched();
}

void StadiumEventHandler::showOtherAppPage()
{
	CreateViewPage();

	Page *viewPane = m_homePane->top();
	ImageCache::getSharedImageCache()->clearMemory();

	setViewPageTitle("Other apps by Astro");

	ListView * otherAppView = viewPane->findChild<ListView *>("otherApps");
	this->displayViewPageContent(otherAppView);

	showActivityIndicator(true);
	//Get data from server.

	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "StadiumAstroAppList");

	params.insert("p2", "BB");

	jsonNetwork->SendGetRequest(params,this, SLOT(setOtherAppsListData()));
}

void StadiumEventHandler::showAboutPage()
{

	CreateViewPage();
	setViewPageTitle("About");

	Page *viewPane = m_homePane->top();

	//Hide admob.
	Container * admob = viewPane->findChild<Container *>("admob");
	admob->setVisible(false);

	Container* infoTab = viewPane->findChild<Container *>("introView");
	WebView * webview = infoTab->findChild<WebView*>("introContent");
	webview->setHtml("<html><body style=\"font-size:36px;\"><font color=\"white\" ><p>Stadium Astro brings you all the action from your favourite sports both local and international. Missed the weekend's big game from the Barclays Premier \
    League or want to know who scored the winning goal in a Malaysian Super League match? Get it all on the app purpose-made for Malaysian sports fans! \
    </p> \
    <ul style=\"line-height:45px; list-style:url(round.png);\"> \
	<li>Video match highlights, pre and post-game interviews from the Barclays Premier League</li> \
    <li>TV schedule and channel listings in Malaysian time</li> \
    <li>Videos, news, statistics and standings for the top European football leagues tournaments and more</li> \
    <li>Local sports content (news, video and features)</li> \
    </ul> \
	</font> \
    </body> \
   </html>");

//	ScrollView *scrollView = infoTab->findChild<ScrollView*>("introScroller");
//	scrollView->zoomToPoint(0,0,2, ScrollAnimation::None);

	//Show Help button
	VisualNode *btn = viewPane->findChild<VisualNode *>("showHelp");
	btn->setVisible(true);

	this->displayViewPageContent(infoTab);
}

void StadiumEventHandler::showFAQPage()
{
	CreateViewPage();
	setViewPageTitle("FAQ");

	Page *viewPane = m_homePane->top();
	//Hide admob.
	Container * admob = viewPane->findChild<Container *>("admob");
	admob->setVisible(false);

	//passion
	//Show Help button
	VisualNode *btn = viewPane->findChild<VisualNode *>("showHelp");
	btn->setVisible(true);

	//Hide Intro Image
	VisualNode *image = viewPane->findChild<VisualNode *>("introImage");
	image->setVisible(false);

	Container* infoTab = viewPane->findChild<Container *>("introView");
	this->displayViewPageContent(infoTab);


	//Get data from server.
	showActivityIndicator(true);

	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "StadiumInfoList");

	params.insert("p2", "FAQ");

	jsonNetwork->SendGetRequest(params,this, SLOT(onGetHelpPageContent()));

}

void StadiumEventHandler::showTermsOfServicePage()
{
	CreateViewPage();

	setViewPageTitle("Terms of Service");

	Page * viewPane = m_homePane->top();

	Container* infoTab = viewPane->findChild<Container *>("introView");

	//Hide admob.
	Container * admob = viewPane->findChild<Container *>("admob");
	admob->setVisible(false);

	//Passion
	//Show Help button
	VisualNode *btn = viewPane->findChild<VisualNode *>("showHelp");
	btn->setVisible(true);

	//Hide Intro Image
	VisualNode *image = viewPane->findChild<VisualNode *>("introImage");
	image->setVisible(false);

	this->displayViewPageContent(infoTab);
	showActivityIndicator(true);
	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "StadiumInfoList");

	params.insert("p2", "TermsOfService");

	jsonNetwork->SendGetRequest(params, this, SLOT(onGetHelpPageContent()));
}

void StadiumEventHandler::showTermsAndConditionPage()
{
	CreateViewPage();

	Page *viewPane = m_homePane->top();

	setViewPageTitle("Terms and Conditions");

	Container* infoTab = viewPane->findChild<Container *>("introView");

	//Hide admob.
	Container * admob = viewPane->findChild<Container *>("admob");
	admob->setVisible(false);

	//Passion
	//Show Help button
	VisualNode *btn = viewPane->findChild<VisualNode *>("showHelp");
	btn->setVisible(true);

	//Hide Intro Image
	VisualNode *image = viewPane->findChild<VisualNode *>("introImage");
	image->setVisible(false);

	this->displayViewPageContent(infoTab);
	showActivityIndicator(true);
	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "StadiumInfoList");

	params.insert("p2", "TermsConditions");

	jsonNetwork->SendGetRequest(params, this,SLOT(onGetHelpPageContent()));
}

void StadiumEventHandler::showPrivacyPolicyPage()
{
	CreateViewPage();
	setViewPageTitle("Privacy Policy");

	Page *viewPage = m_homePane->top();

	Container* infoTab = viewPage->findChild<Container *>("introView");

	//Hide admob.
	Container * admob = viewPage->findChild<Container *>("admob");
	admob->setVisible(false);

	//Passion
	//Show Help button
	VisualNode *btn = viewPage->findChild<VisualNode *>("showHelp");
	btn->setVisible(true);

	//Hide Intro Image
	VisualNode *image = viewPage->findChild<VisualNode *>("introImage");
	image->setVisible(false);

	this->displayViewPageContent(infoTab);
	showActivityIndicator(true);

	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "StadiumInfoList");

	params.insert("p2", "PrivacyPolicy");

	jsonNetwork->SendGetRequest(params,this, SLOT(onGetHelpPageContent()));
}

void StadiumEventHandler::onGetHelpPageContent()
{
	Page *viewPage = m_homePane->top();

	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	if(reply->error()  == 0)
	{
		JsonDataAccess jsondata;

		Container* infoTab = viewPage->findChild<Container *>("introView");
		WebView * webview = infoTab->findChild<WebView*>("introContent");
		// get logo info

		QVariantMap infoList = jsondata.load(reply).toMap();

		QVariantMap content = infoList.value("description").toMap();

		QString htmlContent = content.value("description").toString();

		//Eliminate font tag.

		curHelpContent = "<html><body style=\"font-size:22px;\"><font color=\"white\" >";
		curHelpContent.append(htmlContent);
		curHelpContent.append("</font></body></html>");

		webview->setHtml(curHelpContent);
//		ScrollView *scrollView = infoTab->findChild<ScrollView*>("introScroller");
//		scrollView->zoomToPoint(0,0,2, ScrollAnimation::None);
	}
	reply->deleteLater();
	showActivityIndicator(false);
}
