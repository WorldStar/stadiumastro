/*
 * ImageLoader.cpp
 *
 *  Created on: Jul 31, 2013
 *      Author: team2
 */

#include "ImageLoader.h"
#include <bb/cascades/Image>
#include "ImageCache.hpp"
#include <QNetworkRequest>
static ImageLoader * sharedImageLoader = new ImageLoader();


ImageLoader::ImageLoader() {
	// TODO Auto-generated constructor stub
	mNetManager = new QNetworkAccessManager(this);
	curLoadingUrl = "";
}

ImageLoader::~ImageLoader() {
	// TODO Auto-generated destructor stub
	mNetManager->deleteLater();
}

void ImageLoader::pushImageUrl(QString &url)
{
	urls.push_back(url);
	if(urls.count() - 1 == 0)
	{
		loadImage();
		emit loadingStart();
	}
}
void ImageLoader::loadImage()
{
	qDebug() << "AFS" << "Load Image";
	if(urls.count()== 0)
	{
		qDebug() << "AFS" << "Loading Done";
		emit loadingDone();
		return;
	}
	qDebug() << "AFS" << "Still Loading";
	QString url = urls.front();
	Image *img = ImageCache::getSharedImageCache()->getImageFromMemory(url);
	while(img != NULL)
	{
		urls.pop_front();
		if(urls.count() == 0)
			break;
		url = urls.front();
		img = ImageCache::getSharedImageCache()->getImageFromMemory(url);
	}

	if(urls.count() != 0)
	{
		curLoadingUrl = QUrl(url);
		QNetworkReply *curReply = mNetManager->get(QNetworkRequest(curLoadingUrl));
		connect(curReply, SIGNAL(finished()), this, SLOT(imageLoaded()));
	}
	else
		emit loadingDone();
}

QUrl ImageLoader::redirectUrl(const QUrl& possibleRedirectUrl,
                               const QUrl& oldRedirectUrl) {
	QUrl redirectUrl;
	/*
	 * Check if the URL is empty and
	 * that we aren't being fooled into a infinite redirect loop.
	 * We could also keep track of how many redirects we have been to
	 * and set a limit to it, but we'll leave that to you.
	 */
	if(!possibleRedirectUrl.isEmpty() &&
	   possibleRedirectUrl != oldRedirectUrl) {
		redirectUrl = possibleRedirectUrl;
	}
	return redirectUrl;
}

void ImageLoader::imageLoaded()
{

	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	QVariant possibleRedirectUrl = reply->attribute(QNetworkRequest::RedirectionTargetAttribute);

	curLoadingUrl= redirectUrl(possibleRedirectUrl.toUrl(), curLoadingUrl);

	if(!curLoadingUrl.isEmpty())
	{
		QNetworkReply * curRequest = mNetManager->get(QNetworkRequest(curLoadingUrl));
		connect(curRequest, SIGNAL(finished()), this, SLOT(imageLoaded()));
	}
	else
	{
		qDebug() << "AFS"<< "Image loaded";
		QByteArray jpegData = reply->readAll();

		/** Save data to temp directory. **/
		QString loadedUrl = urls.front();

		Image * img = new Image(jpegData);
		ImageCache::getSharedImageCache()->saveImageToMemory(loadedUrl, img);


		//Pop front and load other image.
		curLoadingUrl = NULL;
		urls.pop_front();
		loadImage();

	}
	reply->deleteLater();
}

ImageLoader * ImageLoader::getSharedImageLoader()
{
	return sharedImageLoader;
}
