/*
 * StadiumEventHandler.cpp
 *
 *  Created on: Jun 9, 2013
 *      Author: Star
 */
#include <bb/cascades/Application>
#include <bb/cascades/Container>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/QmlDocument>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/Label>
#include <bb/cascades/ListView>
#include <bb/cascades/ScrollView>
#include <bb/cascades/ImageView>
#include <bb/cascades/StackLayout>
#include <bb/cascades/StackLayoutProperties>
#include <bb/cascades/WebView>
#include <bb/device/DisplayInfo>
#include <bb/PpsObject>
#include <bb/system/SystemDialog>

#include <QtCore/QDateTime>

#include "facebook/Facebook.hpp"
#include "facebook/FBUser.h"
#include "facebook/FBPlace.h"
#include "facebook/FBPost.h"
#include "facebook/FBComment.h"
#include "facebook/FacebookObject.h"

#include "WebImageView.h"
#include "SportsEventTab.hpp"
#include "SportsEventBar.hpp"
#include "ImageCache.hpp"
#include "ImageLoader.h"
#include "StadiumEventHandler.h"

using namespace bb::cascades;
using namespace bb::data;
using namespace bb::system;

void StadiumEventHandler::ShowVideoPlayScreen(QString url)
{
	if (url.length() == 0)
	{
		SystemDialog *dialog = new SystemDialog("OK");
		dialog->setTitle("Stadium Astro");
		dialog->setBody("Invalid video url.");
		dialog->show();
		return;
	}

	CreateVideoViewPage();
	Page * viewPane = m_homePane->top();

	viewPane->setProperty("videoUrl" , url);

	emit playVideo();

	// TODO: request real video data from data-param's iPad/iPhoneVideoUrl property

}


void StadiumEventHandler::CreateVideoViewPage()
{
	QmlDocument *qml = QmlDocument::create("asset:///VideoPlayScreen.qml").parent(this);


	if(!qml->hasErrors()) {
		// create a event handler object
		qml->setContextProperty("eventHandler", this);

		// create root object for the UI
		Page *playScreen = qml->createRootObject<Page>();
		m_homePane->push(playScreen);
	}
}

