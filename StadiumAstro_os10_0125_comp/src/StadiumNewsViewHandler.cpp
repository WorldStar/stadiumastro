/*
 * StadiumEventHandler.cpp
 *
 *  Created on: Jun 9, 2013
 *      Author: Star
 */
#include <bb/cascades/Application>
#include <bb/cascades/Container>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/QmlDocument>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/Label>
#include <bb/cascades/ListView>
#include <bb/cascades/ScrollView>
#include <bb/cascades/ImageView>
#include <bb/cascades/StackLayout>
#include <bb/cascades/StackLayoutProperties>
#include <bb/cascades/WebView>
#include <bb/device/DisplayInfo>
#include <bb/PpsObject>
#include <bb/system/SystemDialog>

#include <QtCore/QDateTime>

#include "facebook/Facebook.hpp"
#include "facebook/FBUser.h"
#include "facebook/FBPlace.h"
#include "facebook/FBPost.h"
#include "facebook/FBComment.h"
#include "facebook/FacebookObject.h"

#include "WebImageView.h"
#include "SportsEventTab.hpp"
#include "SportsEventBar.hpp"
#include "ImageCache.hpp"
#include "ImageLoader.h"
#include "StadiumEventHandler.h"

#include <bb/system/SystemDialog>

using namespace bb::cascades;
using namespace bb::data;
using namespace bb::system;

//News page implementation
//------------------------

void StadiumEventHandler::ShowNewsCategoryPage()
{
	CreateViewPage();

	setViewPageTitle("News");

	//For Refresh(Passion)
	setViewPageIdentifier(m_sportEventsContent_refKey);
	setViewPageType("NewsCategoryList");

	Page *viewPane = m_homePane->top();
	VisualNode *btn = viewPane->findChild<VisualNode *>("pageRefresh");
	btn->setVisible(true);


	// TODO: request 'News Category List' info to server, see below for its call back method
	// reference info is 'm_sportEventsContent_refKey' when request data
	QMap < QString, QString > params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "NewsCategoryList");

	params.insert("p2", m_sportEventsContent_refKey);

	jsonNetwork->SendGetRequest(params, this, SLOT(setNewsCategoryInfo()));
}

void StadiumEventHandler::requestNewsCategoryInfo(QString strId)
{
	// TODO: request 'News Category List' info to server, see below for its call back method
	// reference info is 'm_sportEventsContent_refKey' when request data
	QMap < QString, QString > params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "NewsCategoryList");

	params.insert("p2", strId);

	jsonNetwork->SendGetRequest(params, this, SLOT(setNewsCategoryInfo()));
}

void StadiumEventHandler::setNewsCategoryInfo()
{
	Page * viewPane = m_homePane->top();

	JsonDataAccess jsondata;
	QVariantList varList;

	//For Refresh(Passion)
	showActivityIndicator(false);

	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	if (reply->error() == 0) {
		// get category info
		QVariantMap varMap = jsondata.load(reply).value<QVariantMap>();
		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(
							err.errorMessage());
			return;
		}
		varList = varMap.value("newsCategoryContents").toList();

		// prepare data model
		GroupDataModel *newsCategoryModel = new GroupDataModel();
		newsCategoryModel->setParent(this);

		// reverse list data and set into model
		QVariantList finalList;
		finalList.clear();
		for (int i = varList.count() - 1; i >= 0; i--) {
			finalList.append(varList.at(i));
		}

		newsCategoryModel->insertList(finalList);
		newsCategoryModel->setGrouping(ItemGrouping::None);

		// set preview list model
		ListView *newsCategoryList = viewPane->findChild<ListView*>(
				"newsCategoryList");
		newsCategoryList->setDataModel(newsCategoryModel);

		// show to screen
		displayViewPageContent(newsCategoryList);
	}
	reply->deleteLater();
}

void StadiumEventHandler::ShowNewsFeedListPage(QString strKey, QString strTitle)
{
	CreateViewPage();

	Page *viewPane = m_homePane->top();
	ImageCache::getSharedImageCache()->clearMemory();

	setViewPageTitle(strTitle);

	//For Refresh(Passion)
	setViewPageIdentifier(strKey);
	setViewPageType("NewsFeedList");
	VisualNode *btn = viewPane->findChild<VisualNode *>("pageRefresh");
	btn->setVisible(true);

	// TODO: request 'News Feed List' info to server, see below for its call back function
	// reference info is strKey
	QMap < QString, QString > params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "NewsFeedList");

	params.insert("p2", strKey);

	jsonNetwork->SendGetRequest(params, this, SLOT(setNewsFeedListInfo()));

	showActivityIndicator(true);
}

void StadiumEventHandler::requestNewsFeedListInfo(QString strId)
{
	// TODO: request 'News Feed List' info to server, see below for its call back function
	// reference info is strKey
	QMap < QString, QString > params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "NewsFeedList");

	params.insert("p2", strId);

	jsonNetwork->SendGetRequest(params, this, SLOT(setNewsFeedListInfo()));
}

void StadiumEventHandler::setNewsFeedListInfo()
{
	Page * viewPane = m_homePane->top();
	JsonDataAccess jsondata;
	QVariantList varList;

	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());

	showActivityIndicator(false);
	if (reply->error() == 0) {
		// get map data from json response
		//varList = jsondata.load(QDir::currentPath() + "/app/native/assets/models/newsFeedListModel.json").value<QVariantList>();
		QVariantMap varMap =
				jsondata.load(
						reply).value<
						QVariantMap>();
		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(
							err.errorMessage());
			return;
		}

		varList = varMap.value("newsFeedContents").toList();

		// prepare data model
		GroupDataModel *newsFeedListModel = new GroupDataModel();
		newsFeedListModel->setParent(this);

		QVariantList::Iterator item = varList.begin();
		while (item != varList.end()) {
			QVariantMap itemMap = (*item).toMap();

			// customize image content
			// In fact, there's no need to add this item, use 'imageLink' item instead.
			itemMap["image"] = QUrl(itemMap["imageLink"].toString());

			// produce date string and add it to data map
			QDateTime dateTime = QDateTime::fromString(
					itemMap["strPublishDate"].toString(), Qt::ISODate);
			itemMap["time"] = dateTime.toString("dddd,  dd  MMM  yyyy h:mm AP");
			itemMap["type"] = "NEWSFEED";
			// save to map data
			(*item) = itemMap;

			++item;
		}

		// reverse list data and set into model
		QVariantList finalList;
		finalList.clear();
		for (int i = varList.count() - 1; i >= 0; i--) {
			finalList.append(varList.at(i));
		}

		newsFeedListModel->insertList(finalList);
		newsFeedListModel->setGrouping(ItemGrouping::None);

		// set video feed list model
		ListView *newsFeedList = viewPane->findChild<ListView*>(
				"newsFeedList");
		newsFeedList->setDataModel(newsFeedListModel);

		// show
		displayViewPageContent(newsFeedList);
	}
	reply->deleteLater();
}
void StadiumEventHandler::ShowNewsViewPage(QVariant data)
{
	OnShowNewsViewPage(data);
}

void StadiumEventHandler::OnShowNewsViewPage(QVariant data)
{
	CreateNewsViewPage();

	setNewsViewPageInfo(data);
}

void StadiumEventHandler::CreateNewsViewPage()
{
	QmlDocument *qml = QmlDocument::create("asset:///NewsViewPage.qml");

	if(!qml->hasErrors()) {
		// create a event handler object
		qml->setContextProperty("eventHandler", this);

		// create root object for the UI
		Page *viewPane = qml->createRootObject<Page>();
		qml->setParent(viewPane);

		if(viewPane){
			// set as member variable value
			m_homePane->push(viewPane);
		}
	}
}

void StadiumEventHandler::getAdsBannerInfoForNews()
{
	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "AdsBannerList");

	params.insert("p2", "BPL");


	jsonNetwork->SendGetRequest(params, this, SLOT(setAdsBannerInfoForNews()));
}

void StadiumEventHandler::setAdsBannerInfoForNews()
{
	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	if(reply->error()  == 0 && reply->bytesAvailable() > 0)
	{
		JsonDataAccess jsondata;

		// get logo info

		QVariantMap varMap = jsondata.load(reply).toMap();

		admobList = varMap.value("adsBannerContents").toList();

		// set image to UI
		if(admobList.count() > 0)
		{
			QString strImgPath =
					admobList.at(0).toMap().value("adsBannerMobileFile").toString();

			// TODO: add server url to image path and request image file content, please remove comment
			int nIdx = strImgPath.indexOf("path=") + 5;
			QString strFilePath = "http://astroapps.static.appxtream.com/" + strImgPath.mid(nIdx);

//			QString siteUrl(jsonNetwork->GetSiteUrl());
//			siteUrl.append(strFilePath);

			Page *viewPane = m_homePane->top();
			WebImageView *imgAdmob = viewPane->findChild<WebImageView*>(
					"imgAdmob");
			imgAdmob->setUrl(QUrl(strFilePath));
		}
	}
	reply->deleteLater();
}

void StadiumEventHandler::setNewsViewPageInfo(QVariant data)
{
	Page *viewPane = m_homePane->top();
	// convert into map
	QVariantMap dataMap = data.toMap();

	// set title string
	QString caption = dataMap.value("type").toString();
	viewPane->setProperty("caption", caption);

	QString strTitle = dataMap.value("title").toString();
	viewPane->setProperty("title", strTitle);

//	//By PASSION
//	SystemDialog *dialog = new SystemDialog("OK");
//	dialog->setTitle(caption);
//	dialog->setBody(QVariant(caption.length()).toString());
//	dialog->show();

	// set date & time string
	QDateTime dateTime = QDateTime::fromString(dataMap["strPublishDate"].toString(), Qt::ISODate);
	QString strDateTime = dateTime.toString("dddd,  dd  MMM  yyyy  h:mm AP");
	viewPane->setProperty("dateTime", strDateTime);

	// set image path
	QString strImagePath = dataMap["imageLink"].toString();
	if(strImagePath.isEmpty())
		strImagePath = dataMap["defaultImage128x96"].toString();
	viewPane->setProperty("url", QUrl(strImagePath));

	// set content

	QString strContent;
	QString videoUrl;
	if((caption.compare("NEWS") == 0) || (caption.compare("") == 0) || (caption.compare("NEWSFEED") == 0))
	{
		if (caption.compare("NEWS") == 0) viewPane->setProperty("caption", "Latest News");
		if (caption.compare("NEWSFEED") == 0) viewPane->setProperty("caption", "News");
		viewPane->setProperty("showPlayMark", false);
		strContent = dataMap["fullBody"].toString();
		viewPane->setProperty("showBanner", true);

		getAdsBannerInfoForNews();
	}
	else
	{
		viewPane->setProperty("caption", "");
		strContent = dataMap["description"].toString();
		videoUrl = dataMap["iphoneVideoUrl"].toString();

		viewPane->setProperty("showPlayMark", true);
		viewPane->setProperty("videoUrl", videoUrl);
		viewPane->setProperty("showBanner", false);
	}

	strContent.replace(QString("\\/"), QString("\\"));
	strContent.replace(QString("<br>"), QString("<br />"));
	strContent ="<html>" + strContent + "</html>";
	viewPane->setProperty("newsContent", strContent);
}
