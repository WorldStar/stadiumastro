/*
 * WebImageView.h
 */

#include <bb/cascades/ImageView>
#include <QtNetwork/QNetworkAccessManager>
#include <QUrl>
#include <bb/cascades/Container>
#include "StadiumEventHandler.h"

#include <QMutex>
#ifndef WEBIMAGEVIEW_H_
#define WEBIMAGEVIEW_H_

using namespace bb::cascades;

class QmlEventHandler;
class StadiumEventHandler;

class WebImageView: public bb::cascades::ImageView {
	Q_OBJECT
	Q_PROPERTY (QUrl url READ url WRITE setUrl NOTIFY urlChanged)
	Q_PROPERTY (QUrl defaultImageSource READ defaultImageSource WRITE setDefaultImageSource NOTIFY defaultImageSourceChanged)

public:
	WebImageView();
	~WebImageView();
	const QUrl& url() const;
	const QUrl& defaultImageSource() const;
//	const QString clickable() const;

public:
	void setUrl(const QUrl& url);
	void setDefaultImageSource(const QUrl& url);

public Q_SLOTS:

	void setClickable(const bool clickable);
	void loadingDone();

private Q_SLOTS:

	void imageLoaded();



	Q_INVOKABLE void onTouch(bb::cascades::TouchEvent*);

	signals:
	void urlChanged();
	void imageDownloaded();
	void defaultImageSourceChanged();

public:
	void setEventHandler(StadiumEventHandler*);
	void setLargeImageUrl(const QUrl& url);
	const QUrl& largeImageUrl();
	QUrl redirectUrl(const QUrl&, const QUrl &);
private:
	QNetworkReply * curRequest;

	QUrl mUrl, m_LargeUrl, mDefaultImageSource;

	bool m_Clickable;
	StadiumEventHandler* m_EventHandler;
	QUrl _oldRedirectUrl;

	QMutex mutex;
};

#endif /* WEBIMAGEVIEW_H_ */
