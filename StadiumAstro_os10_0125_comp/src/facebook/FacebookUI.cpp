/*
 * FacebookUI.cpp
 *
 *  Created on: Aug 1, 2013
 *      Author: team2
 */

#include "FacebookUI.h"

static Dialog * facebookLoginDlg = NULL;
Dialog * loginDialog()
{
	if(facebookLoginDlg == NULL)
	{
		QmlDocument *qml = QmlDocument::create("asset:///FacebookLoginDlg.qml");
		facebookLoginDlg = qml->createRootObject<Dialog>();
	}
	return facebookLoginDlg;
}
