/*
 * FacebookUI.h
 *
 *  Created on: Aug 1, 2013
 *      Author: team2
 */

#ifndef FACEBOOKUI_H_
#define FACEBOOKUI_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Dialog>
#include <bb/cascades/WebView>
#include <bb/cascades/WebStorage>
#include <bb/cascades/WebSettings>

using namespace bb::cascades;

Dialog * loginDialog();

#endif
