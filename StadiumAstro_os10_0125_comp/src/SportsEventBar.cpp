/*
 * SportsEventBar.cpp
 *
 *  Created on: Jul 11, 2013
 *      Author: team2
 */
#include "SportsEventTab.hpp"
#include "SportsEventBar.hpp"

SportsEventBar::SportsEventBar() {
	// TODO Auto-generated constructor stub
	selected = -1;
}

SportsEventBar::~SportsEventBar() {
	// TODO Auto-generated destructor stub
}

void SportsEventBar::addEventTab(SportsEventTab *evtTab)
{
	tabs.append(evtTab);
	connect(evtTab, SIGNAL(click(bb::cascades::TapEvent *)), this, SLOT(onTabTapped()));

}

void SportsEventBar::onTabTapped()
{
	SportsEventTab * tab = qobject_cast<SportsEventTab *>(sender());
	int curSel;

	curSel = 0;
	QList<SportsEventTab *>::iterator i = tabs.begin();
	while(i != tabs.end())
	{
		if(*i == tab)
		{
			break;
		}
		curSel++;
		i++;
	}

	trigger(curSel);
}

void SportsEventBar::trigger(int idx)
{
	if(idx < tabs.count())
	{

		SportsEventTab *old = NULL;
		if(selected != -1)
		{
			old = tabs[selected];
		}

		SportsEventTab *cur = tabs[idx];
//		if(cur->refKey() == "STADIUMASTROFANTASY" || cur->refKey() == "PIALAMSIALIGAFANTASI")
//		{
//			StadiumAstro::callBrowser("http://www.blackberry.com");
//			//({
////			            uri: "http://www.blackberry.com"
////			        }, onInvokeSuccess, onInvokeError);
//		}
//		else
		{
			emit selectionChanged(old, cur);
		}

		selected = idx;
	}
}

void SportsEventBar::addSelectMark(int idx)
{
	tabs[idx]->select();

}

void SportsEventBar::removeSelectMark(int idx)
{
	tabs[idx]->unselect();
}

void SportsEventBar::removeAllSelectMark()
{
	QList<SportsEventTab *>::iterator i = tabs.begin();
	while(i != tabs.end())
	{
		(*i)->unselect();
		i++;
	}
}
