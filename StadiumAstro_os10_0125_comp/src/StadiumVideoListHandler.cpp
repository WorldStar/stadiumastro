/*
 * StadiumVideoListHandler.cpp
 *
 *  Created on: Jun 9, 2013
 *      Author: Star
 */
#include <bb/cascades/Application>
#include <bb/cascades/Container>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/QmlDocument>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/Label>
#include <bb/cascades/ListView>
#include <bb/cascades/ScrollView>
#include <bb/cascades/ImageView>
#include <bb/cascades/StackLayout>
#include <bb/cascades/StackLayoutProperties>
#include <bb/cascades/WebView>
#include <bb/device/DisplayInfo>
#include <bb/PpsObject>

#include <QtCore/QDateTime>

#include "facebook/Facebook.hpp"
#include "facebook/FBUser.h"
#include "facebook/FBPlace.h"
#include "facebook/FBPost.h"
#include "facebook/FBComment.h"
#include "facebook/FacebookObject.h"

#include "WebImageView.h"
#include "SportsEventTab.hpp"
#include "SportsEventBar.hpp"
#include "ImageCache.hpp"
#include "ImageLoader.h"
#include "StadiumEventHandler.h"


using namespace bb::cascades;
using namespace bb::data;
using namespace bb::system;

//Video Category and list view.
//---------------------------

void StadiumEventHandler::ShowVideoCategoryPage()
{
	CreateViewPage();

	setViewPageTitle("Videos");


	Page *viewPane = m_homePane->top();

	//For Refresh(Passion)
	setViewPageIdentifier(m_sportEventsContent_refKey);
	setViewPageType("VideoCategoryList");
	VisualNode *btn = viewPane->findChild<VisualNode *>("pageRefresh");
	btn->setVisible(true);


	// TODO: request 'Video Category List' info to server, see below for its call back method

	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "VideoCategoryList");

	params.insert("p2", m_sportEventsContent_refKey);

	jsonNetwork->SendGetRequest(params, this, SLOT(setVideoCategoryInfo()));

}

void StadiumEventHandler::requestVideoCategoryInfo(QString strId)
{
	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "VideoCategoryList");

	params.insert("p2", strId);

	jsonNetwork->SendGetRequest(params, this, SLOT(setVideoCategoryInfo()));
}


void StadiumEventHandler::setVideoCategoryInfo() {

	Page *viewPane = m_homePane->top();

	JsonDataAccess jsondata;
	QVariantList varList;

	//For Refresh(Passion)
	showActivityIndicator(false);

	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	if (reply->error() == 0) {
		// get preview info
		//varList = jsondata.load(QDir::currentPath() + "/app/native/assets/models/videoCategoryModel.json").value<QVariantList>();
		QVariantMap varMap =
				jsondata.load(
						reply
						).value<
						QVariantMap>();
		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg = tr(
					"Error converting Qt data to JSON: %1").arg(
					err.errorMessage());
			return;
		}

		varList = varMap.value("videoCategoryContents").toList();

		// prepare data model
		GroupDataModel *videoCategoryModel = new GroupDataModel();
		videoCategoryModel->setParent(this);

		// reverse list data and set into model
		QVariantList finalList;
		finalList.clear();
		for (int i = varList.count() - 1; i >= 0; i--) {
			finalList.append(varList.at(i));
		}

		videoCategoryModel->insertList(finalList);
		videoCategoryModel->setGrouping(ItemGrouping::None);

		// set preview list model
		ListView *videoCategoryList = viewPane->findChild<ListView*>(
				"videoCategoryList");
		videoCategoryList->setDataModel(videoCategoryModel);

		// show to screen
		displayViewPageContent(videoCategoryList);
	}
	reply->deleteLater();
}

void StadiumEventHandler::ShowVideoFeedListPage(QString strKey)
{
	CreateViewPage();

	Page *viewPane = m_homePane->top();
	ImageCache::getSharedImageCache()->clearMemory();
	setViewPageTitle("Videos");

	//For Refresh(Passion)
	setViewPageIdentifier(strKey);
	setViewPageType("VideosFeedList");
	VisualNode *btn = viewPane->findChild<VisualNode *>("pageRefresh");
	btn->setVisible(true);

	// TODO: request 'Video Feed List' with strKey
	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "VideosFeedList");

	params.insert("p2", strKey);

	jsonNetwork->SendGetRequest(params, this, SLOT(setVideoFeedListInfo()));

	showActivityIndicator(true);

}

void StadiumEventHandler::requestVideoFeedListInfo(QString strId)
{
	// TODO: request 'Video Feed List' with strKey
		QMap<QString, QString> params;

		params.insert("service", "astroSportsDataService");

		params.insert("action", "grabJsonText");

		params.insert("mimeType", "application/json");

		params.insert("p1", "VideosFeedList");

		params.insert("p2", strId);

		jsonNetwork->SendGetRequest(params, this, SLOT(setVideoFeedListInfo()));
}

void StadiumEventHandler::setVideoFeedListInfo()
{
	Page * viewPane = m_homePane->top();
	JsonDataAccess jsondata;
	QVariantList varList;


	showActivityIndicator(false);
	// get preview info
	//varList = jsondata.load(QDir::currentPath() + "/app/native/assets/models/videoFeedListModel.json").value<QVariantList>();
	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	if (reply->error() == 0) {

		QVariantMap varMap =
				jsondata.load(
						reply).value<
						QVariantMap>();
		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(
							err.errorMessage());
			return;
		}

		qDebug() << "VideoFeedListInfo";

		varList = varMap.value("videoNewsContents").toList();

		// prepare data model
		GroupDataModel *videoFeedListModel = new GroupDataModel();
		videoFeedListModel->setParent(this);

		QVariantList::Iterator item = varList.begin();
		while (item != varList.end()) {
			QVariantMap itemMap = (*item).toMap();

			// customize image content
			// In fact, there's no need to add this item, use 'imageLink' item instead.
			itemMap["image"] = QUrl(itemMap["defaultImage128x96"].toString());

			// produce date string and add it to data map
			QDateTime dateTime = QDateTime::fromString(
					itemMap["creationDate"].toString(), Qt::ISODate);
			itemMap["time"] = dateTime.toString("dddd,  dd  MMM  yyyy  h:mm  AP");

			//BY PASSION
			itemMap["type"] = "VIDEOS";

			// save to map data
			(*item) = itemMap;

			++item;
		}

		// reverse list data and set into model
		QVariantList finalList;
		finalList.clear();
		for (int i = varList.count() - 1; i >= 0; i--) {
			finalList.append(varList.at(i));
		}

		videoFeedListModel->insertList(finalList);
		videoFeedListModel->setGrouping(ItemGrouping::None);

		// set video feed list model
		ListView *videoFeedList = viewPane->findChild<ListView*>(
				"videoFeedList");
		videoFeedList->setDataModel(videoFeedListModel);

		// show
		displayViewPageContent(videoFeedList);
	}
	reply->deleteLater();
}
