/*
 * SportsEventBar.h
 *
 *  Created on: Jul 11, 2013
 *      Author: team2
 */

#ifndef SPORTSEVENTBAR_H_
#define SPORTSEVENTBAR_H_

#include <QObject>
#include <QList>


/** Manage event bar. Watches tap event and trigger selection change event. **/
class SportsEventTab;

class SportsEventBar:public QObject {
	Q_OBJECT

public:
	SportsEventBar();
	virtual ~SportsEventBar();

	void addEventTab(SportsEventTab * evtTab);

	void trigger(int idx);

	void addSelectMark(int idx);
	void removeSelectMark(int idx);
	void removeAllSelectMark();

public slots:
	void onTabTapped();

signals:
	void selectionChanged(SportsEventTab *old, SportsEventTab *cur);
private:

	int selected;
	QList<SportsEventTab *> tabs;
};

#endif /* SPORTSEVENTBAR_H_ */
