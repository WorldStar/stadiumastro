/*
 * StadiumEventHandler.cpp
 *
 *  Created on: Jun 9, 2013
 *      Author: Star
 */
#include <bb/cascades/Application>
#include <bb/cascades/Container>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/QmlDocument>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/Label>
#include <bb/cascades/ListView>
#include <bb/cascades/ScrollView>
#include <bb/cascades/ImageView>
#include <bb/cascades/StackLayout>
#include <bb/cascades/StackLayoutProperties>
#include <bb/cascades/WebView>
#include <bb/device/DisplayInfo>
#include <bb/PpsObject>

#include <QtCore/QDateTime>

#include "facebook/Facebook.hpp"
#include "facebook/FBUser.h"
#include "facebook/FBPlace.h"
#include "facebook/FBPost.h"
#include "facebook/FBComment.h"
#include "facebook/FacebookObject.h"

#include "WebImageView.h"
#include "SportsEventTab.hpp"
#include "SportsEventBar.hpp"
#include "ImageCache.hpp"
#include "ImageLoader.h"
#include "StadiumEventHandler.h"


using namespace bb::cascades;
using namespace bb::data;
using namespace bb::system;

//Other app page screen.

void StadiumEventHandler::setOtherAppsListData()
{
	// define variables
	Page *viewPane = m_homePane->top();
	JsonDataAccess jsondata;
	QVariantList appList;

	// get logo info
	showActivityIndicator(false);
	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	if (reply->error() == 0) {

		QVariantMap varMap =
				jsondata.load(
						reply
						).value<
						QVariantMap>();

		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(
							err.errorMessage());
			return;
		}

		appList = varMap["appContents"].toList();

		// prepare data model
		GroupDataModel *appListModel = new GroupDataModel();
		appListModel->setParent(this);

		// Iterate over all the items in the received data and rearrange data
		QVariantList finalList;
		QVariantList::iterator i = appList.begin();

		QString siteUrl;

		while(i != appList.end())
		{
			QVariantMap map = (*i).toMap();
			siteUrl = jsonNetwork->GetSiteUrl();

			siteUrl.append(map.value("appLogoURL").toString());
			map["appLogoURL"] = QUrl(siteUrl);


			finalList.insert(0,map);
			i++;
		}

		// set list data model
		appListModel->insertList(finalList);
		appListModel->setGrouping(ItemGrouping::None);

		ListView *otherAppList = viewPane->findChild<ListView*>(
				"otherApps");
		otherAppList->setDataModel(appListModel);
	}
	reply->deleteLater();
}
void StadiumEventHandler::triggerAppItem(QString appUrl)
{
	StadiumAstro::callBBWorld(appUrl);
}
