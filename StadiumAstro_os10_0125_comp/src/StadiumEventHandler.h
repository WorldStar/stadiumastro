/*
 * StadiumEventHandler.h
 *
 *  Created on: Jun 9, 2013
 *      Author: Star
 */
#include "StadiumAstro.h"
#include "JSONConnect.hpp"
#include "AstroRegisterService.h"

#include <bb/cascades/Control>
#include <bb/cascades/Container>
#include <bb/cascades/Dialog>
#include <bb/cascades/NavigationPane>

#include <bb/system/InvokeRequest>
#include <bb/system/InvokeManager>


#ifndef STADIUMEVENTHANDLER_H_
#define STADIUMEVENTHANDLER_H_

#define APP_VERSION QString("v1.0.1")

using namespace bb::cascades;
using namespace bb::system;



class SportsEventTab;
class SportsEventBar;
class WebImageView;
class Facebook;

class StadiumEventHandler : public QObject {
	Q_OBJECT
public:
	StadiumEventHandler();
	StadiumEventHandler(QObject *);
	virtual ~StadiumEventHandler();

public: // signals and slots definition

	// relative with getting sports event list at start
	Q_INVOKABLE void getSportsEventsList();
	Q_SLOT void onGetSportsEventList();

	// relative with main home page
	Q_INVOKABLE void ShowHome();
	Q_SLOT void OnShowHome();

	// coding implementation of highlight news images' change
	Q_INVOKABLE void ChangeLogoFrame();
	Q_SLOT void onLogoListActivationChanged(const QVariantList, bool);

	Q_INVOKABLE void getAdsBannerListInfo();
	Q_SLOT void setAdsBannerListInfo();

	//
	Q_INVOKABLE void getAdsBannerInfoForNews();
	Q_SLOT void setAdsBannerInfoForNews();

	Q_INVOKABLE void getSoccerMatchListInfo();
	Q_SLOT void setSoccerMatchListInfo();

	Q_INVOKABLE void getHighlightNewsListInfo();
	Q_SLOT void setHighlightNewsListInfo();

	Q_INVOKABLE void getFeatureNewsListInfo();
	Q_SLOT void setFeatureNewsListInfo();

	// video category page
	Q_INVOKABLE void ShowVideoCategoryPage();
	void requestVideoCategoryInfo(QString strId);

	// video feed list page
	Q_INVOKABLE void ShowVideoFeedListPage(QString strKey);
	Q_SLOT void setVideoFeedListInfo();
	void requestVideoFeedListInfo(QString strId);

	// video play screen
	Q_INVOKABLE void ShowVideoPlayScreen(QString data);

	// news category page
	Q_INVOKABLE void ShowNewsCategoryPage();
	Q_SLOT void setNewsCategoryInfo();
	void requestNewsCategoryInfo(QString strId);

	// for video category page
	void getVideoCategoryinfo();
	Q_SLOT void setVideoCategoryInfo();

	// news feed list page(PASSION)
	Q_INVOKABLE void ShowNewsFeedListPage(QString strKey, QString strTitle);
	Q_SLOT void setNewsFeedListInfo();
	void requestNewsFeedListInfo(QString strId);

	// news view list page
	Q_INVOKABLE void ShowNewsViewPage(QVariant data);
	Q_SLOT void OnShowNewsViewPage(QVariant data);

	// matches page
	Q_INVOKABLE void ShowMatchesPage();
	Q_SLOT void setMatchesPageInfo();
	void requestMatchListInfo(QString strId);

	Q_INVOKABLE void getMatchResultListData(QString);
	Q_SLOT void updateMatchResultListData();

	// analysis page
	Q_INVOKABLE void ShowAnalysisPage(QVariant);

	Q_INVOKABLE void requestMatchPreviewInfo(QString strMatchId);
	Q_SLOT void setMatchPreviewInfo();

	Q_INVOKABLE void requestMatchReportInfo(QString strMatchId);
	Q_SLOT void setMatchReportInfo();

	Q_INVOKABLE void requestMatchLineUpInfo(QString strMatchId);
	Q_SLOT void setMatchLineUpInfo();

	Q_INVOKABLE void requestMatchEventsInfo(QString strMatchId);
	Q_SLOT void setMatchEventsInfo();

	Q_INVOKABLE void requestMatchVideoHighlightsInfo(QString strMatchId);
	Q_SLOT void setMatchVideoHighlightsInfo();

	Q_INVOKABLE void requestInMatchClipsInfo(QString strMatchId);
	Q_SLOT void setInMatchClipsInfo();

	// standings page
	Q_INVOKABLE void ShowStandingsPage();
	Q_SLOT void setStandingsPageInfo();
	void requestStandingInfo(QString strId);

	// standings page
	Q_INVOKABLE void ShowMorePage();
	Q_SLOT void OnShowMorePage();

	// team's logo table
	Q_INVOKABLE void getTeamsLogoInfo();
	Q_SLOT void setTeamsLogoInfo();
	QString getTeamLogoUrl(QString teamId, QString logoimageSize);



	// TV Channel Event
	Q_INVOKABLE void ShowChannelEventPage(QVariant channelData);
	Q_SLOT void OnShowChannelEventPage();
	void requestChannelEventInfo(QString strId);

	Q_INVOKABLE void getChannelEventListData(QString channelCalendarId);
	Q_SLOT void setChannelEventListData();

	//Invoked when sports event item was tapped.
	Q_SLOT void onEventSelectionChanged(SportsEventTab *, SportsEventTab *);

	//Invoked when information tab item was tapped.
	Q_SLOT void onInfoSelectionChanged(SportsEventTab *, SportsEventTab *);

	//Invoked when feedback tab was tapped.
	Q_SLOT void onButtonSelectionChanged(SportsEventTab *, SportsEventTab *);

	// TV Channel list
	Q_INVOKABLE void ShowTVChannelListPage();
	Q_SLOT void setTVChannelListPageInfo();
	void requestTVChannelListInfo(QString strId);

	Q_SLOT void setTVChannelCalendarListInfo();

	// Other app list by Astro.
	Q_SLOT void setOtherAppsListData();

	//Invoked when feedback setting or reporting issue was clicked.
	Q_INVOKABLE void sendEmail(QString &);

	Q_INVOKABLE void openFacebook();
	//On facebook login callback
	Q_SLOT void onFacebookLoginComplete(bool blogin);

	//Scroll match calendar list to end.
	Q_SIGNAL void scrollCalendarListToEnd();

	//When user touch menu item
	Q_SIGNAL void onMenuItemTouched();

	//Reset red menu animations.
	Q_SIGNAL void resetRedMenuForFootball();
	Q_SIGNAL void resetRedMenuForOthers();

	//When user click close button.
	Q_INVOKABLE void onHideAdmob();
	//Invoked when touch admob
	Q_INVOKABLE void onTouchAdmob();

	//When get help page content
	Q_SLOT void onGetHelpPageContent();

	//For Help Page.
	Q_INVOKABLE void showHelpPage();

	//Timer callback;
	Q_SLOT void oneSecUpdate();

	//Push notification page
	Q_INVOKABLE void showPushSetting();

	//Tell a friend
	Q_INVOKABLE void tellFriendByEmail();
	Q_INVOKABLE void tellFriendByBBM();

	//Callback when page navigation was occurred.
	Q_SLOT void onTopPageChanged(bb::cascades::Page *newPage);

	Q_INVOKABLE void enablePushForItem(QString, bool );
	//Page Navigation.
	Q_INVOKABLE void popTop();
	Q_INVOKABLE void popToRootPage();

	//Push Request Callback
	Q_SLOT void onGetPushStatus(QVariant);
	Q_SLOT void changedEventPush(QString, bool);
	Q_SLOT void registerRequestFailed(QString requestUrl);

	//Other app list
	Q_INVOKABLE void triggerAppItem(QString appUrl);

	//Select special tab in analysis page
	Q_SIGNAL void selectAnalysisTab(Container * container);
	Q_SIGNAL void selectFirstTab();

	//Refresh content of current page.
	Q_INVOKABLE void refreshCurPage(QString pageTitle, QString identifier, QString pageType);

	//Video play
	Q_SIGNAL void playVideo();

	void resetMainPage();

private: // data set functions definition
	// for main home page
	void RequestMainPageData();

	// for general view page
	void CreateViewPage();

	void setViewPageTitle(QString);
	void setViewPageIdentifier(QString);
	void setViewPageType(QString);

	void displayViewPageContent(Control* );

	// for video view page
	void CreateVideoViewPage();

	// for news view page
	void setNewsViewPageInfo(QVariant data);
	void CreateNewsViewPage();

	// for analysis page
	void setAnalysisPageBaseInfo(QVariant data);

	//Show about page
	void showAboutPage();

	//Show FAQ page
	void showFAQPage();

	//show terms of service page
	void showTermsOfServicePage();

	//show terms and condition page.
	void showTermsAndConditionPage();

	//show privacy policy
	void showPrivacyPolicyPage();

	// for more page
	void setMorePageInfo();

	// init panes
	void InitializePanes();

	//init main menu.
	void InitializeMainMenu();

	//show mail dialog box.
	void showSocialDialog();

	//Recycle main menu. Pop main menu from previous view and add to new pane.
	void moveMainMenuToPane(AbstractPane *pane);

	//Show other app page;
	void showOtherAppPage();

	//For fantasy display on home page. Show web view instead of main content
	void displayFantasyView(QString address);
	void displayMainHomeView();

	//Show event list for today on home page.
	void showEventListForToday(bool bShow);

	//Show loading view
	void showActivityIndicator(bool bShow);

	//Display facebook post box
	void showFacebookPoster();

	//For generating push setting view manually.

	void addPushItemCtrl(QString title, QString refKey, bool bChecked,bool odd);
	void addPushHeaderCtrl(QString title, QString refKey);
	void setItemChecked(QString refKey, bool bChecked);
	void setItemVisible(QString refKey, bool bVisible);
	bool isPushListEmpty();
	bool toBool(QString sVal);

private:
	StadiumAstro* m_pMainInstance;

	int m_curSpecialLogoIndex;
	NavigationPane* m_homePane;

	Dialog * m_mailDialog;
	Dialog * m_loadingDialog;

	int curDlgPurpose;

	Container * m_mainMenu;

	float m_screenWidth;
	float m_screenHeight;

	SportsEventBar *eventBar;
	SportsEventBar *infoBar;
	SportsEventBar *buttonBar;

private: // session variables
	// current sport event content refkey value for requesting more infos
	QString footballMenuName;
	QString otherMenuName;

	//Admob relative. Admob data and admob control.
	bool isAdmobVisible;
	QVariantList admobList;
	WebImageView * curAdmobView;
	int curAdmobIdx;

	//Current event name and key and channel enabled
	QString m_sportEventsContent_refKey;
	QString m_sportEventsContent_name;
	bool bTVChannel;

	QHash<QString, QVariantMap> *m_teamsLogoTable;
	JSONConnect *jsonNetwork;

	Facebook *facebook;

	InvokeManager *invokeManager;

	//Current loading status of home page. Need for synchronizing.
	int curHomeLoadStatus;

	//Current help content
	QString curHelpContent;

	//Event Information
	QVariantMap sportsEventInfo;

	//Timers
	QTimer *oneSecTimer;

	//Astro Push Register Service
	AstroRegisterService m_registerService;

	//Current Push Options
	QVariantMap curPushOptions;
};

#endif /* STADIUMEVENTHANDLER_H_ */
