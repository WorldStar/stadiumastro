/*
 * StadiumEventHandler.cpp
 *
 *  Created on: Jun 9, 2013
 *      Author: Star
 */
#include <bb/cascades/Application>
#include <bb/cascades/Container>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/QmlDocument>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/Label>
#include <bb/cascades/ListView>
#include <bb/cascades/ScrollView>
#include <bb/cascades/ImageView>
#include <bb/cascades/StackLayout>
#include <bb/cascades/StackLayoutProperties>
#include <bb/cascades/WebView>
#include <bb/device/DisplayInfo>
#include <bb/system/SystemDialog>
#include <bb/PpsObject>

#include <QtCore/QDateTime>

#include "facebook/Facebook.hpp"
#include "facebook/FBUser.h"
#include "facebook/FBPlace.h"
#include "facebook/FBPost.h"
#include "facebook/FBComment.h"
#include "facebook/FacebookObject.h"

#include "WebImageView.h"
#include "SportsEventTab.hpp"
#include "SportsEventBar.hpp"
#include "ImageCache.hpp"
#include "ImageLoader.h"
#include "StadiumEventHandler.h"



using namespace bb::cascades;
using namespace bb::data;
using namespace bb::system;

/*
 * StadiumMatchViewHandler.cpp
 *
 *  Created on: Aug 26, 2013
 *      Author: team2
 */

//Match Analysis Screen implementation.

void StadiumEventHandler::ShowMatchesPage()
{
	CreateViewPage();
	ImageCache::getSharedImageCache()->clearMemory();

	setViewPageTitle("Matches");

	//For Refresh(Passion)
	setViewPageIdentifier(m_sportEventsContent_refKey);
	setViewPageType("SoccerCalendarList");
	Page *viewPane = m_homePane->top();
	VisualNode *btn = viewPane->findChild<VisualNode *>("pageRefresh");
	btn->setVisible(true);

	// TODO: request 'Calendar List' info to server
	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "SoccerCalendarList");

	params.insert("p2", m_sportEventsContent_refKey);

	jsonNetwork->SendGetRequest(params, this,SLOT(setMatchesPageInfo()));

}

void StadiumEventHandler::requestMatchListInfo(QString strId)
{
	// TODO: request 'Calendar List' info to server
	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "SoccerCalendarList");

	params.insert("p2", strId);

	jsonNetwork->SendGetRequest(params, this,SLOT(setMatchesPageInfo()));
}

void StadiumEventHandler::setMatchesPageInfo()
{
	Page *viewPane = m_homePane->top();

	JsonDataAccess jsondata;
	QVariantList varList;

	//For Refresh (Passion)
	showActivityIndicator(false);

	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	if (reply->error() == 0) {
		// get calendar info
		//varList = jsondata.load(QDir::currentPath() + "/app/native/assets/models/calendarListDataModel.json").value<QVariantList>();
		QVariantMap varMap =
				jsondata.load(reply).value<QVariantMap>();
		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(
							err.errorMessage());
			return;
		}
		varList = varMap.value("soccerCalendars").toList();

		// prepare data model
		GroupDataModel *calendarListModel = new GroupDataModel();
		calendarListModel->setParent(this);

		// Iterate over all the items in the received data and rearrange data
		QVariantList::Iterator item = varList.begin();
		QDate today = QDate::currentDate();
//		bool bFirst = true;
		QDate prevDate;
		QVariantMap itemMap;

		QVariantList finalList;
		finalList.clear();

		bool bFoundSinceToday = false;
		int nTodayItem = 0;
		int nCurrentItem = -1;
		while (item != varList.end()) {
			itemMap = (*item).toMap();

			nCurrentItem++;
			// get year/ month/ day value from map
			QString strBuf = itemMap["displayMonth"].toString() + "/"
					+ itemMap["displayDay"].toString() + "/"
					+ itemMap["displayYear"].toString();

			QDate itemDate = QDate::fromString(strBuf, "MMM/dd/yyyy");

			// set date info
			itemMap.insert("date", itemDate);
			itemMap["displayDay"] = itemDate.toString("d");

//			if (bFirst) {
//				bFirst = false;
//			} else {
//				// append inactive dates
//				QDate tempDate = prevDate.addDays(1);
//				while (tempDate < itemDate) {
//					QVariantMap newItemMap;
//
//					// set date info
//					newItemMap["displayYear"] = tempDate.toString("yyyy");
//					newItemMap["displayMonth"] = tempDate.toString("MMM");
//					newItemMap["displayDay"] = tempDate.toString("d");
//					newItemMap["date"] = tempDate;
//
//					// set today info
//					if (tempDate == today)
//						newItemMap.insert("isToday", 1);
//
//					finalList.append(newItemMap);
//
//					tempDate = tempDate.addDays(1);
//				}
//			}

			QString strActiveFlag = itemMap["isActiveFlag"].toString();

			if (strActiveFlag.compare("Y") == 0)
			{
				bFoundSinceToday = true;
				nTodayItem = nCurrentItem;
			}

			// set today info
			if (bFoundSinceToday){
				itemMap.insert("sinceToday", 1);
			}


			finalList.append(itemMap);
			prevDate = itemDate;

			++item;
		}

		// set data model's property

		calendarListModel->insertList(finalList);
		calendarListModel->setGrouping(ItemGrouping::None);

		QStringList sortingKeys;
		sortingKeys << "date";
		calendarListModel->setSortingKeys(sortingKeys);

		// set video feed list model
		ListView *calendarList = viewPane->findChild<ListView*>(
				"calendarList");
		calendarList->setDataModel(calendarListModel);


		// show to screen
		Container *matchesContent = viewPane->findChild<Container*>(
				"matchesContent");
		displayViewPageContent(matchesContent);

		matchesContent->setProperty("nSelectItemIndex", QVariant(nTodayItem));

		emit scrollCalendarListToEnd();
		/* Set Initial day and scroll to end. */
//		QVariantList list;
//		list <<  finalList.count() - 1;
//		calendarList->select(list, true);
//
//		//KJH
//		calendarList->scrollToPosition(ScrollPosition::Beginning, ScrollAnimation::Smooth);
//
//		calendarList->clearSelection();
//		calendarList->select([], true);
//
//		//Get First Item
//		item = varList.begin();
//		itemMap = (*item).toMap();
//
//		// get year/ month/ day value from map
//		QString strBuf = itemMap["displayMonth"].toString() + "/"
//				+ itemMap["displayDay"].toString() + "/"
//				+ itemMap["displayYear"].toString();
//
//		QDate itemDate = QDate::fromString(strBuf, "MMM/dd/yyyy");
//
//		// set date info
//		itemMap.insert("date", itemDate);
//		itemMap["displayDay"] = itemDate.toString("d");
//
//
//		//Set last date;
//		Label * dateLabel = viewPane->findChild<Label*>("lblDateStr");
//		dateLabel->setText(itemDate.toString());
//		//Load match result for last day.
//		getMatchResultListData(itemMap["strId"].toString());

	}
	reply->deleteLater();

}

void StadiumEventHandler::getMatchResultListData(QString strId)
{
	// TODO: request 'Match List For Particular calendar date' to server
	QMap < QString, QString > params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "SoccerCalendarMatchList");

	params.insert("p2", strId);

	jsonNetwork->SendGetRequest(params, this, SLOT(updateMatchResultListData()));

}

void StadiumEventHandler::updateMatchResultListData()
{
	Page *viewPage = m_homePane->top();
	JsonDataAccess jsondata;
	QVariantList varList;

	// get preview info
	//varList = jsondata.load(QDir::currentPath() + "/app/native/assets/models/matchListModelForDate.json").value<QVariantList>();
	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());

	if (reply->error() == 0) {
		QVariantMap varMap =
				jsondata.load( reply
								).value<
						QVariantMap>();
		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(
							err.errorMessage());
			return;
		}
		varList = varMap.value("soccerMatchs").toList();

		// append informations
		QVariantList::Iterator item = varList.begin();

		int nIndex = -1;
		while (item != varList.end()) {
			QVariantMap itemMap = (*item).toMap();

			nIndex++;
			// customize image content
			// In fact, there's no need to add this item, use code as below.
			// 		itemMap["homeTeamLogo"] = getTeamLogoUrl(itemMap["homeTeamId"], "teamLogo100x100")
			// 		itemMap["visitingTeamLogo"] = getTeamLogoUrl(itemMap["visitingTeamId"], "teamLogo100x100")
			QString strUrl;
			strUrl = jsonNetwork->GetSiteUrl();
			strUrl.append(getTeamLogoUrl(itemMap["homeTeamId"].toString(), "teamLogo100x100"));
			itemMap["homeTeamLogo"] = QUrl(strUrl);

			strUrl = jsonNetwork->GetSiteUrl();
			strUrl.append(getTeamLogoUrl(itemMap["visitingTeamId"].toString(), "teamLogo100x100"));
			itemMap["visitingTeamLogo"] = QUrl(strUrl);

			//Get Time for Show Date(Passion)
			QDateTime dateTime = QDateTime::fromString(
									itemMap["matchTimeZoneDate"].toString(), Qt::ISODate);


			itemMap["matchDate"] = dateTime.toString("dddd,  dd  MMM  yyyy  h:mm  AP");
			itemMap["matchTime"] = dateTime.toString("h:mm  AP");

			//Set Date Label Text
			if (nIndex == 0)
			{
				// show to screen
				Container *matchesContent = viewPage->findChild<Container*>("matchesContent");
				matchesContent->setProperty("dateStr", itemMap["matchDate"]);
			}

			// save to map data
			(*item) = itemMap;

			++item;
		}

		// prepare data model
		GroupDataModel *matchListModel = new GroupDataModel();

		// reverse list data and set into model
		QVariantList finalList;
		finalList.clear();
		for (int i = varList.count() - 1; i >= 0; i--) {
			finalList.append(varList.at(i));
		}

		matchListModel->insertList(finalList);
		matchListModel->setGrouping(ItemGrouping::None);

		// set video feed list model
		ListView *matchList = viewPage->findChild<ListView*>(
				"matchResultList");
		matchList->setDataModel(matchListModel);
	}
	reply->deleteLater();
}


void StadiumEventHandler::ShowAnalysisPage(QVariant data)
{

	CreateViewPage();

	setViewPageTitle("Analysis");

	setAnalysisPageBaseInfo(data);

	// request detail data
	QVariantMap dataMap = data.toMap();
	QString strMatchId = dataMap.value("strId").toString();

	showActivityIndicator(true);
	requestMatchPreviewInfo(strMatchId);
	showActivityIndicator(true);
	requestMatchReportInfo(strMatchId);
	showActivityIndicator(true);
	requestMatchLineUpInfo(strMatchId);
	showActivityIndicator(true);
	requestMatchEventsInfo(strMatchId);
	showActivityIndicator(true);
	requestMatchVideoHighlightsInfo(strMatchId);
	showActivityIndicator(true);
	requestInMatchClipsInfo(strMatchId);

}

/** @brief
 *		parsing match data and set into UI components
 *	@param
 *		dataMap: QMap data of selected match, this value is transferred from match result list when triggered.
 **/
void StadiumEventHandler::setAnalysisPageBaseInfo(QVariant data)
{
	Page *viewPane = m_homePane->top();

	/* parse match result data and set into page components */
	QVariantMap matchResultMap = data.toMap();

	// set Home Team Logo image
	// must be replaced with code as below:
	//		QString strHomeLogo = getTeamLogoUrl(matchResultMap["homeTeamId"].toString(), "teamLogo100x100");
	QString strUrl;
	strUrl = jsonNetwork->GetSiteUrl();
	strUrl.append(getTeamLogoUrl(matchResultMap["homeTeamId"].toString(), "teamLogo100x100"));

	WebImageView *imgHomeTeamLogo = viewPane->findChild<WebImageView *>("imgHomeTeamLogo");
	imgHomeTeamLogo->setUrl(QUrl(strUrl));

	// set home team alias
	QString strHomeAlias = matchResultMap.value("homeTeamAlias").toString();
	Label *lblHomeTeamAlias = viewPane->findChild<Label *>("lblHomeTeamAlias");
	lblHomeTeamAlias->setText(strHomeAlias);



	// set visiting team logo image
	// must be replaced with code as below:
	strUrl = jsonNetwork->GetSiteUrl();
	strUrl.append(getTeamLogoUrl(matchResultMap["visitingTeamId"].toString(), "teamLogo100x100"));

	WebImageView *imgVisitingTeamLogo = viewPane->findChild<WebImageView *>("imgVisitingTeamLogo");
	imgVisitingTeamLogo->setUrl(QUrl(strUrl));



	// set match status
	QString strStatus = (matchResultMap.value("matchStatus").toString() == "Played")? "Full Time" : "";
	Label *lblMatchStatus = viewPane->findChild<Label *>("lblMatchStatus");
	lblMatchStatus->setText(strStatus);

	QString strHomeScore = matchResultMap.value("homeTeamScore").toString();
	QString strVisitingScore = matchResultMap.value("visitingTeamScore").toString();

	// set home team score
	Label* lblHomeTeamScore = viewPane->findChild<Label*>("lblMatchScore");
	Label* lblMatchTime = viewPane->findChild<Label*>("lblMatchTime");

	// set visiting team alias
	QString strVisitingAlias = matchResultMap.value("visitingTeamAlias").toString();
	Label *lblVisitingTeamAlias = viewPane->findChild<Label *>("lblVisitingTeamAlias");
	lblVisitingTeamAlias->setText(strVisitingAlias);

	// set date & time string
	QDateTime dateTime = QDateTime::fromString(matchResultMap.value("matchTimeZoneDate").toString(), Qt::ISODate);
	QString strDate = dateTime.toString("dddd,  dd  MMM  yyyy  h:mm  AP");
	QString strMatchTime = dateTime.toString("h:mm AP");

	Label *lblDateStr = viewPane->findChild<Label*>("lblDateStr");
	lblDateStr->setText(strDate);

	Container *analysisContent = viewPane->findChild<Container*>("analysisContent");

	QString scoreText;

	if(strStatus =="Full Time")
	{
		scoreText = strHomeScore + "   -   " + strVisitingScore;
		analysisContent->setProperty("bMatchAvail", true);

	}
	else
	{
		scoreText = "vs";
		lblMatchTime->setText(strMatchTime);
		analysisContent->setProperty("bMatchAvail", false);
	}

	lblHomeTeamScore->setText(scoreText);

	QVariantMap dataMap = data.toMap();
	QString strMatchId = dataMap.value("strId").toString();
	analysisContent->setProperty("strMatchID", strMatchId);

	VisualNode *btn = viewPane->findChild<VisualNode *>("pageRefresh");
	btn->setVisible(true);

	// show to screen
	displayViewPageContent(analysisContent);


}
void StadiumEventHandler::requestMatchPreviewInfo(QString strMatchId)
{
	// TODO: request 'Soccer Match Preview for a Particular Match' to server
	QMap < QString, QString > params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "SoccerMatchPreview");

	params.insert("p2", strMatchId);

	qDebug() << "Request Match Preview";

	jsonNetwork->SendGetRequest(params, this, SLOT(setMatchPreviewInfo()));
}

/** @brief
 *		parsing match preview data and set into UI components
 *	@param
 *		none.
 **/
void StadiumEventHandler::setMatchPreviewInfo()
{
	Page *viewPane = m_homePane->top();
	// define variables
	JsonDataAccess jsondata;
	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());

	showActivityIndicator(false);
	if (reply->error() == 0) {
		// get preview info
		QVariantMap varMap = jsondata.load(reply).value<QVariantMap>();
		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(
							err.errorMessage());
			return;
		}
		// set into UI
		Container *previewContentContainer = viewPane->findChild<Container*>(
				"previewContentContainer");
		// set title

		QString strTitle = varMap["title"].toString();
		previewContentContainer->setProperty("previewTitle", strTitle);

		// set date & time
		QDateTime dateTime = QDateTime::fromString(
				varMap.value("publishedDate").toString(), Qt::ISODate);
		QString strDate = dateTime.toString("dddd, dd MMM yyyy h:mm AP");
		previewContentContainer->setProperty("previewDate", strDate);

		// adjust preview detail string
		QString strPreviewData = varMap.value("body").toString();
		strPreviewData.replace(QString("&lt;"), QString("<"));
		strPreviewData.replace(QString("&gt;"), QString(">"));
		strPreviewData = "<html><font size='5'>" + strPreviewData + "</font></html>";

		previewContentContainer->setProperty("previewDetail", strPreviewData);

	}
	emit selectFirstTab();
	reply->deleteLater();
}

void StadiumEventHandler::requestMatchReportInfo(QString strMatchId)
{
	// TODO: request 'Soccer Match Report for a Particular Match' to server
	QMap < QString, QString > params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "SoccerMatchReport");

	params.insert("p2", strMatchId);

	jsonNetwork->SendGetRequest(params, this, SLOT(setMatchReportInfo()));

}

/** @brief
 *		parsing match preview data and set into UI components
 *	@param
 *		none.
 **/
void StadiumEventHandler::setMatchReportInfo()
{

	Page *viewPane = m_homePane->top();
	// define variables
	JsonDataAccess jsondata;
	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	showActivityIndicator(false);
	if (reply->error() == 0) {
		// get preview info
		QVariantMap varMap =
				jsondata.load(
						reply
						).value<
						QVariantMap>();
		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(
							err.errorMessage());
			return;
		}

		// get content container object
		Container *reportContentContainer = viewPane->findChild<Container*>(
				"reportContentContainer");

		// set title
		QString strTitle = varMap["title"].toString();
		reportContentContainer->setProperty("reportTitle", strTitle);

		// set date & time
		QDateTime dateTime = QDateTime::fromString(
				varMap.value("publishedDate").toString(), Qt::ISODate);
		QString strDate = dateTime.toString("dddd, dd MMM yyyy h:mm AP");
		reportContentContainer->setProperty("reportDate", strDate);

		// set detail data
		QString strReportData = varMap.value("body").toString();
		strReportData.replace(QString("&lt;"), QString("<"));
		strReportData.replace(QString("&gt;"), QString(">"));
		strReportData = "<html><font size=5>" + strReportData + "</font></html>";
		reportContentContainer->setProperty("reportBody", strReportData);
	}
	reply->deleteLater();
}

void StadiumEventHandler::requestMatchLineUpInfo(QString strMatchId)
{
	// TODO: request 'Soccer Match Lineup for a Particular Match' to server
	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "SoccerMatchLineUp");

	params.insert("p2", strMatchId);

	jsonNetwork->SendGetRequest(params, this, SLOT(setMatchLineUpInfo()));
}

/** @brief
 *		parsing team line-up data and set into UI components
 *	@param
 *		none.
 **/
void StadiumEventHandler::setMatchLineUpInfo()
{
	Page *viewPane = m_homePane->top();

	// define variables
	JsonDataAccess jsondata;
	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());

	showActivityIndicator(false);
	if (reply->error() == 0) {
		// get line-up info
		QVariantMap lineUpMap =
				jsondata.load(
						reply
						).value<
						QVariantMap>();
		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(
							err.errorMessage());
			return;
		}

		//set line-up content
		QVariantList homeTeamLineUpList =
				lineUpMap.value("homeTeamLineUp").toList();
		QVariantList visitingTeamLineUpList =
				lineUpMap.value("awayTeamLineUp").toList();

		int nHomeTeamPlayerCount = homeTeamLineUpList.size();
		int nAwayTeamPlayerCount = visitingTeamLineUpList.size();
		int nCount =
				(nHomeTeamPlayerCount < nAwayTeamPlayerCount) ?
						nHomeTeamPlayerCount : nAwayTeamPlayerCount;

		// arrange data with pairs
		QVariantList lineUpList;
		for (int i = 0; i < nCount; i++) {
			QVariantMap homeTeamPlayerMap = homeTeamLineUpList.at(i).toMap();
			QVariantMap visitingTeamLineUpMap =
					visitingTeamLineUpList.at(i).toMap();

			QVariantMap playerMap;
			playerMap["homeTeamPlayerPosition"] = homeTeamPlayerMap.value(
					"position").toString();
			playerMap["awayTeamPlayerPosition"] = visitingTeamLineUpMap.value(
					"position").toString();
			playerMap["homeTeamPlayerName"] = homeTeamPlayerMap.value(
					"playerName").toString();
			playerMap["awayTeamPlayerName"] = visitingTeamLineUpMap.value(
					"playerName").toString();
			playerMap["awayTeamPlayerNo"] = visitingTeamLineUpMap.value(
					"playerNo").toString();
			lineUpList.prepend(playerMap);
		}

		// produce data model
		GroupDataModel *lineUpModel = new GroupDataModel();
		lineUpModel->insertList(lineUpList);
		lineUpModel->setGrouping(ItemGrouping::None);

		// set data model to component
		ListView *lineUpListView = viewPane->findChild<ListView *>(
				"lineUpListView");
		lineUpListView->setDataModel(lineUpModel);
	}
		reply->deleteLater();
}

void StadiumEventHandler::requestMatchEventsInfo(QString strMatchId)
{
	// TODO: request 'Soccer Match Summary for a Particular Match' to server

	QMap<QString, QString> params;

		params.insert("service", "astroSportsDataService");

		params.insert("action", "grabJsonText");

		params.insert("mimeType", "application/json");

		params.insert("p1", "SoccerMatchSummary");

		params.insert("p2", strMatchId);

		jsonNetwork->SendGetRequest(params, this, SLOT(setMatchEventsInfo()));


}

/** @brief
 *		parsing soccer match summary data and set into UI components
 *	@param
 *		none.
 **/
void StadiumEventHandler::setMatchEventsInfo()
{
	Page *viewPane = m_homePane->top();

	// define variables
	JsonDataAccess jsondata;

	// get line-up info
	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	showActivityIndicator(false);
	if (reply->error() == 0) {
		QVariantMap eventMap = jsondata.load(reply).value<QVariantMap>();
		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(
							err.errorMessage());
			return;
		}
		QVariantList eventList = eventMap.value("soccerMatchEvents").toList();

		QVariantList finalList; //For reversing array.

		GroupDataModel *eventModel = new GroupDataModel();

		QVariantList::Iterator item = eventList.begin();
		int beforeMinute = -1;

		while (item != eventList.end()) {
			// Iterate over all the items and replace minute data
			QVariantMap itemMap = (*item).toMap();
			int curMinute = itemMap.value("minute").toInt();

			// compare with before event minute and reset value
			if (curMinute == beforeMinute)
				itemMap["minute"] = "";
			else {
				QString minuteStr = QString::number(curMinute);
				itemMap["minute"] = minuteStr + "'";
				beforeMinute = curMinute;
			}

			// save to map data
			//(*item) = itemMap;
			finalList.insert(0, itemMap);

			++item;
		}

		eventModel->insertList(finalList);
		eventModel->setGrouping(ItemGrouping::None);

		ListView *eventListView = viewPane->findChild<ListView *>(
				"eventListView");
		eventListView->setDataModel(eventModel);
	}
	reply->deleteLater();
}

void StadiumEventHandler::requestMatchVideoHighlightsInfo(QString strMatchId)
{
	// TODO: request 'Soccer Match highlight videos for a Particular Match' to server(undefined)
	QMap < QString, QString > params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "VideosFeedList");

	params.insert("p2", strMatchId);

	jsonNetwork->SendGetRequest(params, this, SLOT(setMatchVideoHighlightsInfo()));
}

void StadiumEventHandler::setMatchVideoHighlightsInfo()
{
	Page * viewPane = m_homePane->top();
	// define variables
	JsonDataAccess jsondata;

	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	showActivityIndicator(false);
	if (reply->error() == 0) {
		// get in-match video clips data
		QVariantMap clipMap =
				jsondata.load(
						reply
						).value<
						QVariantMap>();
		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(
							err.errorMessage());
			return;
		}
		QVariantList inMatchClipsList =
				clipMap.value("videoNewsContents").toList();

		QVariantList::Iterator item = inMatchClipsList.begin();
		while (item != inMatchClipsList.end()) {
			QVariantMap itemMap = (*item).toMap();

			// customize image content
			// In fact, there's no need to add this item, use 'defaultImage128x96' item instead.
			itemMap["image"] = QUrl(itemMap["defaultImage128x96"].toString());
			itemMap["type"] = "VIDEOS";
			// save to map data
			(*item) = itemMap;

			++item;
		}

		//Passion
		if (inMatchClipsList.count() == 0){
			Container *analysisContent = viewPane->findChild<Container*>("analysisContent");
			analysisContent->setProperty("bHasHighLight", QVariant(false));
		}
		else{
			Container *analysisContent = viewPane->findChild<Container*>("analysisContent");
			analysisContent->setProperty("bHasHighLight", QVariant(true));
		}

		// prepare data model
		GroupDataModel *inMatchModel = new GroupDataModel();
		inMatchModel->insertList(inMatchClipsList);
		inMatchModel->setGrouping(ItemGrouping::None);

		// set data model
		ListView *inMatchClipsListView = viewPane->findChild<ListView *>(
				"videoHighlightListView");
		inMatchClipsListView->setDataModel(inMatchModel);
	}
	reply->deleteLater();
}

void StadiumEventHandler::requestInMatchClipsInfo(QString strMatchId)
{
	// TODO: request 'In-Match video clips for a Particular Match' to server
	QString maxis3G("MAXIS3G_");

	QMap < QString, QString > params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "VideosFeedList");

	maxis3G.append(strMatchId);

	params.insert("p2", maxis3G);

	jsonNetwork->SendGetRequest(params, this, SLOT(setInMatchClipsInfo()));
}

/** @brief
 *		parsing soccer in-match video clips data and set into UI components
 *	@param
 *		none.
 **/
void StadiumEventHandler::setInMatchClipsInfo()
{
	Page *viewPane = m_homePane->top();
	// define variables
	JsonDataAccess jsondata;

	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	showActivityIndicator(false);
	if (reply->error() == 0) {
		// get in-match video clips data
		QVariantMap clipMap =
				jsondata.load(
						reply
						).value<
						QVariantMap>();
		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(
							err.errorMessage());
			return;
		}
		QVariantList inMatchClipsList =
				clipMap.value("videoNewsContents").toList();

		QVariantList::Iterator item = inMatchClipsList.begin();
		while (item != inMatchClipsList.end()) {
			QVariantMap itemMap = (*item).toMap();

			// customize image content
			// In fact, there's no need to add this item, use 'defaultImage128x96' item instead.
			itemMap["image"] = QUrl(itemMap["defaultImage128x96"].toString());

			// save to map data
			(*item) = itemMap;

			++item;
		}

		if (inMatchClipsList.count() == 0){
			Container *analysisContent = viewPane->findChild<Container*>("analysisContent");
			analysisContent->setProperty("bHasMatchClip", QVariant(false));
		}
		else{
			Container *analysisContent = viewPane->findChild<Container*>("analysisContent");
			analysisContent->setProperty("bHasMatchClip", QVariant(true));
		}

		// prepare data model
		GroupDataModel *inMatchModel = new GroupDataModel();
		inMatchModel->insertList(inMatchClipsList);
		inMatchModel->setGrouping(ItemGrouping::None);

		// set data model
		ListView *inMatchClipsListView = viewPane->findChild<ListView *>(
				"inMatchClipsListView");
		inMatchClipsListView->setDataModel(inMatchModel);
	}
	reply->deleteLater();
}
