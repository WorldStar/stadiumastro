/*
 * Utils.h
 *
 *  Created on: Jul 30, 2013
 *      Author: team2
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <QObject>
#include <QString>


class Utils {
public:
	Utils();
	virtual ~Utils();

//Static functions
public:
	static QString Url2FileName(QString &);

};


#endif /* UTILS_H_ */
