/*
 * StadiumEventHandler.cpp
 *
 *  Created on: Jun 9, 2013
 *      Author: Star
 */
#include <bb/cascades/Application>
#include <bb/cascades/Container>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/QmlDocument>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/Label>
#include <bb/cascades/ListView>
#include <bb/cascades/ScrollView>
#include <bb/cascades/ImageView>
#include <bb/cascades/StackLayout>
#include <bb/cascades/StackLayoutProperties>
#include <bb/cascades/WebView>
#include <bb/device/DisplayInfo>
#include <bb/PpsObject>

#include <QtCore/QDateTime>

#include "facebook/Facebook.hpp"
#include "facebook/FBUser.h"
#include "facebook/FBPlace.h"
#include "facebook/FBPost.h"
#include "facebook/FBComment.h"
#include "facebook/FacebookObject.h"

#include "WebImageView.h"
#include "SportsEventTab.hpp"
#include "SportsEventBar.hpp"
#include "ImageCache.hpp"
#include "ImageLoader.h"
#include "StadiumEventHandler.h"


using namespace bb::cascades;
using namespace bb::data;
using namespace bb::system;

//Standing list
//---------------------------------

void StadiumEventHandler::ShowStandingsPage()
{
	CreateViewPage();


	ImageCache::getSharedImageCache()->clearMemory();

	setViewPageTitle("Standings");

	//For Refresh(Passion)
	setViewPageIdentifier(m_sportEventsContent_refKey);
	setViewPageType("SoccerStandingList");
	Page *viewPane = m_homePane->top();
	VisualNode *btn = viewPane->findChild<VisualNode *>("pageRefresh");
	btn->setVisible(true);


	// TODO: request 'Table Standing' data to server with 'm_sportEventsContent_refKey' member variable as key
	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "SoccerStandingList");

	params.insert("p2", m_sportEventsContent_refKey);

	jsonNetwork->SendGetRequest(params, this, SLOT(setStandingsPageInfo()));

}

void StadiumEventHandler::requestStandingInfo(QString strId)
{
	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "SoccerStandingList");

	params.insert("p2", m_sportEventsContent_refKey);

	jsonNetwork->SendGetRequest(params, this, SLOT(setStandingsPageInfo()));
}

//void StadiumEventHandler::OnShowStandingsPage()
//{
//	setStandingsPageInfo();
//}

void StadiumEventHandler::setStandingsPageInfo()
{
	Page *viewPane = m_homePane->top();
	// set standings table title
	QString strGroupTitle = "Premier League";
	Label *lblGroupTitle = viewPane->findChild<Label*>("lblGroupTitle");
	lblGroupTitle->setText(strGroupTitle);

	// load json data
	JsonDataAccess jsondata;
	QVariantList varList;

	//For Refresh(passion)
	showActivityIndicator(false);

	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());

	if (reply->error() == 0) {
		// get standings info
		QVariantMap varMap =
				jsondata.load(
						reply
						).value<
						QVariantMap>();
		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(
							err.errorMessage());
			qDebug() << errorMsg;
			return;
		}

		varList = varMap.value("soccerStandings").toList();

		// set team logo image
		QVariantList::Iterator item = varList.begin();
		while (item != varList.end()) {
			QVariantMap itemMap = (*item).toMap();

			// customize image content
			// In the future, write as below instead static image path.
			QString strUrl = jsonNetwork->GetSiteUrl();
			strUrl.append(getTeamLogoUrl(itemMap["teamId"].toString(), "teamLogo44x44"));

			itemMap["teamLogo"] = QUrl(strUrl);
//			itemMap["teamLogo"] = "asset:///images/icon_team_logo.png";

			itemMap["pos"] = itemMap["pos"].toInt();

			//QString strTeamName = itemMap["teamName"].toString();

			// adjust team name string's length for justify
			/*
			const int nLengthLimit = 10;
			if (strTeamName.length() > nLengthLimit) {
				strTeamName = strTeamName.left(nLengthLimit) + "...";
				itemMap["teamName"] = strTeamName;
			}
			*/

			// save to map data
			(*item) = itemMap;

			++item;
		}

		// prepare data model
		GroupDataModel *standingsListModel = new GroupDataModel();

		standingsListModel->insertList(varList);
		standingsListModel->setGrouping(ItemGrouping::None);

		QStringList sortingKeys;
		sortingKeys << "pos";
		standingsListModel->setSortingKeys(sortingKeys);

		// set video feed list model
		ListView *standingList = viewPane->findChild<ListView*>(
				"standingList");
		standingList->setDataModel(standingsListModel);

		// show to screen
		Container *standingsContent = viewPane->findChild<Container*>(
				"standingsContent");
		displayViewPageContent(standingsContent);
	}
	reply->deleteLater();
}
