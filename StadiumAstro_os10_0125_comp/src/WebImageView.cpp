/*
 * WebImageView.cpp
 */
#include "StadiumEventHandler.h"
#include "WebImageView.h"
#include <bb/cascades/Image>
#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/system/SystemDialog>
#include <bb/ImageData>
#include <bb/utility/ImageConverter>

#include "ImageCache.hpp"
#include "ImageLoader.h"
#include "Utils.h"
using namespace bb;
using namespace bb::system;

static QNetworkAccessManager *mNetManager = new QNetworkAccessManager();

WebImageView::WebImageView() {
	mDefaultImageSource = QUrl("");
	curRequest = NULL;
}
WebImageView::~WebImageView()
{
	if(curRequest != NULL)
		curRequest->abort();
	curRequest->deleteLater();
	curRequest = NULL;

}
const QUrl& WebImageView::url() const {
	return mUrl;
}

const QUrl& WebImageView::defaultImageSource() const {
	return mDefaultImageSource;
}


void WebImageView::setEventHandler(StadiumEventHandler* handler) {
	m_EventHandler = handler;
}


void WebImageView::setDefaultImageSource(const QUrl& url) {
	mDefaultImageSource = url;
}

void WebImageView::setUrl(const QUrl& url) {
	if (mDefaultImageSource.path() == ""){
		Image imageTemp = Image(QUrl("asset:///images/bg_black_default.png"));
		setImage(imageTemp);
	}
	else{
		Image imageTemp = Image(QUrl("asset:///images/defaultFeed.jpg"));
		setImage(imageTemp);
	}


	mUrl = url;

	if(!url.isEmpty())
	{
		Image *img = ImageCache::getSharedImageCache()->getImageFromMemory(url.toString());

		if(curRequest != NULL)
		{
			disconnect(curRequest, SIGNAL(finished()), 0, 0);
			curRequest->abort();
			curRequest = NULL;
		}



		if(img == NULL)
		{
			//Use private network manager.
			curRequest = mNetManager->get(QNetworkRequest(url));
			connect(curRequest, SIGNAL(finished()), this, SLOT(imageLoaded()));

			//Use Image loader here
//			QString strUrl = url.toString();
//			ImageLoader::getSharedImageLoader()->pushImageUrl(strUrl);
//			connect(ImageLoader::getSharedImageLoader(), SIGNAL(loadingDone()), this, SLOT(loadingDone()));
		}
		else
		{
			setImage(*img);
		}

		emit urlChanged();

	}
}
/**
 * @function Used for ImageLoader. Not works for self-loading view.
 */
void WebImageView::loadingDone()
{
	Image *img = ImageCache::getSharedImageCache()->getImageFromMemory(mUrl.toString());
	setImage(*img);
	disconnect(ImageLoader::getSharedImageLoader(), SIGNAL(loadingDone()), this, SLOT(loadingDone()));
}
void WebImageView::setLargeImageUrl(const QUrl& url) {
	m_LargeUrl = url;
}

const QUrl& WebImageView::largeImageUrl() {
	return m_LargeUrl;
}

void WebImageView::setClickable(const bool clickable) {
	m_Clickable = clickable;
	connect(this,SIGNAL(touch(bb::cascades::TouchEvent*)), this, SLOT(onTouch(bb::cascades::TouchEvent*)));
}

QUrl WebImageView::redirectUrl(const QUrl& possibleRedirectUrl,
                               const QUrl& oldRedirectUrl) {
	QUrl redirectUrl;

	/*
	 * Check if the URL is empty and
	 * that we aren't being fooled into a infinite redirect loop.
	 * We could also keep track of how many redirects we have been to
	 * and set a limit to it, but we'll leave that to you.
	 */

	if(!possibleRedirectUrl.isEmpty() &&
	   possibleRedirectUrl != oldRedirectUrl) {
		redirectUrl = possibleRedirectUrl;
	}
	return redirectUrl;
}

static bb::ImageData fromQImage(const QImage &qImage) {
	bb::ImageData imageData(bb::PixelFormat::RGBA_Premultiplied, qImage.width(),
			qImage.height());

	unsigned char *dstLine = imageData.pixels();
	for (int y = 0; y < imageData.height(); y++) {
		unsigned char * dst = dstLine;
		for (int x = 0; x < imageData.width(); x++) {
			QRgb srcPixel = qImage.pixel(x, y);
			*dst++ = qRed(srcPixel);
			*dst++ = qGreen(srcPixel);
			*dst++ = qBlue(srcPixel);
			*dst++ = qAlpha(srcPixel);
		}
		dstLine += imageData.bytesPerLine();
	}

	return imageData;
}

void WebImageView::imageLoaded() {

	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	QVariant possibleRedirectUrl = reply->attribute(QNetworkRequest::RedirectionTargetAttribute);

	_oldRedirectUrl = redirectUrl(possibleRedirectUrl.toUrl(), _oldRedirectUrl);

	if(!_oldRedirectUrl.isEmpty())
	{
		curRequest = mNetManager->get(QNetworkRequest(_oldRedirectUrl));
		connect(curRequest, SIGNAL(finished()), this, SLOT(imageLoaded()));
	}
	else
	{
		QByteArray jpegData = reply->readAll();

		if (reply->error() == QNetworkReply::NoError)
		{
//			//Temp Code for testing
//			int nFileSize = 0;
//			if (mUrl.toString().endsWith("5ibhyl9k4dvx1odlvouqrxm9w.jpg"))
//			{
//				nFileSize = jpegData.size();
//				QFile jpegFile("/accounts/1000/shared/photos/image.jpg");
//				if (jpegFile.open(QIODevice::WriteOnly)){
//					jpegFile.write(jpegData);
//					jpegFile.close();
//				}
//			}
//			//
//			if (mUrl.toString().endsWith("markhughes_x5qda4wbzba21f7bvutvvi5ot.jpg"))
//			{
//				nFileSize = jpegData.size();
//				QFile jpegFile1("/accounts/1000/shared/photos/image1.jpg");
//				if (jpegFile1.open(QIODevice::WriteOnly)){
//					jpegFile1.write(jpegData);
//					jpegFile1.close();
//				}
//			}
			/** Save data to temp directory. **/
			Image * img;
			if(jpegData.size() > 640000){
				QImage org;
				org.loadFromData(jpegData);
				QImage scaledimg = org.scaled(800, 800, Qt::KeepAspectRatioByExpanding);
				ImageData imageData = fromQImage(scaledimg);
				img = new Image(imageData);
			}
			else{
				img = new Image(jpegData);
			}

			{
				ImageCache::getSharedImageCache()->saveImageToMemory(mUrl.toString(), img);
				/** Set Image here **/
				setImage(*img);
				emit imageDownloaded();
			}

			curRequest->deleteLater();
			curRequest = NULL;
		}
		else
		{
			Image imageTemp = Image(QUrl("asset:///images/defaultFeed.jpg"));
			setImage(imageTemp);

			emit imageDownloaded();

			curRequest->deleteLater();
			curRequest = NULL;
		}
	}
	reply->deleteLater();
}

void WebImageView::onTouch(bb::cascades::TouchEvent* event) {
	Q_UNUSED(event);
	//if(event->isUp())
	//	m_EventHandler->OnTouchImageSlider(this);
}





