/*
 * ImageCache.h
 *
 *  Created on: Jul 4, 2013
 *      Author: team2
 */

#ifndef IMAGECACHE_H_
#define IMAGECACHE_H_

#include <bb/cascades/Image>
#include <QByteArray>
#include <QObject>
#include <QMutex>

//Save image file into tmp directory, load them when necessary
using namespace bb::cascades;

class ImageCache : public QObject{
public:
	ImageCache();
	virtual ~ImageCache();

	//Get image from local path.
	Image loadImage(const QString & key);
	QString getImageUrl(const QString & key);

	void saveImage (const QString & key, const QString &filename,QByteArray& data);
	void clearFiles();

	//Get image from memory
	Image* getImageFromMemory(const QString &key);
	void saveImageToMemory(const QString &key, Image *img);
	void clearMemory();

	static ImageCache * getSharedImageCache();
private:
	QString getPathFromName(const QString & name);
	QString savePath;
private:
	QMutex mutex;
	QMap<QString,QString> imageFiles;
	QMap<QString,Image *> memImgs;

};

#endif /* IMAGECACHE_H_ */
