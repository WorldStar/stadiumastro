/*
 * AstroRegisterService.cpp
 *
 *  Created on: Aug 28, 2013
 *      Author: team2
 */

#include "AstroRegisterService.h"
#include "Config.h"
#include <bb/device/HardwareInfo>
#include <bb/data/JsonDataAccess>

using namespace bb::device;
using namespace bb::data;

AstroRegisterService::AstroRegisterService(QObject * parent)
: QObject(parent)
{
	// TODO Auto-generated constructor stub
	HardwareInfo info;

	m_PIN = info.pin();

}

AstroRegisterService::~AstroRegisterService() {
	// TODO Auto-generated destructor stub
}

void AstroRegisterService::onFinishSubscribe()
{
	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	if(reply->error())
	{
		qDebug() << "Register was failed.";
	}
}

void AstroRegisterService::onLoadPushSetting()
{
	QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());

	// Network reply error.
	// Can't updated push setting!
	if(reply->error() || reply->bytesAvailable() == 0)
	{
		qDebug() << "Network access error";
		emit requestFailed(reply->url().path());
		reply->deleteLater();
		return;
	}

	JsonDataAccess jsondata;
	//parse response data

	QVariant setting = jsondata.load(reply);
	reply->deleteLater();

	emit onGetPushStatus(setting);
}

void AstroRegisterService::loadPushSetting(QString refKey)
{
	QMap<QString, QString > params;

	params.insert("service", "mobileAppSportsService" );
	params.insert("action", "loadPushSetting");

	params.insert("p1", m_PIN);
	params.insert("p2", APP_ID);
	params.insert("p3", refKey);
	params.insert("mimeType", "application/json");
	m_connector.SendPushRequest(params, this, SLOT(onLoadPushSetting()));

}

void AstroRegisterService::enableEventPush(QString refKey, QString eventKey, bool bEnable)
{
	QMap<QString, QString> params;

	params.insert("service", "mobileAppSportsService" );
	QString keys[5] = {"MSTART", "MHALF", "MFULL", "MGOAL", "MDISC"};

	int i;
	for(i = 0; i < 5; i++){
		if(eventKey == keys[i]) break;
	}
	if(i < 5)
		params.insert("action", "pushMatchEventAlert");
	else
		params.insert("action", "pushTeamAlert");

	QString enableStr;

	if(bEnable)
		enableStr = "Y";
	else
		enableStr = "N";

	params.insert("p1", enableStr);
	params.insert("p2", eventKey);
	params.insert("p3", m_PIN);
	params.insert("p4", APP_ID);
	params.insert("p5", refKey);
	params.insert("mimeType", "application/json");

	curEvtKey = eventKey;

	m_connector.SendPushRequest(params, this, SLOT(onEnableEventResponse()));
}
void AstroRegisterService::enablePush(QString refKey, bool bEnable)
{
	QMap<QString, QString> params;

	params.insert("service", "mobileAppSportsService" );
	params.insert("action", "enablePushSetting");

	QString enableStr;

	if(bEnable)
		enableStr = "Y";
	else
		enableStr = "N";

	params.insert("p1", enableStr);
	params.insert("p2", m_PIN);
	params.insert("p3", APP_ID);
	params.insert("p4", refKey);
	params.insert("mimeType", "application/json");
	m_connector.SendPushRequest(params, this,SLOT(onEnablePushResponse()));
}

void AstroRegisterService::onEnablePushResponse()
{
	QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());

	// Network reply error.
	// Can't updated push setting!
	if(reply->error() || reply->bytesAvailable() == 0)
	{
		qDebug() << "Network access error";
		emit requestFailed(reply->url().path());

		reply->deleteLater();
		return;
	}

	JsonDataAccess jsondata;
	//parse response data
	QVariant setting = jsondata.load(reply);
	QVariantMap status = setting.toMap();
	reply->deleteLater();
	if(status.value("status").toString() == "OK")
	{
		loadPushSetting(status.value("sportsEventRefKey").toString());
	}
}
void AstroRegisterService::onEnableEventResponse()
{
	QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());

	// Network reply error.
	// Can't updated push setting!
	if(reply->error() || reply->bytesAvailable() == 0)
	{
		qDebug() << "Network access error";
		reply->deleteLater();

		emit changedEventPush(curEvtKey, false);
		return;
	}

	JsonDataAccess jsondata;
	//parse response data
	QVariant setting = jsondata.load(reply);
	QVariantMap status = setting.toMap();
	reply->deleteLater();
	if(status.value("status").toString() == "OK" || status.value("status").toString() == "ON" || status.value("status").toString() == "OFF")
		emit changedEventPush(curEvtKey, true);
	else
		emit changedEventPush(curEvtKey, false);
}
