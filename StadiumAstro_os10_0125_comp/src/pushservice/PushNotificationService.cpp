/*
 * PushNotificationService.cpp
 *
 *  Created on: Aug 28, 2013
 *      Author: team2
 */

#include "PushNotificationService.hpp"
#include <QDebug>
#include "Config.h"


PushNotificationService::PushNotificationService(QObject *parent)
:QObject(parent)
,m_pushService(0)
{
	// TODO Auto-generated constructor stub
	m_pushService = NULL;


}
PushNotificationService::~PushNotificationService() {
	// TODO Auto-generated destructor stub
}

void PushNotificationService::createSession()
{
	// Initialize the PushService if it has not been already
	initializePushService();

	// Check to see if the PushService has a connection to the Push Agent.
	// This can occur if the application doesn't have sufficient permissions to use Push.
	// For more information on required permissions, please refer to the developer guide.

	m_pushService->createSession();
	if (m_pushService->hasConnection()){
		emit noPushServiceConnection();
	}
}

void PushNotificationService::createChannel()
{

	if (m_pushService->hasConnection()){
		m_pushService->createChannel(QUrl(PPG_URL));
	} else {
		emit noPushServiceConnection();
	}
}

void PushNotificationService::initializePushService()
{
	if (m_pushService == NULL) {

		// If a PushService instance has never been created or if the app id has changed, then create a new PushService instance
		// Important note: App ids would not change in a real application, but this sample application allows this.
		// To allow the app id change, we delete the previously existing PushService instance.

		m_pushService = new PushService(APP_ID, INVOKE_TARGET_KEY_PUSH);

		//Connect the signals
		QObject::connect(m_pushService, SIGNAL(createSessionCompleted(const bb::network::PushStatus&)),
				this, SIGNAL(createSessionCompleted(const bb::network::PushStatus&)));
		QObject::connect(m_pushService, SIGNAL(createChannelCompleted(const bb::network::PushStatus&, const QString)),
				this, SIGNAL(createChannelCompleted(const bb::network::PushStatus&, const QString)));
		QObject::connect(m_pushService, SIGNAL(destroyChannelCompleted(const bb::network::PushStatus&)),
				this, SIGNAL(destroyChannelCompleted(const bb::network::PushStatus&)));
		QObject::connect(m_pushService, SIGNAL(registerToLaunchCompleted(const bb::network::PushStatus&)),
				this, SIGNAL(registerToLaunchCompleted(const bb::network::PushStatus&)));
		QObject::connect(m_pushService, SIGNAL(unregisterFromLaunchCompleted(const bb::network::PushStatus&)),
				this, SIGNAL(unregisterFromLaunchCompleted(const bb::network::PushStatus&)));
		QObject::connect(m_pushService, SIGNAL(simChanged()), this, SIGNAL(simChanged()));
		QObject::connect(m_pushService, SIGNAL(pushTransportReady(bb::network::PushCommand::Type)),
				this, SIGNAL(pushTransportReady(bb::network::PushCommand::Type)));

	}
}

void PushNotificationService::registerToLaunch()
{
	m_pushService->registerToLaunch();
}

void PushNotificationService::unregisterFromLaunch()
{
	m_pushService->unregisterFromLaunch();
}

void PushNotificationService::destroyChannel()
{
	if (m_pushService->hasConnection()){
		m_pushService->destroyChannel();
	}  else {
		emit noPushServiceConnection();
	}
}

void PushNotificationService::acceptPush(const QString &payloadId)
{
	m_pushService->acceptPush(payloadId);
}

void PushNotificationService::rejectPush(const QString &payloadId)
{
	m_pushService->rejectPush(payloadId);
}

void PushNotificationService::handleSimChange()
{

}

