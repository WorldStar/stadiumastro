/*
 * Config.h
 *
 *  Created on: Aug 28, 2013
 *      Author: team2
 */

#ifndef CONFIG_H_
#define CONFIG_H_
#include <QString>
static const QString APP_ID = "3855-s784457B9m331044n0cr03mii8c725l1615";
static const QString PPG_URL = "http://cp3855-s784457B9m331044n0cr03mii8c725l1615.pushapi.eval.blackberry.com";

static const QString OPEN_INVOCATION_ACTION = "bb.action.OPEN";
static const QString PUSH_INVOCATION_ACTION = "bb.action.PUSH";
// This needs to match the invoke target specified in bar-descriptor.xml
// The Invoke target key for receiving new push notifications
static const QString INVOKE_TARGET_KEY_PUSH = "com.baihu.StadiumAstro";

// This needs to match the invoke target specified in bar-descriptor.xml
// The Invoke target key when selecting a notification in the BlackBerry Hub
static const QString INVOKE_TARGET_KEY_OPEN = "com.baihu.StadiumAstro.invoke.open";


#endif /* CONFIG_H_ */
