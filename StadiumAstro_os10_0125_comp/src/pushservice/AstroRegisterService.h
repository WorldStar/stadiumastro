/*
 * AstroRegisterService.h
 *
 *  Created on: Aug 28, 2013
 *      Author: team2
 */

#ifndef ASTROREGISTERSERVICE_H_
#define ASTROREGISTERSERVICE_H_

#include <QObject>
#include <QVariant>


#include "JSONConnect.hpp"

class AstroRegisterService : public QObject {
	Q_OBJECT

public Q_SLOTS:
	void onFinishSubscribe();

public:
	AstroRegisterService(QObject * parent  = 0);
	virtual ~AstroRegisterService();

	void enablePush(QString refKey, bool bEnable);
	void loadPushSetting(QString refKey);
	void enableEventPush(QString refKey, QString eventKey, bool bEnable);

	Q_SIGNAL void onGetPushStatus(QVariant json);
	Q_SIGNAL void changedEventPush(QString refKey, bool newState);
	Q_SIGNAL void requestFailed(QString url);


public Q_SLOTS:
	void onLoadPushSetting();
	void onEnablePushResponse();
	void onEnableEventResponse();

private:
	JSONConnect m_connector;
	QString m_PIN;

	QString curEvtKey;
};

#endif /* ASTROREGISTERSERVICE_H_ */

