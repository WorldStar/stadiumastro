/*
 * PushNotificationService.h
 *
 *  Created on: Aug 28, 2013
 *      Author: team2
 */

#ifndef PUSHNOTIFICATIONSERVICE_H_
#define PUSHNOTIFICATIONSERVICE_H_


#include <bb/network/PushService>
#include <bb/network/PushStatus>
#include <QObject>
#include <QVariantList>
#include <QNetworkAccessManager>


#include "AstroRegisterService.h"
using namespace bb::network;

class PushNotificationService : public QObject
{
	Q_OBJECT
public:
	PushNotificationService(QObject *parent = 0);
	virtual ~PushNotificationService();
	void createSession();
	void createChannel();
	void registerToLaunch();
	void unregisterFromLaunch();
	void destroyChannel();
	void initializePushService();
	void acceptPush(const QString &payloadId);
	void rejectPush(const QString &payloadId);
	void handleSimChange();

Q_SIGNALS:
	void createSessionCompleted(const bb::network::PushStatus &status);
	void createChannelCompleted(const bb::network::PushStatus &status, const QString &token);
	void destroyChannelCompleted(const bb::network::PushStatus &status);
	void registerToLaunchCompleted(const bb::network::PushStatus &status);
	void unregisterFromLaunchCompleted(const bb::network::PushStatus &status);
	void simChanged();
	void pushTransportReady(bb::network::PushCommand::Type command);
	void noPushServiceConnection();

private:
	PushService *m_pushService;
	AstroRegisterService m_registerService;
};


#endif /* PUSHNOTIFICATIONSERVICE_H_ */
