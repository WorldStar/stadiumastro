/*
 * StadiumEventHandler.cpp
 *
 *  Created on: Jun 9, 2013
 *      Author: Star
 */
#include <bb/cascades/Application>
#include <bb/cascades/Container>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/QmlDocument>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/ArrayDataModel>
#include <bb/cascades/Label>
#include <bb/cascades/ListView>
#include <bb/cascades/ScrollView>
#include <bb/cascades/ImageView>
#include <bb/cascades/StackLayout>
#include <bb/cascades/StackLayoutProperties>
#include <bb/cascades/WebView>
#include <bb/device/DisplayInfo>
#include <bb/PpsObject>

#include <QtCore/QDateTime>

#include "facebook/Facebook.hpp"
#include "facebook/FBUser.h"
#include "facebook/FBPlace.h"
#include "facebook/FBPost.h"
#include "facebook/FBComment.h"
#include "facebook/FacebookObject.h"

#include "WebImageView.h"
#include "SportsEventTab.hpp"
#include "SportsEventBar.hpp"
#include "ImageCache.hpp"
#include "ImageLoader.h"
#include "StadiumEventHandler.h"
#include "AstroRegisterService.h"



using namespace bb::cascades;
using namespace bb::data;
using namespace bb::system;

//More page implementation.
//-----------------------

void StadiumEventHandler::ShowMorePage()
{
	CreateViewPage();

	setViewPageTitle("More");
	OnShowMorePage();
}

void StadiumEventHandler::OnShowMorePage()
{
	// confirm static UI
	setMorePageInfo();
}

void StadiumEventHandler::setMorePageInfo()
{
	// show to screen
	Page *viewPane = m_homePane->top();

	QVariantMap eventInfo = sportsEventInfo[m_sportEventsContent_refKey].toMap();

	Container *moreContent = viewPane->findChild<Container*>("moreContent");
	if(eventInfo.value("includeTVChannelList").toString().compare("Y") == 0)
	{
		qDebug() << "enabled";
		moreContent->setProperty("channelEnabled", true);
	}
	else
	{
		qDebug() << "disabled";
		moreContent->setProperty("channelEnabled", false);
	}

	displayViewPageContent(moreContent);
}

void StadiumEventHandler::ShowTVChannelListPage()
{

	CreateViewPage();

	setViewPageTitle("Channels");

	//For Refresh(Passion)
	setViewPageIdentifier(m_sportEventsContent_refKey);
	setViewPageType("TVChannelList");
	Page *viewPane = m_homePane->top();
	VisualNode *btn = viewPane->findChild<VisualNode *>("pageRefresh");
	btn->setVisible(true);


	showActivityIndicator(true);
	// TODO: request 'TV Channel List' info to server
	QMap < QString, QString > params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "TVChannelList");

	params.insert("p2", m_sportEventsContent_refKey);

	jsonNetwork->SendGetRequest(params, this, SLOT(setTVChannelListPageInfo()));
}

void StadiumEventHandler::requestTVChannelListInfo(QString strId)
{
	// TODO: request 'TV Channel List' info to server
	QMap < QString, QString > params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "TVChannelList");

	params.insert("p2", strId);

	jsonNetwork->SendGetRequest(params, this, SLOT(setTVChannelListPageInfo()));
}

/** @brief
 *		parsing TV channel list data and set into UI components
 *	@param
 *		none.
 **/
void StadiumEventHandler::setTVChannelListPageInfo()
{
	Page *viewPane = m_homePane->top();
	// define variables
	JsonDataAccess jsondata;
	QVariantList varList;

	// get category info
	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());

	showActivityIndicator(false);
	if (reply->error() == 0) {
		QVariantMap varMap =
				jsondata.load(
						reply).value<
						QVariantMap>();
		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(
							err.errorMessage());
			return;
		}
		varList = varMap.value("channels").toList();

		// prepare data model
		GroupDataModel *channelCategoryModel = new GroupDataModel();
		channelCategoryModel->setParent(this);

		// reverse list data and set into model
		QVariantList finalList;
		finalList.clear();
		for (int i = varList.count() - 1; i >= 0; i--) {
			finalList.append(varList.at(i));
		}

		channelCategoryModel->insertList(finalList);
		channelCategoryModel->setGrouping(ItemGrouping::None);

		// set preview list model
		ListView *channelList = viewPane->findChild<ListView*>("channelList");
		channelList->setDataModel(channelCategoryModel);

		// show to screen
		displayViewPageContent(channelList);
	}

	reply->deleteLater();
}

void StadiumEventHandler::ShowChannelEventPage(QVariant channelData)
{
	CreateViewPage();

	QVariantMap channelMap = channelData.toMap();
	setViewPageTitle(channelMap["name"].toString());

	//For Refresh(Passion)
	setViewPageIdentifier(channelMap["id"].toString());
	setViewPageType("TVChannelCalendarList");
	Page *viewPane = m_homePane->top();
	VisualNode *btn = viewPane->findChild<VisualNode *>("pageRefresh");
	btn->setVisible(true);


	showActivityIndicator(true);
	// TODO: request 'TV Channel Calendar List' info to server, its parameter shown below
	//		channelMap["id"].toString()
	QMap < QString, QString > params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "TVChannelCalendarList");

	params.insert("p2", channelMap["id"].toString());

	jsonNetwork->SendGetRequest(params, this, SLOT(setTVChannelCalendarListInfo()));
}

void StadiumEventHandler::requestChannelEventInfo(QString strId)
{
	// TODO: request 'TV Channel Calendar List' info to server, its parameter shown below
	//		channelMap["id"].toString()
	QMap < QString, QString > params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "TVChannelCalendarList");

	params.insert("p2", strId);

	jsonNetwork->SendGetRequest(params, this, SLOT(setTVChannelCalendarListInfo()));
}

void StadiumEventHandler::OnShowChannelEventPage()
{
	setTVChannelCalendarListInfo();
}

/** @brief
 *		parsing TV channel calendar list data and set into UI components
 *	@param
 *		none.
 **/
void StadiumEventHandler::setTVChannelCalendarListInfo()
{
	// define variables
	Page * viewPane = m_homePane->top();
	JsonDataAccess jsondata;
	QVariantList varList;

	showActivityIndicator(false);
	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	if (reply->error() == 0) {
		// get category info
		QVariantMap varMap =
				jsondata.load(
						reply
						).value<
						QVariantMap>();
		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(
							err.errorMessage());
			return;
		}
		varList = varMap.value("tvChannelCalendars").toList();

		// prepare data model
		GroupDataModel *channelCalendarModel = new GroupDataModel();
		channelCalendarModel->setParent(this);

		// reverse list data and set into model
		QVariantList finalList;
		finalList.clear();
		for (int i = varList.count() - 1; i >= 0; i--) {
			finalList.append(varList.at(i));
		}

		channelCalendarModel->insertList(finalList);
		channelCalendarModel->setGrouping(ItemGrouping::None);

		// set calendar list model
		ListView *channelCalendarList = viewPane->findChild<ListView*>(
				"channelCalendarList");
		channelCalendarList->setDataModel(channelCalendarModel);

		// show to screen
		Container *tvChannelEventContent = viewPane->findChild<Container*>(
				"tvChannelEventContent");
		displayViewPageContent(tvChannelEventContent);
	}

	reply->deleteLater();
}

void StadiumEventHandler::getChannelEventListData(QString channelCalendarId)
{
	// TODO: request 'TV Channel Event List' data to server with channelCalendarId parameter
	QMap < QString, QString > params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "TVChannelEventList");

	params.insert("p2", channelCalendarId);

	jsonNetwork->SendGetRequest(params, this, SLOT(setChannelEventListData()));
}

/** @brief
 *		parsing TV channel event list data for particular date and set into UI components
 *	@param
 *		none.
 **/

void StadiumEventHandler::setChannelEventListData()
{
	Page *viewPane = m_homePane->top();
	// load json data
	JsonDataAccess jsondata;
	QVariantList varList;

	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	if (reply->error() == 0) {
		// get event info
		QVariantMap varMap =
				jsondata.load(
						reply
						).value<
						QVariantMap>();
		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(
							err.errorMessage());
			qDebug() << errorMsg;
			return;
		}

		varList = varMap.value("channelEvents").toList();

		// set event date string
		QVariantList::Iterator item = varList.begin();
		while (item != varList.end()) {
			QVariantMap itemMap = (*item).toMap();

			QDateTime dateTime = QDateTime::fromString(
					itemMap["eventDateTime"].toString(), Qt::ISODate);
			itemMap["strEventDate"] = dateTime.toString("dd MMM yyyy | dddd");

			// save to map data
			(*item) = itemMap;

			++item;
		}

		// prepare data model
		GroupDataModel *eventListModel = new GroupDataModel();

		eventListModel->insertList(varList);
		eventListModel->setGrouping(ItemGrouping::None);
		eventListModel->setParent(this);

		QStringList sortingKeys;
		sortingKeys << "eventDateTime";
		eventListModel->setSortingKeys(sortingKeys);

		// set video feed list model
		ListView *channelEventList = viewPane->findChild<ListView*>(
				"channelEventList");
		channelEventList->setDataModel(eventListModel);
	}
}

void StadiumEventHandler::showPushSetting()
{
	CreateViewPage();

	setViewPageTitle("Push Notification");

	Page * viewPane = m_homePane->top();
	Container* pushView = viewPane->findChild<Container *>("PushSetting");

	Container* admob = viewPane->findChild<Container*>("admob");
	admob->setVisible(false);

	this->displayViewPageContent(pushView);

	showActivityIndicator(true);
	m_registerService.loadPushSetting(m_sportEventsContent_refKey);

}

void StadiumEventHandler::enablePushForItem(QString refKey, bool isChecked)
{
	bool bb = toBool(curPushOptions.value(refKey).toString());
	if(toBool(curPushOptions.value(refKey).toString()) == isChecked)
		return;

	showActivityIndicator(true);
	if(refKey == "pushEnableFlag")
	{
		m_registerService.enablePush(m_sportEventsContent_refKey, isChecked);
	}
	else
	{
		m_registerService.enableEventPush(m_sportEventsContent_refKey, refKey, isChecked);
	}
}

void StadiumEventHandler::onGetPushStatus(QVariant pushStatus)
{
	showActivityIndicator(false);

	QString keys[5][3] = {{"matchFullTimeAlert", "Full time alert", "MFULL"},
						{"matchGoalAlert", "Goal alert", "MGOAL"},
						{"matchHalfTimeAlert", "Half time alert", "MHALF"},
						{"matchStartAlert" , "Kick Off", "MSTART"},
						{"matchDisciplineAlert", "Yellow/Red Card" , "MDISC"}
	};


	QVariantMap statusMap = pushStatus.toMap();

	//Save current push options
	curPushOptions["pushEnableFlag"] = statusMap.value("pushEnableFlag").toString();

	for(int i = 0; i < 5; i++)
	{
		curPushOptions[keys[i][2]] = statusMap.value(keys[i][0]).toString();
	}

	QVariantList list = statusMap.value("teams").toList();
//	QVariantList::Iterator iterator = list.begin();
//	while(iterator != list.end())
//	{
//		QVariantMap value = (*iterator).toMap();
//		curPushOptions[value.keys().at(0)] = value.values().at(0);
//		iterator ++;
//	}

	//Set UI
	QString sEnabled;
	bool bEnabled,bInsert;
	bool bOdd = true;
	if(isPushListEmpty())
		bInsert = true;
	else
		bInsert = false;
	sEnabled = statusMap.value("pushEnableFlag").toString();
	bEnabled = toBool(sEnabled);

	bOdd = !bOdd;

	if(bInsert)
	{
		addPushItemCtrl("Enable Push Alert", "pushEnableFlag", bEnabled, bOdd);
		addPushHeaderCtrl("General", "General");
	}
	else
		setItemChecked("pushEnableFlag", bEnabled);

	setItemVisible("General", bEnabled);

	for(int i = 0; i < 5; i++)
	{
		sEnabled = statusMap.value(keys[i][0]).toString();
		bOdd = !bOdd;

		if(bInsert)
			addPushItemCtrl(keys[i][1], keys[i][2], toBool(sEnabled), bOdd);
		else
			setItemChecked(keys[i][2], toBool(sEnabled));

		setItemVisible(keys[i][2], bEnabled);
	}

	if(bInsert)
		addPushHeaderCtrl("Team", "TeamAlert");

	setItemVisible("TeamAlert", bEnabled);

	list = statusMap.value("teams").toList();
//	if(list.size() == 0)
//	{
//		QHash<QString, QVariantMap>::Iterator hashIterator = m_teamsLogoTable->begin();
//		while(hashIterator != m_teamsLogoTable->end())
//		{
//			QString teamId = hashIterator.key();
//			QString title = hashIterator.value()["teamName"].toString();
//			bOdd = !bOdd;
//
//			if(bInsert)
//				addPushItemCtrl(title, teamId, false, bOdd);
//			setItemChecked(teamId, false);
//			setItemVisible(teamId, bEnabled);
//			hashIterator ++;
//		}
//	}
//	else
//	{
//		QVariantList listTeam;
//		QVariantList::Iterator listIterator = list.begin();
//		while(listIterator != list.end())
//		{
//			QVariantMap teamRecord = listIterator->toMap();
//			QString teamId = teamRecord.value("teamID").toString();
//			if(teamId == "clazz"){
//				listIterator ++;
//				continue;
//			}
//			QString teamName = m_teamsLogoTable->value(teamId)["teamName"].toString();
//			sEnabled = teamRecord.value("enableFlag").toString();
//			teamRecord.insert("teamName", teamName);
//			bool bInserted = false;
//
//			QVariantList::Iterator listTeamIterator = listTeam.begin();
//			int i = 0;
//			while(listTeamIterator != listTeam.end())
//			{
//				QVariantMap teamRecord1 = listTeamIterator->toMap();
//				QString str = teamRecord1["teamName"].toString();
//				if(teamName < str){
//					listTeam.insert(i, teamRecord);
//					bInserted = true;
//					break;
//				}
//				i++;
//				listTeamIterator++;
//			}
//			if(bInserted == false)
//				listTeam.append(teamRecord);
//			listIterator ++;
//		}
//
//		listIterator = listTeam.begin();
//		while(listIterator != listTeam.end())
//		{
//			QVariantMap teamRecord = listIterator->toMap();
//			QString teamId = teamRecord.value("teamID").toString();
//			QString teamName = teamRecord.value("teamName").toString();
//			sEnabled = teamRecord.value("enableFlag").toString();
//			bOdd = !bOdd;
//
//			if(bInsert)
//				addPushItemCtrl(teamName, teamId, toBool(sEnabled), bOdd);
//			else
//				setItemChecked(teamId, toBool(sEnabled));
//
//			setItemVisible(teamId, bEnabled);
//			listIterator ++;
//		}
//	}

	QVariantList tmpList;
	QHash<QString, QVariantMap>::Iterator hashIterator = m_teamsLogoTable->begin();
	while(hashIterator != m_teamsLogoTable->end())
	{
		QString teamId = hashIterator.key();
		QString teamName = hashIterator.value()["teamName"].toString();

		bool bSet = false;
		QVariantList::Iterator listIterator = list.begin();
		while(listIterator != list.end())
		{
			QVariantMap teamRecord = listIterator->toMap();
			QString teamIdl = teamRecord.value("teamID").toString();
			if(teamId == teamIdl){
				bSet = true;
				sEnabled = teamRecord.value("enableFlag").toString();
				break;
			}
			listIterator ++;
		}

		QVariantMap teamItem;
		teamItem.insert("teamID", teamId);
		teamItem.insert("teamName", teamName);
		if(bSet == true){
			teamItem.insert("enableFlag", sEnabled);
			curPushOptions[teamId] = sEnabled;
		}else{
			teamItem.insert("enableFlag", "N");
			curPushOptions[teamId] = "N";
		}

		bool bInserted = false;
		listIterator = tmpList.begin();
		int i = 0;
		while(listIterator != tmpList.end())
		{
			QVariantMap teamRecord = listIterator->toMap();
			QString str = teamRecord["teamName"].toString();
			if(teamName < str){
				tmpList.insert(i, teamItem);
				bInserted = true;
				break;
			}
			i++;
			listIterator++;
		}
		if(bInserted == false)
			tmpList.append(teamItem);
		hashIterator ++;
	}


	QVariantList::Iterator listIterator = tmpList.begin();
	while(listIterator != tmpList.end())
	{
		QVariantMap teamRecord = listIterator->toMap();
		QString teamId = teamRecord.value("teamID").toString();
		QString teamName = teamRecord.value("teamName").toString();
		sEnabled = teamRecord.value("enableFlag").toString();
		bOdd = !bOdd;

		if(bInsert)
			addPushItemCtrl(teamName, teamId, toBool(sEnabled), bOdd);
		else
			setItemChecked(teamId, toBool(sEnabled));

		setItemVisible(teamId, bEnabled);
		listIterator ++;
	}
}

bool StadiumEventHandler::toBool(QString sVal)
{
	if(sVal == "Y")
	{
		return true;
	}
	else
		return false;
}

void StadiumEventHandler::addPushItemCtrl(QString title, QString refKey, bool bChecked,bool odd)
{
	Page *curPage = m_homePane->top();

	Container * pContainer = curPage->findChild<Container *>("PushList");

	QmlDocument * qml = QmlDocument::create("asset:///PushAlertItem.qml");
	qml->setContextProperty("eventHandler", this);
	Container * item = qml->createRootObject<Container>();
	qml->setParent(item);

	item->setProperty("itemRoot", odd);
	item->setProperty("itemKey", refKey);
	item->setProperty("itemTitle", title);
	item->setProperty("itemChecked", bChecked);

	Color backColor;
	if(odd)
		backColor = Color::LightGray;
	else
		backColor = Color::White;


	pContainer->add(item);
}

void StadiumEventHandler::addPushHeaderCtrl(QString title, QString refKey)
{
	Page *curPage = m_homePane->top();

	Container * pContainer = curPage->findChild<Container *>("PushList");

	QmlDocument * qml = QmlDocument::create("asset:///PushAlertHeader.qml");
	qml->setContextProperty("eventHandler", this);
	Container * item = qml->createRootObject<Container>();
	qml->setParent(item);
	item->setProperty("text", title);
	item->setProperty("itemKey", refKey);
	pContainer->add(item);
}
void StadiumEventHandler::setItemChecked(QString refKey, bool bChecked)
{
	Page *curPage = m_homePane->top();
	Container * pContainer = curPage->findChild<Container *>("PushList");
	QObjectList childList = pContainer->children();
	QObjectList::Iterator iterator = childList.begin();

	while(iterator != childList.end())
	{

		QString str = (*iterator)->property("itemKey").toString();
		if(str == refKey)
			(*iterator)->setProperty("itemChecked", bChecked);

		iterator++;
	}
}
bool StadiumEventHandler:: isPushListEmpty()
{
	Page *curPage = m_homePane->top();
	Container * pContainer = curPage->findChild<Container *>("PushList");
	if(pContainer->children().size() == 0)
		return true;
	else
		return false;
}
void StadiumEventHandler::setItemVisible(QString refKey, bool bVisible)
{
	Page *curPage = m_homePane->top();
	Container * pContainer = curPage->findChild<Container *>("PushList");
	QObjectList childList = pContainer->children();
	QObjectList::Iterator iterator = childList.begin();

	while(iterator != childList.end())
	{
		QString str = (*iterator)->property("itemKey").toString();
		if(str == refKey)
			(*iterator)->setProperty("visible", bVisible);
		iterator++;
	}
}
void StadiumEventHandler::changedEventPush(QString refKey, bool bSuccess)
{
	showActivityIndicator(false);
	qDebug() << "Event Changed";
	if(bSuccess)
	{
		m_registerService.loadPushSetting(m_sportEventsContent_refKey);
	}
}

void StadiumEventHandler::registerRequestFailed(QString requestUrl)
{
	showActivityIndicator(false);
}
