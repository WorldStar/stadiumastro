/*
 * SportsEventTab.h
 *
 *  Created on: Jul 5, 2013
 *      Author: team2
 */

#ifndef SPORTSEVENTTAB_H_
#define SPORTSEVENTTAB_H_
#include <QObject>
#include <QString>
#include <bb/cascades/CustomControl>
#include <bb/cascades/Container>
#include <bb/cascades/Label>
#include <bb/cascades/TapHandler>
#include <bb/cascades/TapEvent>
#include <bb/cascades/Color>



using namespace bb::cascades;
class WebImageView;

class SportsEventTab : public bb::cascades::CustomControl {
	Q_OBJECT
	Q_PROPERTY(QString imgUrl READ imgUrl WRITE setImgUrl NOTIFY imgUrlChanged)
	Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)
	Q_PROPERTY(QString refKey READ refKey WRITE setRefKey NOTIFY refKeyChanged)
	Q_PROPERTY(QString showMatchForToday READ showMatchForToday WRITE setShowMatchForToday NOTIFY showMatchForTodayChanged)

public:
	SportsEventTab();
	virtual ~SportsEventTab();

	QString imgUrl();
	void setImgUrl(QString newUrl);


	void setImgSource(QString imgSrc);

	QString title();
	void setTitle(QString newTitle);
	QString refKey();
	void setRefKey(QString newRefKey);
	QString inAppUrlLink();
	void select();
	void setInAppUrlLink(QString url);
	void unselect();

	QString showMatchForToday();
	void setShowMatchForToday(QString);

public slots:
	void onTappedHandler(bb::cascades::TapEvent * event);

signals:

	void imgUrlChanged();
	void titleChanged();
	void refKeyChanged();
	void showMatchForTodayChanged();

	void click(bb::cascades::TapEvent *);
private:
	QString mRefKey;
	QString inAppUrl;
	QString mShowMatch;
	bool bSwitch;


	//Color Setting
	Color NORMAL_COLOR;
	Color SELECT_COLOR;

	//Controls
	Container * backContainer;

	WebImageView * mImgView;
	Label *mTitle;

};

#endif /* SPORTSEVENTTAB_H_ */
