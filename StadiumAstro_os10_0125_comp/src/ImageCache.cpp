/*
 * ImageCache.cpp
 *
 *  Created on: Jul 4, 2013
 *      Author: team2
 */

#include "ImageCache.hpp"


static ImageCache * sharedImageCache = new ImageCache();

ImageCache::ImageCache() {
	// TODO Auto-generated constructor stub
	savePath = QDir::homePath();

}

ImageCache::~ImageCache() {
	// TODO Auto-generated destructor stub
	clearFiles();
	clearMemory();
}

void ImageCache::clearFiles()
{
	QDir dir(savePath);
	QMap<QString,QString>::iterator i = imageFiles.begin();

	while(i != imageFiles.end())
	{
		dir.remove(getPathFromName(i.value()));
		i++;
	}

	imageFiles.clear();
}

Image * ImageCache::getImageFromMemory(const QString &key)
{
	return memImgs.value(key);
}

void ImageCache::saveImageToMemory(const QString &key, Image *img)
{
	if(memImgs.value(key) != NULL)
		delete memImgs[key];
	memImgs[key] = img;
}

void ImageCache::clearMemory()
{
	QMap<QString,Image *>::iterator i = memImgs.begin();

	qDebug() << "Free Memory---";
	while(i != memImgs.end())
	{
		delete *i;
		i++;
	}

	memImgs.clear();

}

QString ImageCache::getPathFromName(const QString &name)
{
	QString path(savePath);
	path.append("/");
	path.append(name);
	return path;
}

void ImageCache::saveImage(const QString & key, const QString & filename,QByteArray& data)
{
	QMutexLocker locker(&mutex);
	QString finalfile;
	finalfile.append(filename);
	imageFiles[key] = finalfile;

	QFile file(getPathFromName(finalfile));
	file.open(QIODevice::WriteOnly|QIODevice::Truncate);

	file.write(data);
	file.close();
}
Image ImageCache::loadImage(const QString &key)
{

	QMutexLocker locker(&mutex);
	QFile file(getPathFromName(imageFiles.value(key)));

	file.open(QIODevice::ReadOnly);
	if(file.isOpen()){
		Image img(file.readAll());
		file.close();
		return img;
	}
	file.close();
	return Image();
}
QString ImageCache::getImageUrl(const QString & key)
{
	if(!imageFiles[key].isEmpty())
	{
		QString finalUrl("file://");
		finalUrl.append(savePath);
		finalUrl.append("/");
		finalUrl.append(imageFiles[key]);
		return finalUrl;
	}
	else
		return QString();
}

ImageCache * ImageCache::getSharedImageCache()
{
	return sharedImageCache;
}

