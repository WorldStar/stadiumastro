/* Copyright (c) 2012 Research In Motion Limited.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef JSONConnect_HPP
#define JSONConnect_HPP

#include <bb/cascades/GroupDataModel>
#include <bb/pim/contacts/ContactService>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/NavigationPane>
#include <QtCore/QObject>

/**
 * @short The controller class that makes access to contacts available to the UI.
 */
//! [0]
using namespace bb::cascades;

class JSONConnect : public QObject
{
    Q_OBJECT

public:
    //Initialize.
    JSONConnect(QObject *parent = 0);
    ~JSONConnect();
    bool RequestSportEventList();

    bool SendGetRequest(QMap<QString, QString> &params,QObject * target, const char *slot);
    bool SendGetRequest(QString url,QObject * target, const char *slot);
    bool SendPushRequest(QMap<QString, QString> &params,QObject * target, const char *slot);
    QObject* m_pParent;

    const QString& GetJsonUrl();
    const QString& GetSiteUrl();
public Q_SLOTS:

    /**
     * Marks the contact with the given @p indexPath as current.
     */

Q_SIGNALS:
    // The change notification signal for the property

private Q_SLOTS:


private:

    // The accessor methods of the properties
    bool NetworkCheck();		//network check
    QVariantList qlist;
    QString JSON_URL;
    QString SITE_URL;
    QString SPORT_EVENTLIST;
    QString PUSH_SERVER_URL;
    QNetworkAccessManager *networkAccessManager;

};
//! [0]

#endif
