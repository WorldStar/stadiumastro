/*
 * Utils.cpp
 *
 *  Created on: Jul 30, 2013
 *      Author: team2
 */

#include "Utils.h"

Utils::Utils() {
	// TODO Auto-generated constructor stub

}

Utils::~Utils() {
	// TODO Auto-generated destructor stub
}

QString Utils::Url2FileName(QString & url)
{
	QString strUrl(url);

	strUrl = strUrl.right(128);
	//Remove all special characters.
	strUrl.replace("/", "");
	strUrl.replace("\\", "");
	strUrl.replace("\"", "");
	strUrl.replace("\"", "");
	strUrl.replace(":", "");
	strUrl.replace("*", "");
	strUrl.replace("?", "");
	strUrl.replace("|", "");
	strUrl.replace("<", "");
	strUrl.replace(">", "");

	return strUrl;
}
