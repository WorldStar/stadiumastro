/*
 * StadiumViewHandler.cpp
 *
 *  Created on: Aug 26, 2013
 *      Author: team2
 */

#include <bb/cascades/Application>
#include <bb/cascades/Container>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/QmlDocument>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/Label>
#include <bb/cascades/ListView>
#include <bb/cascades/ScrollView>
#include <bb/cascades/ImageView>
#include <bb/cascades/StackLayout>
#include <bb/cascades/StackLayoutProperties>
#include <bb/cascades/WebView>
#include <bb/device/DisplayInfo>
#include <bb/PpsObject>

#include <QtCore/QDateTime>

#include "facebook/Facebook.hpp"
#include "facebook/FBUser.h"
#include "facebook/FBPlace.h"
#include "facebook/FBPost.h"
#include "facebook/FBComment.h"
#include "facebook/FacebookObject.h"

#include "WebImageView.h"
#include "SportsEventTab.hpp"
#include "SportsEventBar.hpp"
#include "ImageCache.hpp"
#include "ImageLoader.h"
#include "StadiumEventHandler.h"


using namespace bb::cascades;
using namespace bb::data;
using namespace bb::system;

void StadiumEventHandler::displayViewPageContent(Control* childControl)
{
	// if already displayed, then exit
	if(childControl->isVisible()) return;

	// if its not child of content container, then exit
	Page * viewPane = m_homePane->top();
	Container* contentContainer = viewPane->findChild<Container*>("contentContainer");
	if(contentContainer->indexOf(childControl) == -1) return;


	// find displayed control and set to invisible
	for(int i = 0; i < contentContainer->count(); i++)
	{
		Control* curCtrl = contentContainer->at(i);
		if(curCtrl->isVisible()){
			curCtrl->setVisible(false);
		}
	}

	// set control status
	childControl->setVisible(true);
}

void StadiumEventHandler::CreateViewPage()
{
	QmlDocument *qml = QmlDocument::create("asset:///ViewPage.qml");

	if(!qml->hasErrors()) {
		// create a event handler object
		qml->setContextProperty("eventHandler", this);

		// create root object for the UI
		Page*viewPane = qml->createRootObject<Page>();
		qml->setParent(viewPane);

		if(viewPane){
			// save to member variable
			viewPane->setProperty("screenWidth", m_screenWidth);
			ListView *calendar = viewPane->findChild<ListView*>("calendarList");
			connect(this, SIGNAL(scrollCalendarListToEnd()), calendar, SLOT(onInitializeComplete()));
			Container *analysisContent = viewPane->findChild<Container*>("analysisContent");
			QObject::connect(this, SIGNAL(selectFirstTab()), analysisContent, SLOT(selectFirstTab()));

			m_homePane->push(viewPane);
		}
	}
}

void StadiumEventHandler::setViewPageTitle(QString strTitle)
{
	Page * viewPane = m_homePane->top();

	Label *lblTitle = viewPane->findChild<Label*>("pageTitle");
	if(lblTitle)
		lblTitle->setText(strTitle);
	//Show or hide admob.
	Container * admob = viewPane->findChild<Container *>("admob");

	admob->setProperty("visible", isAdmobVisible);
	curAdmobView = admob->findChild<WebImageView*>("imgAdmob");

	getAdsBannerInfoForNews();
}

void StadiumEventHandler::setViewPageIdentifier(QString strIdentifier)
{
	Page * viewPane = m_homePane->top();
	viewPane->setProperty("identifier", strIdentifier);
}

void StadiumEventHandler::setViewPageType(QString strType)
{
	Page * viewPane = m_homePane->top();
	viewPane->setProperty("pageType", strType);
}

void StadiumEventHandler::refreshCurPage(QString pageTitle, QString identifier, QString pageType)
{
	if (pageTitle == "Analysis")
	{
		Page *viewPane = m_homePane->top();
		Container *analysisContent = viewPane->findChild<Container*>("analysisContent");
		QString strMatchId = analysisContent->property("strMatchID").toString();

		showActivityIndicator(true);
		requestMatchPreviewInfo(strMatchId);
		showActivityIndicator(true);
		requestMatchReportInfo(strMatchId);
		showActivityIndicator(true);
		requestMatchLineUpInfo(strMatchId);
		showActivityIndicator(true);
		requestMatchEventsInfo(strMatchId);
		showActivityIndicator(true);
		requestMatchVideoHighlightsInfo(strMatchId);
		showActivityIndicator(true);
		requestInMatchClipsInfo(strMatchId);

		return;
	}

	if (pageTitle == "Videos")
	{
		if (pageType == "VideoCategoryList")
		{
			showActivityIndicator(true);
			requestVideoCategoryInfo(identifier);
			return;
		}
		if (pageType == "VideosFeedList")
		{
			showActivityIndicator(true);
			requestVideoFeedListInfo(identifier);
			return;
		}
	}

	if (pageTitle == "News")
	{
		if (pageType == "NewsCategoryList")
		{
			showActivityIndicator(true);
			requestNewsCategoryInfo(identifier);
			return;
		}
	}

	if (pageTitle == "Matches")
	{
		if (pageType == "SoccerCalendarList")
		{
			showActivityIndicator(true);
			requestMatchListInfo(identifier);
			return;
		}
	}

	if (pageTitle == "Standings")
	{
		if (pageType == "SoccerStandingList")
		{
			showActivityIndicator(true);
			requestStandingInfo(identifier);
			return;
		}
	}

	if (pageTitle == "Channels")
	{
		if (pageType == "TVChannelList")
		{
			showActivityIndicator(true);
			requestTVChannelListInfo(identifier);
			return;
		}
	}

	if (pageType == "TVChannelCalendarList")
	{
		showActivityIndicator(true);
		requestChannelEventInfo(identifier);
		return;
	}

	if (pageType == "NewsFeedList")
	{
		showActivityIndicator(true);
		requestNewsFeedListInfo(identifier);
		return;
	}
}
