// Default empty project template
#include <bb/system/SystemDialog>
#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/TabbedPane>
#include <bb/data/DataSource>
#include <bb/data/JsonDataAccess>
#include <bb/system/InvokeRequest>
#include <bb/system/InvokeManager>

#include <facebook/Facebook.hpp>
#include <facebook/FBUser.h>
#include <facebook/FBPlace.h>
#include <facebook/FBPost.h>
#include <facebook/FBComment.h>
#include <facebook/FacebookObject.h>

#include <bb/network/PushPayload>
#include <bb/platform/Notification>

#include "StadiumAstro.h"
#include "StadiumEventHandler.h"
#include "WebImageView.h"
#include "JSONConnect.hpp"
#include "ImageLoader.h"
#include "Config.h"

#include "Push.hpp"

using namespace bb::cascades;
using namespace bb::data;
using namespace bb::system;
using namespace bb::platform;


StadiumAstro::StadiumAstro(bb::cascades::Application *app)
: QObject(app)
{



	// register event hander as property type
	//qmlRegisterType<StadiumEventHandler>("my.lib", 1, 0, "StadiumEventHandler");

	//add a QTimer class as a qml type
	qmlRegisterType<QTimer>("my.library", 1, 0, "QTimer");

	qmlRegisterType<WebImageView>("my.library", 1, 0, "WebImageView");
	qmlRegisterType<Facebook>("my.library", 1, 0, "Facebook");
	qmlRegisterType<FBUser>("my.library", 1, 0, "FBUser");
	qmlRegisterType<FBPlace>("my.library",1,0,"FBPlace");
	qmlRegisterType<FBPost>("my.library",1,0,"FBPost");
	qmlRegisterType<FBComment>("my.library",1,0,"FBComment");
	qmlRegisterType<FacebookObject>("my.library",1,0,"FacebookObject");

	// create scene document from main.qml asset
	// set parent to created document to ensure it exists for the whole application lifetime
	QmlDocument *qml = QmlDocument::create("asset:///Splash.qml").parent(this);

	if(!qml->hasErrors()) {
		// create a event handler object
		StadiumEventHandler *pHandler = new StadiumEventHandler(this);

		qml->setContextProperty("eventHandler", pHandler);
		qml->setContextProperty("mainInstance", this);

		// create root object for the UI
		connector = new JSONConnect(this);

		root = qml->createRootObject<AbstractPane>();

		connect(this,SIGNAL(replaceToAdsScreen()), root, SLOT(replaceToAdsScreen()));

		if(root){
			// set created root object as a scene
			Application::instance()->setScene(root);
		}
	}
}

void StadiumAstro::onCreateSessionCompleted(const bb::network::PushStatus &status)
{
	if (appWasLaunchedFromRibbon()) {

		m_pushNotificationService.createChannel();
	}

}

bool StadiumAstro::appWasLaunchedFromRibbon()
{
	return (invokeManager->startupMode()
				== ApplicationStartupMode::LaunchApplication);
}

void StadiumAstro::onCreateChannelCompleted(const PushStatus &status, const QString &token)
{
	m_pushNotificationService.registerToLaunch();
}
void StadiumAstro::onDestroyChannelCompleted(const PushStatus &status)
{

}
void StadiumAstro::onNoPushServiceConnection()
{

}
void StadiumAstro::onPushTransportReady(bb::network::PushCommand::Type command)
{

}
void StadiumAstro::onRegisterToLaunchCompleted(const PushStatus &status)
{
}
void StadiumAstro::onSimChanged()
{

}
void StadiumAstro::onUnregisterFromLaunchCompleted(const PushStatus &status)
{

}

StadiumAstro::~StadiumAstro()
{
	delete connector;
}

void StadiumAstro::getAdsBannerListInfo()
{
	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "AdsBannerList");

	params.insert("p2", "FULL_ADS");

	connector->SendGetRequest(params,this, SLOT(setAdsBannerListInfo()));
}

void StadiumAstro::setAdsBannerListInfo()
{
	// get ads banner list data
	qDebug()<<"Read Banner Start";

	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	if(reply->error()  == 0)
	{
		JsonDataAccess jsondata;
		QVariantList varList;
		// get logo info

		QVariantMap varMap = jsondata.load(reply).toMap();

		varList = varMap.value("adsBannerContents").toList();

		// set image to UI
		if(varList.count() > 0)
		{
			adsMap = varList.at(0).toMap();
			QString strImgPath =
					adsMap.value("adsBannerFullFile").toString();

			// TODO: add server url to image path and request image file content, please remove comment

			int nIdx = strImgPath.indexOf("path=") + 5;
			QString strFilePath = "http://astroapps.static.appxtream.com/" + strImgPath.mid(nIdx);

			WebImageView *imgAdmob = root->findChild<WebImageView*>(
					"ads");

			imgAdmob->setUrl(strFilePath);

			//Get notified when loading image is finished.
			connect(imgAdmob, SIGNAL(imageDownloaded()), this, SLOT(onImageDownloaded()));
		}
	}
	reply->deleteLater();
}

void StadiumAstro::onImageDownloaded()
{
	//Push notification setting.
	invokeManager = new InvokeManager(this);
	connect(invokeManager,
			SIGNAL(invoked(const bb::system::InvokeRequest&)), this,
			SLOT(onInvoked(const bb::system::InvokeRequest&)));

	notificationDialog.appendButton(new SystemUiButton("OK"));

	QObject::connect(&m_pushNotificationService, SIGNAL(createSessionCompleted(const bb::network::PushStatus&)),
			this, SLOT(onCreateSessionCompleted(const bb::network::PushStatus&)));
	QObject::connect(&m_pushNotificationService, SIGNAL(createChannelCompleted(const bb::network::PushStatus&, const QString)),
			this, SLOT(onCreateChannelCompleted(const bb::network::PushStatus&, const QString)));
	QObject::connect(&m_pushNotificationService, SIGNAL(destroyChannelCompleted(const bb::network::PushStatus&)),
			this, SLOT(onDestroyChannelCompleted(const bb::network::PushStatus&)));
	QObject::connect(&m_pushNotificationService, SIGNAL(registerToLaunchCompleted(const bb::network::PushStatus&)),
			this, SLOT(onRegisterToLaunchCompleted(const bb::network::PushStatus&)));
	QObject::connect(&m_pushNotificationService, SIGNAL(unregisterFromLaunchCompleted(const bb::network::PushStatus&)),
			this, SLOT(onUnregisterFromLaunchCompleted(const bb::network::PushStatus&)));
	QObject::connect(&m_pushNotificationService, SIGNAL(simChanged()),
			this, SLOT(onSimChanged()));
	QObject::connect(&m_pushNotificationService, SIGNAL(pushTransportReady(bb::network::PushCommand::Type)),
				this, SLOT(onPushTransportReady(bb::network::PushCommand::Type)));
	QObject::connect(&m_pushNotificationService, SIGNAL(noPushServiceConnection()),
			this, SLOT(onNoPushServiceConnection()));

	m_pushNotificationService.createSession();

	emit replaceToAdsScreen();
}

void StadiumAstro::callBrowser(QString & url)
{
	InvokeManager invokeManager;
	InvokeRequest request;
	request.setTarget("sys.browser");
	request.setAction("bb.action.OPEN");
	request.setUri(url);
	request.setMimeType("text/html");
	invokeManager.invoke(request);

}

void StadiumAstro::callBBWorld(QString & url)
{
	InvokeManager invokeManager;
	InvokeRequest request;
	request.setTarget("sys.appworld");
	request.setAction("bb.action.OPEN");
	request.setUri(url);
	request.setMimeType("text/html");
	invokeManager.invoke(request);

}

void StadiumAstro::openAdsLink()
{
	QString link = adsMap.value("adsBannerTabletURL").toString();
	StadiumAstro::callBrowser(link);
}

void StadiumAstro::onInvoked(const bb::system::InvokeRequest &request)
{
    // The underlying PushService instance might not have been
    // initialized when an invoke first comes in
    // Make sure that we initialize it here if it hasn't been already
	// It requires an application ID (for consumer applications) so we have to check
	// that configuration settings have already been stored
	m_pushNotificationService.initializePushService();

	if (request.action().compare(PUSH_INVOCATION_ACTION) == 0) {
		qDebug() << "Received push action";
		// Received an incoming push
		// Extract it from the invoke request and then process it
		PushPayload payload(request);
		if (payload.isValid()) {
			pushNotificationHandler(payload);
		}
	} else if (request.action().compare(OPEN_INVOCATION_ACTION) == 0){
		qDebug() << "Received open action";
		// Received an invoke request to open an existing push (ie. from a notification in the BlackBerry Hub)
		// The payload from the open invoke is the seqnum for the push in the database

		QByteArray data = request.data();
		openPush(data);
	}
}

void StadiumAstro::openPush(QByteArray &data)
{
	Push push(data);
	SystemDialog dialog;
	QString str(push.content());
	dialog.setBody(str);
	dialog.show();
}

void StadiumAstro::pushNotificationHandler(PushPayload &payload)
{
	Push push(payload);

	// Save the push and set the sequence number (ID) of the push
	QString str(push.content());
	notificationDialog.cancel();
	notificationDialog.setTitle("Stadium Astro");
	notificationDialog.setBody(str);
	notificationDialog.setRepeat(true);
	notificationDialog.show();
	// Create a notification for the push that will be added to the BlackBerry Hub
	Notification *notification = new Notification(this);
	notification->setTitle("Stadium Astro");
	notification->setBody(QString("Push was received"));

	// Add an invoke request to the notification
	// This invoke will contain the seqnum of the push.
	// When the notification in the BlackBerry Hub is selected, this seqnum will be used to lookup the push in
	// the database and display it

	InvokeRequest invokeRequest;
	invokeRequest.setTarget(INVOKE_TARGET_KEY_OPEN);
	invokeRequest.setAction(OPEN_INVOCATION_ACTION);
	invokeRequest.setMimeType("text/plain");
	invokeRequest.setData(push.toJSONData());
	notification->setInvokeRequest(invokeRequest);


	// Add the notification for the push to the BlackBerry Hub
	// Calling this method will add a "splat" to the application icon, indicating that a new push has been received
	notification->notify();

	// If an acknowledgement of the push is required (that is, the push was sent as a confirmed push
	// - which is equivalent terminology to the push being sent with application level reliability),
	// then you must either accept the push or reject the push
	if (payload.isAckRequired()) {
		// In our sample, we always accept the push, but situations might arise where an application
		// might want to reject the push (for example, after looking at the headers that came with the push
		// or the data of the push, we might decide that the push received did not match what we expected
		// and so we might want to reject it)
		m_pushNotificationService.acceptPush(payload.id());
	}

	// If the "Launch Application on New Push" checkbox was checked in the config settings, then
	// a new push will launch the app so that it's running in the background (if the app was not
	// already running when the push came in)
	// In this case, the push launched the app (not the user), so it makes sense
	// once our processing of the push is done to just exit the app
	// But, if the user has brought the app to the foreground at some point, then they know about the
	// app running and so we leave the app running after we're done processing the push
//	if (!m_hasBeenInForeground) {
//		Application::instance()->requestExit();
//	}
}
