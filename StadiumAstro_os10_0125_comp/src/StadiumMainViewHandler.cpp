/*
 * StadiumEventHandler.cpp
 *
 *  Created on: Jun 9, 2013
 *      Author: Star
 */
#include <bb/cascades/Application>
#include <bb/cascades/Container>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/QmlDocument>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/Label>
#include <bb/cascades/ListView>
#include <bb/cascades/ScrollView>
#include <bb/cascades/ImageView>
#include <bb/cascades/StackLayout>
#include <bb/cascades/StackLayoutProperties>
#include <bb/cascades/WebView>
#include <bb/device/DisplayInfo>
#include <bb/PpsObject>
#include <bb/cascades/ScrollAnimation>

#include <bb/system/SystemDialog>

#include <QtCore/QDateTime>

#include "facebook/Facebook.hpp"
#include "facebook/FBUser.h"
#include "facebook/FBPlace.h"
#include "facebook/FBPost.h"
#include "facebook/FBComment.h"
#include "facebook/FacebookObject.h"

#include "WebImageView.h"
#include "SportsEventTab.hpp"
#include "SportsEventBar.hpp"
#include "ImageCache.hpp"
#include "ImageLoader.h"
#include "StadiumEventHandler.h"



using namespace bb::cascades;
using namespace bb::data;
using namespace bb::system;

StadiumEventHandler::StadiumEventHandler()
{

}

StadiumEventHandler::StadiumEventHandler(QObject *parent = 0) {
	m_pMainInstance = (StadiumAstro*)parent;
	m_homePane = NULL;

	m_mailDialog = NULL;
	m_loadingDialog = NULL;
	// get current screen width
	bb::device::DisplayInfo display;
	m_screenWidth = display.pixelSize().width();
	m_screenHeight = display.pixelSize().height();

	// init session variables
	this->m_sportEventsContent_refKey = "";
	this->m_sportEventsContent_name = "";
	this->m_teamsLogoTable = NULL;

	isAdmobVisible = true;
	curAdmobIdx = 0;

	oneSecTimer = new QTimer(this);
	oneSecTimer->start(5000);

	facebook = new Facebook();
	invokeManager = new InvokeManager();

	//by passion
//	connect(oneSecTimer, SIGNAL(timeout()), this, SLOT(oneSecUpdate()));

	connect(facebook, SIGNAL(loginComplete(bool)), this, SLOT(onFacebookLoginComplete(bool)));

	connect(&m_registerService, SIGNAL(onGetPushStatus(QVariant)), this, SLOT(onGetPushStatus(QVariant)));
	connect(&m_registerService, SIGNAL(changedEventPush(QString, bool)), this, SLOT(changedEventPush(QString, bool)));
	connect(&m_registerService, SIGNAL(requestFailed(QString)), this, SLOT(registerRequestFailed(QString)));
	//Network connector.f
	jsonNetwork = new JSONConnect(this);

}
StadiumEventHandler::~StadiumEventHandler() {

	if(m_teamsLogoTable)
	{
		delete m_teamsLogoTable;
		m_teamsLogoTable = NULL;
	}

	if(m_loadingDialog)
		delete m_loadingDialog;
	if(m_mailDialog)
		delete m_mailDialog;


	delete eventBar;
	delete infoBar;
	delete buttonBar;
	delete jsonNetwork;
	delete oneSecTimer;
	delete facebook;
}


void StadiumEventHandler::ShowHome(){

	QTimer::singleShot(250, this, SLOT(OnShowHome()));

}

void StadiumEventHandler::onEventSelectionChanged(SportsEventTab *old, SportsEventTab *cur)
{
	if(cur->inAppUrlLink().isEmpty())
	{
	this->m_sportEventsContent_name = cur->title();
	this->m_sportEventsContent_refKey = cur->refKey();

	infoBar->removeAllSelectMark();

	//Add select mark.
	if(old != NULL && old != cur)
	{
		old->unselect();
	}
	cur->select();

	//Clear Image Cache
	ImageCache::getSharedImageCache()->clearMemory();
	OnShowHome();

	//We can get main page data here.
	curHomeLoadStatus = 0;

	//reset main page
	resetMainPage();

		// Configure home view specific for event type;
		showActivityIndicator(true);

		if(cur->refKey().compare("OTHERSSPORTS") == 0)
		{
			emit resetRedMenuForOthers();
			showEventListForToday(false);
		}
		else
		{
			ImageView* fixtureMenu = m_homePane->findChild<ImageView*>("fixtureMenu");
			if(fixtureMenu != NULL){
				if (cur->showMatchForToday() == "Y")
				{
					fixtureMenu->setProperty("bshow", true);
				}
				else
				{
					fixtureMenu->setProperty("bshow", false);
				}
			}
			emit resetRedMenuForFootball();

			if (cur->showMatchForToday() == "Y")
			{
				showEventListForToday(true);
			}
			else
			{
				showEventListForToday(false);
			}
		}
		RequestMainPageData();
		displayMainHomeView();
	}
	else
	{
		qDebug() << "inAppUrl" << cur->inAppUrlLink();
		displayFantasyView(cur->inAppUrlLink());
	}
	emit onMenuItemTouched();

}

void StadiumEventHandler::resetMainPage()
{
	if (!m_homePane)	return;

	//Scroll to top and reset all list view
	ScrollView* homeScroll = m_homePane->findChild<ScrollView*>("homeScroll");
	homeScroll->scrollToPoint(0,0, ScrollAnimation::None);

	//reset logoLabel
	Label* logoLabel = m_homePane->findChild<Label*>("logoString");
	logoLabel->setText("");

	// set scroll status bar
	Container *scrollStatusBar = m_homePane->findChild<Container*>("scrollStatusBar");
	scrollStatusBar->removeAll();

	//reset match list view
	ListView *matchPreviewList = m_homePane->findChild<ListView*>("matchListForToday");
	matchPreviewList->resetDataModel();

	ListView *specialLogoList = m_homePane->findChild<ListView*>("specialLogoList");
	specialLogoList->resetDataModel();

	ListView *videoAndNewsList = m_homePane->findChild<ListView*>("highlightVideosAndNews");
	videoAndNewsList->resetDataModel();
}


//Collection of button tabs - "App feedback", "report an issue", "tell a friend" pages.
void StadiumEventHandler::onButtonSelectionChanged(SportsEventTab *old, SportsEventTab *cur)
{
	QString url;
	if(cur->refKey().compare("appfeedback") == 0)
	{
		curDlgPurpose = 0;
		QString mailTo = QUrl::toPercentEncoding("mobileapps.support@astro.com.my");
		QString subject = QUrl::toPercentEncoding("App Feedback for Stadium " + APP_VERSION);
		url = "mailto:" + mailTo + "?" "&subject=" + subject;
		sendEmail(url);
	}
	else if(cur->refKey().compare("reportIssue") == 0)
	{
		curDlgPurpose = 1;
		QString mailTo = QUrl::toPercentEncoding("mobileapps.support@astro.com.my");
		QString subject = QUrl::toPercentEncoding("Report Issue for Stadium " +  APP_VERSION);
		url = "mailto:" + mailTo + "?" + "&subject=" + subject;

		sendEmail(url);
	}

	else if(cur->refKey().compare("tellfriend") == 0)
	{
		showSocialDialog();
	}
}


void StadiumEventHandler::tellFriendByEmail()
{
	QString subject = QUrl::toPercentEncoding("Stadium Astro - Try this cool, new App for Sports Fans");
	QString body = QUrl::toPercentEncoding("Hello!\n\n"
			"Stadium Astro is a comprehensive mobile app dedicated to bringing you the latest from the world of sports"
			" - Match Updates, Fixtures, Highlights and Videos. There is something here for every fan, whatever your allegiance\n\n"
			"Download the App now at: www.stadiumastro.com/mobile\n\n"
			"Become a Sports Guru with this handy App.\n\n\n");
	QString url;
	url = "mailto:?&subject=" + subject + "&body=" + body;

	sendEmail(url);
}

void StadiumEventHandler::tellFriendByBBM()
{
	InvokeRequest request;
	request.setTarget("sys.bbm.sharehandler");
	request.setAction("bb.action.SHARE");
	request.setMimeType("text/plain");

	QString body = "Hello!\n\nStadium Astro is a comprehensive mobile app dedicated to bringing you the latest from the world of sports- Match Updates, Fixtures, Highlights and Videos. There is something here for every fan, whatever your allegiance\n\nDownload the App now at: www.stadiumastro.com/mobile\n\nBecome a Sports Guru with this handy App.";

	request.setData(body.toAscii());
	invokeManager->invoke(request);
}

void StadiumEventHandler::sendEmail(QString & preValue)
{
	InvokeRequest request;
	request.setTarget("sys.pim.uib.email.hybridcomposer");
	request.setAction("bb.action.OPEN");

	qDebug() << preValue;
	if(!preValue.isNull())
		request.setUri(preValue);

	request.setMimeType("text/plain");
	request.setData("");
	invokeManager->invoke(request);
}

void StadiumEventHandler::openFacebook()
{
	//We have to login first, if we are not logged in.

	//By Passion
	//facebook->logout();
	facebook->login();
	m_mailDialog->close();
}

void StadiumEventHandler::showFacebookPoster()
{
	Dialog *dlg;
	QmlDocument *qml = QmlDocument::create("asset:///FacebookPostDlg.qml").parent(this);
	qml->setContextProperty("facebook", facebook);

	dlg = qml->createRootObject<Dialog>();
	dlg->open();
}
void StadiumEventHandler::onFacebookLoginComplete(bool blogin)
{
	if(blogin)
	{
		showFacebookPoster();
	}
}

void StadiumEventHandler::showSocialDialog()
{
	if(m_mailDialog == NULL)
	{
		QmlDocument *qml = QmlDocument::create("asset:///MailDialog.qml").parent(this);
		qml->setContextProperty("eventHandler",this);
		m_mailDialog = qml->createRootObject<Dialog>();
	}
	m_mailDialog->open();
}



void StadiumEventHandler::OnShowHome()
{
	Container *homeMenuArea; // Pointer to main menu area.

	if(!m_homePane)
	{
		QmlDocument *qml = QmlDocument::create("asset:///MainPage.qml").parent(this);

		footballMenuName = "footballSubMenu";
		otherMenuName = "otherSubMenu";

		if(!qml->hasErrors()) {
			// create a event handler object
			qml->setContextProperty("eventHandler", this);

			// create root object for the UI
			NavigationPane *homePane = qml->createRootObject<NavigationPane>();
			homePane->setProperty("screenWidth", m_screenWidth);
			//homePane->setProperty("screenHeight", m_screenHeight);

			if(homePane){
				// save to member variable and set its lifecycle property
				m_homePane = homePane;
				m_homePane->setParent(m_pMainInstance);

				m_mainMenu = m_homePane->findChild<Container*>("mainMenu");


				homeMenuArea = m_homePane->findChild<Container*>("mainMenuArea");
				m_mainMenu->setParent(homeMenuArea);
				InitializeMainMenu();

				//Connect touch event and hide action.
				connect(this, SIGNAL(onMenuItemTouched()), m_homePane, SLOT(hideMainMenu()));

				connect(this , SIGNAL(resetRedMenuForFootball()), m_homePane, SLOT(resetMenuForFootballSports()));
				connect(this , SIGNAL(resetRedMenuForOthers()), m_homePane, SLOT(resetMenuForOtherSports()));
				connect(m_homePane , SIGNAL(topChanged (bb::cascades::Page *)),this, SLOT(onTopPageChanged(bb::cascades::Page *)));
			}
		}
		else
		{
			QList<QDeclarativeError> errorList = qml->errors ();
			int nErrorCount = errorList.count();
			for (int i = 0; i < nErrorCount; i++)
			{
				QString strDescription = ((QDeclarativeError)(errorList.at(i))).description();

			}
		}

		displayMainHomeView();
	}

	//Remove all selected mark when show home page.
	infoBar->removeAllSelectMark();

	// set created root object as a scene
	Application::instance()->setScene(m_homePane);

	//Hide admob area if it was already closed somewhere.
	Container * admob = m_homePane->findChild<Container *>("admob");
	admob->setVisible(isAdmobVisible);
	curAdmobView = admob->findChild<WebImageView*>("imgAdmob");
}

void StadiumEventHandler::InitializeMainMenu()
{

	//Add more menu items.
	QString moreMenuInfo[30] = {"asset:///images/icon_side_about.png", "About" , "about"
								,"asset:///images/icon_side_faq.png" , "FAQ" , "faq"
								,"asset:///images/icon_side_term-os.png" , "Terms of Service" , "termservice"
								,"asset:///images/icon_side_term-con.png", "Terms & Conditions", "termcondition"
								,"asset:///images/icon_side-privacy.png","Privacy Policy","policy"
								,"asset:///images/icon_side_app-version.png","App Version " + APP_VERSION ,"appversion"
								,"asset:///images/icon_side_app-feedback.png","App Feedback","appfeedback"
								,"asset:///images/icon_report.png","Report an Issue","reportIssue"
								,"asset:///images/icon_tell-a-friend.png","Tell a Friend","tellfriend"
								,"asset:///images/icon_side_default.png","Other Apps by Astro", "otherapp"
	};

	eventBar = new SportsEventBar();
	infoBar = new SportsEventBar();
	buttonBar = new SportsEventBar();

	eventBar->setParent(this);
	infoBar->setParent(this);
	buttonBar->setParent(this);

	connect(eventBar, SIGNAL(selectionChanged(SportsEventTab *, SportsEventTab *)), this, SLOT(onEventSelectionChanged(SportsEventTab *, SportsEventTab *)));
	connect(infoBar, SIGNAL(selectionChanged(SportsEventTab *, SportsEventTab *)), this, SLOT(onInfoSelectionChanged(SportsEventTab *, SportsEventTab *)));
	connect(buttonBar, SIGNAL(selectionChanged(SportsEventTab *, SportsEventTab *)), this, SLOT(onButtonSelectionChanged(SportsEventTab *, SportsEventTab *)));

	int base;
	SportsEventTab *tab;

	Container *infoSubMenu = m_mainMenu->findChild<Container*>("information");
	Container *contactSubMenu = m_mainMenu->findChild<Container*>("contacts");
	Container *otherSubMenu = m_mainMenu->findChild<Container*>("others");

	for(int i = 0; i < 10; i++)
	{
		base = i * 3;
		tab = new SportsEventTab();
		tab->setTitle(moreMenuInfo[base + 1]);
		tab->setImgSource(moreMenuInfo[base]);
		tab->setRefKey(moreMenuInfo[base + 2]);
		if(i >= 0 && i < 6)
		{
			infoSubMenu->add(tab);
			if(i != 5)
				infoBar->addEventTab(tab);
		}
		else if(i >=6 && i <8)
		{
			buttonBar->addEventTab(tab);
			contactSubMenu->add(tab);
		}
		else if(i == 8)
		{
			buttonBar->addEventTab(tab);
			otherSubMenu->add(tab);
		}
		else if(i == 9)
		{
			infoBar->addEventTab(tab);
			otherSubMenu->add(tab);
		}
	}
	getSportsEventsList();
}

void StadiumEventHandler::RequestMainPageData()
{
	if(curHomeLoadStatus == 0)
	{
		getTeamsLogoInfo();
	}

	else if(curHomeLoadStatus == 1)
	{
		getAdsBannerListInfo();
	}
	else if(curHomeLoadStatus == 2)
	{
		getSoccerMatchListInfo();
	}
	else if(curHomeLoadStatus == 3)
	{
		getHighlightNewsListInfo();
	}
	else if(curHomeLoadStatus == 4)
	{
		getFeatureNewsListInfo();
	}
	else
		showActivityIndicator(false);
}


void StadiumEventHandler::getAdsBannerListInfo()
{
	// TODO: request 'Ads Banner List' info to server, see below for its call back method

	if(isAdmobVisible)
	{
		QMap<QString, QString> params;

		params.insert("service", "astroSportsDataService");

		params.insert("action", "grabJsonText");

		params.insert("mimeType", "application/json");

		params.insert("p1", "AdsBannerList");

		params.insert("p2", m_sportEventsContent_refKey);


		jsonNetwork->SendGetRequest(params, this, SLOT(setAdsBannerListInfo()));
	}

}

void StadiumEventHandler::setAdsBannerListInfo()
{
	// get ads banner list data
	qDebug()<<"Read Banner Start";

	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	if(reply->error()  == 0 && reply->bytesAvailable() > 0)
	{
		JsonDataAccess jsondata;

		// get logo info
		QVariantMap varMap = jsondata.load(reply).toMap();

		admobList = varMap.value("adsBannerContents").toList();

		// set image to UI
		if(admobList.count() > 0)
		{
			QString strImgPath =
					admobList.at(0).toMap().value("adsBannerMobileFile").toString();

			// TODO: add server url to image path and request image file content, please remove comment
			//passion
			int nIdx = strImgPath.indexOf("path=") + 5;
			QString strFilePath = "http://astroapps.static.appxtream.com/" + strImgPath.mid(nIdx);

//			QString siteUrl(jsonNetwork->GetSiteUrl());
//			siteUrl.append(strImgPath);

//			//passion Temp
//			SystemDialog *dialog = new SystemDialog("OK");
//			dialog->setTitle("");
//			dialog->setBody(strFilePath);
//			dialog->show();

			WebImageView *imgAdmob = m_homePane->findChild<WebImageView*>(
					"imgAdmob");
			imgAdmob->setUrl(QUrl(strFilePath));
		}
	}
	reply->deleteLater();
	qDebug()<<"Read Banner";

	curHomeLoadStatus = 2;
	RequestMainPageData();
}

void StadiumEventHandler::getSoccerMatchListInfo()
{
	// TODO: request 'Soccer Match List' info to server, see below for its call back method
	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "SoccerMatchList");

	params.insert("p2", m_sportEventsContent_refKey);



	jsonNetwork->SendGetRequest(params, this, SLOT(setSoccerMatchListInfo()));
}

/** @brief
 *		parsing soccer match list data for main page and set into UI components
 *	@param
 *		none.
 **/
void StadiumEventHandler::setSoccerMatchListInfo()
{
	// define variables
	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	qDebug()<<"Read Soccer Start";
	while (reply->error() == 0 && reply->bytesAvailable() > 0) {

		JsonDataAccess jsondata;
		QVariantList varList;
		QVariantList varMatchList;

		// get match list info from json data
		//varList = jsondata.load(QDir::currentPath() + "/app/native/assets/models/matchpreview.json").value<QVariantList>();
		QVariantMap varMap =
				jsondata.load(reply
						).value<
						QVariantMap>();
		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(err.errorMessage());
			break;
		}
		varList = varMap.value("soccerMatchs").toList();

		// append infomations: old
//		QVariantList::Iterator item = varList.begin();
//		while (item != varList.end()) {
//			QVariantMap itemMap = (*item).toMap();
//
//			// customize image content
//			// In fact, there's no need to add this item, use code as below.
//			// 		itemMap["homeTeamLogo"] = getTeamLogoUrl(itemMap["homeTeamId"], "teamLogo44x44")
//			// 		itemMap["visitingTeamLogo"] = getTeamLogoUrl(itemMap["visitingTeamId"], "teamLogo44x44")
//
//			QString strUrl = jsonNetwork->GetSiteUrl();
//			strUrl.append(getTeamLogoUrl(itemMap["homeTeamId"].toString(), "teamLogo44x44"));
//			itemMap["homeTeamLogo"] = QUrl(strUrl);
//
//			strUrl = jsonNetwork->GetSiteUrl();
//			strUrl.append(getTeamLogoUrl(itemMap["visitingTeamId"].toString(), "teamLogo44x44"));
//			itemMap["visitingTeamLogo"] = QUrl(strUrl);
//
//			// append date & time info
//			QDateTime dateTime = QDateTime::fromString(
//					itemMap["matchTimeZoneDate"].toString(), Qt::ISODate);
//			itemMap["month"] = dateTime.toString("MMM");
//			itemMap["date"] = dateTime.toString("d");
//			itemMap["time"] = dateTime.toString("h:mm AP");
//
//
//			qDebug() << itemMap["time"];
//			// save to map data
//			(*item) = itemMap;
//
//			++item;
//		}

		//Passion
		int nListSize = varList.size();

		for (int i = nListSize - 1; i >= 0; i--)
		{
			QVariantMap itemMap = varList.at(i).toMap();

			QString strUrl = jsonNetwork->GetSiteUrl();
			strUrl.append(getTeamLogoUrl(itemMap["homeTeamId"].toString(), "teamLogo44x44"));
			itemMap["homeTeamLogo"] = QUrl(strUrl);

			strUrl = jsonNetwork->GetSiteUrl();
			strUrl.append(getTeamLogoUrl(itemMap["visitingTeamId"].toString(), "teamLogo44x44"));
			itemMap["visitingTeamLogo"] = QUrl(strUrl);

			// append date & time info
			QDateTime dateTime = QDateTime::fromString(
					itemMap["matchTimeZoneDate"].toString(), Qt::ISODate);
			itemMap["month"] = dateTime.toString("MMM");
			itemMap["date"] = dateTime.toString("d");
			itemMap["time"] = dateTime.toString("h:mm AP");


			qDebug() << itemMap["time"];
			// save to map data
			QVariant item = QVariant(itemMap);
			varMatchList.append(item);
		}

		// prepare model
		GroupDataModel *matchPreviewModel = new GroupDataModel();
		matchPreviewModel->setParent(this);
		matchPreviewModel->insertList(varMatchList);
		matchPreviewModel->setGrouping(ItemGrouping::None);

//		QStringList sortingKeys;
//		sortingKeys << "matchTimeZoneDate";
//		matchPreviewModel->setSortingKeys(sortingKeys);

		// set preview list model
		ListView *matchPreviewList = m_homePane->findChild<ListView*>(
				"matchListForToday");
		matchPreviewList->setDataModel(matchPreviewModel);

		break;
	}
	reply->deleteLater();
	curHomeLoadStatus = 3;
	RequestMainPageData();

	qDebug()<<"Read Soccer Match";
}

void StadiumEventHandler::getHighlightNewsListInfo()
{
	// TODO: request 'HighlightNewsList' info to server, see below for its call back method
	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "HighlightNewsList");

	params.insert("p2", m_sportEventsContent_refKey);

	jsonNetwork->SendGetRequest(params,this, SLOT(setHighlightNewsListInfo()));
}

/** @brief
 *		parsing soccer highlight news list data for main page and set into UI components
 *	@param
 *		none.
 **/
void StadiumEventHandler::setHighlightNewsListInfo()
{
	// define variables
	JsonDataAccess jsondata;
	QVariantList logoList;

	// get logo info
	qDebug()<<"Read Highlight Start";
	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	while (reply->error() == 0 && reply->bytesAvailable() > 0) {

		QVariantMap varMap =
				jsondata.load(
						reply
						).value<
						QVariantMap>();
		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(
							err.errorMessage());
			break;
		}
		logoList = varMap["cmsHighlightNewsList"].toList();

		// prepare data model
		GroupDataModel *speicalLogoModel = new GroupDataModel();
		speicalLogoModel->setParent(this);

		QStringList sortingKeys;
		sortingKeys << "displayOrder";
		speicalLogoModel->setSortingKeys(sortingKeys);

		QString title = m_sportEventsContent_name;

		// Iterate over all the items in the received data and rearrange data

		QVariantList::Iterator item = logoList.begin();
		while (item != logoList.end()) {
			QVariantMap itemMap = (*item).toMap();

			// customize image content
			// In fact, there's no need to modify this item, use directly instead.
			//itemMap["imageLink"] = QUrl("asset:///images/main_special_logo.png");

			// save to item

			(*item) = itemMap;

			++item;
		}

		// save total logo image count
		int nImageCount = logoList.size();


		// set list data model
		speicalLogoModel->insertList(logoList);
		speicalLogoModel->setGrouping(ItemGrouping::None);

		ListView *specialLogoList = m_homePane->findChild<ListView*>(
				"specialLogoList");

		specialLogoList->setDataModel(speicalLogoModel);

		// set string data to label control
		Label *logoStringLabel = m_homePane->findChild<Label*>("logoString");
		title = this->m_sportEventsContent_name;
		logoStringLabel->setText(title);

		// set scroll status bar
		Container *scrollStatusBar = m_homePane->findChild<Container*>(
				"scrollStatusBar");
		scrollStatusBar->removeAll();
		ImageView *enabledDot =
				ImageView::create("asset:///images/dot_calendar02.png").leftMargin(
						20.0f).rightMargin(20.0f);
		scrollStatusBar->add(enabledDot); // add enabled mark

		for (int i = 1; i < nImageCount; i++) {
			ImageView *disabledDot =
					ImageView::create("asset:///images/dot_scrollbar.png").leftMargin(
							20.0f).rightMargin(20.0f);
			scrollStatusBar->add(disabledDot); // add disabled marks
		}

		m_curSpecialLogoIndex = 0;
		break;
	}
	reply->deleteLater();
	curHomeLoadStatus = 4;
	RequestMainPageData();
	qDebug()<<"Read Soccer Match";
}

void StadiumEventHandler::ChangeLogoFrame()
{
	// get status bar object
	AbstractPane *pane = m_homePane;
	Container *scrollStatusBar = pane->findChild<Container*>("scrollStatusBar");

	// determine index of next frame
	int nCount = scrollStatusBar->count();
	int nextLogoIndex = m_curSpecialLogoIndex + 1;
	if(nextLogoIndex == nCount) nextLogoIndex = 0;

	// change logo image
	ListView *specialLogoList = pane->findChild<ListView*>("specialLogoList");

	QVariantList changeList;
	changeList << nextLogoIndex;

	if(nextLogoIndex == 0)
		specialLogoList->scrollToItem(changeList, ScrollAnimation::None);
	else
		specialLogoList->scrollToItem(changeList, ::ScrollAnimation::Default);

	// reset status bar's display
	scrollStatusBar->swap(m_curSpecialLogoIndex, nextLogoIndex);


	m_curSpecialLogoIndex = nextLogoIndex;
}

void StadiumEventHandler::onLogoListActivationChanged(const QVariantList indexPath, bool active)
{
	if(active){
		// get index of new activated frame
		int nextLogoIndex = indexPath.at(0).toInt();

		// get status bar object
		AbstractPane *pane = m_homePane;
		Container *scrollStatusBar = pane->findChild<Container*>("scrollStatusBar");

		// reset status bar's display
		scrollStatusBar->swap(m_curSpecialLogoIndex, nextLogoIndex);

		m_curSpecialLogoIndex = nextLogoIndex;
	}
}
void StadiumEventHandler::getFeatureNewsListInfo()
{
	// TODO: request 'FeatureNewsVideoList' info to server, see below for its callback method
	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("mimeType", "application/json");

	params.insert("p1", "FeatureNewsList");

	params.insert("p2", m_sportEventsContent_refKey);

	jsonNetwork->SendGetRequest(params, this,SLOT(setFeatureNewsListInfo()));
}

void StadiumEventHandler::setFeatureNewsListInfo()
{
	// define variables
	JsonDataAccess jsondata;
	QVariantList varList, finalList;
//	QVariantList newsList, videoList;

	// get feature videos & news info
	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());

	qDebug()<<"Read Feature list Start";
	while (reply->error() == 0 && reply->bytesAvailable() > 0) {
		QVariantMap varMap = jsondata.load(reply).value<QVariantMap>();
		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(
							err.errorMessage());
			break;
		}
		varList = varMap.value("featureNewsContents").toList();

		// prepare two header item
		QVariantMap topStories;

		topStories["type"] = "header";
		topStories["title"] = "Top  Stories";
		varList.prepend(topStories);
		// find corresponding position to insert news header
		QVariantList::Iterator item;

		// rearrange data
		item = varList.begin();
		QDateTime nowTime = QDateTime::currentDateTime();
		while (item != varList.end()) {
			QVariantMap itemMap = (*item).toMap();
			QString strType = itemMap["type"].toString();

			if (strType == "NEWS" || strType == "VIDEO") {
				// For test(need to modify)!!!!!!!!
				// In fact, there's no need to append this property, directly use 'imageLink' instead

//				itemMap["image"] = "asset:///images/icon_video_news1.png";
				itemMap["imageUrl"] = QUrl(itemMap["imageLink"].toString());
				itemMap["itemType"] = strType;

				// rearrange title data
				const int nLengthLimit = 50;
				QString strTitle = itemMap["title"].toString();
				if (strTitle.length() > nLengthLimit) {
					itemMap["subject"] = strTitle.left(nLengthLimit - 2)
							+ "...";
				} else {
					itemMap["subject"] = strTitle;
				}

				// append time difference info
				QDateTime dateTime = QDateTime::fromString(
						itemMap["strPublishDate"].toString(), Qt::ISODate);

				//by KJH
//				int nDiffHours = floor((float) dateTime.secsTo(nowTime) / 3600);
//				QString strDiffTime = QString("%1 hour ago").arg(nDiffHours);
//				itemMap["time"] = strDiffTime;

				itemMap["time"] = dateTime.toString("dddd,  dd  MMM  yyyy  h:mm  AP");

//				QVariantMap newitemMap(itemMap);
//				if (strType == "NEWS"){
//					newsList.append(newitemMap);
//				}else{
//					videoList.append(newitemMap);
//				}
			}

			// save to list
			(*item) = itemMap;

			++item;
		}

		// reverse list data and set into model
		finalList.clear();

		qDebug() << "Feature List Finalizing";
		for (int i = varList.count() - 1; i >= 0; i--) {
			finalList.append(varList.at(i));
		}
//		for (int i = newsList.count() - 1; i >= 0; i--) {
//			finalList.append(newsList.at(i));
//		}
//		for (int i = videoList.count() - 1; i >= 0; i--) {
//			finalList.append(videoList.at(i));
//		}
		qDebug() << "Feature List Success";
		// prepare group model
		GroupDataModel *videoAndNewsModel = new GroupDataModel();
		videoAndNewsModel->setParent(this);
		videoAndNewsModel->insertList(finalList);
		videoAndNewsModel->setGrouping(ItemGrouping::None);

		// set preview list model
		ListView *videoAndNewsList = m_homePane->findChild<ListView*>(
				"highlightVideosAndNews");
		videoAndNewsList->setDataModel(videoAndNewsModel);

		break;
	}
	reply->deleteLater();

	qDebug()<<"Read Feature List";

	curHomeLoadStatus = 5;
	RequestMainPageData();
}

/** @brief
 * 		invoked when user click hide button of admob
 * 	@param
 * 		none.
 */

void StadiumEventHandler::onHideAdmob()
{
	isAdmobVisible = false;
}


void StadiumEventHandler::getSportsEventsList()
{
	// TODO: request sport event list info to server
	// API path: /jsonFeed.action?service=astroSportsDataService&action=grabJsonText&p1=SportsEventList&mimeType=application/json
	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("p1", "SportsEventList");

	params.insert("mimeType", "application/json");


	jsonNetwork->SendGetRequest(params, this, SLOT(onGetSportsEventList()));
}

// processing sports event list data from server, includes confirming main menu and setting data model
void StadiumEventHandler::onGetSportsEventList()
{
	// load json data
	JsonDataAccess jsondata;
	QVariantMap varMap, varSubMap;
	QVariantList varList;
	QString varStr;


 	Container * footballSubMenu = m_mainMenu->findChild<Container*>(footballMenuName);
	Container * otherSubMenu = m_mainMenu->findChild<Container*>(otherMenuName);

	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	if(reply->error() == 0)
	{
		// get sport event contents
//		varMap =
//				jsondata.load(
//						QDir::currentPath()
//								+ "/app/native/assets/responses/SportsEventList.json").value<
//						QVariantMap>();
		varMap = jsondata.load(reply).toMap();

		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg =
					tr("Error converting Qt data to JSON: %1").arg(
							err.errorMessage());
			qDebug() << errorMsg;
			return;
		}
		varList = varMap.value("sportsEventContents").toList();

		//Get events list here.

		QVariantList::iterator i = varList.begin();
		QString strGroup;
		QString refKey;
		QString strFixtureShow;
		while(i != varList.end())
		{
			SportsEventTab * newTab = new SportsEventTab();

			varSubMap = i->toMap();

			refKey = varSubMap.value("refKey").toString();
			sportsEventInfo[refKey] = varSubMap;
			newTab->setRefKey(refKey);

			//Passion
			strFixtureShow = varSubMap.value("includeFixture").toString();
			newTab->setShowMatchForToday(strFixtureShow);

			newTab->setInAppUrlLink( varSubMap.value("inAppUrlLink").toString() );

			newTab->setTitle(varSubMap.value("sportsEventName").toString());

			QString imgUrl = jsonNetwork->GetSiteUrl();
			imgUrl.append(varSubMap.value("sportsEventIconURL").toString());
			newTab->setImgUrl(imgUrl);

			strGroup = varSubMap.value("bySportsGroupName").toString();
			if(strGroup.compare("Football") == 0)
				footballSubMenu->add(newTab);
			else if(strGroup.compare("Others") == 0)
				otherSubMenu->add(newTab);

			eventBar->addEventTab(newTab);
			i++;
		}

		this->m_sportEventsContent_refKey = varList.at(0).toMap().value(
				"refKey").toString();

		// set default content name
		this->m_sportEventsContent_name = varList.at(0).toMap().value(
				"sportsEventName").toString();

		eventBar->trigger(0);

	}
	reply->deleteLater();
}



void StadiumEventHandler::getTeamsLogoInfo()
{
	// TODO: request 'Teams Logo' list to server with refKey
	QMap<QString, QString> params;

	params.insert("service", "astroSportsDataService");

	params.insert("action", "grabJsonText");

	params.insert("p1", "TeamList");

	params.insert("p2", m_sportEventsContent_refKey);


	params.insert("mimeType", "application/json");


	jsonNetwork->SendGetRequest(params, this,SLOT(setTeamsLogoInfo()));
}

/** @brief
 * 		parsing server's response about teams' logo info and set it to member variable.
 */
void StadiumEventHandler::setTeamsLogoInfo()
{
	JsonDataAccess jsondata;
	QVariantMap varMap;
	QVariantList varList;

	// parse json data
	QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
	while(reply->error() == 0 && reply->bytesAvailable() > 0)
	{
		varMap = jsondata.load(
				reply
				).value<QVariantMap>();

		if (jsondata.hasError()) {
			const DataAccessError err = jsondata.error();
			const QString errorMsg = tr("Error converting Qt data to JSON: %1").arg(err.errorMessage());
			qDebug()<<errorMsg;
			break;
		}
		varList = varMap.value("teamContents").toList();

		// initialize teams' logo hash table
		if(m_teamsLogoTable)
		{
			m_teamsLogoTable->clear();
		}
		else
		{
			m_teamsLogoTable = new QHash<QString, QVariantMap>();
		}

		// append to hash table
		QVariantList::Iterator item = varList.begin();
		while (item != varList.end()) {
			QVariantMap itemMap = item->toMap();
			m_teamsLogoTable->insert(itemMap["teamId"].toString(), itemMap);

			++item;
		}

		break;
	}

	qDebug() << "Read TeamList";
	curHomeLoadStatus = 1;
	RequestMainPageData();

	reply->deleteLater();
}

/** @brief
 * 		find specific team's logo image url from team's ID and image size string.
 *  @param
 *      teamId: ID of team.
 *      logoImageSize: size specific string of logo image, its value is "teamLogo100x100", "teamLogo44x44" or "teamLogo22x22".
 *  @return
 *      Return url of logo image.
 **/
QString StadiumEventHandler::getTeamLogoUrl(QString teamId, QString logoImageSize)
{
	// find team info
	if(m_teamsLogoTable != NULL)
	{
		QHash<QString, QVariantMap>::iterator index = m_teamsLogoTable->find(teamId);
		if(index != m_teamsLogoTable->end())
		{
			QVariantMap teamDataMap = index.value();
			return teamDataMap.value(logoImageSize).toString();
		}
	}
	// if not exist, return invalid value
	return "";
}


void StadiumEventHandler::oneSecUpdate()
{
	if(curAdmobView == NULL)
		return;
	curAdmobIdx ++;
	if(admobList.empty())
		return;

	if(curAdmobIdx >= admobList.count())
	{
		curAdmobIdx = 0;
	}

	if(isAdmobVisible)
	{
		QVariantMap map = admobList[curAdmobIdx].toMap();
		QString imgUrl = map.value("adsBannerMobileFile").toString();
		QString siteUrl = jsonNetwork->GetSiteUrl();
		siteUrl.append(imgUrl);
		curAdmobView->setUrl(QUrl(siteUrl));
	}
	else
		oneSecTimer->stop();
}
void StadiumEventHandler::onTouchAdmob()
{
	if(!admobList.isEmpty() && curAdmobIdx < admobList.count())
	{
		QVariantMap map = admobList[curAdmobIdx].toMap();
		QString admobUrl = map.value("adsBannerMobileURL").toString();
		StadiumAstro::callBrowser(admobUrl);
	}
}

void StadiumEventHandler::displayFantasyView(QString address)
{
//	Control *view = m_homePane->findChild<Control *>("fantasyview");
//	view->setVisible(true);
//
//	WebView *fantasyBrowser = m_homePane->findChild<WebView *>("fantasybrowser");
//	fantasyBrowser->setUrl(QUrl(address));
//
//	Container *container = m_homePane->findChild<Container*>("maincontent");
//	container->setVisible(false);
	StadiumAstro::callBrowser(address);
}
void StadiumEventHandler::displayMainHomeView()
{
	Container *container = m_homePane->findChild<Container*>("maincontent");
	container->setVisible(true);

	Control *view = m_homePane->findChild<Control *>("fantasyview");
	view->setVisible(false);

	WebView *fantasyBrowser = m_homePane->findChild<WebView *>("fantasybrowser");
	fantasyBrowser->setUrl(QUrl(""));

}

void StadiumEventHandler::showActivityIndicator(bool bShow)
{

	static int showCount = 0;

	if(bShow)
	{
		showCount ++;
		if(m_loadingDialog == NULL)
		{
			QmlDocument *qml = QmlDocument::create("asset:///LoadingDialog.qml").parent(this);
			qml->setContextProperty("eventHandler",this);
			m_loadingDialog = qml->createRootObject<Dialog>();
		}
		if(!m_loadingDialog->isOpened())
			m_loadingDialog->open();
	}
	else
	{
		showCount --;
		if(showCount <= 0)
		{
			m_loadingDialog->close();
			showCount = 0;
		}

	}

}


void StadiumEventHandler::showEventListForToday(bool bShow)
{
	Control * eventList = m_homePane->findChild<Control *>("matchListForToday");
	if(eventList != NULL)
		eventList->setVisible(bShow);
//
//	ImageView* fixtureMenu = m_homePane->findChild<ImageView*>("fixtureMenu");
//	if(fixtureMenu != NULL)
//		fixtureMenu->setVisible(bShow);
}

void StadiumEventHandler::onTopPageChanged(bb::cascades::Page * newPage)
{
	//PASSION
//	Container * admob = newPage->findChild<Container *>("admob");
//	if(admob != NULL)
//	{
//		admob->setVisible(isAdmobVisible);
//		curAdmobView = admob->findChild<WebImageView*>("imgAdmob");
//	}
//	else
//		curAdmobView = NULL;
}

void StadiumEventHandler::showHelpPage(){
	QmlDocument *qml = QmlDocument::create("asset:///HelpPage.qml").parent(this);
	qml->setContextProperty("eventHandler", this);
	Page *helpPage = qml->createRootObject<Page>();
	m_homePane->push(helpPage);
}

void StadiumEventHandler::popTop()
{
	m_homePane->pop();

}
void StadiumEventHandler::popToRootPage()
{
	Page *rootPage = m_homePane->at(0);
	m_homePane->navigateTo(rootPage);
}
