/*
 * SportsEventTab.cpp
 *
 *  Created on: Jul 5, 2013
 *      Author: team2
 */


#include <bb/cascades/StackLayout>
#include <bb/cascades/TextStyle>
#include <bb/cascades/VerticalAlignment>
#include <bb/cascades/ScalingMethod>
#include "SportsEventTab.hpp"
#include "WebImageView.h"

SportsEventTab::SportsEventTab() {
	// TODO Auto-generated constructor stub
	bSwitch = false;
	Container *rootContainer;
	rootContainer = Container::create().background(Color::Gray);
	rootContainer->setTopPadding(1);
	rootContainer->setBottomPadding(1);

	TapHandler *pTapHandler = TapHandler::create()
	    .onTapped(this, SLOT(onTappedHandler(bb::cascades::TapEvent*)));
	rootContainer->addGestureHandler(pTapHandler);

	Container *container = Container::create().preferredWidth(520).background(
			Color::fromARGB(0xff202020)).layout(
			StackLayout::create().orientation(LayoutOrientation::LeftToRight));

	container->setLeftPadding(20);
	container->setTopPadding(20);
	container->setBottomPadding(20);

	backContainer = container;

	rootContainer->add(container);

	mImgView = new WebImageView();
	mImgView->setVerticalAlignment(VerticalAlignment::Center);
	mImgView->setMinWidth(90);
	mImgView->setMaxWidth(90);
	mImgView->setPreferredWidth(90);
	mImgView->setPreferredHeight(90);
	mImgView->setScalingMethod(ScalingMethod::Fill);
	TextStyle style;
	style.setColor(Color::White);
	style.setFontSize(FontSize::PointValue);
	style.setFontSizeValue(8);
	style.setTextAlign(TextAlign::Left);
	style.setFontWeight(FontWeight::Bold);

	mTitle =
			Label::create().vertical(VerticalAlignment::Center).multiline(true).textStyle(
					style);

	container->add(mImgView);
	container->add(mTitle);

	setRoot(rootContainer);


	//Color setting

	NORMAL_COLOR = Color::fromARGB(0xff202020);
	SELECT_COLOR = Color::DarkGray;
}

void SportsEventTab::select()
{
	backContainer->setBackground(SELECT_COLOR);
}

void SportsEventTab::unselect()
{
	backContainer->setBackground(NORMAL_COLOR);
}

void SportsEventTab::onTappedHandler(TapEvent *eventHandler)
{
	emit click(eventHandler);
}

QString SportsEventTab::imgUrl()
{
	return mImgView->url().toString();
}
QString SportsEventTab::title()
{
	return mTitle->text();
}
QString SportsEventTab::refKey()
{
	return mRefKey;
}

QString SportsEventTab::showMatchForToday()
{
	return mShowMatch;
}

void SportsEventTab::setShowMatchForToday(QString strShowMatch)
{
	mShowMatch = strShowMatch;
	emit showMatchForTodayChanged();
}

void SportsEventTab::setImgUrl(QString newUrl)
{
	mImgView->setUrl(newUrl);
	emit imgUrlChanged();

}
void SportsEventTab::setTitle(QString newTitle)
{
	mTitle->setText(newTitle);
	emit titleChanged();
}
void SportsEventTab::setRefKey(QString newRefKey)
{
	mRefKey = newRefKey;
	emit refKeyChanged();

}

void SportsEventTab::setImgSource(QString _imgSrc)
{
	mImgView->setImage(Image(QUrl(_imgSrc)));
}

void SportsEventTab::setInAppUrlLink(QString url)
{
	inAppUrl = url;
}
QString SportsEventTab::inAppUrlLink()
{
	return inAppUrl;
}

SportsEventTab::~SportsEventTab() {
	// TODO Auto-generated destructor stub
}
